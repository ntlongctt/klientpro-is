## Usage
- Install Yarn: `https://yarnpkg.com/lang/en/docs/install/`
```sh
$ npm install -g typescript
$ npm install -g @angular/cli@latest
$ yarn
$ npm run proxy https://pabe.klientpro.cz:8843
# In the development we reverse request to http://localhost:3333, this server will redirect to https://pabe.klientpro.cz:8843 to bypass the ssl self-signed issue
$ yarn start
```
