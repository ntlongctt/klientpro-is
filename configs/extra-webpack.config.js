const SentryCliPlugin = require('@sentry/webpack-plugin');

module.exports = {
  plugins: [
    new SentryCliPlugin({
      include: './dist',
      // ignoreFile: '.sentrycliignore',
      // ignore: ['assets', 'humans.txt', 'robots.txt'],
      // configFile: 'sentry.properties',
    }),
  ],
};
