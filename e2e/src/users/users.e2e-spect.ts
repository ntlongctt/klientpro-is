import { UsersPage } from './users.po';
import { browser, by, element } from 'protractor';

describe('User page', () => {
    let page: UsersPage;

    beforeAll(() => {
        page = new UsersPage();
        page.login();
        page.navigateToUsers();
    });

    it('should display header', () => {
        expect(page.getHeader()).toContain('Users management');
    });

    it('should should be load user list', () => {
        expect(page.getUserList()).toBeTruthy();
    });

    it('should return result when search with: nois', () => {
        page.searchWith('nois');
        expect(page.getUserList()).toBeTruthy();
    });

    it('should not return result when search with: noisabcxyz', () => {
        page.searchWith('abcxyz');
        expect(page.getUserList().count()).toEqual(0);
    });

    it('should load create user page', () => {
        page.navigateToCreateUser();
        expect(page.getById('header').getText()).toContain('User Details: No name');
    });

    it('should not create user without any field entered', () => {
        page.getButtonByText('Save').click();
        expect(page.getElementByTag('mat-error')).toBeTruthy();
    });

    it('should not create user with invalid email', () => {
        page.fieldInput('fullName', `e2etestUserTest_${browser.browserName}`);
        page.fieldInput('userName', `e2etestUserName_${browser.browserName}`);
        page.selectDropdownByIndex('perspective', 0);
        page.selectDropdownByIndex('userStateId', 0);
        page.fieldInput('email', 'e2etestEmail');
        page.fieldInput('phone', '0420 111 222 333');
        page.fieldInput('note', 'this user created by automation test');
        page.getButtonByText('Save').click();
        expect(page.getElementByTag('mat-error')).toBeTruthy();
    });

    it('should not create user with invalid phone', () => {
        page.fieldInput('email', 'e2etestEmail@gmail.com');
        page.fieldInput('phone', '0420 111 222 aaa');
        page.getButtonByText('Save').click();
        expect(page.getElementByTag('mat-error')).toBeTruthy();
    });

    it('should create user success', () => {
        page.fieldInput('phone', '603 123 456');
        page.getButtonByText('Save').click();
        expect(page.getModal().getText()).toContain('User created successfully');
        browser.driver.sleep(1000);
        page.closeModal();
    });

    it('should edit user success', () => {

        page.getById('btnCancel').click();
        browser.driver.sleep(2000);

        // check search new user
        page.searchWith(`e2etestUserTest_${browser.browserName}`);
        page.getUserList().get(0).click();
        browser.driver.sleep(1000);

        // change user fullname and user name
        page.fieldInput('fullName', `e2etestEdit_${browser.browserName}`);
        page.fieldInput('userName', `e2etestUserNameEditedt_${browser.browserName}`);

        // change user privileages
        page.activePrivilesTab();
        expect(page.getElementByTag('field-chooser')).toBeTruthy();
        page.setSelectedPrivileges();

        page.getButtonByText('Save').click();
        browser.driver.sleep(500);

        // check save user successfully and back to user list
        expect(page.getHeader()).toContain('Users management');


        // check search new user
        page.searchWith(`e2etestEdit_${browser.browserName}`);
        page.getUserList().get(0).click();
        browser.driver.sleep(1000);

        // check show user infomations correct after edit
        expect(page.getElmentByName('fullName').getAttribute('value')).toContain(`e2etestEdit_${browser.browserName}`);
        expect(page.getElmentByName('userName').getAttribute('value')).toContain(`e2etestUserNameEditedt_${browser.browserName}`);

        // check user's selected privileges
        page.activePrivilesTab();
        expect(page.getSelectedPivileges().count()).toBe(1);
    });

    it ('user can revert change on privileges', () => {
        // add one more item to selected list
        page.setSelectedPrivileges();

        // check selected list count
        expect(page.getSelectedPivileges().count()).toBe(2);

        page.getById('btnRevertChanges').click();

        // check selected list count
        expect(page.getSelectedPivileges().count()).toBe(1);
    });

    it('user can reset password', () => {
        page.getById('btnResetPwd').click();

        // verify show confirm modal
        expect(page.getModal().getText()).toContain('Reset password confirmation');

        page.getById('btnYes').click();
        browser.driver.sleep(2000);

         // verify reset password success
         expect(page.getModal().getText()).toContain('User password reseted succeccfully');
         page.closeModal();
    });

    it('Cancel button should work correctly', () => {
        page.getById('btnCancel').click();
        browser.driver.sleep(2000);

        // verify back to user list without change
        expect(page.getHeader()).toContain('Users management');
        page.getUserList().get(0).click();
        browser.driver.sleep(2000);

        // change user name
        page.fieldInput('fullName', 'testCancel');
        page.getById('btnCancel').click();

        // verify show cancel confirmation
        expect(page.getModal().getText()).toContain('Leave page confirmation');
        page.getById('btnYes').click();
        browser.driver.sleep(2000);

        // verify back to user list after confirm cancel
        expect(page.getHeader()).toContain('Users management');
        page.getUserList().get(0).click();
        browser.driver.sleep(1000);

        // check user info not change
        expect(page.getElmentByName('fullName').getAttribute('value')).toContain('e2etestEdit');

        // change privileges
        page.activePrivilesTab();
        page.setSelectedPrivileges();
        page.getById('btnCancel').click();
        
        expect(page.getModal().getText()).toContain('Leave page confirmation');
        page.getById('btnYes').click();
        browser.driver.sleep(1000);

        // verify back to user list after confirm cancel
        expect(page.getHeader()).toContain('Users management');
        page.getUserList().get(0).click();
        browser.driver.sleep(1000);

        // check privileges not change
        page.activePrivilesTab();
        expect(page.getSelectedPivileges().count()).toBe(1);
    });

    it('can delete user success', () => {
        page.getById('btnDelete').click();

        // verify delete user confirmation showed
        expect(page.getModal().getText()).toContain('Delete user confirmation');
        page.getById('btnYes').click();
        browser.driver.sleep(1000);

        expect(page.getHeader()).toContain('Users management');
        page.searchWith(`e2etestEdit_${browser.browserName}`);

        // verify cannot search user after deleted
        expect(page.getUserList().count()).toBe(0);
    });

});

