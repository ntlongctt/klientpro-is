import { browser, by, element } from 'protractor';

export class UsersPage {

    login() {
        browser.driver.manage().window().maximize();
        browser.get('/sign-in');
        element(by.id('usernameInput')).sendKeys('nois');
        element(by.id('passwordInput')).sendKeys('test');
        element(by.className('submit-button')).click();
        browser.waitForAngular();
        browser.driver.sleep(3000);
    }

    navigateToUsers() {
        // element.all(by.tagName('a')).filter(function(elem) {
        //     return elem.getText().then(function(text) {
        //         return text === 'PA user';
        //     });
        // }).first().click();
        browser.get('/pa-user-administrator');
        browser.waitForAngular();
        browser.driver.sleep(2000);
    }

    navigateToEditUsers() {
        browser.get('/pa-user-administrator/12');
        browser.driver.sleep(2000);
    }

    navigateToCreateUser() {
        element(by.buttonText('Add new user')).click();
        browser.driver.sleep(2000);
    }
    
    getUserList() {
        return element.all(by.tagName('mat-row'));
    }
    
    getHeader() {
        return element(by.className('logo-text h1')).getText();
    }

    searchWith(keywork: string) {
        element(by.id('search')).clear();
        browser.driver.sleep(500);
        element(by.id('search')).sendKeys(keywork);
        browser.driver.sleep(2000);
    }

    getById(id: string) {
        return element(by.id(id));
    }

    getElmentByName(id: string) {
        return element(by.name(id));
    }

    getButtonByText(text: string) {
        return element(by.partialButtonText(text));
    }

    getElementByTag(tagName: string) {
        return element(by.tagName(tagName));
    }

    fieldInput(inputName: string, value: string) {
        element(by.name(inputName)).clear();
        browser.driver.sleep(500);
        element(by.name(inputName)).sendKeys(value);
        browser.driver.sleep(500);
        
    }

    selectDropdownByIndex(dropdownName: string, option: number) {
        element(by.css(`mat-select[formcontrolname=${dropdownName}]`)).click();
        browser.driver.sleep(500);
        element.all(by.tagName('mat-option')).get(option).click();
        browser.driver.sleep(500);
    }

    getModal() {
        browser.driver.sleep(500);
        return element(by.className('mat-dialog-title'));
    }

    closeModal() {
        element(by.id('btnOk')).click();
    }

    activePrivilesTab() {
        element.all(by.className('mat-tab-label-content')).get(1).click();
        browser.driver.sleep(500);
    }

    setSelectedPrivileges() {
        const availableList = element.all(by.tagName('mat-selection-list')).get(1);
        availableList.all(by.tagName('mat-list-option')).get(0).click();
        element(by.id('btnToSelected')).click();
    }

    getSelectedPivileges() {
        const selectedList = element.all(by.tagName('mat-selection-list')).get(0);
        return selectedList.all(by.tagName('mat-list-option'));
    }
}
