const child = require("child_process");
const path = require('path');
const colors = require('colors');
const args = process.argv.slice(2);
const reverse_proxy = args[0] || 'http://pabe.klientpro.cz:9012';

console.log(colors.green(`Updating Webdriver...`));
child.execSync(`./node_modules/protractor/node_modules/.bin/webdriver-manager --ignore_ssl update --ie32`, { cwd: path.join(__dirname, '..'), stdio: 'inherit' });
child.execSync(`npm run proxy ${reverse_proxy} & ng e2e`, { cwd: path.join(__dirname, '..'), stdio: 'inherit' });
