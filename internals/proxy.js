const express = require('express');
const proxy = require('http-proxy-middleware');
const colors = require('colors');
const kill = require('kill-port');
const app = express();
const args = process.argv.slice(2);
const target = args[0];
const port = args[1] || "3333";

console.log(colors.red(`Killing process on port ${port}...`));
kill(port)
  .then(() => {
    // proxy middleware options
    const options = {
      target: target, // target host
      changeOrigin: true, // needed for virtual hosted sites
      secure: false,
      onProxyRes(proxyRes, req, res) {
        const sc = proxyRes.headers['set-cookie'];
        // console.log('onProxyRes', sc);

        if (Array.isArray(sc)) {
          proxyRes.headers['set-cookie'] = sc.map(sc => {
            return sc.split(';')
              .filter(v => v.trim().toLowerCase() !== 'secure')
              .join('; ')
          });
        }
      }
    };

    app.use('*', proxy(options));
    app.listen(port, () => {
      console.log(colors.green(`Reverse proxy is listening on ${port}`));
      console.log(colors.red(`http://localhost:${port} >> ${target}`));
    });
  }, (e) => console.error('Failed to kill port ' + port, e));


