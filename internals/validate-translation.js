const path = require('path');
const fs = require('fs');
const colors = require('colors');

let array = [];

let convertObjectPropsToArray = function (obj, parent) {
  if (!parent) {
    array = [];
  }
  Object.keys(obj).forEach(function (key) {
    if (typeof obj[key] === 'object') {
      convertObjectPropsToArray(obj[key], parent ? parent + '.' + key : key);
    } else {
      array.push(parent ? parent + '.' + key : key);
    }
  });

  return array;
};

let walkSync = function (dir, filelist) {
  let files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function (file) {
    if (fs.statSync(path.join(dir, file)).isDirectory()) {
      filelist = walkSync(path.join(dir, file), filelist);
    }
    else {
      filelist.push(path.join(dir, file));
    }
  });
  return filelist;
};

let extractJson = function (filePath) {
  let file = fs.readFileSync(filePath, 'utf8');
  const matches = /=\s+([^;]+)/g.exec(file);
  file = matches[1];
  file = file.replace(/\/\/.*/g, ''); // replace comments
  file = file.replace(/([{,])(\s*)([A-Za-z0-9_\-]+?)\s*:/g, '$1"$3":'); // add " to keys
  file = file.replace(/([{:,])(\s+?)'(.*?)'/g, '$1"$3"'); // replace ' to "
  file = file.replace(/\n/g, ''); // replace line break
  file = file.replace(/,\s+}/g, '}'); // remove unneeded commas
  file = JSON.parse(file);

  return file;
};

const diff = function (A, B) {
  return A.filter(x => !B.includes(x));
};



const allFiles = walkSync(path.join(__dirname, '../src'));

const czFiles = allFiles.filter(f => /cs\.ts$/.test(f));
const enFiles = allFiles.filter(f => /en\.ts$/.test(f));
let hasUntranslatedTexts = false;

console.log(colors.yellow('============== CHECKING MISSING CZECH TRANSLATIONS AGAINST ENGLISH ================='));
console.log('');

czFiles.forEach((file) => {
  const czFile = file;
  const enFile = file.substring(0, file.lastIndexOf(path.sep) + 1) + 'en.ts';
  if (fs.existsSync(enFile)) {
    let cz = extractJson(czFile);
    let en = extractJson(enFile);
    let czp = convertObjectPropsToArray(cz.data).sort();
    let enp = convertObjectPropsToArray(en.data).sort();
    const missing = diff(enp, czp);

    if(missing.length > 0) {
      console.log(colors.cyan('>>>>> MISSING CZECH TRANSLATIONS'), `(${colors.magenta(czFile)})`);
      console.log(missing);
      console.log('');
      hasUntranslatedTexts = true;
    }
  }
});

if(!hasUntranslatedTexts) {
  console.log('');
  console.log(colors.blue('CZECH TRANSLATIONS ARE UP-TO-DATE!'));
}

hasUntranslatedTexts = false;
console.log('');
console.log(colors.yellow('============== CHECKING MISSING ENGLISH TRANSLATIONS AGAINST CZECH ================='));
console.log('');

enFiles.forEach((file) => {
  const enFile = file;
  const csFile = file.substring(0, file.lastIndexOf(path.sep) + 1) + 'cs.ts';
  if (fs.existsSync(csFile)) {
    let cz = extractJson(csFile);
    let en = extractJson(enFile);
    let czp = convertObjectPropsToArray(cz.data).sort();
    let enp = convertObjectPropsToArray(en.data).sort();
    const missing = diff(czp, enp);

    if(missing.length > 0) {
      console.log(colors.cyan('>>>>> MISSING ENGLISH TRANSLATIONS'), `(${colors.magenta(enFile)})`);
      console.log(missing);
      console.log('');
      hasUntranslatedTexts = true;
    }
  }
});

if(!hasUntranslatedTexts) {
  console.log('');
  console.log(colors.blue('ENGLISH TRANSLATIONS ARE UP-TO-DATE!'));
}
