import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { navigation } from 'app/navigation/navigation';
import { locale as navigationEnglish } from 'app/navigation/i18n/en';
import { locale as navigationCzech } from 'app/navigation/i18n/cs';
import { RouterService } from '@modules/route-caching';

import { locale as enCommon } from './i18n/common/en';
import { locale as csCommon } from './i18n/common/cs';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { Subject } from 'rxjs/Subject';
import { AppStoreService } from '@store/store.service';
import { Router } from '@angular/router';
import { AppConstant } from './app.constant';
import 'moment/locale/cs';
import * as moment from 'moment';
import * as fromUserActions from '@store/user/user.action';
import * as fromMenuActions from '@store/menu/menu.actions';

@Component({
    selector   : 'app',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy
{
    navigation: any;
    fuseConfig: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseSplashScreenService} _fuseSplashScreenService
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     * @param {TranslateService} _translateService
     * @param {RouterService} _routerService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _fuseSidebarService: FuseSidebarService,
        private _fuseSplashScreenService: FuseSplashScreenService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _routerService: RouterService,
        private _store: Store<AppState>,
        private appStoreService: AppStoreService
    )
    {
        // Get default navigation
        this.navigation = navigation;

        // Register the navigation to the service
        this._fuseNavigationService.register('main', this.navigation);

        // Set the main navigation as our current navigation
        this._fuseNavigationService.setCurrentNavigation('main');

        // Add languages
        this._translateService.addLangs(['en', 'cs']);

        // Set the default language
        this._translateService.setDefaultLang(AppConstant.defaultLanguage);

        // Set the navigation translations
        this._fuseTranslationLoaderService.loadTranslations(navigationEnglish, navigationCzech);

        // Load common translations
        this._fuseTranslationLoaderService.loadTranslations(enCommon, csCommon);

        // Use a language
        this._translateService.use(AppConstant.defaultLanguage);

        // Set the private defaults
        this._unsubscribeAll = new Subject();

        // Set default date time format
        moment.locale(AppConstant.defaultLanguage);

        // Change date time format when language has changed
        this._translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            moment.locale(event.lang);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });

        // Init Route Caching
        this._routerService.init();

        // get user info
        const appState = this.appStoreService.getState();
        if (appState.auth.isLoggedIn) {
            this._store.dispatch(new fromMenuActions.GetMenu());
            this._store.dispatch(new fromUserActions.GetUserInfo());
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void
    {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }
}
