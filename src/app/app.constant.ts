import {environment} from 'environments/environment';

export class AppConstant {
  public static domain = environment.domain;
  public static baseHref = environment.baseHref;
  public static defaultLanguage = environment.defaultLanguage;
  public static enableChangeLanguage = environment.enableChangeLanguage;
  public static requireToken = environment.REQUIRE_TOKEN;
  public static defaultStateName = environment.defaultStateName;
  public static defaultStateCode = environment.defaultStateCode;
  public static defaultTimeZoneDisplay = environment.defaultTimeZoneDisplay;
}
