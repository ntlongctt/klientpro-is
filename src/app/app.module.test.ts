import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppStoreModule } from '@store/store.module';
import { ToastrModule } from 'ngx-toastr';
import { FuseAppModule } from 'app/app.module';
import { TranslateModule } from '@ngx-translate/core';
import { LocalStorageModule } from 'angular-2-local-storage';
import { RestModule } from './rest/NgModule';
import { AppSharedTestModule } from '@common/shared.test';
import { InputSearchModule } from '@common/modules/input-search';
import { MatSelectSearchModule } from '@common/modules/mat-select-search';
import { FieldChooserModule } from '@common/modules/field-chooser/field-chooser.module';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot(),
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
    }),
    ToastrModule.forRoot(),
    FuseAppModule,
    AppSharedTestModule,
    AppStoreModule,
    RestModule,
    InputSearchModule,
    MatSelectSearchModule,
    FieldChooserModule
  ],
  exports: [
    BrowserModule,
    BrowserAnimationsModule,
    TranslateModule,
    LocalStorageModule,
    ToastrModule,
    FuseAppModule,
    AppSharedTestModule,
    AppStoreModule,
    RestModule,
    InputSearchModule,
    MatSelectSearchModule,
    FieldChooserModule
  ]
})
export class AppTestModule {
}
