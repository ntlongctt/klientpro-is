import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppStoreModule } from '@store/store.module';
import { ToastrModule } from 'ngx-toastr';
import { appRoutes } from './routes';
import { PageModule } from './pages';
import { AppComponent } from './app.component';
import { AppCustomPreloader } from '@services/app-custom-preloader';
import { AppSharedModule } from '@common/shared';
import { FuseAppModule } from 'app/app.module';
import { TranslateModule } from '@ngx-translate/core';
import { LocalStorageModule } from 'angular-2-local-storage';
import { RouteCachingModule, RouterService } from '@modules/route-caching';
import { NotFoundModule } from '@pages/not-found';
import { NoPermissionModule } from '@pages/no-permission';
import { RestModule } from './rest/NgModule';
import { AppConstant } from './app.constant';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';

const getBaseHref = () => {
  const href = AppConstant.baseHref
    .replace(/^\//g, '') // remove forward splash at beginning
    .replace(/\/$/g, ''); // remove backward splash at beginning

  if (href) {
    return `/${href}/`;
  }

  return '/';
};

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FuseAppModule,
    TranslateModule.forRoot(),
    RouteCachingModule,
    AppStoreModule,
    AppSharedModule,
    RouterModule.forRoot(appRoutes, { preloadingStrategy: AppCustomPreloader }),
    PageModule,
    NotFoundModule,
    NoPermissionModule,
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
    }),
    ToastrModule.forRoot({
      positionClass: 'toast-top-center',
      closeButton: true
    }),
    RestModule
  ],
  providers: [
    AppCustomPreloader,
    RouterService,
    { provide: APP_BASE_HREF, useValue: getBaseHref() },
    {provide: MAT_DATE_LOCALE, useValue: AppConstant.defaultLanguage},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
