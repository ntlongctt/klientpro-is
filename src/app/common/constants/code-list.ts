export enum CODE_LIST {
  BP_ADDRESS_TYPE = 'BPAddressType',
  MC_SYSTEM_TYPE = 'MCSystemType',
  AREA = 'Area',
  DEPARTMENT_TYPE = 'DepartmentType',
  SPECIALTY = 'Specialty',
  DEPARTMEN_MC_SYSTEM_TYPE = 'DepartmentMCSystemType',
}
