import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { AppStoreService } from '@store/store.service';


@Directive({
  selector: '[activeFor]'
})

export class ActiveByRoleDirective implements OnInit {
  @Input('activeFor') roles: string[];

  constructor(
    private el: ElementRef,
    private appStoreService: AppStoreService
    ) {
  }

  ngOnInit() {
    if (!this.appStoreService.getState().auth.currentUser) {
      return;
    }
    const currentRoles = this.appStoreService.getState().auth.currentUser.authorities;
    const hasPermision = this.roles.some(role => currentRoles.includes(role));
    if (!hasPermision) {
      this.el.nativeElement.style.display = 'none';
    }
  }
  
}
