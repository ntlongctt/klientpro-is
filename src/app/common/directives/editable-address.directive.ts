import { Directive, ElementRef, Input, OnInit, OnChanges } from '@angular/core';


@Directive({
  selector: '[editable]'
})

export class EditableAddressDirective implements OnInit, OnChanges {
  @Input('editable') status: string;

  constructor(
    private el: ElementRef,
    ) {
  }

  ngOnInit() {
  
  }

  ngOnChanges() {
    switch (this.status) {
      case 'EXIST': 
        this.el.nativeElement.classList.add('edit-address');
        break;
    }
  }
  
}
