import { Directive, ElementRef, HostListener, Input, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[inactiveAddress]'
})

export class InactiveAddressDirective implements AfterViewInit {
  @Input('inactiveAddress') inactiveAddress: boolean;

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
    if (this.inactiveAddress) {
      this.el.nativeElement.style.color = '#bfbfbf';
    }
  }
}
