import { Directive, ElementRef, HostListener, Input, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[inactiveSystem]'
})

export class InactiveMcSystemDirective implements AfterViewInit {
  @Input('inactiveSystem') dateRange: {from: string, to: string};

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
    const toDate = moment(this.dateRange.to, moment.ISO_8601);
    const currentDate = moment(new Date(), moment.ISO_8601);
    if (!toDate.endOf('day').isAfter(currentDate) && this.dateRange.to !== null ) {
      this.el.nativeElement.style.color = '#bfbfbf';
    }
  }
}
