import { Directive, ElementRef, HostListener, Input, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[inactivePartner]'
})

export class InactivePartnerDirective implements AfterViewInit {
  @Input('inactivePartner') dateRange: {from: string, to: string};

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
    const fromDate = moment(this.dateRange.from, moment.ISO_8601);
    const toDate = moment(this.dateRange.to, moment.ISO_8601);
    const currentDate = moment(new Date(), moment.ISO_8601);

    if (!currentDate.isBetween(fromDate, toDate.endOf('day')) && this.dateRange.to !== null ) {
      this.el.nativeElement.style.color = '#bfbfbf';
    }
  }
}
