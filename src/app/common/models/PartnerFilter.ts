
export interface PartnerFilter {
  departmentTypes: any[];
  mcSystems: any[];
  departmentTags: {
    selected: any[],
    available: any[];
  };
}

