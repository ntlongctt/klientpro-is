export class UserInfo {
  public authorities: string[];
  public id: number;
  public name?: string;
  public fullName?: string;
  public email?: string;
  public nickname?: string;
  public picture?: string;
  public phoneNumber?: string;
  public role?: string;
  public note?: string;
  public passwordChangedTime?: string;
  public perspective?: string;
  public phone?: string;
  public updatedTime?: string;
  public userName?: string;
  public userStateId: string;
  

  constructor(data: any) {
    Object.assign(this, data);
  }
}
