import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'address-format',
  templateUrl: './address-format.component.html',
  styleUrls: ['./address-format.component.scss']
})
export class AddressFormatComponent implements OnInit {

  @Input() addressDetail;

  constructor() { }

  ngOnInit() {
  }

}
