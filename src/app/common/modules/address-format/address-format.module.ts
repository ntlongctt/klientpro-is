import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressFormatComponent } from './address-format.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AddressFormatComponent],
  exports: [AddressFormatComponent]
})
export class AddressFormatModule { }
