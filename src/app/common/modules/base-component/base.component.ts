import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {
  FormControl,
  FormGroup
} from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'base-component',
  encapsulation: ViewEncapsulation.None,
  template: '',
})
export class BaseComponent implements OnInit, OnDestroy {

  public frm: FormGroup;
  // public formErrors: { [key: string]: Object };
  public formErrors: any;
  // public validationMessages: { [key: string]: { [key: string]: string } } = {};
  public validationMessages: any = {};
  public controlConfig: { [key: string]: FormControl };

  // Private
  private _unsubscribeAll: Subject<any> = new Subject();

  public ngOnInit() {
    this.buildForm();
  }

  public buildForm() {
    
    this.frm = new FormGroup(this.controlConfig);

    this.frm.valueChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        this.onFormValuesChanged();
      });
  }

  public revalidateOnChanges(control): void {
    if (control && control._parent && !control._revalidateOnChanges) {
      control._revalidateOnChanges = true;
      control._parent
        .valueChanges
        .distinctUntilChanged((a, b) => {
          // These will always be plain objects coming from the form, do a simple comparison
          if (a && !b || !a && b) {
            return false;
          } else if (a && b && Object.keys(a).length !== Object.keys(b).length) {
            return false;
          } else if (a && b) {
            for (let i in a) {
              if (a[i] !== b[i]) {
                return false;
              }
            }
          }
          return true;
        })
        .subscribe(() => {
          control.updateValueAndValidity();
        });

      control.updateValueAndValidity();
    }
    return;
  }

  public conditional(conditional, validator) {
    return (control) => {
      this.revalidateOnChanges(control);

      if (control && control._parent) {
        if (conditional(control._parent)) {
          return validator(control);
        }
      }
    };
  }

  public onFormValuesChanged(): void {
    for (const field in this.formErrors) {
      if (!this.formErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.formErrors[field] = {};

      // Get the control
      const control = this.frm.get(field);

      if (control && control.errors !== null && control.dirty && !control.valid) {
        Object.keys(control.errors).forEach(key => {
          this.formErrors[field][key] = this.validationMessages[field][key];
        });
      }
    }
  }

  public buildFormErrorMessage () {
    for (const field in this.formErrors) {
      if (!this.formErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.formErrors[field] = {};

      // Get the control
      const control = this.frm.get(field);

      if (control && control.errors !== null && !control.valid) {
        Object.keys(control.errors).forEach(key => {
          this.formErrors[field][key] = this.validationMessages[field][key];
        });
      }
    }
  }

  public buildFormErrorsFromConfigs(configs) {
    const formErrors = {};
    Object.keys(configs).forEach((key, index) => {
      formErrors[key] = {};
    });
    return formErrors;
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
