import {
  NgModule
} from '@angular/core';

import {
  BaseComponent
} from './base.component';

@NgModule({
  declarations: [
    BaseComponent
  ],
  imports: [],
})
export class BaseComponentModule {

}
