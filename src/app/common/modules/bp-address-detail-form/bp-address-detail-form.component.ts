import { Component, OnInit, Optional, Inject, Input, OnDestroy, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { ContactService } from '@klient/rest/services/common/Contact.service';
import { ToastrService } from 'ngx-toastr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { ConfirmModalService } from '../confirm-modal';
import { BaseComponent } from '../base-component';
import { FormControl, Validators } from '@angular/forms';
import { takeUntil, map, debounceTime } from 'rxjs/operators';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { locale as en } from './i18n/en';
import { locale as cs} from './i18n/cs';
import { AppConstant } from '@klient/app.constant';
import { INPUT_DEBOUNCE_TIME } from '@common/constants/input-debounce-time';
import * as _ from 'lodash';
import { CODE_LIST } from '@common/constants/code-list';
import { DeactiveWarningModalService } from '../deactivate-warning-modal/deactivate-warning-modal.service';

@Component({
  selector: 'bp-address-detail-form',
  templateUrl: './bp-address-detail-form.component.html',
  styleUrls: ['./bp-address-detail-form.component.scss']
})
export class BpAddressDetailFormComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() data;
  @Input() enableActions = true;
  @Input() isDisableForm = false;
  @Input() showToast = true;
  @Input() rebindingData = false;

  @Output() onCancelChange = new EventEmitter();
  @Output() onActionSuccess = new EventEmitter();

  unSubAll = new Subject();

  addressTypes = [];

  isLoading = false;

  states = [];

  cities = [];

  streets = [];

  submitted = false;

  defaultState = {
    code: AppConstant.defaultStateCode,
    name: AppConstant.defaultStateName
  };

  addressDetail: any;

  selectedAddressType: any;

  formInitValue;

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private contactService: ContactService,
    private toast: ToastrService,
    private fuseTranslationLoaderService: FuseTranslationLoaderService,
    private confirmModalService: ConfirmModalService,
    private deactiveWarningModalService: DeactiveWarningModalService
  ) { 
    super();

    this.fuseTranslationLoaderService.loadTranslations(en, cs);
    this.states = _.uniqBy([this.defaultState, ...this.states], 'code');

    // init form control
    this.controlConfig = {
      bpaddressType: new FormControl('', [Validators.required]),
      active: new FormControl(true),
      invoiceAddress: new FormControl(false),
      postalAddress: new FormControl(false),
      recipient: new FormControl(''),
      state: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      zipCode: new FormControl('', [Validators.required, Validators.pattern(/\s*(?:[\d]\s*){5}/)]),
      street: new FormControl(''),
      streetNumber: new FormControl('', [Validators.required]),
      streetNote: new FormControl(''),
      mapLink: new FormControl(''),
      transportInfo: new FormControl(''),
      note: new FormControl(''),
    };

    // Get address detail success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_ADDRESS_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.bindData(detail);
    });

    // Get address types success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_ADDRESS_TYPES_SUCCEEDED),
      map((action: any) => action.payload.items)
    ).subscribe(types => {
      this.addressTypes = types;
      if (this.data) {
        this.frm.get('bpaddressType').patchValue(this.controlConfig.bpaddressType.value);
      }
    });

    // add, update, delete success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        partnerActions.ADD_PARTNER_ADDRESS_SUCCEEDED,
        partnerActions.UPDATE_PARTNER_ADDRESS_SUCCEEDED,
        partnerActions.DELETE_PARTNER_ADDRESS_SUCCEEDED
      )
    ).subscribe((action: any) => {
      if (this.showToast) {
        switch (action.type) {
          case partnerActions.ADD_PARTNER_ADDRESS_SUCCEEDED: 
            this.toast.success(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.ADD_PARTNER_ADDRESS_SUCCESS'));
            break;
          case partnerActions.UPDATE_PARTNER_ADDRESS_SUCCEEDED:
            this.toast.success(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.UPDATE_PARTNER_ADDRESS_SUCCESS'));
            break;
          case partnerActions.DELETE_PARTNER_ADDRESS_SUCCEEDED:
            this.toast.success(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_PARTNER_ADDRESS_SUCCESS'));
            break;
        }
      }
      if (this.rebindingData) {
        this.frm.enable();
        this.bindData(action.payload);
        if (this.isDisableForm) {
          this.frm.disable();
          this.frm.get('bpaddressType').disable();
        }
      }
      this.onActionSuccess.emit(action.payload);
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.formInitValue = _.cloneDeep(this.frm.value);
    this.store.dispatch(new partnerActions.GetAddressTypes(CODE_LIST.BP_ADDRESS_TYPE));
  }

  ngAfterViewInit() {

    setTimeout(() => {
      if (this.isDisableForm) {
        this.frm.disable();
        // this.disableTransportInfo = true;
      }
    });

    this.frm.get('city').valueChanges
    .pipe(debounceTime(INPUT_DEBOUNCE_TIME))
    .subscribe((value) => {
      const selectedVal = this.controlConfig.city.value;

      // not search with small text length
      if (!value || value.length < 3) {
        return;
      }

      // auto field zipcode when choose city
      if (selectedVal.zipCode) {
        this.frm.get('zipCode').patchValue(selectedVal.zipCode);
      }

      if (selectedVal.cityCode && value.cityCode === selectedVal.cityCode) {
        return;
      }

      this.isLoading = true;

      if (typeof value === 'string' || value instanceof String) {
        this.contactService.findCity({query: value.trim()}).subscribe(resp => {
          this.cities = resp.cities;
          this.isLoading = false;
        });
      }
    });

    this.frm.get('street').valueChanges
    .pipe(debounceTime(INPUT_DEBOUNCE_TIME))
    .subscribe((value) => {

      if (!value || value.length < 3) {
        return;
      }

      const selectedVal = this.controlConfig.street.value;
      if (selectedVal.streetName && value.streetName === selectedVal.streetName) {
        return;
      }

      const cityCode = this.controlConfig.city.value.cityCode;
      this.isLoading = true;
      if (typeof value === 'string' || value instanceof String) {
        this.contactService.findCityStreet({cityCode: cityCode, query: value.trim()}).subscribe(resp => {
          this.streets = resp.streets;
          this.isLoading = false;
        });
      }
    });

    // this.frm.controls['transportInfo'].valueChanges.subscribe((value) => {
    //   console.log(value);
    // });

  }

  bindData(detail) {
    this.frm.reset();
    this.frm.patchValue(detail);
    this.frm.get('bpaddressType').setValue(detail.bpaddressType.id);
    this.fieldAddress(detail.address);
    this.addressDetail = detail;
    this.selectedAddressType = detail.bpaddressType.id;

    this.formInitValue = _.cloneDeep(this.frm.value);
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  onSearchState(keywork) {
    if (keywork.length >= 3) {
      this.isLoading = true;
      this.contactService.findState({query: keywork.trim()}).subscribe(resp => {
        this.states = _.uniqBy([this.defaultState, ...resp.states], 'code');
        this.isLoading = false;
      });
    }
  }

  fieldAddress(address: any) {
    if (address.stateName) {
      const state = {
        name: address.stateName,
        code: address.stateCode
      };
      this.states = _.uniqBy([...this.states, state], 'code');
      this.frm.get('state').patchValue(state.code);
    } else {
      this.frm.get('state').patchValue(this.defaultState.code);
    }

    if (address.cityName) {
      const city = {
        cityName: address.cityName,
        cityCode: address.cityCode,
        zipCode: address.zipCode
      };
      this.frm.get('city').patchValue(city);
    }

    if (address.street) {
      this.frm.get('street').patchValue({streetName: address.street});
    }

    this.frm.get('zipCode').patchValue(address.zipCode);
    this.frm.get('streetNumber').patchValue(address.streetNumber);
  }

  onSubmit() {
    const {valid, value} = this.frm;
    
    if (valid) {
      const objectPost = {
        address: {
          cityCode: value.city.cityCode ? value.city.cityCode : null,
          cityName: value.city.cityName ? value.city.cityName : value.city,
          stateCode: value.state,
          stateName: this.states.find(s => s.code === value.state).name,
          street: value.street ? (value.street.streetName ? value.street.streetName : value.street) : '',
          streetNote: value.streetNote,
          streetNumber: value.streetNumber,
          zipCode: value.zipCode,
          note: value.note,
          version: value.version,
          geoCoordsStateId: value.geoCoordsStateId,
          geoLatitude: value.geoLatitude,
          geoLongitude: value.geoLongitude,
          id: value.addressId,
        },
        active: value.active,
        bpaddressType: this.addressTypes.find(t => t.id === value.bpaddressType),
        invoiceAddress: value.invoiceAddress,
        mapLink: value.mapLink,
        postalAddress: value.postalAddress,
        recipient: value.recipient,
        transportInfo: value.transportInfo,
        id: this.addressDetail ? this.addressDetail.id : null,
        version: this.addressDetail !== undefined ? (this.addressDetail.version !== null ? this.addressDetail.version : null) : undefined
      };
     
      if (!this.data.bpAddressId) {
        delete objectPost.id;
        this.store.dispatch(new partnerActions.AddPartnerAddress({
          partnerId: this.data.partnerId,
          address: objectPost
        }));
      } else {
        this.store.dispatch(new partnerActions.UpdatePartnerAddress({
          partnerId: this.data.partnerId,
          bpAddressId: this.data.bpAddressId,
          address: objectPost
        }));
      }
    }
  }

  /**
   * Display city name on city input
   */
  displayCityFn(city) {
    if (city) { 
      return city.cityName; 
    }
  }

  displayStreetFn(street) {
    if (street) {
       return street.streetName;
    }
  }

  delete() {
    this.confirmModalService
      .show(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_PARTNER_ADDRESS_CONFIRM_MSG'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new partnerActions.DeletetPartnerAddress(this.data));
        }
      }, 
      this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_PARTNER_ADDRESS_CONFIRM_TITLE'));
  }

  cancel() {
    if (!_.isEqual(this.formInitValue, this.frm.value)) {
      const subject = new Subject<boolean>();
      this.deactiveWarningModalService.subject = subject;

      subject.pipe(takeUntil(this.unSubAll)).subscribe((result) => {
        if (result) {
          this.onCancelChange.emit();
        }
      });
      this.deactiveWarningModalService.show();
    } else {
      this.onCancelChange.emit();
    }
  }

}
