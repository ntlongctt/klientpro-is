import { NgModule } from '@angular/core';
import { BpAddressDetailFormComponent } from './bp-address-detail-form.component';
import { AppSharedModule } from '@common/shared';
import { RichTextEditorModule } from '../rich-text-editor/rich-text-editor.module';
import { MatSelectSearchModule } from '../mat-select-search';

@NgModule({
  imports: [
    AppSharedModule,
    RichTextEditorModule,
    MatSelectSearchModule
  ],
  declarations: [BpAddressDetailFormComponent],
  exports: [BpAddressDetailFormComponent]
})
export class BpAddressDetailFormModule { }
