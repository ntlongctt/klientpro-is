export const locale = {
  lang: 'cs',
  data: {
	BP_DETAIL: {
      LABEL: 'Adresy',
      STREET: 'Ulice',
      CITY: 'Město',
      ZIP: 'PSČ',
      STATE: 'Stát',
      ADDRESS_TYPE: 'Typ adresy',
	  IS_ACTIVE: 'Aktivní',
      INVOICE_ADDRESS: 'Fakturační adresa',
      POSTAL_ADDRESS: 'Korespondenční adresa',
      RECIPIENT: 'Alternativní jméno příjemce',
      STREET_SUPLEMENT: 'Doplněk ulice',
      MAPLINK: 'Odkaz do mapy',
      TRANSPORT_INFO: 'Informace o dopravě',
      NOTE: 'Poznámka',
	  TITLE: 'Adresa obchodního partnera',
      ZIP_CODE: 'PSČ',
      STREET_NUMBER: 'číslo domu',
      SEARCH_FOR_STATE: 'Stát',
      SEARCH_FOR_CITY: 'Město',
      SEARCH_FOR_STREET: 'Ulice',
      FORM_VALIDATOR: {
        STATE_REQUIRED: 'Stát je povinný',
        STATE_INVALID: 'Stát je neplatný',
        CITY_REQUIRE: 'Město je povinné',
        CITY_INVALID: 'Město je neplatné',
        ZIP_CODE_REQUIRED: 'PSČ je povinné',
        ZIP_CODE_RULE: 'PSČ je neplatné',
        STREET_REQUIRED: 'Ulice je povinná',
        STREET_NUMBER: 'Číslo domu je povinné',
        BP_ADDRESS_TYPE:  'Typ adresy je povinný',
      }
    },
  }
};
