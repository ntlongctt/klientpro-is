export const locale = {
  lang: 'en',
  data: {
    BP_DETAIL: {
      LABEL: 'Addresses',
      STREET: 'Street',
      CITY: 'City',
      ZIP: 'Zip code',
      STATE: 'State',
      ADDRESS_TYPE: 'Address Type',
      IS_ACTIVE: 'Active',
      INVOICE_ADDRESS: 'Invoice address',
      POSTAL_ADDRESS: 'Postal address',
      RECIPIENT: 'Alternative recipient name',
      STREET_SUPLEMENT: 'Street suplement',
      MAPLINK: 'Map link',
      TRANSPORT_INFO: 'Transport instructions',
      NOTE: 'Note',
      TITLE: 'Bussiness partner address',
      ZIP_CODE: 'ZIP code',
      STREET_NUMBER: 'number',
      SEARCH_FOR_STATE: 'Search for state',
      SEARCH_FOR_CITY: 'Search for city',
      SEARCH_FOR_STREET: 'Search for street',
      FORM_VALIDATOR: {
        STATE_REQUIRED: 'State is required',
        STATE_INVALID: 'State is invalid',
        CITY_REQUIRE: 'City is required',
        CITY_INVALID: 'City is invalid',
        ZIP_CODE_REQUIRED: 'Zip code is required',
        ZIP_CODE_RULE: 'Zip code must be 5 digits',
        STREET_REQUIRED: 'Street name is required',
        STREET_NUMBER: 'Address number is required',
        BP_ADDRESS_TYPE:  'Address type is required',
      }
    },
    
  }
};
