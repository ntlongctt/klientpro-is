import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { locale as en } from './i18n/en';
import { locale as cs } from './i18n/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

@Component({
  selector: 'bp-addresses-list',
  templateUrl: './bp-addresses-list.component.html',
  styleUrls: ['./bp-addresses-list.component.scss']
})
export class BpAddressesListComponent implements OnInit {

  @Input() isDepartmentAddress = false;
  @Input() dataSource;
  @Output() onRowClick = new EventEmitter();

  displayedColumns = [];

  constructor(
    private translationLoaderService: FuseTranslationLoaderService
  ) {
    this.translationLoaderService.loadTranslations(en, cs);
   }

  ngOnInit() {
    this.displayedColumns = ['street', 'city', 'zip', 'stateName', 'bpAddressTypeName', 'invoiceAddress', 'postalAddress'];
    if (this.isDepartmentAddress) {
      this.displayedColumns.push('publicVisible');
      this.displayedColumns.push('partnerVisible');
    }
  }

  rowClick(row) {
    this.onRowClick.emit(row);
  }

}
