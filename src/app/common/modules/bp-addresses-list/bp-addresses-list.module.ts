import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BpAddressesListComponent } from './bp-addresses-list.component';
import { AppSharedModule } from '@common/shared';
import { MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    AppSharedModule,
    MatTableModule
  ],
  declarations: [BpAddressesListComponent],
  exports: [BpAddressesListComponent]
})
export class BpAddressesListModule { }
