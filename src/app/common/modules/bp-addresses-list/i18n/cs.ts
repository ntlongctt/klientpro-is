export const locale = {
  lang: 'cs',
  data: {
	BP_ADDRESSES: {
      LABEL: 'Adresy',
      STREET: 'Ulice',
      CITY: 'Město',
      ZIP: 'PSČ',
      STATE: 'Stát',
      ADDRESS_TYPE: 'Typ adresy',
	  INVOICE: 'Fakt.',
      POSTAL: 'Koresp.',
	  VISIBLE_FOR_PUBLIC: 'Pro veřejnost',
      VISIBLE_FOR_PARTNERS: 'Pro partnery',
    }
  }
};
