export const locale = {
  lang: 'en',
  data: {
    BP_ADDRESSES: {
      LABEL: 'Addresses',
      STREET: 'Street',
      CITY: 'City',
      ZIP: 'Zip code',
      STATE: 'State',
      ADDRESS_TYPE: 'Address Type',
      INVOICE: 'Invoice',
      POSTAL: 'Postal',
      VISIBLE_FOR_PUBLIC: 'Visible for public',
      VISIBLE_FOR_PARTNERS: 'Visible for partners',
    }
  }
};
