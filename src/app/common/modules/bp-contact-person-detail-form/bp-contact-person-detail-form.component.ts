import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { extend } from 'webdriver-js-extender';
import { BaseComponent } from '../base-component';
import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { FormControl, Validators } from '@angular/forms';
import { NumberValidator } from '@common/validators/number-only.validator';
import { takeUntil, map } from 'rxjs/operators';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { ToastrService } from 'ngx-toastr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ConfirmModalService } from '../confirm-modal';
import { SelectBpAddressComponent } from './select-bp-address/select-bp-address.component';
import { DeactiveWarningModalService } from '../deactivate-warning-modal/deactivate-warning-modal.service';
import * as _ from 'lodash';

@Component({
  selector: 'bp-contact-person-detail-form',
  templateUrl: './bp-contact-person-detail-form.component.html',
  styleUrls: ['./bp-contact-person-detail-form.component.scss']
})
export class BpContactPersonDetailFormComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() data;
  @Input() enableActions = true;
  @Input() isDisableForm = false;
  @Input() rebindingData = false;
  @Input() showToast = true;

  @Output() onCancelChange = new EventEmitter();
  @Output() onActionSuccess = new EventEmitter();

  unSubAll = new Subject();
  isSelectAddress = false;
  submitted = false;
  partnerAddressesDialogRef: any;
  address;

  initialFrmValues;
  

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private toast: ToastrService,
    private translationLoaderService: FuseTranslationLoaderService,
    private dialog: MatDialog,
    private confirmModalService: ConfirmModalService,
    private deactiveWarningModalService: DeactiveWarningModalService
  ) {
    super();

    this.controlConfig = {
      salutation: new FormControl(''),
      name: new FormControl('', [Validators.required]),
      position: new FormControl(''),
      phone: new FormControl('', [NumberValidator.numberOnly()]),
      phone2: new FormControl('', [NumberValidator.numberOnly()]),
      email: new FormControl('', [Validators.email]),
      website: new FormControl('', []),
      otherContact: new FormControl(''),
      note: new FormControl(''),
      address: new FormControl(''),
      active: new FormControl(true),
      smsSupport: new FormControl(),
      mmsSupport: new FormControl(),
      version: new FormControl(),
      id: new FormControl(),
      bpaddressId: new FormControl(),
    };

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_CONTACT_PERSON_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((detail) => {
      this.bindData(detail);
    });

    // bind address detail
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_ADDRESS_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((detail) => {
      this.address = detail.address;
      // this.frm.get('address').patchValue(this.buildAddressInfo(detail.address));
      this.frm.get('bpaddressId').patchValue(detail.id);
      if (this.isSelectAddress) {
        this.frm.get('address').markAsDirty();
        this.isSelectAddress = false;
      }
      this.initialFrmValues = _.cloneDeep(this.frm.value);
    });

    // bind address detail
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        partnerActions.ADD_BANK_ACCOUNT_FAILED,
        partnerActions.UPDATE_CONTACT_PERSON_FAILED,
        partnerActions.DELETE_CONTACT_PERSON_FAILED,
      ),
      map((action: any) => action.payload)
    ).subscribe((detail) => {
      this.submitted = false;
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        partnerActions.CREATE_CONTACT_PERSON_SUCCEEDED,
        partnerActions.UPDATE_CONTACT_PERSON_SUCCEEDED,
        partnerActions.DELETE_CONTACT_PERSON_SUCCEEDED,
      )
    ).subscribe((action: any) => {
      if (this.showToast) {
        switch (action.type) {
          case partnerActions.CREATE_CONTACT_PERSON_SUCCEEDED: 
            this.toast.success(this.translationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.ADD_CONTACT_PERSON_SUCCESS'));
            break;
          case partnerActions.UPDATE_CONTACT_PERSON_SUCCEEDED:
            this.toast.success(this.translationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.UPDATE_CONTACT_PERSON_SUCCESS'));
            break;
          case partnerActions.DELETE_CONTACT_PERSON_SUCCEEDED:
            this.toast.success(this.translationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_CONTACT_PERSON_SUCCESS'));
            break;
        }
      }

      if (this.rebindingData) {
        this.frm.enable();
        this.bindData(action.payload);
        if (this.isDisableForm) {
          this.frm.disable();
        }
      }

      // this.dialogRef.close();
      this.onActionSuccess.emit(action.payload);
    });
   }

  ngOnInit() {
    super.ngOnInit();
    this.initialFrmValues = _.cloneDeep(this.frm.value);
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.isDisableForm) {
        this.frm.disable();
        // this.disableTransportInfo = true;
      }
    });
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  bindData(detail) {
    this.frm.reset();
    if (!detail) {
      return;
    }
    this.frm.patchValue(detail.contact);
    this.frm.patchValue(detail);
    this.frm.get('id').patchValue(detail.id);
    this.frm.get('version').patchValue(detail.version);
    this.frm.get('note').patchValue(detail.note);

    // load address detail
    if (detail.bpaddressId) {
      this.store.dispatch(new partnerActions.GetPartnerAddressDetail({
        partnerId: this.data.partnerId,
        bpAddressId: detail.bpaddressId
      }));
    }

    this.initialFrmValues = _.cloneDeep(this.frm.value);
  }

  buildAddressInfo(address) {
    return `${address.recipient ? address.recipient : ''}\n${
      address.street ? address.street : ''} ${address.streetNumber ? address.streetNumber : ''}\n${
      address.zipCode ? address.zipCode : ''} ${address.cityName ? address.cityName : ''}\n${
      address.stateName ? address.stateName : 'Czech'}`.trim();
  }

  selectAddress() {
    this.partnerAddressesDialogRef = this.dialog.open(SelectBpAddressComponent, {
      maxHeight: 600,
      width: '800px',
      autoFocus: false, // disable focus button on modal
      // disableClose: true,
      data: {
        partnerId: this.data.partnerId,
      },
    } as MatDialogConfig);

    this.partnerAddressesDialogRef.afterClosed().subscribe((result) => {
      if (result && result.id) {
        this.isSelectAddress = true;
        // load address detail
        this.store.dispatch(new partnerActions.GetPartnerAddressDetail({
          partnerId: this.data.partnerId,
          bpAddressId: result.id
        }));
      }
    });
  }

  onSubmit() {
    const {valid, value} = this.frm;

    if (!valid) {
      return;
    }

    const objectPost = {
      contact: {
        email: value.email,
        mmsSupport: value.mmsSupport,
        smsSupport: value.smsSupport,
        otherContact: value.otherContact,
        phone: value.phone ? value.phone.replace(/ /g, '') : null,
        phone2: value.phone2 ? value.phone2.replace(/ /g, '') : null,
        website: value.website
      },
      active: value.active,
      name: value.name,
      position: value.position,
      salutation: value.salutation,
      bpaddressId: value.bpaddressId,
      note: value.note
    };

    if (this.data.personId) {
      objectPost['id'] = this.data.personId;
      objectPost['version'] = value.version;

      this.store.dispatch(new partnerActions.UpdateContactPerson({
        ...this.data,
        person: objectPost
      }));

    } else {
      this.store.dispatch(new partnerActions.CreateContactPerson({
        partnerId: this.data.partnerId,
        person: objectPost
      }));
    }
  }

  delete() {
    this.confirmModalService
    .show(this.translationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_CONTACT_PERSON_CONFRIM_MSG'), (status) => {
      if (status) {
        this.submitted = true;
        this.store.dispatch(new partnerActions.DeleteContactPerson(this.data));
      }
    }, 
    this.translationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_CONTACT_PERSON_CONFRIM_TITLE'));
  }

  cancel() {
    if (!_.isEqual(this.initialFrmValues, this.frm.value)) {
      const subject = new Subject<boolean>();
      this.deactiveWarningModalService.subject = subject;

      subject.subscribe((result) => {
        if (result) {
          this.onCancelChange.emit();
        }
      });
      this.deactiveWarningModalService.show();
    } else {
      this.onCancelChange.emit();
    }
  }


}
