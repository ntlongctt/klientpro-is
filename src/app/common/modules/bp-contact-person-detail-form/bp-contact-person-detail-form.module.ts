import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BpContactPersonDetailFormComponent } from './bp-contact-person-detail-form.component';
import { AppSharedModule } from '@common/shared';
import { PhoneNumberInputModule } from '../phone-number-input/phone-number-input.module';
import { SelectBpAddressComponent } from './select-bp-address/select-bp-address.component';
import { BpAddressesListModule } from '../bp-addresses-list/bp-addresses-list.module';
import { AddressFormatModule } from '../address-format/address-format.module';

@NgModule({
  imports: [
    AppSharedModule,
    PhoneNumberInputModule,
    BpAddressesListModule,
    AddressFormatModule
  ],
  declarations: [BpContactPersonDetailFormComponent, SelectBpAddressComponent],
  entryComponents: [BpContactPersonDetailFormComponent, SelectBpAddressComponent],
  exports: [BpContactPersonDetailFormComponent]
})
export class BpContactPersonDetailFormModule { }
