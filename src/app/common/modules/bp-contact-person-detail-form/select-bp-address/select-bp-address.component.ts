import { Component, OnInit, Optional, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'select-bp-address',
  templateUrl: './select-bp-address.component.html',
  styleUrls: ['./select-bp-address.component.scss']
})
export class SelectBpAddressComponent implements OnInit, OnDestroy {

  dataSource = [];

  unSubAll = new Subject();

  partnerId;
  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public partnerAddressesDialogRef: MatDialogRef<SelectBpAddressComponent>,
    private store: Store<AppState>,
    private actions$: Actions
  ) {
    this.partnerId = this.data.partnerId;

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_ADDRESSES_SUCCEEDED),
      map((action: any) => action.payload.addresses)
    ).subscribe(addresses => {
      this.dataSource = addresses;
    });
   }

  ngOnInit() {
    this.store.dispatch(new partnerActions.GetPartnerAddresses(this.partnerId));
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  onRowClick(row) {
    this.partnerAddressesDialogRef.close(row);
  }

}
