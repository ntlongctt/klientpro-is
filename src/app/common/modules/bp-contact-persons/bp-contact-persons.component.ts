import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from './i18n/en';
import { locale as cs } from './i18n/cs';
@Component({
  selector: 'bp-contact-persons',
  templateUrl: './bp-contact-persons.component.html',
  styleUrls: ['./bp-contact-persons.component.scss']
})
export class BpContactPersonsComponent implements OnInit {

  @Input() dataSource = [];
  @Input() useForDepartment = false;
  @Output() onRowClick = new EventEmitter();

  displayedColumns = [];

  constructor(
    private translationLoaderService: FuseTranslationLoaderService
  ) { 
    this.translationLoaderService.loadTranslations(en, cs);
  }

  ngOnInit() {
    this.displayedColumns = ['name', 'position', 'phone', 'email', 'address'];
    if (this.useForDepartment) {
      this.displayedColumns.push('publicVisible');
      this.displayedColumns.push('partnerVisible');
    }
  }

  rowClick(row) {
    this.onRowClick.emit(row);
  }


}
