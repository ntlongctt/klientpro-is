import { NgModule } from '@angular/core';
import { BpContactPersonsComponent } from './bp-contact-persons.component';
import { AppSharedModule } from '@common/shared';
import { MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    AppSharedModule,
    MatTableModule
  ],
  entryComponents: [BpContactPersonsComponent],
  declarations: [BpContactPersonsComponent],
  exports:  [BpContactPersonsComponent]
})
export class BpContactPersonsModule { }
