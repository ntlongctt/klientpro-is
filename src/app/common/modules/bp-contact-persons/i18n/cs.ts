export const locale = {
  lang: 'cs',
  data: {
    BP_CONTACT: {
      NAME: 'Jméno',
      POSITION: 'Pozice',
      PHONE: 'Telefon',
      EMAIL: 'E-mail',
      ADDRESS: 'Adresa',
      VISIBLE_FOR_PUBLIC: 'Pro veřejnost',
      VISIBLE_FOR_PARTNERS: 'Pro partnery',
    }
  }
};
