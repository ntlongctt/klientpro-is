export const locale = {
  lang: 'en',
  data: {
    BP_CONTACT: {
      NAME: 'Name',
      POSITION: 'Position',
      PHONE: 'Phone',
      EMAIL: 'Email',
      ADDRESS: 'Address',
      VISIBLE_FOR_PUBLIC: 'Visible for public',
      VISIBLE_FOR_PARTNERS: 'Visible for partners',
    }
  }
};
