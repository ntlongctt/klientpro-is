import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router, ActivatedRouteSnapshot } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as userManagementCs } from '../../../pages/users/i18n/cs';
import { locale as userManagementEn } from '../../../pages/users/i18n/en';
import { locale as tagModuleCs } from '../tags/i18n/cs';
import { locale as tagModuleEn } from '../tags/i18n/en';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-breadscumb',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadscumbComponent implements OnInit {
  
  paths = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translationLoaderService: FuseTranslationLoaderService,
    private translateService: TranslateService
  ) { 
    this.translationLoaderService.loadTranslations(userManagementCs, userManagementEn, tagModuleCs, tagModuleEn);

    this.translateService.onLangChange.subscribe(() => {
      const breadcrumbs = this.buildBreadscumb(this.activatedRoute.snapshot);
      this.paths = breadcrumbs.reverse();
    });
  }

  ngOnInit() {
    const breadcrumbs = this.buildBreadscumb(this.activatedRoute.snapshot);
    this.paths = breadcrumbs.reverse();
  }

  /**
   * Build breadscrumb base on current url
   * @param route 
   * @param path 
   */
  buildBreadscumb(route: ActivatedRouteSnapshot, path = []): any[] {
    const routeConfig = route.routeConfig;

    if (!routeConfig) {
      return path;
    }

    const routeData = routeConfig.data;

    if (routeData) {
      if (routeData['key']) {
        if (route.queryParams[routeData['key']]) {
          path.push({
            name: this.translationLoaderService.instant(route.queryParams[routeData['key']]),
            route,
          });
        }
      } else if (routeData['title']) {
        path.push({
          name: this.translationLoaderService.instant(routeData['title']),
          route,
        });
      }
    }
    if (route.parent) {
      return this.buildBreadscumb(route.parent, path);
    }
    return path;
    
  }

  /**
   * Generate routing url when click on breadscrumb
   * @param route 
   * @param path 
   */
  getRouteName(route: ActivatedRouteSnapshot, path = []) {
    if (!route.url) {
      return path;
    }

    route.url.reverse().forEach(i => {
      path.push(i.path);
    }) ;

    if (route.parent) {
      this.getRouteName(route.parent, path);
    }

    return path;

  }

  /**
   * Navigate to page click on bre
   * @param route 
   * @param idx 
   */
  onClick(route: ActivatedRouteSnapshot, idx = 0) {

    if (idx === this.paths.length - 1) {
      return;
    }
    const paths = this.getRouteName(route);

    let queryParams = {};
    for (const key in route.queryParams) {
      if (route.queryParams.hasOwnProperty(key)) {
        if (route.params.hasOwnProperty(key)) {
          
          queryParams = {...queryParams, [key]: route.queryParams[key]};
        }
      }
    }

    this.router.navigate(paths.reverse(), {queryParams});
  }

  goHome() {
    this.router.navigate(['dashboard']);
  }
}
