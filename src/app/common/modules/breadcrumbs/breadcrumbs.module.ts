import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadscumbComponent } from './breadcrumbs.component';
import { AppSharedModule } from '@common/shared';

@NgModule({
  imports: [
    AppSharedModule
  ],
  exports: [BreadscumbComponent],
  declarations: [BreadscumbComponent],
  entryComponents: [BreadscumbComponent]
})
export class BreadscumbModule { }
