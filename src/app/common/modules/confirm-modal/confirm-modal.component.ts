import {
  Component,
  Inject, Optional,
  ViewEncapsulation,
  OnInit
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from './i18n/en';
import { locale as cs} from './i18n/cs';
import { Actions, ofType } from '@ngrx/effects';
import * as fromErrorActions from '@store/error/error.actions';
import { AppStoreService } from '@store/store.service';

interface IConfirmData {
  title?: string;
  message: string;
  type?: string;
  showCancel: boolean;
  errorCode: string;
  warningMsg: string;
}

@Component({
  selector: 'confirm-modal',
  templateUrl: 'confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmModalComponent {
  public confirmTitle: string;
  public message: string;
  public showCancel: boolean;
  public type = '';
  public errorCount;
  public warningMsg = '';
  constructor(
    private _translationLoader: FuseTranslationLoaderService,
    public dialogRef: MatDialogRef<ConfirmModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: IConfirmData,
    private actions$: Actions,
    private appStoreService: AppStoreService
  ) {
    this._translationLoader.loadTranslations(en, cs);
    this.confirmTitle = this.data.title ? this.data.title : this._translationLoader.instant('CONFIRMATION_TITLE');
    this.message = this.data.message ? this.data.message : this._translationLoader.instant('CONFIRMATION_MESSAGE');
    this.showCancel = this.data.showCancel;
    this.type = this.data.type ;
    this.warningMsg = this.data.warningMsg;

    this.actions$.pipe(
      ofType(fromErrorActions.ADD_ERROR_SHOWING)
    ).subscribe((action: any) => {
        if (this.data.errorCode === action.payload) {
          this.errorCount = this.appStoreService.getState().error.showing.filter(e => e === this.data.errorCode).length - 1;
        }
    });

    // listen event to close modal
    this.actions$.pipe(
      ofType(fromErrorActions.CLOSE_LOGOUT_MODAL)
    ).subscribe((action: any) => {
      this.close(false);
    });

  }

  public close(status?: boolean): void {
    this.dialogRef.close(status);
  }
}

