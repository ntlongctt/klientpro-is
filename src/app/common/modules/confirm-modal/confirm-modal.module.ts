import {
  NgModule
} from '@angular/core';

import {
  ConfirmModalComponent
} from './confirm-modal.component';

import { ConfirmModalService } from './confirm-modal.service';
import { AppSharedModule } from '@common/shared';

@NgModule({
  declarations: [
    ConfirmModalComponent,
  ],
  imports: [
    AppSharedModule
  ],
  providers: [
    ConfirmModalService
  ],
  entryComponents: [
    ConfirmModalComponent
  ]
})
export class ConfirmModalModule {

}
