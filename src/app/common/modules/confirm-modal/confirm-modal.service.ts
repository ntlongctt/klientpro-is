import {
  Injectable
} from '@angular/core';

import { ConfirmModalComponent } from './confirm-modal.component';
import {
  MatDialog,
  MatDialogConfig
} from '@angular/material';

export const DELAY_AFTER_OPEN = 700;

@Injectable()
export class ConfirmModalService {

  

  constructor(public dialog: MatDialog) {
  }

  public show(message: string, cb?: Function, title?: string, showCancel = true, type: string = 'confirm', errorCode: string = '', warningMsg: string = '') {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      data: {
        message,
        title,
        type,
        showCancel,
        errorCode,
        warningMsg
      },
      autoFocus: false, // disable focus button on modal
      disableClose: true, // prevent user close model by lick on backdrop
      maxWidth: 700
    } as MatDialogConfig);

    // enable close modal by click on backdrop after delay time
    setTimeout(() => {
      dialogRef.disableClose = false;
    }, DELAY_AFTER_OPEN);

    dialogRef.afterClosed().subscribe((result) => {
      if (cb) {
        cb(result);
      }
    });
  }
}
