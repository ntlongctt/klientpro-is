export const locale = {
  lang: 'cs',
  data: {
    CONFIRMATION_TITLE: 'Potvrzení',
    CONFIRMATION_MESSAGE: 'Jste si jistá(ý)?',
    ERROR_COUNT: 'krát stejná chyba nastala znovu.'
  }
};
