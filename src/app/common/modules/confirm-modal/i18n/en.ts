export const locale = {
  lang: 'en',
  data: {
    CONFIRMATION_TITLE: 'Confirmation',
    CONFIRMATION_MESSAGE: 'Are you sure?',
    ERROR_COUNT: 'more error(s) of the same type occured.'
  }
};
