import {
  Component,
  OnInit,
  forwardRef,
  DoCheck,
  Optional,
  Host,
  SkipSelf,
  Input
} from '@angular/core';
import {
  NG_VALUE_ACCESSOR,
  ControlValueAccessor,
  AbstractControl,
  ControlContainer,
  FormControlName,
  FormControl,
  FormGroupDirective,
  NgForm,
  FormGroup,
  Validators
} from '@angular/forms';
import { Util } from '@common/services/util';
import * as moment from 'moment';
import * as lodash from 'lodash';
import { ErrorStateMatcher } from '@angular/material';

// ngModel
const DATE_RANGE_PICKER_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DateRangePickerComponent),
  multi: true
};

@Component({
  selector: 'date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.scss'],
  providers: [DATE_RANGE_PICKER_VALUE_ACCESSOR]
})
export class DateRangePickerComponent implements OnInit, ControlValueAccessor {

  @Input() formControlName;
  @Input() startLabel = '';
  @Input() endLabel = '';
  @Input() requiredErrorMsg = '';
  @Input() invalidRangeMsg = '';

  _value: {
    start: string;
    end: string;
  } = {start: '', end: ''};

  _startDate;
  _endDate;
  _disabled;

  public control: AbstractControl;
  
  controlConfig = {
    startDate: new FormControl('', [Validators.required]),
    endDate: new FormControl(''),
  };
  frmDate = new FormGroup(this.controlConfig);

  constructor(
    private util: Util,
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer
  ) { }

  ngOnInit() {
    // listen markAsPristine event 
    if (this.controlContainer && this.formControlName) {
      const seft = this;
      this.control = this.controlContainer.control.get(this.formControlName);
      this.control.markAsDirty = function () {
        seft.validate();
      };
    }
  }

  onStartDateChange(value) {
    this._value = {...this._value, start: this.util.formatDate(value, false)};
    this._onChange(this._value);
    this.validate();
  }

  onEndDateChange(value) {
    this._value = {...this._value, end: this.util.formatDate(value, false)};
    this._onChange(this._value);
    this.validate();
  }

  validate() {
    // mark start date as touched to show error
    this.controlConfig.startDate.markAsTouched();
    
    // check start date value
    if (!this._value.start) {
      this.control.setErrors({invalid: true});
      this.controlConfig.startDate.setErrors({required: true});
      
    } else {
      if (this.controlConfig.startDate.errors) {
        delete this.controlConfig.startDate.errors['required'];
      }
    }

    // check date range invalid
    const startDate = moment(this._value.start);
    const endDate = moment(this._value.end);
    if (startDate.isAfter(endDate)) {
      this.control.setErrors({invalid: true});
      this.controlConfig.startDate.setErrors({invalid: true});
    } else {
      if (this.controlConfig.startDate.errors) {
        delete this.controlConfig.startDate.errors['invalid'];
      }
    }

    // set dateRange valid
    if (lodash.isEmpty(this.controlConfig.startDate.errors)) {
      this.controlConfig.startDate.setErrors(null);
      this.control.setErrors(null);
    }
  }

  // get accessor
  public get value(): any {
    return this._value;
  }

  // set accessor including call the onchange callback
  // ngModal changes from outside
  public set value(v: any) {
    this._onChange(v);
  }

  // From ControlValueAccessor interface
  public registerOnChange(fn: (value: any) => any): void {
    this._onChange = fn;
  }

  // From ControlValueAccessor interface
  public registerOnTouched(fn: () => any): void {
    this._onTouched = fn;
  }

  // From ControlValueAccessor interface
  // ngModel change 
  public writeValue(value: any) {
    if (!value) {
      return this._value;
    }
    this._value = {
      start: value.start,
      end: value.end
    };

    this.controlConfig.startDate.patchValue(value.start);
    this.controlConfig.endDate.patchValue(value.end);
  }

  // ngModel
  private _onTouched = () => {
  }

  private _onChange = (_: any) => {
  }

  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean) {
    this._disabled = isDisabled;
  }

}
