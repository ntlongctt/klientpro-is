import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateRangePickerComponent } from './date-range-picker.component';
import { AppSharedModule } from '@common/shared';
import { MatDatepickerModule } from '@angular/material';

@NgModule({
  imports: [
    AppSharedModule,
    MatDatepickerModule
  ],
  declarations: [DateRangePickerComponent],
  exports: [DateRangePickerComponent],
})
export class DateRangePickerModule { }
