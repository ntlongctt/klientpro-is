import { Component, OnInit, Optional, Inject } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { locale as en } from '../../../i18n/common/en';
import { locale as cs } from '../../../i18n/common/cs';

@Component({
  selector: 'deactivate-warning-modal',
  templateUrl: './deactivate-warning-modal.component.html',
  styleUrls: ['./deactivate-warning-modal.component.scss']
})
export class DeactiveWarningModalComponent implements OnInit {

  constructor(
    private _translationLoader: FuseTranslationLoaderService,
    public dialogRef: MatDialogRef<DeactiveWarningModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    this._translationLoader.loadTranslations(en, cs);
  }

  public close(status?: boolean): void {
    this.dialogRef.close(status);
  }

}
