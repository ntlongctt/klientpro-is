import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeactiveWarningModalService } from './deactivate-warning-modal.service';
import { MatButtonModule, MatDialogModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { DeactiveWarningModalComponent } from './deactivate-warning-modal.component';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    FlexLayoutModule,
    TranslateModule
  ],
  declarations: [DeactiveWarningModalComponent],
  providers: [DeactiveWarningModalService],
  entryComponents: [DeactiveWarningModalComponent]
})
export class DeactiveWarningModalModule { }
