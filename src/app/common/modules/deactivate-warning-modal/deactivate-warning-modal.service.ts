import {
  Injectable
} from '@angular/core';

import {
  MatDialog,
  MatDialogConfig
} from '@angular/material';
import { Subject } from 'rxjs/Subject';
import { DeactiveWarningModalComponent } from './deactivate-warning-modal.component';

@Injectable()
export class DeactiveWarningModalService {

  subject: Subject<boolean>;
  isShowing = false;

  constructor(public dialog: MatDialog) {
  }

  public show(cb?: Function) {
    this.isShowing = true;
    const dialogRef = this.dialog.open(DeactiveWarningModalComponent, {
      autoFocus: false // disable focus button on modal
    } as MatDialogConfig);
    dialogRef.afterClosed().subscribe((result) => {
      this.subject.next(result);
      this.subject.complete();
      this.isShowing = false;
    });
  }
}
