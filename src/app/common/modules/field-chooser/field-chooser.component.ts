import { Component, OnInit, Input, ViewChild, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from './i18n/en';
import { locale as cs } from './i18n/cs';

@Component({
  selector: 'field-chooser',
  templateUrl: './field-chooser.component.html',
  styleUrls: ['./field-chooser.component.scss']
})
export class FieldChooserComponent implements OnInit, OnChanges {

  @Input()
  selectedList = [];
  @Input()
  availableList = [];
  @Input()
  selectedListTitle = this.translationLoaderService.instant('SELECTED_LIST_TITLE');
  @Input()
  availableListTitle = this.translationLoaderService.instant('AVAILABLE_LIST_TITLE');

  @Output()
  onValueChange = new EventEmitter();

  _selectedListValue = [];
  _availableListValue = [];
  selectedListOriginal = [];
  availableListOriginal = [];
  constructor(
    private translationLoaderService: FuseTranslationLoaderService
  ) {
    this.translationLoaderService.loadTranslations(en, cs);
   }

  ngOnInit() {
    this._selectedListValue = [];
    this._availableListValue = [];
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedList && changes.selectedList.currentValue) {
      this.selectedListOriginal = [...this.selectedList];
    }

    if (changes.availableList && changes.availableList.currentValue) {
      this.availableListOriginal = [...this.availableList];
    }
  }

  moToAvailable() {
    this._selectedListValue.forEach(item => {
      this.availableList.push(item);
      const idx = this.selectedList.indexOf(item);
      this.selectedList.splice(idx, 1);
    });
    this.onValueChange.emit(this.selectedList);
  }

  moToSelected() {
    this._availableListValue.forEach(item => {
      this.selectedList.push(item);
      const idx = this.availableList.indexOf(item);
      this.availableList.splice(idx, 1);
    });
    this.onValueChange.emit(this.selectedList);
  }

  revertChanges() {
    this.selectedList = [...this.selectedListOriginal];
    this.availableList = [...this.availableListOriginal];
    this.onValueChange.emit(this.selectedList);
  }

}
