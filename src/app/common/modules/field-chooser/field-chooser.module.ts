import { NgModule } from '@angular/core';
import { FieldChooserComponent } from './field-chooser.component';
import { AppSharedModule } from '@common/shared';

@NgModule({
  imports: [
    AppSharedModule
  ],
  declarations: [FieldChooserComponent],
  exports: [FieldChooserComponent]
})
export class FieldChooserModule { }
