import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'input-search',
  templateUrl: './input-search.template.html'
})
export class InputSearchComponent implements OnChanges, AfterViewInit {

  @ViewChild('inputSearch') public inputSearch: ElementRef;
  @Input() public placeholder: string = '';
  @Input() public delay: number = 300;
  @Output() public onSearch = new EventEmitter<any>();
  @Input() public disabled: boolean = false;
  @Input() public showIcon = true;
  @Input() public autofocus = false;

  @Input()
  public value: string = '';

  public inputValue: string;

  public searchValue: string = '';

  private _onTextChangeSubject: Subject<string> = new Subject();

  public ngAfterViewInit() {
    this._onTextChangeSubject.pipe(debounceTime(this.delay)).subscribe((searchTextValue) => {
      this.onSearch.emit(searchTextValue);
    });
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('value')) {
      this.inputValue = this.value;
    }
  }

  public onInputChange($event) {
    this._onTextChangeSubject.next($event.target.value);
  }

  public cleanText() {
    this.searchValue = '';
  }
}
