import {
  CommonModule
} from '@angular/common';

import {
  NgModule
} from '@angular/core';

import { InputSearchComponent } from './input-search.component';
import { AppSharedModule } from '@common/shared';


@NgModule({
  declarations: [
    InputSearchComponent
  ],
  imports: [
    AppSharedModule
  ],
  exports: [
    InputSearchComponent,
  ],
})
export class InputSearchModule {

}
