import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingViewComponent } from './loading-view.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    LoadingViewComponent
  ],
  exports: [
    LoadingViewComponent
  ]
})

export class LoadingViewModule {
}
