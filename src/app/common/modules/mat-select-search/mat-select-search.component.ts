import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component, Input,
  forwardRef,
  OnDestroy,
  OnInit,
  ViewChild,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
} from '@angular/core';

import {
  NG_VALUE_ACCESSOR,
  FormControl
} from '@angular/forms';
import { MatSelect } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';

// ngModel
const MAT_SELECT_SEARCH_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => MatSelectSearchComponent),
  multi: true
};

@Component({
  selector: 'mat-select-search',
  templateUrl: './mat-select-search.template.html',
  providers: [MAT_SELECT_SEARCH_VALUE_ACCESSOR]
})
export class MatSelectSearchComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  public placeholder: string;

  public _disabled = false;
  @Input()
  get disabled() {
    return this._disabled;
  }

  set disabled(dis) {
    this._disabled = coerceBooleanProperty(dis);
  }

  @Input()
  public showClear: boolean = false;
  @Input()
  public placeholderSearch: string;
  @Input()
  public noEntriesFoundLabel: string = 'Not Found';
  @Input()
  public clearSearchInput: boolean = false;
  @Input()
  public list = [];
  @Input()
  public keyField: string;
  @Input()
  public displayField: string = 'name';
  // @Input()
  // public disabled: boolean = false;
  @Input()
  public selectValue: any;
  @Input()
  public searchOnServer: boolean = false;
  @Input()
  public defaultList: any[];
  @Input()
  public debounceTime: number = 0;
  @Input()
  public displayMulti = [];

  @Input() public multiple = false;

  @Input() public multiOther = 'Other';
  @Input() public multiOthers = 'Others';
  @Input() public isRequired = false;
  @Input() public hasNoneOption = false;

  @Output()
  public onSelectionChange = new EventEmitter();
  @Output()
  public textSearchServerChange = new EventEmitter();
  @Output()
  public ontoggle = new EventEmitter();

  public filteredList = [];
  public selected: any;
  public filterCtrl: FormControl = new FormControl();

  @ViewChild('singleSelect') singleSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  private _onDestroy = new Subject<void>();

  public ngOnInit() {

    // listen for search field value changes
    this.filterCtrl.valueChanges
      .debounceTime(this.debounceTime)
      .pipe(takeUntil(this._onDestroy))
      .subscribe((value) => {
        if (this.searchOnServer) {
          this.textSearchServerChange.emit(value);
        } else {
          this.filter(value);
        }
      });
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('list') && changes['list'].currentValue) {
      this.filteredList = this.list;
    }
    if (changes.hasOwnProperty('defaultList') && changes['defaultList'].currentValue) {
      this.filteredList = [this.defaultList, ...this.list];
    }

    if (changes['selectValue'] && changes['selectValue'].currentValue) {
      this.selected = this.selectValue;
    }
  }

  public filter(search) {
    if (!this.list) {
      return;
    }
    // get the search keyword
    if (!search) {
      this.filteredList = [...this.list];
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredList = this.list.filter(obj => {
      return obj[this.displayField].toLowerCase().indexOf(search) > -1;
    });
  }

  public selectionChange(value) {
    this.selected = value;
    this.onSelectionChange.emit(value);
    this._onChange(value);
  }

  public openedChange(event) {
    this.ontoggle.emit(event);
  }

  public cleanValue(event = null) {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }
    if (this.multiple) {
      this.selected = [];
      this.onSelectionChange.emit([]);
      this._onChange('');
      return;
    }
    this.selected = '';
    this.onSelectionChange.emit('');
    this._onChange('');
  }

  public ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  displayWithFn(item) {
    const rs = [];
    this.displayMulti.forEach(field => {
      if (item[field]) {
        rs.push(item[field]);
      }
    });
    return rs.join(' | ');
  }

  getFirstValue() {
    const a = this.list.find(i => i[this.keyField] === this.selected[0]);
    return a ? a[this.displayField] : '';
  }

  // get accessor
  public get value(): any {
    return this.selected;
  }

  // set accessor including call the onchange callback
  // ngModal changes from outside
  public set value(v: any) {
    this._onChange(v);
  }

  // From ControlValueAccessor interface
  public registerOnChange(fn: (value: any) => any): void {
    this._onChange = fn;
  }

  // From ControlValueAccessor interface
  public registerOnTouched(fn: () => any): void {
    this._onTouched = fn;
  }

  // From ControlValueAccessor interface
  // ngModel change
  public writeValue(value: any) {
    this.selected = value;
  }

  // ngModel
  private _onTouched = () => {
  }

  private _onChange = (_: any) => {
  }

  setDisabledState(isDisabled: boolean) {
    this._disabled = isDisabled;
  }
}
