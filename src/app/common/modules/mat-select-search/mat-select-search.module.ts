import {
  NgModule
} from '@angular/core';
import { NgxMatSelectSearchModule } from '@modules/mat-select-search/ngx-mat-select-search/ngx-mat-select-search.module';

import { MatSelectSearchComponent } from './mat-select-search.component';
import { AppSharedModule } from '@common/shared';

@NgModule({
  declarations: [
    MatSelectSearchComponent
  ],
  imports: [
    AppSharedModule,
    NgxMatSelectSearchModule
  ],
  exports: [
    MatSelectSearchComponent,
  ],
})
export class MatSelectSearchModule {

}
