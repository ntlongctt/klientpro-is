import { Component, OnInit, forwardRef, Input, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { parse, format, AsYouType, isValidNumber, parseNumber, getCountryCallingCode, getPhoneCode } from 'libphonenumber-js';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

// ngModel
const PHONG_NUMBER_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => PhoneNumberInputComponent),
  multi: true
};

@Component({
  selector: 'phone-number-input',
  templateUrl: './phone-number-input.component.html',
  styleUrls: ['./phone-number-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [PHONG_NUMBER_VALUE_ACCESSOR]
})

export class PhoneNumberInputComponent implements OnInit, ControlValueAccessor {

  @Input() public placeholder = '';
  @Input() public disabled = false;

  public phoneNumber;
  public _value;
  public _disabled = false;
  public phoneNumberOption = {
    initialCountry: 'cz',
    autoPlaceholder: 'off'
  };
  public currentDialCode = '+420';
  public intlTelInputObj: any;
  constructor() { }

  ngOnInit() {
  }

  /**
   * Get instance of phone number filed
   */
  telInputObject(obj) {
    this.intlTelInputObj = obj;
    if (this._value) {
      const phone = parseNumber(this._value);
      if (phone.country) {
        this.intlTelInputObj.intlTelInput('setCountry', phone.country);
      }
    }
  }

  // set current dial code
  onCountryChange(event) {
    this.currentDialCode = `+${event.dialCode}`;
    const newPhone = this.intlTelInputObj[0].value ? this.intlTelInputObj[0].value : this.phoneNumber;
    this.onChange(newPhone);
  }

  /**
   * Format phone number before send to server. The format should be somethings like: +420603123123
   */
  formatPhoneNumber(phoneNumber: string) {
    if (phoneNumber) {
      const phone = parseNumber(phoneNumber);
      if (phone.country || phoneNumber.includes('+')) {
        return format(phoneNumber.replace(/\s/g, ''), 'International');
      }
      return this.currentDialCode + phoneNumber;
    }
    
  }

  /**
   * Call when input change
   */
  onChange(event) {
    setTimeout(() => {
      this.phoneNumber = event;
      this._value = this.formatPhoneNumber(this.phoneNumber);
      this._onChange(this._value);
    });
    
  }

  // get accessor
  public get value(): any {
    return this._value;
  }

  // set accessor including call the onchange callback
  // ngModal changes from outside
  public set value(v: any) {
    this._onChange(v);
  }

  // From ControlValueAccessor interface
  public registerOnChange(fn: (value: any) => any): void {
    this._onChange = fn;
  }

  // From ControlValueAccessor interface
  public registerOnTouched(fn: () => any): void {
    this._onTouched = fn;
  }

  // From ControlValueAccessor interface
  // ngModel change
  public writeValue(value: any) {
    this.phoneNumber = value ? format(value, 'International') : '';
    this._value = this.formatPhoneNumber(value);
    if (this.intlTelInputObj && this._value) {
      const phone = parseNumber(this._value);
      if (phone.country) {
        this.intlTelInputObj.intlTelInput('setCountry', phone.country);
      }
    }
  }

  // ngModel
  private _onTouched = () => {
  }

  private _onChange = (_: any) => {
  }

  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean) {
    this._disabled = isDisabled;
  }

}
