import { NgModule } from '@angular/core';
import { AppSharedModule } from '@common/shared';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { PhoneNumberInputComponent } from './phone-number-input.component';

@NgModule({
  imports: [
    AppSharedModule,
    Ng2TelInputModule
  ],
  declarations: [PhoneNumberInputComponent],
  exports: [PhoneNumberInputComponent]
})
export class PhoneNumberInputModule { }
