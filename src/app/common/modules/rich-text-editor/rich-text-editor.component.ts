import { Component, OnInit, forwardRef, Input, ViewChild, OnChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormGroup, FormBuilder } from '@angular/forms';
import { QuillEditorComponent } from 'ngx-quill';

// ngModel
const RICH_TEXT_EDITOR_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => RichTextEditorComponent),
  multi: true
};

@Component({
  selector: 'rich-text-editor',
  templateUrl: './rich-text-editor.component.html',
  styleUrls: ['./rich-text-editor.component.scss'],
  providers: [RICH_TEXT_EDITOR_VALUE_ACCESSOR]
})

export class RichTextEditorComponent implements OnInit, OnChanges, ControlValueAccessor {

  @ViewChild('quillEditor') quillEditor: QuillEditorComponent;

  @Input() readOnly = false;
  @Input() public label = '';
  @Input() public inputText = '';
  public _value;
  public _disabled = false;
  public quill;

  public richTextForm: FormGroup;

  toolbarConfigDefault = [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],
  
      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction
  
      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  
      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],
  
      ['clean'],                                         // remove formatting button
  
      ['link']                         // link and image, video ['link', 'image', 'video']
  ];

  // setting for rich text
  modules: any = {
    toolbar: this.toolbarConfigDefault
  };


  constructor(
    fb: FormBuilder
  ) {
    this.richTextForm = fb.group({
      editor: ''
    });
   }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.readOnly) {
      this.quillEditor.readOnly = true;
      this.modules = { toolbar: false };
      this.richTextForm.get('editor').patchValue(this.inputText);
    }
  }

  onContentChanged(event) {
    this._value = event.html;
    this._onChange(this._value);
  }

  
  // get accessor
  public get value(): any {
    return this._value;
  }

  // set accessor including call the onchange callback
  // ngModal changes from outside
  public set value(v: any) {
    this._onChange(v);
  }

  // From ControlValueAccessor interface
  public registerOnChange(fn: (value: any) => any): void {
    this._onChange = fn;
  }

  // From ControlValueAccessor interface
  public registerOnTouched(fn: () => any): void {
    this._onTouched = fn;
  }

  // From ControlValueAccessor interface
  // ngModel change
  public writeValue(value: any) {
    // this._value = value;
    this.richTextForm.get('editor').patchValue(value);
  }

  // ngModel
  private _onTouched = () => {
  }

  private _onChange = (_: any) => {
  }

  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean) {
    this._disabled = isDisabled;
  }

}
