import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RichTextEditorComponent } from './rich-text-editor.component';
import { AppSharedModule } from '@common/shared';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    AppSharedModule,
    QuillModule
  ],
  declarations: [RichTextEditorComponent],
  exports: [RichTextEditorComponent]
})
export class RichTextEditorModule { }
