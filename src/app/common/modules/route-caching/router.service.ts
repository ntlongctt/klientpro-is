import {
  Injectable, Injector
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationExtras, Router } from '@angular/router';
import { Util } from '@services/util';
import { filter } from 'rxjs/operators';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

export interface RouterOptions {
  enableBackView: boolean;
  scrollTop?: number;
  scrollElement?: FusePerfectScrollbarDirective;
}


@Injectable()
export class RouterService {
  private _enableBackView = false;
  private _scrollTop = 0;
  private _storedRoutes: any;
  private _scrollElement: FusePerfectScrollbarDirective;

  constructor(private injector: Injector,
    private utilService: Util) {

  }

  public set(options: RouterOptions) {
    this._enableBackView = options.enableBackView;
    this._scrollElement = options.scrollElement ? options.scrollElement : this._scrollElement;
    if (this.enableBackView) {
      const currentScrollTop = this._scrollElement.ps ? this._scrollElement.ps['lastScrollTop'] : 0;
      console.log('currentScrollTop', currentScrollTop);
      this.saveScrollTop(currentScrollTop);
    }
  }

  public navigate(commands: any[], extras?: NavigationExtras, options?: RouterOptions) {
    if (options) {
      this.set(options);
    }
    this.router.navigate(commands, extras);
  }

  public init() {
    this.router.events.pipe(
      filter(item => item instanceof NavigationEnd))
      .subscribe(() => {
        const lastActivatedRoute = this.utilService.getLastActivatedRoute(this.activatedRoute);
        const keyPath = this.utilService.getFullRoutePathByActivatedRoute('', lastActivatedRoute);
        const storedRoutes = this.storedRoutes();
        console.log('keyPath', keyPath);
        console.log('storedRoutes', storedRoutes);
        if (storedRoutes && storedRoutes[keyPath]) {
          const storedRoute = storedRoutes[keyPath];
          this.scrollTo(storedRoute.scrollTop);
          delete storedRoutes[keyPath];
        }
      });
  }

  // this creates router property on your service.
  public get router(): Router {
    return this.injector.get(Router);
  }

  public get activatedRoute(): ActivatedRoute {
    return this.injector.get(ActivatedRoute);
  }

  public enableBackView() {
    return this._enableBackView;
  }

  public storedRoutes() {
    return this._storedRoutes;
  }

  public setStoredRoutes(value) {
    this._storedRoutes = value;
  }

  public saveScrollTop(value) {
    this._scrollTop = value;
  }

  public scrollTop() {
    return this._scrollTop;
  }

  public getScrollElement() {
    return this._scrollElement;
  }

  public setScroller(scroller: FusePerfectScrollbarDirective) {
    this._scrollElement = scroller;
  }

  public scrollTo(posY: number){
    if (this._scrollElement) {
      // Set timeout for fix scrollY in safari
      setTimeout(() => {
        this._scrollElement.scrollToY(posY, 0);
      }, 10); // Need to wait a little bit for the view renderer
    }
  }
}
