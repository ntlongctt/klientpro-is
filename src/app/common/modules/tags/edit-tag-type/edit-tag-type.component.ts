import { Component, OnInit, OnDestroy, HostListener, ViewChild } from '@angular/core';
import { BaseComponent } from '@common/modules/base-component';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from '../i18n/en';
import { locale as cz } from '../i18n/cs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import * as fromTagActions from '@store/tag/tag.actions';
import { Subject } from 'rxjs/Subject';
import { takeUntil, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { Location, PlatformLocation } from '@angular/common';
import * as _ from 'lodash';
import * as fromFormActions from '@store/form/form.actions';
import { BreadscumbComponent } from '@common/modules/breadcrumbs/breadcrumbs.component';

@Component({
  selector: 'app-edit-tag-type',
  templateUrl: './edit-tag-type.component.html',
  styleUrls: ['./edit-tag-type.component.scss']
})

export class EditTagTypeComponent extends BaseComponent implements OnInit, OnDestroy {

  @ViewChild('breadscrumb') breadscrumb: BreadscumbComponent;

  public entityName;
  public tagTypeId;
  public unsubscribe_all = new Subject();
  public submitted = false;
  public isFormTagChanged = false;
  public initialForm: any;
  public tagTypeName = '';
  public isLoadingTags = false;

  /**
   * Before tab/browser close or refresh, check if form data has changed,show alter confirm
   */
  @HostListener('window:beforeunload', ['$event'])
  beforeunload(event) {
    if (this.isFormTagChanged) {
      return false;
    }
  }

  constructor(
    private translationLoader: FuseTranslationLoaderService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private action$: Actions,
    private toast: ToastrService,
    private confirmModalService: ConfirmModalService,
  ) {
    super();

    this.translationLoader.loadTranslations(en, cz);

    this.entityName = this.activatedRoute.snapshot.params['entityName'];
    this.tagTypeId = this.activatedRoute.snapshot.params['tagTypeId'];

    this.controlConfig = {
      name: new FormControl('', [Validators.required]),
      version: new FormControl(''),
      id: new FormControl(null),
    };

    /**
     * Binding data to form when get tag type detail success
     */
    this.action$.pipe(
      takeUntil(this.unsubscribe_all),
      ofType(fromTagActions.GET_TAG_TYPES_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(typeDetail => {
      this.isLoadingTags = false;
      this.frm.patchValue(typeDetail);
      this.tagTypeName = typeDetail.name;
      this.initialForm = this.frm.value;
    });

    /**
     * Show toast ans back to tag type list when create, update, delete success
     */
    this.action$.pipe(
      takeUntil(this.unsubscribe_all),
      ofType(
        fromTagActions.CREATE_TAG_TYPE_SUCCEEDED,
        fromTagActions.UPDATE_TAG_TYPE_SUCCEEDED,
        fromTagActions.DELETE_TAG_TYPE_SUCCEEDED
      )
    ).subscribe((action: any) => {
      switch (action.type) {
        case fromTagActions.CREATE_TAG_TYPE_SUCCEEDED:
          this.toast.success(this.translationLoader.instant('TAGS_MANAGEMENT.MESSAGE.CREATE_TAG_TYPE_SUCCESS'));
          break;
        case fromTagActions.UPDATE_TAG_TYPE_SUCCEEDED:
          this.toast.success(this.translationLoader.instant('TAGS_MANAGEMENT.MESSAGE.UPDATED_TAG_TYPE_SUCCESS'));
          break;
        case fromTagActions.DELETE_TAG_TYPE_SUCCEEDED:
          this.toast.success(this.translationLoader.instant('TAGS_MANAGEMENT.MESSAGE.DELETE_TAG_TYPE_SUCCESS'));
          break;
      }
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
      this.close();
    });

    // enable button when ation failed
    this.action$.pipe(
      takeUntil(this.unsubscribe_all),
      ofType(
        fromTagActions.CREATE_TAG_TYPE_FAILED,
        fromTagActions.UPDATE_TAG_TYPE_FAILED,
        fromTagActions.DELETE_TAG_TYPE_FAILED,
      )
    ).subscribe(() => {
      this.submitted = false;
    });

    /**
     * Set default form status
     */
    this.store.dispatch(new fromFormActions.ChangeFormStatus(false));

   }

  ngOnInit() {
    super.ngOnInit();

    if (this.tagTypeId) {
      this.loadTagTypeDetail();
    }

    /**
     * Check if user has change form value
     */
    this.frm.valueChanges.subscribe(() => {
      this.checkFormStatus();
    });
  }

   /**
   * check if form has changes, dispath to store
   */
  checkFormStatus() {
    const isFormChange = !_.isEqual(this.initialForm, this.frm.value) && this.frm.dirty;
    if (isFormChange !== this.isFormTagChanged) {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(isFormChange));
      this.isFormTagChanged = isFormChange;
    }
  }

  /**
   * Back to tag type list
   */
  close() {
    this.breadscrumb.onClick(this.activatedRoute.snapshot.parent);
  }

  /**
   * Show confirm and dispatch action delete tag type
   */
  delete() {
    this.confirmModalService
    .show(this.translationLoader.instant('TAGS_MANAGEMENT.MODAL.CONTENT.DELETE_TAG_TYPE'), (status) => {
      if (status) {
        this.submitted = true;
        this.store.dispatch(new fromTagActions.DeleteTagType({
          entityName: this.entityName,
          tagTypeId: this.tagTypeId
        }));
      }
    }, this.translationLoader.instant('TAGS_MANAGEMENT.MODAL.TITLE.DELETE_TAG_TYPE'));
  }

  save() {
    const {valid, value} = this.frm;
    if (valid) {
      this.submitted = true;
      const data = {
        entityName: this.entityName,
        tagType: value
      };
  
      if (this.tagTypeId) {
        data.tagType = {...data.tagType, id: parseInt(this.tagTypeId, 0)};
        this.store.dispatch(new fromTagActions.UpdateTagType({...data, tagTypeId: this.tagTypeId}));
      } else {
        this.store.dispatch(new fromTagActions.CreateTagType(data));
      }
    }
  }

  /**
   * Dispatch action to get tag type detail
   */
  loadTagTypeDetail() {
    this.isLoadingTags = true;
    this.store.dispatch(new fromTagActions.GetTagTypeDetail({
      tagTypeId: this.tagTypeId,
      entityName: this.entityName
    }));
  }

  ngOnDestroy() {
    this.unsubscribe_all.next();
    this.unsubscribe_all.complete();
  }  
}
