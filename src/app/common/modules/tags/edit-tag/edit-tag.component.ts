import { Component, OnInit, HostListener, OnDestroy, ViewChild } from '@angular/core';
import { BaseComponent } from '@common/modules/base-component';
import { FormControl, Validators } from '@angular/forms';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { ActivatedRoute, Router } from '@angular/router';
import * as fromTagActions from '@store/tag/tag.actions';
import * as fromFormActions from '@store/form/form.actions';
import * as fromUserAction from '@store/user/user.action';
import { map, takeUntil } from 'rxjs/operators';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import * as _ from 'lodash';
import { Location, PlatformLocation } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { Util } from '@services/util';
import { BreadscumbComponent } from '@common/modules/breadcrumbs/breadcrumbs.component';

@Component({
  selector: 'edit-tag',
  templateUrl: './edit-tag.component.html',
  styleUrls: ['./edit-tag.component.scss']
})
export class EditTagComponent extends BaseComponent implements OnInit, OnDestroy {

  @ViewChild('breadscrumb') breadscrumb: BreadscumbComponent;

  public entityName = '';
  public tagId = '';
  public tagTypes = [];
  public tagName = '';
  public submitted = false;
  public isFormTagChanged = false;
  public initialForm: any;
  public unSubscribeAll = new Subject();
  public isLoadingTags = false;

  /**
   * Before tab/browser close or refresh, check if form data has changed,show alter confirm
   */
  @HostListener('window:beforeunload', ['$event'])
  beforeunload(event) {
    if (this.isFormTagChanged) {
      return false;
    }
  }

  constructor(
    private translationLoader: FuseTranslationLoaderService,
    private store: Store<AppState>,
    private actions$: Actions,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toast: ToastrService,
    private confirmModalService: ConfirmModalService,
    private _util: Util
  ) {
    super();

    this.translationLoader.loadTranslations(en, cs);

    this.entityName = this.activatedRoute.snapshot.params['entityName'];
    this.tagId = this.activatedRoute.snapshot.params['tagId'];

    this.controlConfig = {
      name: new FormControl('', [Validators.required]),
      tagTypeId: new FormControl('', Validators.required),
      partnerVisible: new FormControl(false),
      publicVisible: new FormControl(false),
      version: new FormControl(''),
      id: new FormControl(null),
    };

    /**
     * Listen action get tag types
     */
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromTagActions.GET_TAG_TYPES_SUCCEEDED),
      map((action: any) => action.payload.tagTypes)
    ).subscribe(tagTypes => {
      this.tagTypes = tagTypes;

    });

    /**
     * Listen action get tag detail
     */
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromTagActions.GET_TAG_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((tag: any ) => {
      this.tagName = tag.name;
      this.isLoadingTags = false;
      this.bindTagDetail(tag);
    });

    
    /**
     * When create/update tag success, show toast message
     */
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromTagActions.CREATE_TAG_SUCCEEDED, fromTagActions.UPDATE_TAG_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(tag => {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
      this.frm.patchValue(tag);
      this.initialForm = this.frm.value;
      this.tagName = tag.name;
      if (this.tagId) {
        this.toast.success(this.translationLoader.instant('TAGS_MANAGEMENT.MESSAGE.UPDATED_TAG_SUCCESS'));
      } else {
        // if create tag success, set tagId for current tag
        this.tagId = tag.id;
        this.toast.success(this.translationLoader.instant('TAGS_MANAGEMENT.MESSAGE.CREATE_TAG_SUCCESS'));
      }
      this.close();
    });

    /**
     * When tag delete success, navigate back to tag management
     */
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromTagActions.DELETE_TAG_SUCCEEDED),
    ).subscribe(() => {
      this.toast.success(this.translationLoader.instant('TAGS_MANAGEMENT.MESSAGE.DELETE_TAG_SUCCESS'));
      this.close();
    });

    /**
     * Listen action get tag detail
     */
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(
        fromTagActions.DELETE_TAG_FAILED,
        fromTagActions.CREATE_TAG_FAILED,
        fromTagActions.UPDATE_TAG_FAILED,
      ),
      map((action: any) => action.payload)
    ).subscribe((tag: any ) => {
      this.submitted = false;
    });


    /**
     * Set default form status
     */
    this.store.dispatch(new fromFormActions.ChangeFormStatus(false));

   }

  ngOnInit() {
    super.ngOnInit();
    this.initialForm = this.frm.value;
    this.loadTagTypes();
    
    if (this.tagId) {
      this.getTagDetail();
    }

    /**
     * Check if user has change form value
     */
    this.frm.valueChanges.subscribe(() => {
      this.checkFormStatus();
    });
  }

  /**
   * check if form has changes, dispath to store
   */
  checkFormStatus() {
    const isFormChange = !_.isEqual(this.initialForm, this.frm.value) && this.frm.dirty;
    if (isFormChange !== this.isFormTagChanged) {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(isFormChange));
      this.isFormTagChanged = isFormChange;
    }
  }

  /**
   * Dispatch action to get list tag type
   */
  loadTagTypes() {
    this.store.dispatch(new fromTagActions.GetTagTypes({ entityName: this.entityName }));
  }

  /**
   * Dispatch action to get tag detail
   */
  getTagDetail() {
    this.isLoadingTags = true;
    this.store.dispatch(new fromTagActions.GetTagDetail({ entityName: this.entityName, tagId: this.tagId }));
  }

  /**
   * binding tag detail info to form
   * @param tagDetail
   */
  bindTagDetail(tagDetail) {
    this.frm.patchValue(tagDetail);
    // set initial value to check form value change
    this.initialForm = this.frm.value;
  }

  /**
   * Dispatch action to save tag
   */
  saveTag() {

    const {valid, value} = this.frm;
    
    if (valid) {
      const data = {
        entityName: this.entityName,
        tag: value
      };
      if (this.tagId) {
        this.store.dispatch(new fromTagActions.UpdateTag({...data, tagId: this.tagId}));
      } else {
        this.store.dispatch(new fromTagActions.CreateTag(data));
      }
    }  else {
      this.buildFormErrorMessage();
    }

  }

  /**
   * Dispatch action to delete tag
   */
  delete() {
    this.confirmModalService
      .show(this.translationLoader.instant('TAGS_MANAGEMENT.MODAL.CONTENT.DELETE_TAG'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new fromTagActions.DeleteTag({
            entityName: this.entityName,
            tagId: this.tagId
          }));
        }
      }, this.translationLoader.instant('TAGS_MANAGEMENT.MODAL.TITLE.DELETE_TAG'));
  }

  /**
   * Back to tag management
   */
  close() {
    this.breadscrumb.onClick(this.activatedRoute.snapshot.parent.parent);
  }

  /**
   * Navigate to namage tag type page
   */
  manageTagType() {
    const queryParams = {...this.activatedRoute.snapshot.queryParams};
    this.router.navigate([this._util.getPathName(), 'tag-type'], {queryParams: queryParams});
  }

  ngOnDestroy() {
    this.unSubscribeAll.next();
    this.unSubscribeAll.complete();
  }

}
