export const locale = {
  lang: 'cs',
  data: {
    TAGS_MANAGEMENT: {
      MESSAGE: {
        UPDATE_ASSIGNED_TAG_SUCCEEDED: 'Přiřazené nálepky byly aktualizovány',
        CREATE_TAG_SUCCESS: 'Nová nálepka byla vytvořena',
        UPDATED_TAG_SUCCESS: 'Nálepka byla aktualizována',
        DELETE_TAG_SUCCESS: 'Nálepka byla smazána',
        CREATE_TAG_TYPE_SUCCESS: 'Nový typ nálepky byl vytvořen',
        UPDATED_TAG_TYPE_SUCCESS: 'Typ nálepky byl aktualizován',
        DELETE_TAG_TYPE_SUCCESS: 'Typ nálepky byl vymazán',
        UNSAVE_CHANGES_WARNING: 'Nezapomeňte uložit změny'
      },
	  UNSPECIFIED_TAG_TYPE: 'Neuvedený typ nálepky',
      TITLE: 'Správa nálepek',
      EDIT_TAG_TITLE: 'Upravit nálepku',
      CREATE_TAG_TITLE: 'Vytvořit nálepku',
      EDIT_TAG_TYPE_TITLE: 'Upravit typ nálepky',
      CREATE_TAG_TYPE_TITLE: 'Vytvořit typ nálepky',
      PARTNER_VISIBLE: 'Viditelná pro poskytovatele',
      PUBLIC_VISIBLE: 'Veřejně viditelná',
      TAG_TYPE_MANAGMENT_TITLE: 'Správa typů nálepek',
      TAGS_BREADSCRUMB: 'Nálepky',
      TAGS_TYPE_BREADSCRUMB: 'Typy nálepek',
      BUTTON: {
        NEW_TAG: 'Přidat nálepku',
        MANAGE_TAG_TYPES: 'Spravovat typy nálepek',
        MANAGE_TAGS: 'Spravovat nálepky',
        NEW_TAG_TYPE: 'Přidat typ nálepky'
      },
      PLACEHOLDER: {
        TAG_NAME: 'Název nálepky',
        TAG_TYPE: 'Zvolte typ nálepky',
        TAG_TYPE_NAME: 'Název typu nálepky'
      },
      FORM_VALIDATOR: {
        TAG_NAME_REQUIRED: 'Název nálepky je povinný',
        TAG_TYPE_REQUIRED: 'Typ nálepky je povinný',
        TAG_TYPE_NAME_REQUIRED: 'Název typu je povinný'
      },
      MODAL: {
        TITLE: {
          DELETE_TAG: 'Potvrdit smazání nálepky',
          DELETE_TAG_TYPE: 'Potvrdit smazání typu nálepky'
        },
        CONTENT: {
          DELETE_TAG: 'Smazat nálepku?',
          DELETE_TAG_TYPE: 'Smazat typ nálepky?'
        }
      }
    }
  }
};
