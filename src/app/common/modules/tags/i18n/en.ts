export const locale = {
  lang: 'en',
  data: {
    TAGS_MANAGEMENT: {
      MESSAGE: {
        UPDATE_ASSIGNED_TAG_SUCCEEDED: 'Assigned tags was updated successful',
        CREATE_TAG_SUCCESS: 'New tag was created successful',
        UPDATED_TAG_SUCCESS: 'Tag was updated successful',
        DELETE_TAG_SUCCESS: 'Tag was deleted successfull',
        CREATE_TAG_TYPE_SUCCESS: 'New tag type was created successful',
        UPDATED_TAG_TYPE_SUCCESS: 'Tag type was updated successful',
        DELETE_TAG_TYPE_SUCCESS: 'Tag type was deleted successfull',
        UNSAVE_CHANGES_WARNING: 'There are unsaved changes'
      },
      UNSPECIFIED_TAG_TYPE: 'Unspecified tag type',
      TITLE: 'Tag management',
      EDIT_TAG_TITLE: 'Edit tag',
      CREATE_TAG_TITLE: 'Create tag',
      EDIT_TAG_TYPE_TITLE: 'Edit tag type',
      CREATE_TAG_TYPE_TITLE: 'Create tag type',
      PARTNER_VISIBLE: 'Partner visible',
      PUBLIC_VISIBLE: 'Public visible',
      TAG_TYPE_MANAGMENT_TITLE: 'Tag type management',
      TAGS_BREADSCRUMB: 'Tags',
      TAGS_TYPE_BREADSCRUMB: 'Tag types',
      BUTTON: {
        NEW_TAG: 'Add new tag',
        MANAGE_TAG_TYPES: 'Manage tag types',
        MANAGE_TAGS: 'Manage tags',
        NEW_TAG_TYPE: 'Add new tag type'
      },
      PLACEHOLDER: {
        TAG_NAME: 'Tag name',
        TAG_TYPE: 'Select tag type',
        TAG_TYPE_NAME: 'Type name',
      },
      FORM_VALIDATOR: {
        TAG_NAME_REQUIRED: 'Tag name is required',
        TAG_TYPE_REQUIRED: 'Tag type is required',
        TAG_TYPE_NAME_REQUIRED: 'Type name is required'
      },
      MODAL: {
        TITLE: {
          DELETE_TAG: 'Delete tag confirmation',
          DELETE_TAG_TYPE: 'Delete tag type confirmation',
        },
        CONTENT: {
          DELETE_TAG: 'Are your sure to delete this tag?',
          DELETE_TAG_TYPE: 'Are your sure to delete this tag type?'
        },
      },
    }
  }
};
