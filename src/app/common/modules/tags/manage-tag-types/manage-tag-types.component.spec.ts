import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTagTypesComponent } from './manage-tag-types.component';

describe('ManageTagTypesComponent', () => {
  let component: ManageTagTypesComponent;
  let fixture: ComponentFixture<ManageTagTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageTagTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTagTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
