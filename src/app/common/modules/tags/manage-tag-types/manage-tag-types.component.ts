import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromTagActions from '@store/tag/tag.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Location, PlatformLocation } from '@angular/common';
import { Util } from '@services/util';
import { BreadscumbComponent } from '@common/modules/breadcrumbs/breadcrumbs.component';

@Component({
  selector: 'manage-tag-types',
  templateUrl: './manage-tag-types.component.html',
  styleUrls: ['./manage-tag-types.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ManageTagTypesComponent implements OnInit, OnDestroy {

  @ViewChild('breadscrumb') breadscrumb: BreadscumbComponent;

  public tagTypes: any;
  public entityName = '';
  public unsubscribe_all = new Subject();
  public submitted = false;
  public isLoadingTags = false;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private actions$: Actions,
    private fuseTranslationLoaderService: FuseTranslationLoaderService,
    private router: Router,
    private location: Location,
    private _util: Util
  ) {

    this.fuseTranslationLoaderService.loadTranslations(en, cs);

    this.actions$.pipe(
      takeUntil(this.unsubscribe_all),
      ofType(fromTagActions.GET_TAG_TYPES_SUCCEEDED),
      map((action: any) => action.payload.tagTypes)
    ).subscribe(types => {
      this.tagTypes = types;
      this.isLoadingTags = false;
    });
   }

  ngOnInit() {
    this.entityName = this.activatedRoute.snapshot.params['entityName'];
    this.loadTagtypes();
  }

  /**
   * Dispatch action to get list tag type
   */
  loadTagtypes() {
    this.isLoadingTags = true;
    this.store.dispatch(new fromTagActions.GetTagTypes({ entityName: this.entityName }));
  }

  /**
   * Nagivate to tag type detail page
   * @param type
   */
  onSelectType(type) {
    const queryParams = {...this.activatedRoute.snapshot.queryParams, tagTypeId: type.name};
    this.router.navigate([this._util.getPathName(), 'edit', type.id], {queryParams: queryParams});
  }

  /**
   * Navigate to create tag type page
   */
  newTagtype() {
    // this.router.navigate([this.location.path(), 'create']);
    const queryParams = {...this.activatedRoute.snapshot.queryParams};
    this.router.navigate([this._util.getPathName(), 'create'], {queryParams: queryParams});
  }

  /**
   * Back to previous page
   */
  close() {
    this.breadscrumb.onClick(this.activatedRoute.snapshot.parent.parent);
  }

  ngOnDestroy() {
    this.unsubscribe_all.next();
    this.unsubscribe_all.complete();
  }
}
