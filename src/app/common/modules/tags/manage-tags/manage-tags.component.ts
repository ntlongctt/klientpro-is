import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromTagActions from '@store/tag/tag.actions';
import { Actions, ofType } from '@ngrx/effects';
import { map, takeUntil } from 'rxjs/operators';
import * as _ from 'lodash';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Location, PlatformLocation } from '@angular/common';
import {ViewEncapsulation} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Util } from '@services/util';
import { BreadscumbComponent } from '@common/modules/breadcrumbs/breadcrumbs.component';

@Component({
  selector: 'manage-tags',
  templateUrl: './manage-tags.component.html',
  styleUrls: ['./manage-tags.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ManageTagsComponent implements OnInit, OnDestroy {

  @ViewChild('breadscrumb') breadscrumb: BreadscumbComponent;

  public entityName = '';
  public listTagType: any[];
  public isLoadingTags = true;

  public panelOpenState: boolean = false;
  public allExpandState = false;
  public submitted = false;

  public unSubscribeAll = new Subject();

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private actions$: Actions,
    private fuseTranslationLoaderService: FuseTranslationLoaderService,
    private router: Router,
    private _util: Util
  ) {

    this.fuseTranslationLoaderService.loadTranslations(en, cs);
    this.entityName = this.activatedRoute.snapshot.params['entityName'];

    /**
     * Update list available tag when GET_TAGS_SUCCEEDED
     */
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromTagActions.GET_TAGS_SUCCEEDED),
      map((aciton: any) => aciton.payload.tags),
    ).subscribe((tags: any) => {
      this.isLoadingTags = false;
      // remove tag type if not have any tag
      const availableTags = _.cloneDeep(tags);
      // set name for unspecified tag type
      availableTags.forEach(i => {
        if (i.id === null) {
          i.name = this.fuseTranslationLoaderService.instant('TAG.UNSPECIFIED_TAG_TYPE');
        }
      });
      this.listTagType = availableTags;
    });
  }

  ngOnInit() {
    this.loadListTags();
  }

  /**
   * dispatch action get list tags
   */
  loadListTags() {
    this.isLoadingTags = true;
    this.store.dispatch(new fromTagActions.GetTags({entityName: this.entityName}));
  }

  /**
   * Navigate to create page create tag
   */
  newTag() {
    this.router.navigate([this._util.getPathName(), 'create'], {queryParams: this.activatedRoute.snapshot.queryParams});
  }

  /**
   * Back to previous location
   */
  close() {
    this.breadscrumb.onClick(this.activatedRoute.snapshot.parent.parent);
  }

  /**
   * Nagivate to tag detail
   * @param tag
   */
  onSelectTag(tag) {
    const queryParams = {...this.activatedRoute.snapshot.queryParams, tagId: tag.name};
    this.router.navigate([this._util.getPathName(), 'edit', tag.id], {queryParams: queryParams});
  }

  manageTagType() {
    this.router.navigate([this._util.getPathName(), 'tag-type'], {queryParams: this.activatedRoute.snapshot.queryParams});
  }

  ngOnDestroy() {
    this.unSubscribeAll.next();
    this.unSubscribeAll.complete();
  }

}

