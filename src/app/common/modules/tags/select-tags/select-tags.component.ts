import { Component, OnInit, OnDestroy, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromTagActions from '@store/tag/tag.actions';
import * as fromFormActions from '@store/form/form.actions';
import { Actions, ofType } from '@ngrx/effects';
import { map, takeUntil } from 'rxjs/operators';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { Subject } from 'rxjs/Subject';
import { IntegerListDTO } from '../../../../rest/models/common/common/IntegerListDTO';
import { TreeviewChooserComponent } from '@common/modules/treeview-chooser/treeview-chooser.component';
@Component({
  selector: 'select-tags',
  templateUrl: './select-tags.component.html',
  styleUrls: ['./select-tags.component.scss']
})
export class SelectTagsComponent implements OnInit, OnDestroy {

  @Input() entityName;
  @Input() itemId;
  @Output() onCancel = new EventEmitter();
  @Output() onSelectedChange = new EventEmitter();
  @Output() onGotoManageTags = new EventEmitter();
  @ViewChild('treeViewChooser') treeViewChooser: TreeviewChooserComponent;

  public selectedTree: any;
  public availableTree: any;
  public assginedTags: any[];
  public submitted = false;
  public initialSelected = [];
  public canEditTag = ['ROLE_TAG_EDITOR'];
  public assignedTagChange;

  private _unsubscribeAll = new Subject();
  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private toast: ToastrService,
    private fuseTranslationLoaderService: FuseTranslationLoaderService,
  ) { 

    this.fuseTranslationLoaderService.loadTranslations(en, cs);

    /**
     * Update list assigned tag when GET_ASSIGNED_TAG_SUCCEEDED
     */
    this.actions$.pipe(
      takeUntil(this._unsubscribeAll),
      ofType(fromTagActions.GET_ASSIGNED_TAG_SUCCEEDED),
      map((action: any) => action.payload.tags)
    ).subscribe((tags: any) => {
      const assignedTags = [];
      const availableTags = [];

      /**
       * Prepare assigned list and available list
       */
      _.cloneDeep(tags).forEach(type => {
        if (type.tags.length) {
          const assginedList = [];
          const availableList = [];
          type.tags.forEach(tag => {
            if (tag.assigned === true) {
              assginedList.push(tag);
            } else {
              availableList.push(tag);
            }
          });

          if (assginedList.length) {
            assignedTags.push({
              ...type, tags: assginedList
            });
          }
          
          if (availableList.length) {
            availableTags.push({
              ...type, tags: availableList
            });
          }

        }
      });

      // set name for unspecified tag type
      assignedTags.forEach(i => {
        if (i.id === null) {
          i.name = this.fuseTranslationLoaderService.instant('TAG.UNSPECIFIED_TAG_TYPE');
        }
      });
      this.selectedTree = assignedTags;
      this.initialSelected = [...assignedTags].reduce(function(ids, type) {
        return [...ids, ...type.tags.filter(tag => tag.id)];
      }, []);

      // expand assigned tags
      setTimeout(() => {
        this.treeViewChooser.selected.treeControl.expandAll();
      }, 50);
      
      // set name for unspecified tag type
      availableTags.forEach(i => {
        if (i.id === null) {
          i.name = this.fuseTranslationLoaderService.instant('TAG.UNSPECIFIED_TAG_TYPE');
        }
      });
      this.availableTree = availableTags;

      // expand available tags if there are less than 20 tags total or less than 3 tag types
      const totalTagtypes = availableTags.length;
      const totalTags = availableTags.reduce((total, tagType) => {
        return total + tagType.tags.length;
      }, 0);

      if (totalTagtypes < 3 || totalTags < 20) {
        setTimeout(() => {
          this.treeViewChooser.available.treeControl.expandAll();
        }, 50);
      }
    });

    /**
     * Update list available tag when GET_TAGS_SUCCEEDED
     */
    this.actions$.pipe(
      takeUntil(this._unsubscribeAll),
      ofType(fromTagActions.GET_TAGS_SUCCEEDED),
      map((aciton: any) => aciton.payload.tags)
    ).subscribe((tags: any) => {
      // remove tag type if not have any tag
      const availableTags = _.cloneDeep(tags.filter(i => i.tags.length));
      // set name for unspecified tag type
      availableTags.forEach(i => {
        if (i.id === null) {
          i.name = this.fuseTranslationLoaderService.instant('TAG.UNSPECIFIED_TAG_TYPE');
        }
      });
      this.availableTree = availableTags;
    });

    /**
     * Update list available tag when GET_TAGS_SUCCEEDED
     */
    this.actions$.pipe(
      takeUntil(this._unsubscribeAll),
      ofType(fromTagActions.UPDATE_ASSIGNED_TAG_SUCCEEDED),
    ).subscribe(() => {
      this.assignedTagChange = false;
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
      this.toast.success(this.fuseTranslationLoaderService.instant('TAGS_MANAGEMENT.MESSAGE.UPDATE_ASSIGNED_TAG_SUCCEEDED'));
    });
  }

  ngOnInit() {
    this.loadAssginedTag();
  }

  /**
   * Dispatch action to get list assigned tag
   */
  loadAssginedTag() {
    this.store.dispatch(new fromTagActions.GetAssignedTag({entityName: this.entityName, itemId: this.itemId}));
  }

  /**
   * Dispatch action to get list available tag
   */
  loadAvailableTags() {
    this.store.dispatch(new fromTagActions.GetTags({entityName: this.entityName}));
  }

  /**
   * Set list tag assigned 
   * @param assginedTags 
   */
  onSelectedListChange(assginedTags) {
    this.assginedTags = assginedTags;
    this.checkSelectedChange();
  }

  /**
   * Dispatch action to update assigned tag
   */
  save() {

    // Prepare object intList
    const ids: IntegerListDTO = {
      items: []
    };

    this.assginedTags.map(tag => {
      ids.items.push({
        value: tag.id
      });
    });
    
    this.store.dispatch(new fromTagActions.UpdateAssignedTag(
      {
        entityName: this.entityName,
        itemId: this.itemId,
        intList: ids
      }
    ));
  }

  /**
   * Navigate to manage tags
   */
  manageTags() {
    this.onGotoManageTags.emit();
  }

  /**
   * fire event cancel change of assigned tag
   */
  cancel() {
    this.onCancel.emit();
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  checkSelectedChange() {
    const initialList = _.sortBy(this.initialSelected.map(i => i.id));
    const currentList = _.sortBy(this.assginedTags.map(i => i.id));
    this.assignedTagChange = !_.isEqual(initialList, currentList);
    this.onSelectedChange.emit(this.assignedTagChange);
  }

}
