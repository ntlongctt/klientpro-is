import { ManageTagsComponent } from './manage-tags/manage-tags.component';
import { EditTagComponent } from './edit-tag/edit-tag.component';
import { ManageTagTypesComponent } from './manage-tag-types/manage-tag-types.component';
import { EditTagTypeComponent } from './edit-tag-type/edit-tag-type.component';
import { CanDeactivateGuard } from '@common/services/deactivate-guard';

export function getTagRouting(path) {
  return [
    {
      path: `${path}/tags`,
      data: {'title': 'TAGS_MANAGEMENT.TAGS_BREADSCRUMB'},
      children: [
        {
          path: '',
          component: ManageTagsComponent,
        },
        {
          path: 'create',
          data: {'title': 'TAGS_MANAGEMENT.CREATE_TAG_TITLE'},
          children: [
            {
              path: '',
              component: EditTagComponent,
              canDeactivate: [CanDeactivateGuard],
            },
            ...generateTagTypeRoute()
          ]
        },
        {
          path: 'edit/:tagId',
          data: {'title': 'TAGS_MANAGEMENT.EDIT_TAG_TITLE', 'key': 'tagId'},
          children: [
            {
              path: '',
              component: EditTagComponent,
              canDeactivate: [CanDeactivateGuard],
            },
            ...generateTagTypeRoute()
          ]
        },
        ...generateTagTypeRoute()
      ]
    },
    
    
  ];
}


export function generateTagTypeRoute() {
  return [
    {
      path: `tag-type`,
      data: {'title': 'TAGS_MANAGEMENT.TAGS_TYPE_BREADSCRUMB'},
      children: [
        {
          path: '',
          component: ManageTagTypesComponent,
        },
        {
          path: 'edit/:tagTypeId',
          component: EditTagTypeComponent,
          canDeactivate: [CanDeactivateGuard],
          data: {'title': 'TAGS_MANAGEMENT.CREATE_TAG_TYPE_TITLE', 'key': 'tagTypeId'},
        },
        {
          path: `create`,
          component: EditTagTypeComponent,
          canDeactivate: [CanDeactivateGuard],
          data: {'title': 'TAGS_MANAGEMENT.EDIT_TAG_TYPE_TITLE'},
        },
      ]
    },
  ];
}
