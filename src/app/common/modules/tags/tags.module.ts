import { NgModule } from '@angular/core';

import { AppSharedModule } from '@common/shared';
import { TreeviewChooserModule } from '@common/modules/treeview-chooser/treeview-chooser.module';
import { ManageTagsComponent } from './manage-tags/manage-tags.component';
import { TreeViewModule } from '@common/modules/tree-view/tree-view.module';
import { EditTagComponent } from './edit-tag/edit-tag.component';
import { ManageTagTypesComponent } from './manage-tag-types/manage-tag-types.component';
import { EditTagTypeComponent } from './edit-tag-type/edit-tag-type.component';
import { SelectTagsComponent } from './select-tags/select-tags.component';
import { BreadscumbModule } from '../breadcrumbs/breadcrumbs.module';

@NgModule({
  imports: [
    AppSharedModule,
    TreeviewChooserModule,
    TreeViewModule,
    BreadscumbModule
  ],
  declarations: [ManageTagsComponent, EditTagComponent, ManageTagTypesComponent, EditTagTypeComponent, SelectTagsComponent],
  entryComponents: [SelectTagsComponent],
  exports: [ManageTagsComponent, EditTagComponent, ManageTagTypesComponent, EditTagTypeComponent, SelectTagsComponent]
})
export class TagsModule { }
