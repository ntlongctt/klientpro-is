export const locale = {
  lang: 'cs',
  data: {
    TREE_VIEW: {
      BUTTON: {
        NEW_CHILD: 'Nový potomek',
        COPY_NODE: 'Kopírovat prvek',
        ACTIONS: 'Akce',
        DETAIL: 'Detail',
        REMOVE: 'Odstranit'
      }
    }
  }
};
