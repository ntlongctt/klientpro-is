export const locale = {
  lang: 'en',
  data: {
    TREE_VIEW: {
      BUTTON: {
        NEW_CHILD: 'New child',
        COPY_NODE: 'Copy this node',
        ACTIONS: 'Actions',
        DETAIL: 'Detail',
        REMOVE: 'Remove'
      }
    }
  }
};
