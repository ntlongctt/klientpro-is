import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Injectable, Output, EventEmitter, Input, OnChanges, OnDestroy, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/internal/observable/of';
import * as _ from 'lodash';
import { locale as en } from './i18n/en';
import { locale as cs } from './i18n/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

export class TagNode {
  tags?: TagNode[];
  name: string;
  id: any;
  assigned: boolean;
  isSelected: boolean;
}

export class TagFlatNode {
  name: string;
  level: number;
  expandable: boolean;
  id: any;
  assigned: boolean;
  isSelected: boolean;
}

/**
 * @title Tree with flat nodes
 */
@Component({
  selector: 'tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss'],
})

export class TreeViewComponent implements OnChanges, OnDestroy {
  @Output() doAction = new EventEmitter();
  @Output() onAddNewNode = new EventEmitter();
  @Output() onSelectNode = new EventEmitter();
  @Input() treeData: any;
  @Input() showCoppy = false;
  @Input() showActions = false;
  @Input() showAddNew = false;
  @Input() showEdit = false;
  @Input() showDelete = false;
  @Input() showCheckbox = false;

  treeControl: FlatTreeControl<TagFlatNode>;
  treeFlattener: MatTreeFlattener<TagNode, TagFlatNode>;
  dataSource: MatTreeFlatDataSource<TagNode, TagFlatNode>;

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap: Map<TagFlatNode, TagNode> = new Map<TagFlatNode, TagNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap: Map<TagNode, TagFlatNode> = new Map<TagNode, TagFlatNode>();

  dataChange: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  /** A selected parent node to be inserted */
  selectedParent: TagFlatNode | null = null;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<TagFlatNode>(true /* multiple */);

  constructor(
    private fuseTranslationLoaderService: FuseTranslationLoaderService
  ) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this._getLevel,
      this._isExpandable, this._getChildren);
    this.treeControl = new FlatTreeControl<TagFlatNode>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    this.fuseTranslationLoaderService.loadTranslations(en, cs);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['treeData'] && changes['treeData'].currentValue) {
      this.dataSource.data = this.treeData;
    }
  }

  /**
  * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
  */
  transformer = (node: TagNode, level: number) => {
    const flatNode = this.nestedNodeMap.has(node) && this.nestedNodeMap.get(node).id === node.id
      ? this.nestedNodeMap.get(node)
      : new TagFlatNode();
    flatNode.id = node.id;
    flatNode.level = level;
    flatNode.name = node.name;
    flatNode.expandable = !!node.tags;
    flatNode.assigned = node.assigned;
    flatNode.isSelected = node.isSelected;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  public _getLevel = (node: TagFlatNode) => node.level;

  private _isExpandable = (node: TagFlatNode) => node.expandable;

  private _getChildren = (node: TagNode): Observable<TagNode[]> => of(node.tags);

  // tslint:disable-next-line:no-shadowed-variable
  hasChild = (_: number, _nodeData: TagFlatNode) => _nodeData.expandable;

  action(node, action) {
    const nodeLevel = this._getLevel(node);
    this.doAction.emit({ node, action, nodeLevel });
  }

  /** Whether all the descendants of the node are selected */
  descendantsAllSelected(node: TagFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    return descendants.every(child => this.checklistSelection.isSelected(child));
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TagFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TagFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);

    if (node.isSelected !== undefined) {
      node.isSelected = !node.isSelected;
      this.onSelectNode.emit({ tag: node, tagType: this.getParent(node), isSelect: node.isSelected });
      return;
    }

    if (this.checklistSelection.isSelected(node)) {
      this.checklistSelection.select(...descendants);
    } else {
      this.checklistSelection.deselect(...descendants);
    }
    this.onSelectNode.emit({ tag: node, tagType: this.getParent(node), isSelect: this.checklistSelection.isSelected(node) });
  }

  /**
  * Iterate over each node in reverse order and return the first node that has a lower level than the passed node.
  */
  getParent(node: TagFlatNode) {
    const { treeControl } = this;
    const currentLevel = treeControl.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = treeControl.dataNodes[i];

      if (treeControl.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
  }

  getSelectedIds() {
    return this.checklistSelection.selected;
  }

  /** Select the category so we can insert the new item. */
  addNewItem(node: TagFlatNode, child: TagNode) {
    const parentNode = this.flatNodeMap.get(node);
    if (!parentNode.tags) {
      parentNode.tags = [];
    }
    parentNode.tags.push(child);
    this.treeControl.expand(node);
    this.refresh();

    const newNode = Array.from(this.flatNodeMap).pop();
    this.onAddNewNode.emit(newNode[0]);
  }

  /**
   * add new node (with childs) or child to node
   * @param node 
   * @param childs 
   */
  addNewNode(node: TagFlatNode, childs: any[]) {
    const exitsNode = this.dataSource.data.findIndex(i => i.id === node.id);

    if (exitsNode === -1) {
      this.dataSource.data.push(node);
    } else {
      this.dataSource.data[exitsNode].tags.push.apply(this.dataSource.data[exitsNode].tags, childs);
    }
  }

  /**
   * remove child node
   * @param childId 
   */
  removeChildNode(childId: number) {
    this.dataSource.data.forEach((type, idx) => {
      _.remove(type.tags, (tag) => tag.id === childId);

      // Remove parent node if no child
      if (!type.tags.length) {
        this.dataSource.data.splice(idx, 1);
      }
    });
  }

  /**
   * Remove multiple child node node by list ids
   * @param ids 
   */
  removeChilds(ids: number[]) {
    if (ids.length) {
      ids.forEach(i => {
        this.removeChildNode(i);
      });
    }
  }

  /**
   * refresh data of tree
   */
  refresh() {
    const _data = [...this.dataSource.data];
    this.dataSource.data = [];
    this.dataSource.data = _data;
  }

  ngOnDestroy() {

  }
}
