import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreeViewComponent } from './tree-view.component';
import { AppSharedModule } from '@common/shared';
import { MatTreeModule } from '@angular/material';

@NgModule({
  imports: [
    AppSharedModule,
    MatTreeModule
  ],
  declarations: [TreeViewComponent],
  exports: [
    TreeViewComponent
  ]
})
export class TreeViewModule { }
