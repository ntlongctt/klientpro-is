import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';
import { TreeViewComponent } from '../tree-view/tree-view.component';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';


@Component({
  selector: 'treeview-chooser',
  templateUrl: './treeview-chooser.component.html',
  styleUrls: ['./treeview-chooser.component.scss']
})
export class TreeviewChooserComponent implements OnInit {

  @ViewChild('selected') selected: TreeViewComponent;
  @ViewChild('available') available: TreeViewComponent;

  @Input() selectedTreeData: any[];
  @Input() availableTreeData: any[];

  @Output() onSelectedListChange = new EventEmitter();

  public selectedListValue: any[] = [];
  public availableListValue: any[] = [];

  @Input()
  selectedListTitle = this.translationLoaderService.instant('SELECTED_LIST_TITLE');
  @Input()
  availableListTitle = this.translationLoaderService.instant('AVAILABLE_LIST_TITLE');

  constructor(
    private translationLoaderService: FuseTranslationLoaderService
  ) {
  }

  ngOnInit() {
  }

  /**
   * Move tag to available list
   */
  moToAvailable() {

    // remove from source list
    this.selectedListValue.map(item => {
      const ids = item.tags.map(i => i.id);
      this.selected.removeChilds(ids);
    });

    // add to target list
    this.selectedListValue.map(item => {
      this.available.addNewNode(item, item.tags);
    });

    this.selected.refresh();
    this.available.refresh();
    this.selectedListValue = [];
    this.onSelectedListChange.emit(this.getSelectedValue());
  }

  /**
   * Return list current tag assigned
   */
  getSelectedValue() {
    const selectedTags = [...this.selected.dataSource.data].reduce((tags, type) => {
      return [...tags, ...type.tags.filter(tag => tag.id)];
    }, []);

    return selectedTags;
  }

  /**
   * Move tag to assigned list
   */
  moToSelected() {
    // remove from source list
    const items = _.cloneDeep(this.availableListValue);
    items.map(item => {
      const ids = item.tags.map(i => i.id);
      this.available.removeChilds(_.cloneDeep(ids));
    });

    // // add to target list
    this.availableListValue.map(item => {
      this.selected.addNewNode(item, item.tags);
    });

    this.availableListValue = [];
    this.available.refresh();
    this.selected.refresh();

    this.onSelectedListChange.emit(this.getSelectedValue());
  }

  /**
   * When user click on tag, check if tag exits on target list then do nothing
   * If tag doesn't exist, add tag to selected list
   */
  onSelectNode({ tag, tagType, isSelect }, applyforSelectedList) {
    let sourceList;
    let targetList;

    if (applyforSelectedList) {
      sourceList = this.selectedListValue;
      targetList = this.available.dataSource.data;
    } else {
      sourceList = this.availableListValue;
      targetList = this.selected.dataSource.data;
    }

    // check if value is exits on target list
    const temp = targetList.find(i => i.id === tagType.id);
    if (temp) {
      const exitsTag = temp.tags.find(i => i.id === tag.id);
      if (exitsTag) {
        return;
      }
    }

    const exitsType = sourceList.find(t => t.id === tagType.id);

    // oncheck
    if (isSelect) {
      if (exitsType) {
        exitsType.tags.push(tag);
      } else {
        const newType = {
          id: tagType.id,
          name: tagType.name,
          tags: [tag]
        };
        sourceList.push(newType);
      }
    } else {
      // on uncheck
      _.remove(exitsType.tags, t => t['id'] === tag.id);
      if (exitsType.tags.length === 0) {
        _.remove(sourceList, i => i['id'] === exitsType.id);
      }
    }
  }

}
