import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreeviewChooserComponent } from './treeview-chooser.component';
import { TreeViewModule } from '../tree-view/tree-view.module';
import { AppSharedModule } from '@common/shared';

@NgModule({
  imports: [
    AppSharedModule,
    TreeViewModule
  ],
  declarations: [TreeviewChooserComponent],
  exports: [TreeviewChooserComponent]
})
export class TreeviewChooserModule { }
