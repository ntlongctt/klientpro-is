import { Component, OnInit, Optional, Inject } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { locale as en } from '../../../i18n/common/en';
import { locale as cs } from '../../../i18n/common/cs';
import { AppStoreService } from '@store/store.service';

@Component({
  selector: 'app-unauthorized-modal',
  templateUrl: './unauthorized-modal.component.html',
  styleUrls: ['./unauthorized-modal.component.scss']
})
export class UnauthorizedModalComponent implements OnInit {

  showWarning = false;

  constructor(
    private _translationLoader: FuseTranslationLoaderService,
    public dialogRef: MatDialogRef<UnauthorizedModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private _appStoreService: AppStoreService
  ) { }

  ngOnInit() {
    this._translationLoader.loadTranslations(en, cs);
    this.showWarning = this._appStoreService.getState().form.hasChanged;
  }

  public close(status?: boolean): void {
    this.dialogRef.close(status);
  }

}
