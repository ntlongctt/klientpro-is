import { NgModule } from '@angular/core';
import { UnauthorizedModalComponent } from './unauthorized-modal.component';
import { UnauthorizedModalService } from '@common/modules/unauthorized-modal/unauthorized-modal.service';
import { AppSharedModule } from '@common/shared';

@NgModule({
  imports: [
    AppSharedModule
  ],
  declarations: [UnauthorizedModalComponent],
  entryComponents: [UnauthorizedModalComponent],
  providers: [UnauthorizedModalService]
})
export class UnauthorizedModalModule { }
