import {
  Injectable
} from '@angular/core';

import {
  MatDialog,
  MatDialogConfig
} from '@angular/material';
import { UnauthorizedModalComponent } from '@common/modules/unauthorized-modal/unauthorized-modal.component';

@Injectable()
export class UnauthorizedModalService {

  constructor(public dialog: MatDialog) {
  }

  public show(cb?: Function) {
    const dialogRef = this.dialog.open(UnauthorizedModalComponent, {
      autoFocus: false // disable focus button on modal
    } as MatDialogConfig);
    dialogRef.afterClosed().subscribe((result) => {
      if (cb) {
        cb(result);
      }
    });
  }
}
