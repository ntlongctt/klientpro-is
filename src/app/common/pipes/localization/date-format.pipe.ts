import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';

@Pipe({
  name: 'localizedDate',
  pure: true
})

export class LocalizedDatePipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
  ) { }

  /**
   * Return date format by current language
   */
  transform(value: any, ...args) {

    if (!value) {
      return;
    }

    return Observable.create((observer) => {
      this.translate.onLangChange.subscribe(() => {
        observer.next(moment(value).format('l').split('. ').join('.'));
      });
      observer.next(moment(value).format('l').split('. ').join('.'));
    });
  }
}
