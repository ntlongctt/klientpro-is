import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import * as momentTimeZone from 'moment-timezone';
import * as moment from 'moment';
import { AppConstant } from '@klient/app.constant';

@Pipe({
  name: 'localizedDateTime',
  pure: true
})

export class LocalizedDateTimePipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
  ) { }

  /**
   * Return date time format by current language
   */
  transform(value: any, ...args) {

    if (!value) {
      return;
    }
    // momentTimeZone(momentTimeZone(value).tz(AppConstant.defaultTimeZoneDisplay).format('L LTS'))
    return Observable.create((observer) => {
      this.translate.onLangChange.subscribe(() => {
        observer.next(moment(momentTimeZone(value).tz(AppConstant.defaultTimeZoneDisplay).format('L LTS')).format('l LTS').split('. ').join('.'));
      });
      observer.next(moment(momentTimeZone(value).tz(AppConstant.defaultTimeZoneDisplay).format('L LTS')).format('l LTS').split('. ').join('.'));
    });
  }
}
