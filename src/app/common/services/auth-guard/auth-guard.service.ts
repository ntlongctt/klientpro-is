import {
  Injectable
} from '@angular/core';

import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { AppStoreService } from '@store/store.service';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromAuthActions from '@store/auth/auth.actions';
import { SystemService } from '../../../../app/rest/services/pa/System.service';
import { of } from 'rxjs/internal/observable/of';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import * as fromUserAction from '@store/user/user.action';
import * as fromMenuActions from '@store/menu/menu.actions'; 
import * as fromErrorActions from '@store/error/error.actions'; 

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
              private appStoreService: AppStoreService,
              private store: Store<AppState>,
              private systemService: SystemService,
              ) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const appState = this.appStoreService.getState();

    if (!appState.auth.isLoggedIn) {
      this.store.dispatch(new fromAuthActions.SetReturnUrl(state.url));
      return this.systemService.getUserData()
      .pipe(
        map((data) => {
          this.store.dispatch(new fromMenuActions.GetMenu());
          this.store.dispatch(new fromAuthActions.SetIsLoggedIn(true));
          this.store.dispatch(new fromUserAction.GetUserInfoSucceeded(data));
          return true;
        }),
        catchError(() => {
          this.router.navigate(['sign-in']);
          return of(false);
        })
      );
    } else {
      return of(true);
    }
    
  }
}
