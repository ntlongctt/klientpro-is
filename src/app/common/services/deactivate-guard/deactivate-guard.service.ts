import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { AppStoreService } from '@store/store.service';
import { DeactiveWarningModalService } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.service';

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<any> {

  constructor(
    private deactiveWarningModalService: DeactiveWarningModalService,
    private appStoreService: AppStoreService
  ) {

  }

  /**
   * Before redirect to another page, check and show modal confirm if form data has change
   */
  canDeactivate() {
    // get form status from store
    const hasChanged = this.appStoreService.getState().form.hasChanged;
    if (!hasChanged) {
      return true;
    } else {
      const subject = new Subject<boolean>();
      this.deactiveWarningModalService.subject = subject;
      this.deactiveWarningModalService.show();
      return subject;
    }
  }
}
