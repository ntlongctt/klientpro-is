import * as _ from 'lodash';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, tap, timeout } from 'rxjs/operators';
import { Util } from '../util';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromErrorActions from '@store/error/error.actions';
import { AppConstant } from '../../../app.constant';

export interface IRequestOptions {
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}

export const REQUEST_TIME_OUT = 20000; 

@Injectable()
export class CustomHttpClient {
  private _defaultRequestOptions: IRequestOptions;
  constructor(private http: HttpClient,
              private _util: Util,
              private _store: Store<AppState>
  ) {
    this._defaultRequestOptions = {
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      }
    };
  }


  private mergeRequestOption(options = {}): IRequestOptions {
    return _.merge({...this._defaultRequestOptions, params: {}}, options);
  }

  private getApiDomain(): string {
    if (!AppConstant.domain) {
      return '';
    }

    const domain = AppConstant.domain
      .replace(/^\//g, '') // remove forward splash at beginning
      .replace(/\/$/g, ''); // remove backward splash at beginning

    if (/^http/.test(domain)) {
      return domain;
    }

    return `/${domain}`;
  }

  private generateDomain(route) {
    let route2 = route;
    if (!/^\//.test(route2)) {
      route2 = `/${route2}`;
    }
    return this.getApiDomain() + route2;
  }

  /**
   * GET request
   * @param {string} endPoint it doesn't need / in front of the end point
   * @param {IRequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Get<T>(endPoint: string, options?: IRequestOptions): Observable<T> {
    return this.intercept<T>(this.http.get<T>(this.generateDomain(endPoint), this.mergeRequestOption(options)));
  }

  /**
   * POST request
   * @param {string} endPoint end point of the api
   * @param {Any} payload body of the request.
   * @param {IRequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Post<T>(endPoint: string, payload: any = {}, options?: IRequestOptions): Observable<T> {
    console.log('POST PAYLOAD', payload);
    return this.intercept<T>(this.http.post<T>(this.generateDomain(endPoint), payload, this.mergeRequestOption(options)));
  }

  /**
   * PUT request
   * @param {string} endPoint end point of the api
   * @param {Any} payload body of the request.
   * @param {IRequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Put<T>(endPoint: string, payload: any = {}, options?: IRequestOptions): Observable<T> {
    return this.intercept<T>(this.http.put<T>(this.generateDomain(endPoint), payload, this.mergeRequestOption(options)));
  }

  /**
   * PATCH request
   * @param {string} endPoint end point of the api
   * @param {Any} payload body of the request.
   * @param {IRequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Patch<T>(endPoint: string, payload: any = {}, options?: IRequestOptions): Observable<T> {
    return this.intercept<T>(this.http.patch<T>(this.generateDomain(endPoint), payload, this.mergeRequestOption(options)));
  }

  /**
   * DELETE request
   * @param {string} endPoint end point of the api
   * @param {IRequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Delete<T>(endPoint: string, options?: IRequestOptions): Observable<T> {
    return this.intercept<T>(this.http.delete<T>(this.generateDomain(endPoint), this.mergeRequestOption(options)));
  }

  public intercept<T>(observable: Observable<T>): Observable<T> {
    return new Observable<T>(subscriber => {
      observable
      .pipe(
        timeout(REQUEST_TIME_OUT), // set timeout for request
      )
      .subscribe(
        (data) => {
          // subscribe
          subscriber.next(data);
        },
        (err) => {
          // error
          switch (err.status) {
            case 401:
              // check if error when call get userDate, then navigate to sign, so do not show error dialog
              if (err.url && !err.url.includes('/pa/userdata')) {
                this._store.dispatch(new fromErrorActions.UnauthorizedError());
              }
              break;
            case 403:
              this._store.dispatch(new fromErrorActions.ForbiddenError());
              break;
            case 500:
              this._store.dispatch(new fromErrorActions.ServerInternalError());
              break;
            case 400:
              this._store.dispatch(new fromErrorActions.KnownError(err.error));
              break;
            default:
              if (['TimeoutError', 'HttpErrorResponse'].includes(err.name) || !navigator.onLine) {
                this._store.dispatch(new fromErrorActions.ConnectionTimeoutError());
              } else if (err.message) {
                this._store.dispatch(new fromErrorActions.UnKnownError(err.message));
              }
          }
          subscriber.error(err);
        },
        () => {
          // complete
          subscriber.complete();
        }
      );
    });
  }

  public refreshToken() {
    const userToken = this._util.getToken();
    const refreshToken = userToken ? userToken.refreshToken : null;

    const httpParams = new HttpParams()
      .set('grant_type', 'refresh_token')
      .set('refresh_token', refreshToken);

    return this.http.get(this.generateDomain(`/api/auth`), { params: httpParams })
      .pipe(
        map((response: any) => response.data),
        tap((responseData: any) => {
          this._util.setToken(responseData.access_token, responseData.refresh_token);
        })
      );
  }
}
