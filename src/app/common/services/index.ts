import { AuthenticationService } from './auth';
import { AuthGuard } from './auth-guard';
import { Util } from './util';
import { CustomHttpClient } from './http';
import { Interceptors } from './interceptors';
// import { ErrorHandler } from '@angular/core';
// import { SentryErrorHandler } from '@services/sentry/SentryErrorHandler';

export const SHARED_SERVICES = [
  CustomHttpClient,
  AuthGuard,
  AuthenticationService,
  Util,
  // { provide: ErrorHandler, useClass: SentryErrorHandler },
  ...Interceptors
];
