import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {Util} from '../util';
import { AppStoreService } from '@store/store.service';
import { AppConstant } from '../../../../app/app.constant';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {
  constructor(private _util: Util,
              private storeService: AppStoreService) {

  }

  public intercept(req: HttpRequest<any>,
                   next: HttpHandler): Observable<HttpEvent<any>> {
    
    const authToken = this.storeService.getState().auth.authToken;
    const isLoginRequest = req.urlWithParams.includes('/login') || req.urlWithParams.includes('/logout');
    if (authToken && (AppConstant.requireToken || isLoginRequest)) {
      const authReq = req.clone({
        setHeaders: {
          'Auth-Token': authToken,
        }
      });
      return next.handle(authReq);
    }
    return next.handle(req);
  }
}
