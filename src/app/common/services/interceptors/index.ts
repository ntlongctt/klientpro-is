import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthTokenInterceptor } from '@services/interceptors/auth.interceptor';

export const Interceptors = [
  { useClass: AuthTokenInterceptor, provide: HTTP_INTERCEPTORS, multi: true },
];
