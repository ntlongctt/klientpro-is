import { ErrorHandler, Injectable } from '@angular/core';
import * as Raven from 'raven-js';
import { environment } from 'environments/environment';

if (environment.SENTRY_ENABLED) {
  Raven
    .config(environment.SENTRY_DSN, {
      // enabled: environment.production,
      environment: environment.env.toLowerCase(),
      level: 'error',
      stacktrace: true,
      // release: ''
    })
    .install();
}

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {
  }

  handleError(err: any): void {
    if (environment.SENTRY_ENABLED) {
      Raven.captureException(err);
      // alert('An error occurs while processing your action!');
    } else {
      console.error(err);
    }
  }
}
