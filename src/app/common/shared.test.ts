import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SHARED_SERVICES } from './services';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule, MatDialogModule, MatDividerModule, MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule, MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRippleModule, MatSelectModule,
  MatTabsModule, MatTooltipModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BaseComponentModule } from '@modules/base-component';
import { LocalStorageModule } from 'angular-2-local-storage';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterTestingModule } from '@angular/router/testing';

const ANGULAR_MATERIAL_COMPONENTS = [
  MatInputModule,
  MatCheckboxModule,
  MatRippleModule,
  MatButtonModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatSelectModule,
  MatTabsModule,
  MatListModule,
  MatDialogModule,
  MatAutocompleteModule,
  MatProgressBarModule,
  MatTooltipModule,
  MatFormFieldModule,
  MatDividerModule
];

const MODULES = [
  CommonModule,
  HttpClientModule,
  TranslateModule,
  FuseSharedModule,
  BaseComponentModule,
  FlexLayoutModule,
  FormsModule,
  ReactiveFormsModule,
  RouterTestingModule,
  LocalStorageModule,
  NgxDatatableModule,
  ...ANGULAR_MATERIAL_COMPONENTS
];


@NgModule({
  imports: MODULES,
  exports: MODULES,
  providers: [
    ...SHARED_SERVICES
  ]
})
export class AppSharedTestModule {
}
