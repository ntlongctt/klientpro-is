import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SHARED_SERVICES } from './services';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule, MatDialogModule, MatDividerModule, MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule, MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRippleModule, MatSelectModule,
  MatTabsModule, MatTooltipModule, MatPaginatorModule, MatExpansionModule, MatRadioModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BaseComponentModule } from '@modules/base-component';
import { RouterModule } from '@angular/router';
import { LocalStorageModule } from 'angular-2-local-storage';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CdkTableModule } from '@angular/cdk/table';
import { LoadingViewModule } from '@common/modules/loading-view';
import { AutofocusDirective } from '@common/directives/auto-focus.directive';
import { InactivePartnerDirective } from './directives/inactive-partner.directive';
import { LocalizedDateTimePipe } from './pipes/localization/date-time-format.pipe';
import { LocalizedDatePipe } from './pipes/localization/date-format.pipe';
import { ActiveByRoleDirective } from './directives/active-by-role.directive';
import { EditableAddressDirective } from './directives/editable-address.directive';
import { InactiveAddressDirective } from './directives/inactive-address.directive';
import { InactiveMcSystemDirective } from './directives/inactive-mc-system.directive';
import { NumberDirective } from './directives/numbers-only.directive';
import { DeactiveWarningModalModule } from './modules/deactivate-warning-modal/deactivate-warning-modal.module';

const ANGULAR_MATERIAL_COMPONENTS = [
  MatInputModule,
  MatCheckboxModule,
  MatRippleModule,
  MatButtonModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatSelectModule,
  MatTabsModule,
  MatListModule,
  MatDialogModule,
  MatAutocompleteModule,
  MatProgressBarModule,
  MatTooltipModule,
  MatFormFieldModule,
  MatDividerModule,
  MatPaginatorModule,
  MatExpansionModule,
  MatRadioModule
];

const MODULES = [
  CommonModule,
  HttpClientModule,
  TranslateModule,
  FuseSharedModule,
  BaseComponentModule,
  FlexLayoutModule,
  FormsModule,
  ReactiveFormsModule,
  RouterModule,
  LocalStorageModule,
  NgxDatatableModule,
  CdkTableModule,
  LoadingViewModule,
  DeactiveWarningModalModule,
  ...ANGULAR_MATERIAL_COMPONENTS
  
];

const DIRECTIVES = [
  AutofocusDirective,
  ActiveByRoleDirective,
  InactivePartnerDirective,
  EditableAddressDirective,
  InactiveAddressDirective,
  InactiveMcSystemDirective,
  NumberDirective
];

const PIPES = [
  LocalizedDatePipe,
  LocalizedDateTimePipe,
];


@NgModule({
  imports: MODULES,
  exports: [...MODULES, ...DIRECTIVES, ...PIPES],
  providers: [
    ...SHARED_SERVICES
  ],
  declarations: [...DIRECTIVES, ...PIPES]
})
export class AppSharedModule {
}
