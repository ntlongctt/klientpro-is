import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export class BankAccountNumberValidator extends Validators {
  static valid(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (!control.value) {
        return null;
      }
      const value = control.value;
      
      // digits and 0-1 dash between them
      if (value.match(/[\d]*[-]?[\d]*/g).length !== 2) {
        return { invalidFormat: true };
      }

      return null;
    };
  }
}
