import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export class EmailValidator extends Validators {
  /**
   * Validation email with format:
   * mysite@ourearth.com
   * my.ownsite@ourearth.org
   * mysite@you.me.net
   * @param isNotRequired boolean
   */
  static validEmail(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (!control.value) {
        return null;
      }
      const value = control.value;
      const emailRex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      const isValid = value.match(emailRex);
      return (isValid) ? null : { isInvalidEmail: true };
    };
  }
}
