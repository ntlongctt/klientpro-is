import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export class MatchValidator extends Validators {
  /**
   * Validate current control valus match with input control
   * @param ipt1 input name
   */
  static matchWith(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      const newPasswordCrl = control.get('newPassword');
      const newPasswordConfirmCrl = control.get('newPasswordConfirm');
      if (!newPasswordCrl || !newPasswordConfirmCrl || !newPasswordConfirmCrl.value || !newPasswordCrl.value) {
        return null;
      }
      return newPasswordCrl.value === newPasswordConfirmCrl.value ? null : { nomatch: true };
    };
  }
}
