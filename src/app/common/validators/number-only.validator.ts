import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export class NumberValidator extends Validators {
  static numberOnly(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (!control.value) {
        return null;
      }
      const value = control.value;
      const isValid = /^([+][0-9 ]|[0-9 ])*$/.test(value);
      return (isValid) ? null : { invalidNumber: true };
    };
  }
}
