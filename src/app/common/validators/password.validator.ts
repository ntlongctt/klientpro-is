import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export class PasswordValidator extends Validators {
  static valiPassword(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      const value: string = control.value;
      // check password must be at least 7 characters long
      if (!value || value.length <= 7) {
        return { invalidLengthRequire: true };
      }

      // Password must contain a combination of small and capital letters
      if ([value.toLowerCase(), value.toUpperCase()].includes(value)) {
        return { invalidUpperCaseAndLowerCaseRule: true };
      }

      // Password must contain at least one numeral or special (non-letter) character.
      if (
        !/\d/.test(value) &&
        !/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value)
      ) {
        return { invalidspecialCharacterRequire: true };
      }

      return null;
    };
  }
}
