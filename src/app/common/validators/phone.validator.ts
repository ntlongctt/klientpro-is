import { AbstractControl, ValidatorFn, Validators} from '@angular/forms';
import * as _ from 'lodash';
import { parseNumber, isValidNumberForRegion } from 'libphonenumber-js';

export class PhoneNumberValidator extends Validators {
  /**
   * Validation phone number
   */
  static validPhoneNumber(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (!control.value) {
        return null;
      }
    
      const phoneNumber = parseNumber(control.value);
      if (_.isEmpty(phoneNumber)) {
        return { invalidPhoneNumber: true };
      }

      const isValidNumber = isValidNumberForRegion(phoneNumber.phone, phoneNumber.country);
      if (!isValidNumber) {
        return { invalidPhoneNumber: true };
      }

      return null;
      
      // return null;for (const iterator of object) {
        
      // }
    };
  }
}
