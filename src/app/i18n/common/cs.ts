export const locale = {
  lang: 'cs',
  data: {
    BUTTON: {
      TEST_KNOWN_EXCEPTION: 'Test námi ošetřená chyba backendu',
      TEST_401: 'Test 401 (nepřihlášen)',
      TEST_403: 'Test 403 (nedostatečná práva)',
      TEST_500: 'Test 500 (obecná chyba backendu)',
      TEST_INFO: 'Test INFO hlášky',
      TEST_WARN: 'Test WARN hlášky',
      TEST_ERROR: 'Test ERROR hlášky',
      TEST_ERRORS: 'Test více ERRORS',
      TEST_COMMUNICATION: 'Test komunikace',
      TEST_3X401: 'Test 3x401',
      OK: 'OK',
      NO: 'Ne',
      YES: 'Ano',
      CHANGE: 'Změnit',
      LOGIN: 'Přihlásit',
      SAVE: 'Uložit',
      DELETE: 'Smazat',
      RESET_PASSWORD: 'Resetovat heslo',
      GOTO_LOGIN_PAGE: 'Přejít na přihlášení',
      CANCEL: 'Zrušit',
      LOGIN_WITH_NEW_TAB: 'Přihlásit se na nové záložce prohlížeče, nechat tuto stránku otevřenou.',
      CHANGE_PASSWORD: 'Změnit heslo',
      LOG_OUT: 'Odhlásit',
      MANAGE_TAG: 'Spravovat nálepky',
      CLOSE: 'Zavřít',
	  APPLY: 'Použít',				 
	  SELECT: 'Vybrat',
	  EDIT: 'Editovat',
      NEW: 'Nový',
      SWITCH: 'Přepnout',
    },

    TOAST: {
      INFO: 'Testovací zpráva INFO',
      WARN: 'Testovací zpráva WARN'
    },

    MODAL: {
      TITLE: {
        ERROR_MESSAGE: 'Chyba',
        UNAUTHORIZED: 'Nepřihlášen',
        FORBIDDEN: 'Nedostatečná práva',
        SERVER_INTERNAL_ERROR: 'Interní chyba serveru',
        SESSION_EXPIRED: 'Přihlášení vypršelo',
        CONNECTION_TIMEOUT_ERROR: 'Nepodařilo se získat odpověď ze serveru',
        UNKNOWN_ERROR: 'Neznámá chyba',
        LEAVE_PAGE: 'Potvrďte opuštění stránky',
      },
      CONTENT: {
        UNAUTHORIZED: 'Pro další práci s aplikací se prosím přihlaste.',
        FORBIDDEN: 'Pro tuto akci nemáte dostatečná práva.',
        SERVER_INTERNAL_ERROR: 'Prosím, kontaktujte administrátora.',
        ERROR_MESSAGE: 'Test chybové zprávy',
        SESSION_EXPIRED: 'Vaše přihlášení vypršelo, přihlaste se prosím znovu.',
        CONNECTION_TIMEOUT_ERROR: 'Nepodařilo se získat odpověď ze serveru. Zkontrolujte své připojení k internetu a zkuste akci opakovat později.',
        LEAVE_PAGE: 'Chcete opustit tuto stránku a zahodit neuložené změny?',
      },
    },

    MESSAGE: {
      WARNING_LOST_DATA: 'Vámi provedené změny budou ztraceny.',
	  NOT_FOUND: 'Nenalezeno'
    },
    // Menu
    MENU_HOME: 'Hlavní stránka',
    MENU_TEST: 'Testy',
    MENU_REGISTERS: 'Registry',
    MENU_CLIENTS: 'Klienti',
    MENU_DEPARTMENTS: 'Zařízení',
    MENU_LOGOUT: 'Odhlásit',
    MENU_USERS: 'Správa uživatelů',
    MENU_BASIC_REGISTRY: 'Základní registry',
    MENU_MCSYSTEM: 'Systémy řízené péče',
    MENU_BUSINESSPARTNER: 'Obchodní partneři',
    MENU_DEPARTMENT: 'Zařízení',
    MENU_SYSTEM: 'Systém',
    MENU_PAUSER: 'Uživatelé portálu pro administraci',
    MENU_CMS: 'Správa článků',
    MENU_COMMUNICATION: 'Komunikace',
    MENU_DATABOX: 'Schránka zpráv',
    MENU_MEETING: 'Schůzky',
    MENU_ERRORS: 'Testování chyb',

    // PERSPECTIVE
    PERSPECTIVE_BACKOFFICE: 'Kancelář (back office)',
    PERSPECTIVE_CALL: 'Call centrum',
    PERSPECTIVE_CLIENT: 'Klientské pracoviště',
    // user state
    USER_STATE_ACTIVE: 'Aktivní',
    USER_STATE_BLOCKED: 'Blokovaný',

     // Field chooser
    SELECTED_LIST_TITLE: 'Vybrané položky:',
    AVAILABLE_LIST_TITLE: 'Dostupné položky:',

     // Table paging
     PAGINATOR: {
      ITEMS_PER_PAGE: 'Položek na stránku',
      NEXT_PAGE: 'Další stránka',
      PREVIOUS_PAGE: 'Předchozí stránka',
      OF_LABEL: 'z'
    },

    TAG: {
      UNSPECIFIED_TAG_TYPE: 'Neuvedený typ nálepky',
    },
    NO_NAME: 'Jméno neuvedeno',
	NONE_OPTION: '---',
    FORM_VALIDATOR: {
      DATE_RANGE_INVALID: 'Počáteční datum musí být před koncovým',
    },
    NO_RESULTS: 'Nebyly nalezeny žádné výsledky',
  }
};
