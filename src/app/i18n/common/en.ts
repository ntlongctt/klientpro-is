export const locale = {
  lang: 'en',
  data: {
    BUTTON: {
      TEST_KNOWN_EXCEPTION: 'Test Known Exception',
      TEST_401: 'Test 401',
      TEST_403: 'Test 403',
      TEST_500: 'Test 500',
      TEST_INFO: 'Test INFO',
      TEST_WARN: 'Test WARN',
      TEST_ERROR: 'Test ERROR',
      TEST_ERRORS: 'Test ERRORS',
      TEST_COMMUNICATION: 'Test Communication',
      TEST_3X401: 'Test 3x401',
      OK: 'OK',
      NO: 'NO',
      YES: 'YES',
      CHANGE: 'Change',
      LOGIN: 'LOGIN',
      SAVE: 'Save',
      DELETE: 'Delete',
      RESET_PASSWORD: 'Reset Password',
      GOTO_LOGIN_PAGE: 'Go to the log-in page',
      CANCEL: 'Cancel',
      LOGIN_WITH_NEW_TAB: 'Log-in in a new browser tab, leave this page open.',
      CHANGE_PASSWORD: 'Change password',
      LOG_OUT: 'Log out',
      MANAGE_TAG: 'Manage tags',
      CLOSE: 'Close',
      APPLY: 'Apply',
      SELECT: 'Select',
      EDIT: 'Edit',
      NEW: 'New',
      SWITCH: 'Switch',
    },

    TOAST: {
      INFO: 'Test info message',
      WARN: 'Test warning message'
    },

    MODAL: {
      TITLE: {
        ERROR_MESSAGE: 'Error message',
        UNAUTHORIZED: 'Unauthorized Error',
        FORBIDDEN: 'Forbidden Error',
        SERVER_INTERNAL_ERROR: 'Server Internal Error',
        SESSION_EXPIRED: 'Your session has expired',
        CONNECTION_TIMEOUT_ERROR: 'Connection timeout error',
        UNKNOWN_ERROR: 'Unknown Error',
        LEAVE_PAGE: 'Leave page confirmation',
      },
      CONTENT: {
        UNAUTHORIZED: 'Please re-login to continue using app.',
        FORBIDDEN: 'You dont have permission on this server.',
        SERVER_INTERNAL_ERROR: 'Please contact administrator for more details.',
        ERROR_MESSAGE: 'Test Error message',
        SESSION_EXPIRED: 'You session has expired, please log-in again.',
        CONNECTION_TIMEOUT_ERROR: 'There was an error while connecting to the server. Please, check your connection and try again later.',
        LEAVE_PAGE: 'Are you sure to cancel all changes?',
      },
    },

    MESSAGE: {
      WARNING_LOST_DATA: 'Changes on current page will be lost.',
      NOT_FOUND: 'Not found'
    },
    // Menu
    MENU_HOME: 'Home',
    MENU_TEST: 'Test',
    MENU_REGISTERS: 'Register',
    MENU_CLIENTS: 'Clients',
    MENU_DEPARTMENTS: 'Departments',
    MENU_LOGOUT: 'Logout',
    MENU_USERS: 'Users management',
    MENU_BASIC_REGISTRY: 'Basic registry',
    MENU_MCSYSTEM: 'MC system',
    MENU_BUSINESSPARTNER: 'Business partner',
    MENU_DEPARTMENT: 'Department',
    MENU_SYSTEM: 'System',
    MENU_PAUSER: 'PA user',
    MENU_CMS: 'Article administration',
    MENU_COMMUNICATION: 'Communication',
    MENU_DATABOX: 'Message box',
    MENU_MEETING: 'Meetings',
    MENU_ERRORS: 'Test Errors',

    // PERSPECTIVE
    PERSPECTIVE_BACKOFFICE: 'Back office',
    PERSPECTIVE_CALL: 'Call',
    PERSPECTIVE_CLIENT: 'Client',
    // user state
    USER_STATE_ACTIVE: 'Active',
    USER_STATE_BLOCKED: 'Blocked',

    // Field chooser
    SELECTED_LIST_TITLE: 'Selected list:',
    AVAILABLE_LIST_TITLE: 'Available list:',

    // Table paging
    PAGINATOR: {
      ITEMS_PER_PAGE: 'Items per page',
      NEXT_PAGE: 'Next',
      PREVIOUS_PAGE: 'Previous',
      OF_LABEL: 'of'
    },

    TAG: {
      UNSPECIFIED_TAG_TYPE: 'Unspecified tag type'
    },
    NO_NAME: 'No name',
    NONE_OPTION: 'None',
    FORM_VALIDATOR: {
      DATE_RANGE_INVALID: 'Start date must be before end date',
    },
    NO_RESULTS: 'There were no results',
  }
};
