import { Component, OnInit, OnDestroy } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as czech } from './i18n/cs';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { PasswordValidator } from '@common/validators/password.validator';
import { MatchValidator } from '@common/validators/match.validator';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromUserActions from '@store/user/user.action';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil } from 'rxjs/operators';
import { Util } from '@common/services/util';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  
  public _unsubscribe = new Subject();
  public submited = false;
  public changePasswordPrompt = '';
  public passwordFrm: FormGroup;
  public newPasswordFrm: FormGroup;
  public validationMessages = {};
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _toast: ToastrService,
    private store: Store<AppState>,
    private actions$: Actions,
    public fb: FormBuilder,
    public util: Util,
    private router: Router
  ) {
    this._fuseTranslationLoaderService.loadTranslations(english, czech);
    
    this.actions$.pipe(
      ofType(fromUserActions.CHANGE_PASSWORD_SUCCEEDED),
      takeUntil(this._unsubscribe)
    ).subscribe(() => {
      this.submited = false;
      this.router.navigate(['dashboard']);
      this._toast.success(this._fuseTranslationLoaderService.instant('MESSAGE.CHANGE_PASSWORD_SUCCEEDED'));
    });

    this.actions$.pipe(
      ofType(fromUserActions.CHANGE_PASSWORD_FAILED),
      takeUntil(this._unsubscribe)
    ).subscribe((action: any) => {
      this.submited = false;
      this.passwordFrm.reset();
    });
  }

  ngOnInit() {
    this.newPasswordFrm = this.fb.group(
      {
        newPassword: ['', [Validators.required, PasswordValidator.valiPassword()]],
        newPasswordConfirm: ['', Validators.required],
      },
      { validator: MatchValidator.matchWith() }
    );

    this.passwordFrm = this.fb.group({
      oldPassword: ['', Validators.required],
      newPasswordFrm: this.newPasswordFrm
    });

  }

  ngOnDestroy() {
    this._unsubscribe.next();
    this._unsubscribe.complete();
  }

  
  submit() {
    const {valid, value} = this.passwordFrm;
    if (valid) {
      this.submited = true;
      this.store.dispatch(new fromUserActions.ChangePassword({oldPassword: value.oldPassword , newPassword: value.newPasswordFrm.newPassword}));
    }
  }
}
