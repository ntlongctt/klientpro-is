import { NgModule } from '@angular/core';
import { AppSharedModule } from '@common/shared';

import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { ChangePasswordComponent } from './change-password.component';
import { AuthService } from '../../rest/services/pa/Auth.service';

@NgModule({
  imports: [
    AppSharedModule,
    ChangePasswordRoutingModule
  ],
  declarations: [ChangePasswordComponent],
  exports: [ChangePasswordComponent],
})
export class ChangePasswordModule { }
