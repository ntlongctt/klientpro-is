export const locale = {
  lang: 'cs',
  data: {
      CHANGE_PASSWORD_HEADER: 'Změna Vašeho hesla',
      FORM_VALIDATOR: {
        OLD_PASSWORD_REQUIRE: 'Zadejte původní heslo',
        NEW_PASSWORD_REQUIRE: 'Zadejte nové heslo',
        NEW_PASSWORD_CONFIRM_REQUIRE: 'Zadejte nové heslo pro kontrolu znovu',
        INVALID_LENGTH: 'Nové heslo musí být dlouhé minimálně 8 znaků',
        UPPERCASE_LOWERCASE_REQUIRE: 'Nové heslo musí obsahovat kombinaci malých a velkých písmen',
        SPECIAL_CHARACTER_REQUIRE: 'Nové heslo musí obsahovat minimálně jeden znak jiný než písmena (např. číslo)',
        CONFIRM_NOT_MATCH: 'Nové heslo a jeho ověření se neshodují'
      },
      PLACEHOLDER: {
        OLD_PASSWORD: 'Původní heslo',
        NEW_PASSWORD: 'Nové heslo',
        NEW_PASSWORD_CONFIRM: 'Nové heslo (ověření)'
      },
      MESSAGE: {
        CHANGE_PASSWORD_SUCCEEDED: 'Vaše heslo bylo úspěšně změněno'
      }
  }
};
