export const locale = {
  lang: 'en',
  data: {
    CHANGE_PASSWORD_HEADER: 'Change your password',
    FORM_VALIDATOR: {
      OLD_PASSWORD_REQUIRE: 'Old password is require',
      NEW_PASSWORD_REQUIRE: 'New password is require',
      NEW_PASSWORD_CONFIRM_REQUIRE: 'New password confimation is require',
      INVALID_LENGTH: 'New password must be at least 8 characters long',
      UPPERCASE_LOWERCASE_REQUIRE: 'New password must contain a combination of small and capital letters',
      SPECIAL_CHARACTER_REQUIRE: 'New password must contain at least one numeral or special (non-letter) character',
      CONFIRM_NOT_MATCH: 'New password and confirmation must match'
    },
    PLACEHOLDER: {
      OLD_PASSWORD: 'Old password',
      NEW_PASSWORD: 'New password',
      NEW_PASSWORD_CONFIRM: 'New password confirmation'
    },
    MESSAGE: {
      CHANGE_PASSWORD_SUCCEEDED: 'Password changed successfully'
    }
}
};
