import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from './i18n/en';
import { locale as cs } from './i18n/cs';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { SystemService } from '../../../app/rest/services/pa/System.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  /**
   * Constructor
   *
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   */
  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _store: Store<AppState>,
    private systemService: SystemService,
  ) {
    this._fuseTranslationLoaderService.loadTranslations(en, cs);
    // this._store.dispatch(new fromUserActions.GetUserInfo());
  }

  ngOnInit() {
  }

}
