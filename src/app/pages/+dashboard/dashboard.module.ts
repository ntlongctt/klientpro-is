import { NgModule } from '@angular/core';
import { AppSharedModule } from '@common/shared';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  imports: [
    AppSharedModule,
    DashboardRoutingModule
  ],
  declarations: [DashboardComponent],
  exports: [DashboardComponent],
})
export class DashboardModule { }
