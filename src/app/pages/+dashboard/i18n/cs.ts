export const locale = {
  lang: 'cs',
  data: {
      DASHBOARD: {
		  TITLE: 'Vítejte na domovské stránce administračního portálu MEDIPARTNER!',
		  MESSAGE: 'Zde najdete novinky a přehled Vaší agendy.'
      }
  }
};
