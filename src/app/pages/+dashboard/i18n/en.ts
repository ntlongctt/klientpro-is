export const locale = {
    lang: 'en',
    data: {
        DASHBOARD: {
            TITLE: 'Welcome to the MEDIPARTNER administration portal dashboard!',
            MESSAGE: 'Here will be displayed the summary of your agenda and important news.'
        }
    }
};
