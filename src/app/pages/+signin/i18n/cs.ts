export const locale = {
  lang: 'cs',
  data: {
      TITLE: 'Přihlášení k portálu',
      WELCOME_MSG: 'Vítejte na portálu pro administraci!',
      REMEMBER_ME: 'Zapamatovat si mě',
      FORGOT_PASS: 'Zapomněl(a) jste heslo?',
      USER_NAME: 'Uživatelské jméno',
      PASSWORD: 'Heslo',
      MESSAGE: {
        WRONG_USERNAME_PASSWORD: 'Chybné jméno nebo uživatelské heslo',
        USERNAME_REQUIRED: 'Zadejte uživatelské jméno',
        PASSWORD_REQUIRED: 'Zadejte heslo'
    }
  }
};
