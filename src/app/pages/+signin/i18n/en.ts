export const locale = {
    lang: 'en',
    data: {
        TITLE: 'LOGIN TO YOUR ACCOUNT',
        WELCOME_MSG: 'Welcome to the KLIENT!',
        REMEMBER_ME: 'Remember Me',
        FORGOT_PASS: 'Forgot Password?',
        USER_NAME: 'User name',
        PASSWORD: 'Password',
        MESSAGE: {
            WRONG_USERNAME_PASSWORD: 'Username or password incorrect',
            USERNAME_REQUIRED: 'Username is required',
            PASSWORD_REQUIRED: 'Password is invalid'
        }
    }
};
