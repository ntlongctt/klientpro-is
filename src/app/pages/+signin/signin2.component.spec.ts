import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Signin2Component } from './signin2.component';
import { AppTestModule } from '../../app.module.test';

describe('SigninComponent', () => {
  let component: Signin2Component;
  let fixture: ComponentFixture<Signin2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestModule
      ],
      declarations: [Signin2Component]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    // create component and test fixture
    fixture = TestBed.createComponent(Signin2Component);

    // get test component from the fixture
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
