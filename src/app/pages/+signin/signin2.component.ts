import { Component, OnInit, OnDestroy } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as czech } from './i18n/cs';
import { FuseConfigService } from '@fuse/services/config.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/internal/Subject';
import { Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromLoginActions from '@store/login/login.actions';
import { Observable } from 'rxjs/Observable';
import { BaseComponent } from '@modules/base-component';
import { ActivatedRoute } from '@angular/router';
import { AppStoreService } from '@store/store.service';
import { Actions, ofType } from '@ngrx/effects';

@Component({
  selector: 'app-signin2',
  templateUrl: './signin2.component.html',
  styleUrls: ['./signin2.component.scss'],
  animations: fuseAnimations
})
export class Signin2Component extends BaseComponent {

  errorMessage = '';
  login$: Observable<any>;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   * @param {FuseTranslationLoaderService} translationLoader
   * @param {Store<AppState>} store
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private translationLoader: FuseTranslationLoaderService,
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private appStoreService: AppStoreService,
    private actions$: Actions
  ) {
    super();

    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        }
      }
    };

    this.translationLoader.loadTranslations(english, czech);

    /**
     * Form Configuration
     */

    this.controlConfig = {
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', Validators.required)
    };

    this.formErrors = this.buildFormErrorsFromConfigs(this.controlConfig);

    this.validationMessages = {
      username: {
        required: this.translationLoader.instant('MESSAGE.USERNAME_REQUIRED'),
      },
      password: {
        required: this.translationLoader.instant('MESSAGE.PASSWORD_REQUIRED'),
      }
    };

    /**
     * End Form Configuration
     */

    this.login$ = this.store.select(state => state.login);

    this.actions$.pipe(
      ofType(fromLoginActions.LOGIN_FAILED)
    ).subscribe((action: any) => {
      this.errorMessage = action.payload ? action.payload  : this.translationLoader.instant('MESSAGE.WRONG_USERNAME_PASSWORD');
    });
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  public login() {
    const { valid, value } = this.frm;
    if (valid) {
      this.store.dispatch(new fromLoginActions.Login({
        password: value.password,
        username: value.username,
      }));
    }
  }

}
