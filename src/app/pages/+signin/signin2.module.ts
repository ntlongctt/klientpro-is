import { NgModule } from '@angular/core';
import { Signin2RoutingModule } from './signin2-routing.module';
import { Signin2Component } from './signin2.component';
import { AppSharedModule } from '@common/shared';

@NgModule({
  imports: [
    AppSharedModule,
    Signin2RoutingModule,
  ],
  declarations: [
    Signin2Component
  ],
  exports: [Signin2Component],
})
export class SigninModule { }
