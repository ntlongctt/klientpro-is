// tslint:disble
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {Signup2Component} from './signup2.component';
import {Component, Directive} from '@angular/core';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import { AppTestModule } from '../../app.module.test';

describe('Signup2Component', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestModule
      ],
      declarations: [
        Signup2Component
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(Signup2Component);
    component = fixture.debugElement.componentInstance;
  });

  it('should create a component', async(() => {
    expect(component).toBeTruthy();
  }));


  it('should run #ngOnInit()', async(() => {
    // const result = component.ngOnInit();
  }));

});
