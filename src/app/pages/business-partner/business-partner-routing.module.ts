import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusinessPartnerComponent } from './business-partner.component';
import { PartnerDetailComponent } from './partner-detail/partner-detail.component';
import { getDepartmentsRouting } from '@pages/departments/departments.routing';
import { CanDeactivateGuard } from '@common/services/deactivate-guard';

const routes: Routes = [
  {
    path: '',
    component: BusinessPartnerComponent
  },
  {
    path: 'company/:companyId/create',
    component: PartnerDetailComponent,
    canDeactivate: [CanDeactivateGuard],
    data: {'title': 'BUSINESS_PARTNER.PARTNER_DETAIL_TITLE'},
  },
  {
    path: ':partnerId',
    data: {'title': 'BUSINESS_PARTNER.PARTNER_DETAIL_TITLE'},
    canDeactivate: [CanDeactivateGuard],
    children: [
      {
        path: '',
        component: PartnerDetailComponent,
      },
      ...getDepartmentsRouting()
    ]
  },
  // {
  //   path: 'companyId:companyId/edit',
  //   data: {'title': 'BUSINESS_PARTNER.PARTNER_DETAIL_TITLE'},
  //   component: PartnerDetailComponent,
  //   canDeactivate: [CanDeactivateGuard],
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [CanDeactivateGuard],
  exports: [RouterModule]
})
export class BusinessPartnerRoutingModule { }
