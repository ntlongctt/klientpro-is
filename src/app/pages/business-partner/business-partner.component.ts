import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogConfig, MatPaginator } from '@angular/material';
import { FilterPartnerComponent } from './filter-partner/filter-partner.component';
import { BusinessPartnerDataSource } from './business-partner.datasource';
import { PAGING_DEFAULT } from '@common/constants/paging-default';
import { Subject } from 'rxjs/Subject';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { tap, map, takeUntil } from 'rxjs/internal/operators';
import { locale as en } from './i18n/en';
import { locale as cs } from './i18n/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import * as fromBusinessPartnerActions from '@store/business-patner/business-patner.actions';
import { Actions, ofType } from '@ngrx/effects';
import { AppState } from '@store/store.reducers';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { BusinessPartnerFilterTO } from '@klient/rest/models/pa/businesspartner/BusinessPartnerFilterTO';
import { BusinessPartnerService } from '@klient/rest/services/pa/BusinessPartner.service';
import { NewPartnerComponent } from './new-partner/new-partner.component';
import { RouterService } from '@common/modules/route-caching';
import { ActivatedRoute } from '@angular/router';
import { CODE_LIST } from '@common/constants/code-list';
import * as departmentActions from '@store/department/department.actions';

@Component({
  selector: 'app-business-partner',
  templateUrl: './business-partner.component.html',
  styleUrls: ['./business-partner.component.scss']
})
export class BusinessPartnerComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('tablePaginator') paginator: MatPaginator;
  @ViewChild(FusePerfectScrollbarDirective) directiveScroll: FusePerfectScrollbarDirective;
  
  public isOpenFilter = false;
  public dialogRef: any;
  public newPartnerDialogRef: any;
  public dataSource: BusinessPartnerDataSource;
  public initFilter: BusinessPartnerFilterTO = {
    activeOnly: false,
    departmentTags: [],
    departmentTypeId: null,
    mcSystemId: null,
    searchQuery: ''
  };
  public displayedColumns = ['name', 'departmentName', 'validFrom', 'validTill'];
  public searchText = '';
  public deparmenttype;
  public mcSystems;
  public availableTags;
  private _unsubscribeAll = new Subject();
  public isFilterActived = false;
  
  constructor(
    private dialog: MatDialog,
    private businessPartnerService: BusinessPartnerService,
    private translationLoaderService: FuseTranslationLoaderService,
    private actions$: Actions,
    private store: Store<AppState>,
    private _routerService: RouterService,
    private activatedRoute: ActivatedRoute
  ) {
    this.translationLoaderService.loadTranslations(en, cs);

    this.actions$.pipe(
      takeUntil(this._unsubscribeAll),
      ofType(fromBusinessPartnerActions.UPDATE_FILTER),
      map((action: any) => action.payload)
    ).subscribe(filter => {
      this.initFilter = {
        activeOnly: filter.activeOnly,
        departmentTags: filter.departmentTags,
        departmentTypeId: filter.departmentTypeId,
        mcSystemId: filter.mcSystemId,
        searchQuery: this.searchText
      };

      if (filter.activeOnly === false 
        && filter.departmentTags.length === 0 
        && filter.departmentTypeId === ''
        && filter.mcSystemId === '') {
          this.isFilterActived = false;
      } else {
        this.isFilterActived = true;
      }

      this.reloadData();
    });

    this.actions$.pipe(
      takeUntil(this._unsubscribeAll),
      ofType(
        fromBusinessPartnerActions.ADD_PARTNER_SUCCEEDED,
        fromBusinessPartnerActions.UPDATE_PARTNER_SUCCEEDED,
        fromBusinessPartnerActions.DELETE_PARTNER_SUCCEEDED,
        departmentActions.ADD_DEPARTMENT_SUCCEEDED,
        departmentActions.UPDATE_DEPARTMENT_INFO_SUCCEEDED,
        departmentActions.DELETE_DEPARTMENT_SUCCEEDED,
      )
    ).subscribe(() => {
      this.reloadData();
    });

   }

  ngOnInit() {
    this.dataSource = new BusinessPartnerDataSource(this.businessPartnerService);
    this.dataSource.loadBusinessPartner(this.initFilter, PAGING_DEFAULT.START, PAGING_DEFAULT.LIMIT);
    this.loadFilterData();
  }

  loadFilterData() {
    this.store.dispatch(new fromBusinessPartnerActions.GetFilterData({
      codelistName: CODE_LIST.DEPARTMENT_TYPE,
      activeOnly: false,
      mcSystemTypeId: 1,
      entityName: 'Department'
    }));
  }

  ngAfterViewInit(): void {
    if (this.paginator) {
      this.paginator.page
        .pipe(
          tap(() => this.loadBusinessPartnerPage())
        )
        .subscribe();
    }
  }

  loadBusinessPartnerPage() {
    this.dataSource.loadBusinessPartner(
      this.initFilter,
      this.paginator.pageIndex * this.paginator.pageSize,
      this.paginator.pageSize
    );
    // Make perfect scrollbar scroll to top list also update content scroll height after change page, page size, ...
    setTimeout(() => {
      this.directiveScroll.scrollToTop();
      this.directiveScroll.update();
    });
  }

  showFilter() {
    this.isOpenFilter = !this.isOpenFilter;
    
    if (this.isOpenFilter) {
       this.dialogRef = this.dialog.open(FilterPartnerComponent, {
        // maxHeight: 600,
        width: '650px',
        // position: {top: '150px'},
        autoFocus: false, // disable focus button on modal
      } as MatDialogConfig);

      this.dialogRef.afterClosed().subscribe((result) => {
        this.isOpenFilter = false;
      });
    } else {
      this.dialogRef.close();
    }
  }

  reloadData() {
    this.dataSource.clear(); // clear current data
    this.paginator.firstPage(); // reset paginator
    this.loadBusinessPartnerPage(); // load data
  }

  filterChange(keyword) {
    this.searchText = keyword;
    this.initFilter.searchQuery = keyword;
    this.reloadData();
  }

  addNew() {
    this.newPartnerDialogRef = this.dialog.open(NewPartnerComponent, {
      autoFocus: false // disable focus button on modal
    } as MatDialogConfig);
    
    this.newPartnerDialogRef.afterClosed().subscribe((result) => {
      if (result && result.status) {
        this._routerService.navigate(['business-partners', 'company', result.value.companyId, 'create'], {}, {
          enableBackView: true,
          scrollElement: this.directiveScroll
        });
      }
    });
  }

  goToCompanyDetail(row) {
    if (row.id) {
      this._routerService.navigate(['business-partners', row.id], {}, {
        enableBackView: true,
        scrollElement: this.directiveScroll
      });
    }
  }

  goToDepartmentDetail(row) {
    if (row.id) {
      this._routerService.navigate(['business-partners', row.id, 'department', row.departmentId, 'edit']);
    }

  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
