import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import { catchError, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/internal/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PAGING_DEFAULT } from '@common/constants/paging-default';
import { BusinessPartnerListDTO } from '@klient/rest/models/pa/businesspartner/BusinessPartnerListDTO';
import { BusinessPartnerService } from '@klient/rest/services/pa/BusinessPartner.service';
import { BusinessPartnerFilterTO } from '@klient/rest/models/pa/businesspartner/BusinessPartnerFilterTO';

export class BusinessPartnerDataSource implements DataSource<BusinessPartnerListDTO> {

  private dataSubject = new BehaviorSubject<BusinessPartnerListDTO[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public totalCount = 0;

  constructor(private businessPartnerService: BusinessPartnerService) {}

  connect(collectionViewer: CollectionViewer): Observable<BusinessPartnerListDTO[]> {
      return this.dataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
      this.dataSubject.complete();
      this.loadingSubject.complete();
  }

  loadBusinessPartner(filter: BusinessPartnerFilterTO,  start = PAGING_DEFAULT.START, limit = PAGING_DEFAULT.LIMIT) {

      this.loadingSubject.next(true);
      this.businessPartnerService.findBusinessPartners({
        filter: filter,
        limit: limit,
        start: start
      }).pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
      )
      .subscribe((bussinessPartner: any) => {
        this.totalCount = bussinessPartner.totalCount;
        this.dataSubject.next(bussinessPartner.partners);
      });
  }

  clear() {
    this.dataSubject.next([]);
  }
}
