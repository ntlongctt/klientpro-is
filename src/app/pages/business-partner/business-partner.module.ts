import { NgModule } from '@angular/core';

import { BusinessPartnerRoutingModule } from './business-partner-routing.module';
import { BusinessPartnerComponent } from './business-partner.component';
import { FilterPartnerComponent } from './filter-partner/filter-partner.component';
import { AppSharedModule } from '@common/shared';
import { InputSearchModule } from '@common/modules/input-search';
import {
  MatTableModule,
  MatPaginatorIntl,
} from '@angular/material';
import { MatSelectSearchModule } from '@common/modules/mat-select-search';
import { ConfirmModalModule } from '@common/modules/confirm-modal';
import { FieldChooserModule } from '@common/modules/field-chooser/field-chooser.module';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { TreeviewChooserModule } from '@common/modules/treeview-chooser/treeview-chooser.module';
import { CustomMatPaginatorIntl } from '@common/services/custom-paginator/CustomMatPaginatorIntl';
import { NewPartnerComponent } from './new-partner/new-partner.component';
import { PartnerDetailComponent } from './partner-detail/partner-detail.component';
import { BasicInformationComponent } from './partner-detail/basic-information/basic-information.component';
import { PartnerAddressesComponent } from './partner-detail/partner-addresses/partner-addresses.component';
import { TreeViewModule } from '@common/modules/tree-view/tree-view.module';
import { AddressDetailComponent } from './partner-detail/address-detail/address-detail.component';
import { PartnerContactPersonComponent } from './partner-detail/partner-contact-person/partner-contact-person.component';
import { ContactPersonDetailComponent } from './partner-detail/contact-person-detail/contact-person-detail.component';
import { PartnerBankAccountsComponent } from './partner-detail/partner-bank-accounts/partner-bank-accounts.component';
import { BankAccountDetailComponent } from './partner-detail/bank-account-detail/bank-account-detail.component';
import { PhoneNumberInputModule } from '@common/modules/phone-number-input/phone-number-input.module';
import { ListAddressComponent } from './partner-detail/contact-person-detail/list-address/list-address.component';
import { SubordinateDepartmentsComponent } from './partner-detail/subordinate-departments/subordinate-departments.component';
import { RichTextEditorModule } from '@common/modules/rich-text-editor/rich-text-editor.module';
import { BpAddressesListModule } from '@common/modules/bp-addresses-list/bp-addresses-list.module';
import { BpAddressDetailFormModule } from '@common/modules/bp-address-detail-form/bp-address-detail-form.module';
import { DepartmentsModule } from '@pages/departments/departments.module';
import { BpContactPersonsModule } from '@common/modules/bp-contact-persons/bp-contact-persons.module';
import { BpContactPersonDetailFormModule } from '@common/modules/bp-contact-person-detail-form/bp-contact-person-detail-form.module';
import { BreadscumbModule } from '@common/modules/breadcrumbs/breadcrumbs.module';
import { DeactiveWarningModalModule } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.module';
import { DateRangePickerModule } from '@common/modules/date-range-picker/date-range-picker.module';
import { AddressFormatModule } from '@common/modules/address-format/address-format.module';

@NgModule({
  imports: [
    AppSharedModule,
    BusinessPartnerRoutingModule,
    InputSearchModule,
    MatTableModule,
    MatSelectSearchModule,
    ConfirmModalModule,
    FieldChooserModule,
    Ng2TelInputModule,
    DeactiveWarningModalModule,
    TreeviewChooserModule,
    BreadscumbModule,
    TreeViewModule,
    PhoneNumberInputModule,
    RichTextEditorModule,
    BpAddressesListModule,
    BpAddressDetailFormModule,
    DepartmentsModule,
    BpContactPersonsModule,
    BpContactPersonDetailFormModule,
    TreeViewModule,
    PhoneNumberInputModule,
    DateRangePickerModule,
    AddressFormatModule
  ],
  declarations: [
    BusinessPartnerComponent,
    FilterPartnerComponent,
    NewPartnerComponent,
    PartnerDetailComponent,
    BasicInformationComponent,
    PartnerAddressesComponent,
    AddressDetailComponent,
    PartnerBankAccountsComponent,
    ContactPersonDetailComponent,
    ListAddressComponent,
    PartnerContactPersonComponent,
    BankAccountDetailComponent,
    SubordinateDepartmentsComponent
  ],
  entryComponents: [
    FilterPartnerComponent,
    NewPartnerComponent,
    AddressDetailComponent,
    ContactPersonDetailComponent,
    ListAddressComponent,
    BankAccountDetailComponent
  ],
  providers: [
    { provide: MatPaginatorIntl, useClass: CustomMatPaginatorIntl },
  ]
})
export class BusinessPartnerModule {}
