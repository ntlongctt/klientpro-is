import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterPartnerComponent } from './filter-partner.component';

describe('FilterPartnerComponent', () => {
  let component: FilterPartnerComponent;
  let fixture: ComponentFixture<FilterPartnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterPartnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
