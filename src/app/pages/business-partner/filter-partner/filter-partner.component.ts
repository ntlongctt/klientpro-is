import { Component, OnInit, ViewEncapsulation, Optional, Inject, ViewChild, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PartnerFilter } from '@common/models/PartnerFilter';
import * as _ from 'lodash';
import { TreeviewChooserComponent } from '@common/modules/treeview-chooser/treeview-chooser.component';
import * as fromBusinessPartnerActions from '@store/business-patner/business-patner.actions';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { AppStoreService } from '@store/store.service';
import { Actions, ofType } from '@ngrx/effects';
import { TreeViewComponent } from '@common/modules/tree-view/tree-view.component';
import { P } from '@angular/core/src/render3';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

@Component({
  selector: 'filter-partner',
  templateUrl: './filter-partner.component.html',
  styleUrls: ['./filter-partner.component.scss'],
})


export class FilterPartnerComponent implements OnInit, AfterContentInit {

  @ViewChild('listTags') listTags: TreeViewComponent;
  @Output() onChangeFilter = new EventEmitter();

  departmentTypes = [];
  mcSystems = [];
  availableTree = [];
  selectedTree = [];
  selectedTag: any;
  msSystemId = null;
  displayActive = false;

  currentFilter = {
    activeOnly: false,
    departmentTypeId: null,
    msSystemId: null,
    availableTags: []
  };

  availableTags = [];
  _selectedValues = [];
  
  
  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: PartnerFilter,
    private store: Store<AppState>,
    private appStoreService: AppStoreService,
    public dialogRef: MatDialogRef<FilterPartnerComponent>,
    private actions$: Actions,
    private fuseTranslationLoaderService: FuseTranslationLoaderService
  ) { 
    this.actions$.pipe(
      ofType(fromBusinessPartnerActions.GET_FILTER_DATA_SUCCEEDED)
    ).subscribe((filterData: any) => {

    });
  }

  ngOnInit() {

    const {departments, mcSystems, tagsClone} = _.cloneDeep(this.appStoreService.getState().businessPartner.filterData);  
    this.departmentTypes = departments ;
    this.mcSystems = mcSystems;
    this.availableTags = tagsClone.tagTypes.filter(type => type.tags.length > 0);

    const currentFilter = this.appStoreService.getState().businessPartner.filter;
    const availableTags = [];

    if (currentFilter) {
      this._selectedValues = currentFilter.departmentTags;
      this.availableTree = currentFilter.availableTags;
      this.availableTags = currentFilter.availableTags;
      this.displayActive = currentFilter.activeOnly;
      this.currentFilter.departmentTypeId = currentFilter.departmentTypeId ? currentFilter.departmentTypeId : 0;
      this.currentFilter.msSystemId = currentFilter.mcSystemId ? currentFilter.mcSystemId : 0;
      
    } else {
      _.cloneDeep(tagsClone.tagTypes).forEach(type => {
        const lst = [];
        type.tags.forEach(tag => {
          lst.push(_.cloneDeep({...tag, isSelected: false}));
        });

        if (lst.length) {
          if (!type.name) {
            type.name = this.fuseTranslationLoaderService.instant('TAG.UNSPECIFIED_TAG_TYPE');
          }
          availableTags.push({...type, tags: lst});
        }
      });
      this.availableTree = availableTags;
      this.availableTags = availableTags;
      this.currentFilter.departmentTypeId = 0;
      this.currentFilter.msSystemId = 0;
    }

    
  }

  ngAfterContentInit() {
    setTimeout(() => {
      this.listTags.treeControl.expandAll();
    });
  }

  /**
   * change display active only 
   */
  onDisplayActiveChange(event) {
    this.changeFilter();
  }


  departmentTypesChange() {
    this.changeFilter();
  }

  msSystemChange(systemId) {
    this.currentFilter.msSystemId = systemId;
    this.changeFilter();
  }

  /**
   * 
   */
  changeFilter() {
    const filterObj = {
      activeOnly: this.displayActive,
      departmentTags: this._selectedValues,
      departmentTypeId: this.currentFilter.departmentTypeId ? this.currentFilter.departmentTypeId : '',
      mcSystemId: this.currentFilter.msSystemId ? this.currentFilter.msSystemId : '',
      searchQuery: '',
      availableTags: this.availableTags
    };
    this.store.dispatch(new fromBusinessPartnerActions.UpdateFilter(filterObj));
  }

  close() {
    this.dialogRef.close();
  }

  resetFilter() {
    this._selectedValues = [];
    this.availableTree = this.availableTags;
    this.currentFilter.availableTags = this.availableTags;
    this.currentFilter.departmentTypeId = 0,
    // this.currentFilter.activeOnly = false;
    this.displayActive = false;
    this.currentFilter.msSystemId = 0;

    const temp = _.cloneDeep(this.availableTags);
    temp.forEach(type => {
      type.tags.forEach(t => t.isSelected = false);
    });
    this.availableTags = temp;
    this.availableTree = temp;
    
    this.changeFilter();
    setTimeout(() => {
      
      this.listTags.treeControl.expandAll();
    }, 100);
  }

  onSelectNode({ tag, tagType, isSelect }) {
    if (isSelect) {
      this._selectedValues = [...this._selectedValues, tag.id];
    } else {
      this._selectedValues = this._selectedValues.filter(t => t !== tag.id);
    }

    // update selected tree value
    const temp = _.cloneDeep(this.availableTags);
    temp.forEach(type => {
      type.tags.forEach(t => {
        if (t.id === tag.id) {
          t.isSelected = isSelect;
        }
      });
    });
    this.availableTags = temp;

    // update filter
    this.changeFilter();
  }
}
