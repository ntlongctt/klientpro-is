import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { BaseComponent } from '@common/modules/base-component';
import { FormControl, Validators } from '@angular/forms';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';

@Component({
  selector: 'app-new-partner',
  templateUrl: './new-partner.component.html',
  styleUrls: ['./new-partner.component.scss']
})
export class NewPartnerComponent extends BaseComponent implements OnInit {



  constructor(
    public dialogRef: MatDialogRef<NewPartnerComponent>,
    private translateService: FuseTranslationLoaderService,
    private store: Store<AppState>
  ) {
    super();
    this.controlConfig = {
      companyId: new FormControl('', [Validators.maxLength(8), Validators.minLength(8), Validators.required, Validators.pattern(/^[0-9]*$/)]),
    };
   }

  ngOnInit() {
    super.ngOnInit();
  }

  close(status) {
    if (status) {
      const {valid, value} = this.frm;
      if (valid) {
        return this.dialogRef.close({status, value});
      }
    } else {
      this.dialogRef.close(status);
    }
    
  }

}
