import { Component, OnInit, Optional, Inject, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Subject } from 'rxjs/Subject';
import * as _ from 'lodash';
import { BpAddressDetailFormComponent } from '@common/modules/bp-address-detail-form/bp-address-detail-form.component';

@Component({
  selector: 'address-detail',
  templateUrl: './address-detail.component.html',
  styleUrls: ['./address-detail.component.scss']
})
export class AddressDetailComponent implements OnInit, OnDestroy {

  @ViewChild('addressDetail') addressDetailComp: BpAddressDetailFormComponent;

  unSubAll = new Subject();

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddressDetailComponent>,
    private store: Store<AppState>,
  ) { 
  }

  ngOnInit() {
    this.loadData();
    
    this.dialogRef.backdropClick().subscribe(() => {
      this.addressDetailComp.cancel();
    });
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  loadData() {
    if (this.data.bpAddressId) {
      this.store.dispatch(new partnerActions.GetPartnerAddressDetail(this.data));
    }
    // this.store.dispatch(new partnerActions.GetAddressTypes(CODE_LIST.BP_ADDRESS_TYPE));
  }

  onCancelChange() {
    this.dialogRef.close();
  }

  onActionSuccess() {
    this.dialogRef.close();
  }
}
