import { Component, OnInit, Optional, Inject, OnDestroy, AfterViewInit } from '@angular/core';
import { BaseComponent } from '@common/modules/base-component';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ContactService } from '@klient/rest/services/common/Contact.service';
import { Subject } from 'rxjs/Subject';
import { MatDialogRef, MAT_DIALOG_DATA, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { BankAccountDTO } from '@klient/rest/models/common/bankaccount/BankAccountDTO';
import { BpBankAccountDTO } from '@klient/rest/models/pa/businesspartner/detail/bankaccount/BpBankAccountDTO';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { takeUntil, filter, map, debounceTime } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { BankAccountNumberValidator } from '@common/validators/bank-account-number.validator';
import { INPUT_DEBOUNCE_TIME } from '@common/constants/input-debounce-time';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { AppConstant } from '@klient/app.constant';
import { variable } from '@angular/compiler/src/output/output_ast';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { DeactiveWarningModalService } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.service';
import { Util } from '@common/services/util';
import * as _ from 'lodash';

@Component({
  selector: 'bank-account-detail',
  templateUrl: './bank-account-detail.component.html',
  styleUrls: ['./bank-account-detail.component.scss']
})
export class BankAccountDetailComponent extends BaseComponent implements OnInit, OnDestroy {

  submitted = false;

  states = [];

  isLoading = false;

  defaultState = {
    code: AppConstant.defaultStateCode,
    name: AppConstant.defaultStateName
  };

  unSubAll = new Subject();
  dateError = false;
 
  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BankAccountDetailComponent>,
    private contactService: ContactService,
    private deactiveWarningModalService: DeactiveWarningModalService,
    private store: Store<AppState>,
    private actions$: Actions,
    private toast: ToastrService,
    private fuseTranslationLoaderService: FuseTranslationLoaderService,
    private confirmModalService: ConfirmModalService,
    private translateService: TranslateService,
    private adapter: DateAdapter<any>,
    private util: Util
  ) {
    super();

    this.states = _.uniqBy([this.defaultState, ...this.states], 'code');

    this.controlConfig =  {
      accountNumber: new FormControl('', [Validators.required, BankAccountNumberValidator.valid()]),
      bankCode: new FormControl('', [Validators.minLength(4), Validators.required, Validators.pattern(/[\d]+/)]),
      bankName: new FormControl(''),
      state: new FormControl('', [Validators.required]),
      bic: new FormControl(''),
      iban: new FormControl(''),
      note: new FormControl(''),
      version: new FormControl(''),
      dateRange: new FormControl({start: null, end: null}),
    };

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_BANK_ACCOUNT_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.frm.patchValue(detail.bankAccount);
      this.frm.patchValue(detail);
      this.fieldAddress(detail.bankAccount);
      this.frm.get('dateRange').patchValue({start: detail.validFrom, end: detail.validTill});
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        partnerActions.ADD_BANK_ACCOUNT_FAILED,
        partnerActions.UPDATE_BANK_ACCOUNT_FAILED,
        partnerActions.DELETE_BANK_ACCOUNT_FAILED,
      ),
      map((action: any) => action.payload)
    ).subscribe(() => {
      this.submitted = false;
    });

    // add, update, delete success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        partnerActions.ADD_BANK_ACCOUNT_SUCCEEDED,
        partnerActions.UPDATE_BANK_ACCOUNT_SUCCEEDED,
        partnerActions.DELETE_BANK_ACCOUNT_SUCCEEDED,
      )
    ).subscribe((action: any) => {
      switch (action.type) {
        case partnerActions.ADD_BANK_ACCOUNT_SUCCEEDED: 
          this.toast.success(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.ADD_BANK_ACCOUNT_SUCCESS'));
          break;
        case partnerActions.UPDATE_BANK_ACCOUNT_SUCCEEDED:
          this.toast.success(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.UPDATE_BANK_ACCOUNT_SUCCESS'));
          break;
        case partnerActions.DELETE_BANK_ACCOUNT_SUCCEEDED:
          this.toast.success(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_BANK_ACCOUNT_SUCCESS'));
          break;
      }
      this.dialogRef.close();
    });

    // update translate for date picker
    this.translateService.onLangChange.subscribe(e => {
      this.adapter.setLocale(e.lang);
    });
   }

  ngOnInit() {
    super.ngOnInit();

    this.loadData();

    this.frm.get('state').patchValue(this.defaultState.code);

    this.dialogRef.backdropClick().subscribe(() => {
      this.cancel();
    });

    this.translateService.onLangChange.subscribe(() => {
      this.defaultState.name = this.fuseTranslationLoaderService.instant('DEFAULT_STATE.DISPLAY_NAME');
    });
  }

  onSearchState(keywork) {
    if (keywork.length >= 3) {
      this.isLoading = true;
      this.contactService.findState({query: keywork.trim()}).subscribe(resp => {
        this.states = _.uniqBy([this.defaultState, ...resp.states], 'code');
        this.isLoading = false;
      });
    }
  }

  loadData() {
    if (this.data.accountId) {
      this.store.dispatch(new partnerActions.GetBankAccountDetail(this.data));
    }
  }

  displayStateFn(state) {
    if (state) {
       return state.name;
    }
  }

  cancel() {
    if (this.frm.dirty) {
      const subject = new Subject<boolean>();
      this.deactiveWarningModalService.subject = subject;

      subject.subscribe((result) => {
        if (result) {
          this.dialogRef.close();
        }
      });
      this.deactiveWarningModalService.show();
    } else {
      this.dialogRef.close();
    }
  }

  onSubmit() {
    const {valid, value} = this.frm;
    this.controlConfig.dateRange.markAsDirty();

    if (valid) {
      this.submitted = true;
      const objectPost = {
        bankAccount : {
         accountNumber: value.accountNumber,
         bankCode: value.bankCode,
         bankName: value.bankName,
         bankStateCode: value.state,
         bankStateName: this.states.find(s => s.code === value.state).name,
         bic: value.bic,
         iban: value.iban,
         note: value.note
        },
        validFrom: value.dateRange.start,
        validTill: value.dateRange.end
      };

      if (this.data.accountId) {
        const temp = {...objectPost, id: this.data.accountId, version: value.version};
        this.store.dispatch( new partnerActions.UpdateBankAccount({
          partnerId: this.data.partnerId,
          accountId: this.data.accountId,
          account: temp
        }));
      } else {
        this.store.dispatch( new partnerActions.AddBankAccount({
          partnerId: this.data.partnerId,
          account: objectPost
        }));
      }
    }
  }

  fieldAddress(address: any) {
    if (address.bankStateCode) {
      const state = {
        name: address.bankStateName,
        code: address.bankStateCode
      };
      this.states = _.uniqBy([this.defaultState, state], 'code');
      this.frm.get('state').patchValue(state.code);
    } else {
      this.frm.get('state').patchValue(this.defaultState.code);
    }

  }

  delete() {
    this.confirmModalService
      .show(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_BANK_ACCOUNT_CONFIRM_MSG'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new partnerActions.DeleteBankAccount(this.data));
        }
      }, 
      this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_BANK_ACCOUNT_CONFIRM_TITLE'));
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

}
