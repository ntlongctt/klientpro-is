import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '@common/modules/base-component';
import { FormControl, Validators } from '@angular/forms';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from '../../i18n/en';
import { locale as cs } from '../../i18n/cs';
import { DateAdapter, MatDialog, MatDialogConfig } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { CodeListService } from '@klient/rest/services/common/CodeList.service';
import { ContactService } from '@klient/rest/services/common/Contact.service';
import { debounceTime, takeUntil, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromBussinessActions from '@store/business-patner/business-patner.actions';
import { Actions, ofType } from '@ngrx/effects';
import { Subject } from 'rxjs/Subject';
import * as fromFormActions from '@store/form/form.actions';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { PartnerStatus } from '@common/constants/partner-status';
import { INPUT_DEBOUNCE_TIME } from '@common/constants/input-debounce-time';
import { AddressDetailComponent } from '../address-detail/address-detail.component';
import { AppConstant } from '@klient/app.constant';
import { Util } from '@common/services/util';

@Component({
  selector: 'partner-basic-information',
  templateUrl: './basic-information.component.html',
  styleUrls: ['./basic-information.component.scss']
})
export class BasicInformationComponent extends BaseComponent implements OnInit, OnDestroy {

  @Input() basicInfor: any; 
  @Input() partnerID: any; 
  @Output() onEditAddress = new EventEmitter();

  isLoading = false;

  paymentMethods = [];

  states = [];

  cities = [];

  streets = [];

  companyId: string;

  unSubscribeAll = new Subject;

  partnerStatus = '';

  initialForm: any;

  currentFormStatus = false;

  submitted = false;

  dateError = false;

  defaultState = {
    code: AppConstant.defaultStateCode,
    name: AppConstant.defaultStateName
  };

  dialogRef: any;
  partnerDetail;

  address;

  constructor(
    private translationLoaderService: FuseTranslationLoaderService,
    private translateService: TranslateService,
    private adapter: DateAdapter<any>,
    private codeListService: CodeListService,
    private contactService: ContactService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private actions$: Actions,
    private toast: ToastrService,
    private dialog: MatDialog,
    private util: Util
  ) {
    super();

    this.store.dispatch(new fromFormActions.ChangeFormStatus(false));

    this.translationLoaderService.loadTranslations(en, cs);

    this.controlConfig = {
      name: new FormControl('', [Validators.required]),
      nameShortcut: new FormControl('', []),
      companyId: new FormControl('', [Validators.maxLength(8), Validators.minLength(8), Validators.required, Validators.pattern(/^[0-9]*$/)]),
      vatin: new FormControl(''),
      paymentMethodId: new FormControl(''),
      state: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      street: new FormControl(''),
      zipCode: new FormControl('', [Validators.required, Validators.pattern(/\s*(?:[\d]\s*){5}/)]),
      streetNumber: new FormControl('', [Validators.required]),
      executive: new FormControl(''),
      registrationRecord: new FormControl(''),
      dateRange: new FormControl({start: null, end: null}),
      id: new FormControl(''),
      version: new FormControl(''),
    };

    // update translate for date picker
    this.translateService.onLangChange.subscribe(e => {
      this.adapter.setLocale(e.lang);
    });

    // find bussiness result
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromBussinessActions.FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((resp: any) => {
      this.partnerStatus = resp.searchCode;
      this.frm.patchValue(resp.data);
      this.frm.get('dateRange').patchValue({start: resp.data.partnerFrom, end: resp.data.partnerTill});
      this.fieldAddress(resp.data.headquartersAddress.address);
      this.frm.get('companyId').patchValue(resp.data.companyId);
      this.initialForm = this.frm.value;
      this.partnerDetail = resp.data;
      this.address = resp.data.headquartersAddress.address;
      this.companyId = resp.data.companyId;
    });

    // create partner success
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromBussinessActions.ADD_PARTNER_SUCCEEDED)
    ).subscribe((resp: any) => {
      // this.initialForm = resp.payload;
      // this.partnerStatus = PartnerStatus.EXIST;
      // this.frm.patchValue(resp.payload);
      // this.frm.get('dateRange').patchValue({start: resp.payload.partnerFrom, end: resp.payload.partnerTill});
      // this.fieldAddress(resp.payload.headquartersAddress.address);
      // this.initialForm = this.frm.value;
      // this.partnerDetail = resp.payload;
      // this.address = resp.payload.headquartersAddress.address;
      this.store.dispatch(new fromBussinessActions.FindBusinessPartnerInPublicRegisterSuccess({
        searchCode: PartnerStatus.EXIST, data: resp.payload
      }));
      this.toast.success(this.translationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.CREATE_PARTNER_SUCCESS'));
    });

    // save partner success
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromBussinessActions.UPDATE_PARTNER_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      // this.partnerStatus = PartnerStatus.EXIST;
      // this.frm.patchValue(detail);
      // this.frm.get('dateRange').patchValue({start: detail.partnerFrom, end: detail.partnerTill});
      // this.fieldAddress(detail.headquartersAddress.address);
      // this.initialForm = this.frm.value;
      // this.partnerDetail = detail;
      // this.address = detail.headquartersAddress.address;
      this.store.dispatch(new fromBussinessActions.FindBusinessPartnerInPublicRegisterSuccess({
        searchCode: PartnerStatus.EXIST, data: detail
      }));
      
    });

    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromBussinessActions.GET_PARTER_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.partnerStatus = PartnerStatus.EXIST;
      this.frm.patchValue(detail);
      this.frm.get('dateRange').patchValue({start: detail.partnerFrom, end: detail.partnerTill});
      this.fieldAddress(detail.headquartersAddress.address);
      this.initialForm = this.frm.value;
      this.partnerDetail = detail;
      this.address = detail.headquartersAddress.address;
    });

    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromBussinessActions.UPDATE_PARTNER_ADDRESS_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(() => {
      this.loadData();
    });
   
   }

  ngOnInit() {
    super.ngOnInit();

    this.loadData();

    this.states = _.uniqBy([this.defaultState, ...this.states], 'code');

    this.frm.valueChanges.subscribe(() => {
      this.checkFormStatus();
    });
    
    this.frm.get('city').valueChanges
      .pipe(debounceTime(INPUT_DEBOUNCE_TIME))
      .subscribe((value) => {
        const selectedVal = this.controlConfig.city.value;

        // not search with small text length
        if (value.length < 3) {
          return;
        }

        // auto field zipcode when choose city
        if (selectedVal.zipCode) {
          this.frm.get('zipCode').patchValue(selectedVal.zipCode);
        }

        if (selectedVal.cityCode && value.cityCode === selectedVal.cityCode) {
          return;
        }

        this.isLoading = true;
        if (typeof value === 'string' || value instanceof String) {
          this.contactService.findCity({query: value.trim()}).subscribe(resp => {
            this.cities = resp.cities;
            this.isLoading = false;
          });
        }
        

      });

    this.frm.get('street').valueChanges
      .pipe(debounceTime(INPUT_DEBOUNCE_TIME))
      .subscribe((value) => {

        if (value.length < 3) {
          return;
        }

        const selectedVal = this.controlConfig.street.value;
        if (selectedVal.streetName && value.streetName === selectedVal.streetName) {
          return;
        }

        const cityCode = this.controlConfig.city.value.cityCode;
        this.isLoading = true;
        if (typeof value === 'string' || value instanceof String) {
          this.contactService.findCityStreet({cityCode: cityCode, query: value.trim()}).subscribe(resp => {
            this.streets = resp.streets;
            this.isLoading = false;
          });
        }
        
      }); 
    
  }

  loadData() {
    this.companyId = this.activatedRoute.snapshot.params['companyId'];

    if (this.companyId) {
      this.store.dispatch(new fromBussinessActions.FindBusinessPartnerInPublicRegister(this.companyId));
    }

    if (this.partnerID) {
      this.store.dispatch(new fromBussinessActions.GetPartnerDetail(this.partnerID));
    }
    
    this.codeListService.getCodeListItems({codelistName: 'PaymentMethod'}).subscribe(resp => {
      this.paymentMethods = resp.items;
    });
  }


  /**
   * Display city name on city input
   */
  displayCityFn(city) {
    if (city) { 
      return city.cityName; 
    }
  }

  displayStreetFn(street) {
    if (street) {
       return street.streetName;
    }
  }

  fieldAddress(address: any) {
    if (address.stateName) {
      const state = {
        name: address.stateName,
        code: address.stateCode
      };
      this.states = _.uniqBy([this.defaultState, state], 'code');
      this.frm.get('state').patchValue(state.code);
    } else {
      this.frm.get('state').patchValue(this.defaultState.code);
    }

    if (address.cityName) {
      const city = {
        cityName: address.cityName,
        cityCode: address.cityCode,
        zipCode: address.zipCode
      };
      this.frm.get('city').patchValue(city);
    }

    if (address.street) {
      this.frm.get('street').patchValue({streetName: address.street});
    }

    this.frm.get('zipCode').patchValue(address.zipCode);
    this.frm.get('streetNumber').patchValue(address.streetNumber);

    // disable address if partner is existed
    if (this.partnerStatus === PartnerStatus.EXIST) {
      this.frm.get('state').disable();
      this.frm.get('city').disable();
      this.frm.get('street').disable();
      this.frm.get('zipCode').disable();
      this.frm.get('streetNumber').disable();
    }
  }

  ngOnDestroy() {
    this.unSubscribeAll.next();
    this.unSubscribeAll.complete();
  }

  editAddress() {
    if (this.partnerID) {
      this.dialogRef = this.dialog.open(AddressDetailComponent, {
        maxHeight: 750,
        // maxWidth: 700,
        // position: {top: '150px'},
        autoFocus: false, // disable focus button on modal
        disableClose: true,
        data: {
          partnerId: this.partnerID,
          bpAddressId: this.partnerDetail.headquartersAddress.id
        },
      } as MatDialogConfig); 
    }
  }

  onSearchState(keywork) {
    if (keywork.length >= 3) {
      this.isLoading = true;
      this.contactService.findState({query: keywork.trim()}).subscribe(resp => {
        this.states = _.uniqBy([this.defaultState, ...resp.states], 'code');
        this.isLoading = false;
      });
    }
  }

  /**
   * check if user form or privileges has changes, dispath to store
   */
  checkFormStatus() {
    const isFormChange = !_.isEqual(this.initialForm, this.frm.value) && this.frm.dirty;
    if (isFormChange !== this.currentFormStatus) {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(isFormChange));
      this.currentFormStatus = isFormChange;
    }
  }

  validateBasicInfo() {
    for (const key in this.controlConfig) {
      if (this.controlConfig.hasOwnProperty(key) && this.controlConfig[key].hasError) {
       this.controlConfig[key].markAsTouched();
       this.controlConfig[key].markAsDirty();
      }
    }
  }

}
