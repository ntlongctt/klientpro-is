import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactPersonDetailComponent } from './contact-person-detail.component';

describe('AddressDetailComponent', () => {
  let component: ContactPersonDetailComponent;
  let fixture: ComponentFixture<ContactPersonDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactPersonDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPersonDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
