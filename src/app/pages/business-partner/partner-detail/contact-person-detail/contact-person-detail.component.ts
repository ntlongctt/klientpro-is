import { Component, OnInit, Optional, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { BpContactPersonDetailFormComponent } from '@common/modules/bp-contact-person-detail-form/bp-contact-person-detail-form.component';

@Component({
  selector: 'contact-person-detail',
  templateUrl: './contact-person-detail.component.html'
})
export class ContactPersonDetailComponent implements OnInit, OnDestroy {

  @ViewChild('contactDetail') contactDetail: BpContactPersonDetailFormComponent;

  addressTypes = [];
  submitted = false;
  
  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private store: Store<AppState>,
    public dialogRef: MatDialogRef<ContactPersonDetailComponent>,
  ) {
  }

  ngOnInit() {
    this.loadData();

    this.dialogRef.backdropClick().subscribe(() => {
      this.contactDetail.cancel();
    });
  }
  
  loadData() {
    if (this.data.personId) {
      this.store.dispatch(new partnerActions.GetPartnerContactPersonDetail(this.data));
    }
  }
  onCancelChange() {
    this.dialogRef.close();
  }

  onActionSuccess() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
  }
}
