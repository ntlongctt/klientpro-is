import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'list-address',
  templateUrl: './list-address.component.html',
  styleUrls: ['./list-address.component.scss']
})
export class ListAddressComponent implements OnInit {

  partnerID;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public partnerAddressesDialogRef: MatDialogRef<ListAddressComponent>,
  ) {
    this.partnerID = this.data.partnerId;
   }

  ngOnInit() {
  }

  onSelectRow(event) {
    this.partnerAddressesDialogRef.close(event);
  }

}
