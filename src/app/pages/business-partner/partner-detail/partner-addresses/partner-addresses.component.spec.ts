import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerAddressesComponent } from './partner-addresses.component';

describe('PartnerAddressesComponent', () => {
  let component: PartnerAddressesComponent;
  let fixture: ComponentFixture<PartnerAddressesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerAddressesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerAddressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
