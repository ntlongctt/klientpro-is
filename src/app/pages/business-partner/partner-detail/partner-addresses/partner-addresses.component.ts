import { Component, OnInit, Input, OnDestroy, Optional, Inject, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil, filter, map } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddressDetailComponent } from '../address-detail/address-detail.component';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'partner-addresses',
  templateUrl: './partner-addresses.component.html',
  styleUrls: ['./partner-addresses.component.scss']
})
export class PartnerAddressesComponent implements OnInit, OnDestroy {

  @Input() dialogMode = false;
  @Input() partnerID: string;
  @Output() onSelectRow = new EventEmitter();

  unSubAll = new Subject();
  dataSource = [];
  dialogRef: any;

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private dialog: MatDialog,
  ) {
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_ADDRESSES_SUCCEEDED),
      map((action: any) => action.payload.addresses)
    ).subscribe(addresses => {
      this.dataSource = addresses;
    });

    // create address success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        partnerActions.ADD_PARTNER_ADDRESS_SUCCEEDED,
        partnerActions.UPDATE_PARTNER_ADDRESS_SUCCEEDED,
        partnerActions.DELETE_PARTNER_ADDRESS_SUCCEEDED,
      ),
    ).subscribe(action => {
      this.reloadData();
    });
   }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    if (this.partnerID) {
      this.store.dispatch(new partnerActions.GetPartnerAddresses(this.partnerID));
    }
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  goToDetail(row) {
    this.dialogRef = this.dialog.open(AddressDetailComponent, {
      maxHeight: 750,
      // maxWidth: 700,
      // position: {top: '150px'},
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        partnerId: this.partnerID,
        bpAddressId: row.id
      },
    } as MatDialogConfig); 
  }

  onRowClick(row) {
    if (this.dialogMode) {
      this.onSelectRow.emit(row.id);
    } else {
      this.goToDetail(row);
    }
  }

  newAddress() {
    this.dialogRef = this.dialog.open(AddressDetailComponent, {
      maxHeight: 750,
      // maxWidth: 700,
      data: {
        partnerId: this.partnerID,
      },
      autoFocus: false, // disable focus button on modal
      disableClose: true,
    } as MatDialogConfig); 
  }

}
