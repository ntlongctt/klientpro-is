import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerBankAccountsComponent } from './partner-bank-accounts.component';

describe('PartnerBankAccountsComponent', () => {
  let component: PartnerBankAccountsComponent;
  let fixture: ComponentFixture<PartnerBankAccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerBankAccountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerBankAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
