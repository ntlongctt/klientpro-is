import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { takeUntil, filter, map } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { BankAccountDetailComponent } from '../bank-account-detail/bank-account-detail.component';

@Component({
  selector: 'partner-bank-accounts',
  templateUrl: './partner-bank-accounts.component.html',
  styleUrls: ['./partner-bank-accounts.component.scss']
})
export class PartnerBankAccountsComponent implements OnInit, OnDestroy {

  @Input() partnerID: string;

  unSubAll = new Subject();

  dataSource = [];

  displayedColumns = ['accountNumber', 'bankCode', 'validFrom', 'validTill'];

  dialogRef: any;

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private dialog: MatDialog,
  ) { 
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_BANK_ACCOUNTS_SUCCEEDED),
      map((action: any) => action.payload.bankAccounts)
    ).subscribe(bankAccounts => {
      this.dataSource = bankAccounts;
    });

     // create address success
     this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        partnerActions.ADD_BANK_ACCOUNT_SUCCEEDED,
        partnerActions.UPDATE_BANK_ACCOUNT_SUCCEEDED,
        partnerActions.DELETE_BANK_ACCOUNT_SUCCEEDED,
      )
    ).subscribe(() => {
      this.loadData();
    });
  }

  ngOnInit() {
  }

  loadData( ){
    this.store.dispatch(new partnerActions.GetBankAccounts(this.partnerID));
  }

  newBankAccount() {
    this.dialogRef = this.dialog.open(BankAccountDetailComponent, {
      maxHeight: 750,
      maxWidth: 700,
      data: {
        partnerId: this.partnerID,
      },
      // position: {top: '150px'},
      autoFocus: false, // disable focus button on modal
      disableClose: true,
    } as MatDialogConfig); 
  }

  goToDetail(row) {
    this.dialogRef = this.dialog.open(BankAccountDetailComponent, {
      maxHeight: 750,
      maxWidth: 700,
      // position: {top: '150px'},
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        partnerId: this.partnerID,
        accountId: row.id
      },
    } as MatDialogConfig); 
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

}
