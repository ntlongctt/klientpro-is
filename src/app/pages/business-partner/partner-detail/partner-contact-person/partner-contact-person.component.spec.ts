import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerContactPersonComponent } from './partner-contact-person.component';

describe('PartnerContactPersonComponent', () => {
  let component: PartnerContactPersonComponent;
  let fixture: ComponentFixture<PartnerContactPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerContactPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerContactPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
