import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil, filter, map } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { ContactPersonDetailComponent } from '../contact-person-detail/contact-person-detail.component';

@Component({
  selector: 'partner-contact-person',
  templateUrl: './partner-contact-person.component.html',
  styleUrls: ['./partner-contact-person.component.scss']
})
export class PartnerContactPersonComponent implements OnInit, OnDestroy {

  @Input() partnerID: string;

  unSubAll = new Subject();

  dataSource = [];

  dialogRef: any;

  isLoading$: any;
  public searchText = '';

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private dialog: MatDialog,
  ) {

    this.isLoading$ = this.store.select(s => s.businessPartner.isLoading);

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_CONTACT_PERSON_SUCCEEDED),
      map((action: any) => action.payload.contactPersons)
    ).subscribe(contactPersons => {
      this.dataSource = contactPersons;
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        partnerActions.CREATE_CONTACT_PERSON_SUCCEEDED,
        partnerActions.UPDATE_CONTACT_PERSON_SUCCEEDED,
        partnerActions.DELETE_CONTACT_PERSON_SUCCEEDED,
      ),
    ).subscribe(() => {
      this.reloadData();
    });

  }

  ngOnInit() {
  }

  reloadData() {
    this.store.dispatch(new partnerActions.GetPartnerContactPerson({ partnerId: this.partnerID, query: this.searchText }));
  }

  filterChange(keyword) {
    if (this.searchText === keyword) {
      return;
    }
    this.searchText = keyword;
    this.reloadData();
  }

  addNew() {
    this.dialogRef = this.dialog.open(ContactPersonDetailComponent, {
      // maxHeight: 600,
      autoFocus: false, // disable focus button on modal
      // disableClose: true,
      data: {
        partnerId: this.partnerID
      },
    } as MatDialogConfig);

    this.dialogRef.afterClosed().subscribe((result) => {
    });
  }
  
  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  onRowClick(row) {
    this.dialogRef = this.dialog.open(ContactPersonDetailComponent, {
      // maxHeight: 600,
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        partnerId: this.partnerID,
        personId: row.id
      },
    } as MatDialogConfig);

    this.dialogRef.afterClosed().subscribe((result) => {
    });
  }
}
