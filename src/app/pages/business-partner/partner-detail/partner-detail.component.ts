import { Component, OnInit, HostListener, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { AppStoreService } from '@store/store.service';
import { BasicInformationComponent } from './basic-information/basic-information.component';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, map } from 'rxjs/operators';
import { ofType, Actions } from '@ngrx/effects';
import { Subject } from 'rxjs/Subject';
import * as fromPartnerActions from '@store/business-patner/business-patner.actions';
import * as _ from 'lodash';
import * as fromFormActions from '@store/form/form.actions';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { PartnerAddressesComponent } from './partner-addresses/partner-addresses.component';
import { PartnerBankAccountsComponent } from './partner-bank-accounts/partner-bank-accounts.component';
import { PartnerContactPersonComponent } from './partner-contact-person/partner-contact-person.component';
import { SubordinateDepartmentsComponent } from './subordinate-departments/subordinate-departments.component';
import { Util } from '@common/services/util';
import { DeactiveWarningModalService } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.service';
import { MatTabGroup, MatTab, MatTabHeader } from '@angular/material';

@Component({
  selector: 'app-partner-detail',
  templateUrl: './partner-detail.component.html',
  styleUrls: ['./partner-detail.component.scss']
})
export class PartnerDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('tabs') tabs: MatTabGroup;
  @ViewChild('basicInfo') basicInfo: BasicInformationComponent;
  @ViewChild('patnerAddresses') patnerAddresses: PartnerAddressesComponent;
  @ViewChild('bankAccounts') bankAccounts: PartnerBankAccountsComponent;
  @ViewChild('partnerContact') partnerContact: PartnerContactPersonComponent;
  @ViewChild('subordinateDepartments') subordinateDepartments: SubordinateDepartmentsComponent;

  public submitted = false;
  public unSubscribeAll = new Subject;
  public partnerDetail: any;
  public partnerName = '';
  public isNewPartner = false;
  public partnerID = '';
  public selectedTab = 0;
  public isFormChange$;

  /**
 * Before tab/browser close or refresh, check if form data has changed,show alter confirm
 */
  @HostListener('window:beforeunload', ['$event'])
  beforeunload(event) {
    const hasChange = this.appStoreService.getState().form.hasChanged;
    if (hasChange) {
      return false;
    }
  }

  constructor(
    private fuseTranslationLoaderService: FuseTranslationLoaderService,
    private appStoreService: AppStoreService,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private actions$: Actions,
    private router: Router,
    private toast: ToastrService,
    private confirmModalService: ConfirmModalService,
    private util: Util,
    private deactiveWarningModalService: DeactiveWarningModalService
  ) {

    this.isFormChange$ = this.store.select(s => s.form.hasChanged);

    this.fuseTranslationLoaderService.loadTranslations(en, cs);

    this.partnerID = this.activatedRoute.snapshot.params['partnerId'];

    // find bussiness result
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromPartnerActions.FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((resp: any) => {
      this.partnerDetail = resp.data;
      this.isNewPartner = resp.searchCode !== 'EXIST' ? true : false;
      this.partnerName = this.partnerDetail.name;
      this.partnerID = this.partnerDetail.id;
    });

    // update partner success
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromPartnerActions.ADD_PARTNER_SUCCEEDED)
    ).subscribe((action: any) => {
      this.submitted = false;
      this.isNewPartner = false;
      this.partnerName = action.payload.name;
      this.partnerID = action.payload.id;
      this.partnerDetail = action.payload;
    });

    // update partner success
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromPartnerActions.UPDATE_PARTNER_SUCCEEDED),
    ).subscribe((action: any) => {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
      this.toast.success(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.UPDATE_PARTNER_SUCCESS'));
      this.submitted = false;
    });

    // delete partner success
    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromPartnerActions.DELETE_PARTNER_SUCCEEDED)
    ).subscribe((action: any) => {
      this.toast.success(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_PARTNER_SUCCESS'));
      this.router.navigate(['business-partners']);
    });

    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(fromPartnerActions.GET_PARTER_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.partnerDetail = detail;
      this.partnerName = detail.name ? detail.name : '';
    });

    this.actions$.pipe(
      takeUntil(this.unSubscribeAll),
      ofType(
        fromPartnerActions.UPDATE_PARTNER_FAILED,
        fromPartnerActions.ADD_PARTNER_FAILED,
        fromPartnerActions.DELETE_PARTNER_FAILED,
      ),
    ).subscribe(() => {
      this.submitted = false;
    });    
  }

  ngOnInit() {
    this.tabs._handleClick = this.interceptTabChange.bind(this);
    
  }

  ngAfterViewInit() {
    setTimeout(() => {
      // Set active tab is 'Select tags' when back from tags module
      const preURL = this.appStoreService.getState().router.state.previousUrl;
      console.log('%c PRE-ROUTE', 'color: orange', preURL);
      if (preURL && preURL.includes('/department')) {
        this.selectedTab = 4;
        // this.tabChanged(4);
      }
    });
  }

  interceptTabChange(tab: MatTab, tabHeader: MatTabHeader, idx: number) {
    const agrs = arguments;
    const subject = new Subject<boolean>();
    this.deactiveWarningModalService.subject = subject;

    if (this.selectedTab === 0 && idx !== 0 && this.basicInfo.currentFormStatus) {
      subject.subscribe((result) => {
        if (result) {
          return MatTabGroup.prototype._handleClick.apply(this.tabs, agrs);
        }
      });
      this.deactiveWarningModalService.show();
    } else {
      return MatTabGroup.prototype._handleClick.apply(this.tabs, agrs);
    }
  }


  tabChanged(event) {
    this.selectedTab = event.index;
    switch (event.index) {
      case 0:
        this.basicInfo.loadData();
        break;
      case 1: 
        this.patnerAddresses.reloadData();
        break;
      case 2:
        this.partnerContact.reloadData();
        break;
      case 3:
        this.bankAccounts.loadData();
        break;
      case 4:
        this.subordinateDepartments.loadData('');
        break;
    }
  }

  
  onSubmit() {
    const { valid, value } = this.basicInfo.frm;
    this.basicInfo.validateBasicInfo();
    if (!valid) {
      return;
    }

    this.submitted = true;
    
    let objectPost = _.cloneDeep(this.appStoreService.getState().businessPartner.findRegisterPartnerResult);

    objectPost = _.merge(objectPost.searchCode ? objectPost.data : objectPost, value);
    objectPost.headquartersAddress.address.stateName = this.basicInfo.states.find(s => s.code === this.basicInfo.controlConfig.state.value).name;
    objectPost.headquartersAddress.address.stateCode = this.basicInfo.controlConfig.state.value;
    objectPost.headquartersAddress.address.cityName = this.basicInfo.controlConfig.city.value.cityName;
    objectPost.headquartersAddress.address.cityCode = this.basicInfo.controlConfig.city.value.cityCode;

    const street = this.basicInfo.controlConfig.street.value;
    objectPost.headquartersAddress.address.street = street.streetName ? street.streetName : street;
    objectPost.headquartersAddress.address.streetNumber = this.basicInfo.controlConfig.streetNumber.value;
    objectPost.headquartersAddress.address.zipCode = this.basicInfo.controlConfig.zipCode.value.replace(/\s/g, '');

    objectPost['partnerFrom'] = value.dateRange.start;
    objectPost['partnerTill'] = value.dateRange.end;

    delete objectPost['state'];
    delete objectPost['city'];
    delete objectPost['street'];
    delete objectPost['streetNumber'];
    delete objectPost['zipCode'];
    delete objectPost['dateRange'];

    if (this.partnerDetail.id) {
      this.store.dispatch(new fromPartnerActions.UpdatePartner({ partnerId: this.partnerDetail.id, body: objectPost }));
    } else {
      delete objectPost['version'];
      delete objectPost['id'];
      
      delete objectPost['headquartersAddress']['id'];
      delete objectPost['headquartersAddress']['version'];
      
      delete objectPost['headquartersAddress']['address']['id'];
      delete objectPost['headquartersAddress']['address']['version'];

      this.store.dispatch(new fromPartnerActions.AddPartner(objectPost));
    }

  }

  delete() {
    this.confirmModalService
      .show(this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_PARTNER_MESSAGE'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new fromPartnerActions.DeletePartner(this.partnerDetail.id));
        }
      },
      this.fuseTranslationLoaderService.instant('BUSINESS_PARTNER.MESSAGE.DELETE_PARTNER_TITLE'));
  }

  cancel() {
    this.router.navigate(['business-partners']);
  }

  ngOnDestroy() {
    this.unSubscribeAll.next();
    this.unSubscribeAll.complete();
  }

  addNew() {
    switch (this.selectedTab) {
      case 1:
        this.patnerAddresses.newAddress();
        break;
      case 2:
        this.partnerContact.addNew();
        break;
      case 3:
        this.bankAccounts.newBankAccount();
        break;
      case 4:
        this.router.navigate(['business-partners', this.partnerID, 'department', 'create']);
        break;
    }
  }

  filterChange(event) {
    switch (this.selectedTab) {
      case 2:
        this.partnerContact.filterChange(event);
        break;
      case 4:
        this.subordinateDepartments.filterChange(event);
        break;
    }
  }

}
