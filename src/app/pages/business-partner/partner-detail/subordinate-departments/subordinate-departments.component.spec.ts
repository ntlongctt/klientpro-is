import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubordinateDepartmentsComponent } from './subordinate-departments.component';

describe('SubordinateDepartmentsComponent', () => {
  let component: SubordinateDepartmentsComponent;
  let fixture: ComponentFixture<SubordinateDepartmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubordinateDepartmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubordinateDepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
