import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import * as departmentActions from '@store/department/department.actions';
import { Subject } from 'rxjs/Subject';
import { takeUntil, map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'subordinate-departments',
  templateUrl: './subordinate-departments.component.html',
  styleUrls: ['./subordinate-departments.component.scss']
})
export class SubordinateDepartmentsComponent implements OnInit, OnDestroy {

  displayedColumns = ['name', 'typeName', 'specialtyName', 'validFrom', 'validTill'];

  dataSource = [];

  unSubAll = new Subject();

  partnerID;

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { 
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENTS_SUCCEEDED),
      map((action: any) => action.payload.departments)
    ).subscribe(items => {
      this.dataSource = items;
    });
  }

  ngOnInit() {
  } 

  loadData(queryString: string) {
    this.partnerID = this.activatedRoute.snapshot.params['partnerId'];
    this.store.dispatch(new departmentActions.GetDepartments({
      query: queryString,
      bpId: this.partnerID,
      departmentId: ''
    }));
  }

  filterChange(event) {
    this.loadData(event);
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }
  onRowclick(row) {
    this.router.navigate(['business-partners', this.partnerID, 'department', row.id, 'edit']);
  }

}
