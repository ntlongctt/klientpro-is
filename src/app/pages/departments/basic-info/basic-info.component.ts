import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { BaseComponent } from '@common/modules/base-component';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { AppState } from '@store/store.reducers';
import * as departmentActions from '@store/department/department.actions';
import { Subject } from 'rxjs/Subject';
import { takeUntil, map } from 'rxjs/operators';
import { DepartmentService } from '@klient/rest/services/pa/Department.service';
import { FormControl, Validators } from '@angular/forms';
import * as commonActions from '@store/commons/common.actions';
import { CODE_LIST } from '@common/constants/code-list';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { BusinessPartnerListComponent } from '../business-partner-list/business-partner-list.component';
import { SuperirorDepartmentComponent } from '../superiror-department/superiror-department.component';
import { BusinessPartnerDialogComponent } from '../business-partner-dialog/business-partner-dialog.component';
import * as fromFormActions from '@store/form/form.actions';
import * as fromPartnerActions from '@store/business-patner/business-patner.actions';
import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { Util } from '@common/services/util';

@Component({
  selector: 'department-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss']
})
export class BasicInfoComponent extends BaseComponent implements OnInit, OnDestroy {

  @Input() departmentId;
  @Input() businessPartnerId;
  unSubAll = new Subject();
  parentDepartmentName = '';
  parentDepartmentId;
  
  businessPartner;

  businessPartners = [];
  departmentTypes = [];
  specialties = [];
  superiorDepartmentDialog;
  initialForm: any;
  currentFormStatus = false;
  bpChangeDate;
  bpChangeId;
  parentId;

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private util: Util,
    private departmentService: DepartmentService
  ) {
    super();
    
    this.store.dispatch(new fromFormActions.ChangeFormStatus(false));

    this.controlConfig = {
      name: new FormControl('', [Validators.required]),
      shortcut: new FormControl(''),
      parentDepartmentName: new FormControl(''),
      departmentTypeId: new FormControl(null, [Validators.required]),
      specialtyId: new FormControl(''),
      minAge: new FormControl('', [Validators.max(99), Validators.min(0)]),
      maxAge: new FormControl('', [Validators.max(99), Validators.min(0)]),
      publicNote: new FormControl(''),
      privateNote: new FormControl(''),
      partnerNote: new FormControl(''),
      parentDepartmentId: new FormControl(''),
      id: new FormControl(''),
      version: new FormControl(''),
    };

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENT_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((detail) => {
      this.frm.patchValue(detail);
      this.parentDepartmentName = detail.parentDepartmentName ? detail.parentDepartmentName : '';
      this.parentDepartmentId = detail.parentDepartmentId;
      this.businessPartners = detail.businessPartners;
      this.initialForm = this.frm.value;
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.ADD_DEPARTMENT_SUCCEEDED,
        departmentActions.UPDATE_DEPARTMENT_INFO_SUCCEEDED,
      )
    ).subscribe((action: any) => {
      this.departmentId = action.payload.id;
      this.loadData();
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(commonActions.GET_CODE_LIST_ITEMS_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((payload) => {
      switch (payload.codeList) {
        case CODE_LIST.DEPARTMENT_TYPE:
          this.departmentTypes = payload.data.items;  
          break;
      
        case CODE_LIST.SPECIALTY:
          this.specialties = payload.data.items;  
          break;
      }
    });

    // bind bussniess partner info
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(fromPartnerActions.GET_PARTER_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((detail) => {
      this.businessPartners.push({
        nameShortcut: detail.nameShortcut,
        departmentName: '',
        validTill: detail.partnerTill,
        departmentId: null,
        name: detail.name,
        id: detail.id,
        validFrom: detail.partnerFrom,
        subDepartmentsCount: null,
      });
    });
   }

  ngOnInit() {
    super.ngOnInit();
    this.loadData();

    this.frm.valueChanges.subscribe(() => {
      this.checkFormStatus();
    });

    this.frm.get('minAge').valueChanges.subscribe(() => this.validateAge());
    this.frm.get('maxAge').valueChanges.subscribe(() => this.validateAge());

    // get business partner info incase create new department
    if (!this.departmentId) {
      this.getBusinessPartnerInfo();
    }

    // load Superior department
    this.parentId = this.activatedRoute.snapshot.params['parentId'];
    if (this.parentId) {
      this.departmentService.getDepartment({departmentId: this.parentId}).subscribe(detail => {
        this.frm.get('parentDepartmentName').patchValue(detail.name);
        this.frm.get('parentDepartmentId').patchValue(detail.id);
        this.parentDepartmentName = detail.name;
        this.parentDepartmentId = detail.id;
      });
    }
  }

  getBusinessPartnerInfo() {
    this.store.dispatch(new fromPartnerActions.GetPartnerDetail(this.businessPartnerId));
  }

  loadData() {
    this.businessPartnerId = this.activatedRoute.snapshot.params['partnerId'];
    this.departmentId = this.departmentId ? this.departmentId : this.activatedRoute.snapshot.params['departmentId'];

    if (this.departmentId) {
      this.store.dispatch(new departmentActions.GetDepartmentDetail(this.departmentId));
    }
    this.store.dispatch(new commonActions.GetCodeListItems(CODE_LIST.DEPARTMENT_TYPE));
    this.store.dispatch(new commonActions.GetCodeListItems(CODE_LIST.SPECIALTY));
  }

  validateAge() {
    const minAge = this.controlConfig.minAge.value;
    const maxAge = this.controlConfig.maxAge.value;

    if (!minAge || !maxAge) {
      return;
    }

    if (parseInt(minAge, 0) > parseInt(maxAge, 0)) {
      this.controlConfig.minAge.setErrors({...this.controlConfig.minAge.errors, invalid: true});
    } else {
      this.controlConfig.minAge.setErrors(null);
    }
  }

  showAllBusinessPartner() {
    this.dialog.open(BusinessPartnerListComponent, {
      maxHeight: 750,
      data: {
        datasources: this.businessPartners,
      },
      // position: {top: '150px'},
      autoFocus: false, // disable focus button on modal
      // disableClose: true,
    } as MatDialogConfig);
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  changeSuperiorDepartment() {
    const dialogRef = this.dialog.open(SuperirorDepartmentComponent, {
      maxHeight: 750,
      // minHeight: 500,
      minWidth: 700,
      data: {
        departmentId: this.departmentId,
        bpId: this.businessPartnerId
      },
      // position: {top: '150px'},
      autoFocus: false, // disable focus button on modal
      // disableClose: true,
    } as MatDialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.frm.get('parentDepartmentId').patchValue(result.parentDepartmentId);
        this.frm.get('parentDepartmentName').patchValue(result.parentDepartmentName);
        this.bpChangeDate = result.bpChangeDate;
        // this.bpChangeId = result.parentDepartmentId;
        this.onSubmit();
      }
    });
  }

  changeBusinessPartner() {
    const dialogRef = this.dialog.open(BusinessPartnerDialogComponent, {
      maxHeight: 750,
      // minHeight: 500,
      minWidth: 700,
      data: {
        // datasources: this.businessPartners,
      },
      // position: {top: '150px'},
      autoFocus: false, // disable focus button on modal
      // disableClose: true,
    } as MatDialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.bpChangeDate = result.bpChangeDate;
        this.bpChangeId = result.businessPartner;
        this.onSubmit();
      }
    }); 
  }

  onSubmit() {
    const {valid, value} = this.frm;
    if (!valid) {
      return null;
    }

    const objectPost = {
      parentDepartmentId: value.parentDepartmentId,
      parentDepartmentName: value.parentDepartmentName,
      departmentTypeId: value.departmentTypeId,
      businessPartners: this.businessPartners,
      partnerNote: value.partnerNote,
      shortcut: value.shortcut,
      privateNote: value.privateNote,
      maxAge: value.maxAge,
      specialtyId: value.specialtyId,
      minAge: value.minAge,
      name: value.name,
      publicNote: value.publicNote,
    };

    if (this.departmentId) {
      objectPost['id'] = value.id;
      objectPost['version'] = value.version;

      this.store.dispatch(new departmentActions.UpdateDepartmentInfo({
        departmentId: this.departmentId,
        department: objectPost,
        bpChangeDate: this.bpChangeDate ? this.util.formatDate(this.bpChangeDate, false) : '',
        bpChangeId: this.bpChangeId ? this.bpChangeId : ''
      }));
    } else {
      this.store.dispatch(new departmentActions.AddDepartment({
        department: objectPost,
        bpId: this.businessPartnerId
      }));
    }
  }

  /**
   * check if user form or privileges has changes, dispath to store
   */
  checkFormStatus() {
    if (!this.initialForm) {
      return;
    }
    const isFormChange = !_.isEqual(this.initialForm, this.frm.value) && this.frm.dirty;
    if (isFormChange !== this.currentFormStatus) {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(isFormChange));
      this.currentFormStatus = isFormChange;
    }
  }

  validateBasicInfo() {
    for (const key in this.controlConfig) {
      if (this.controlConfig.hasOwnProperty(key) && this.controlConfig[key].hasError) {
       this.controlConfig[key].markAsTouched();
      }
    }
  }

}
