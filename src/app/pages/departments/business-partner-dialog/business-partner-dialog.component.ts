import { Component, OnInit, Optional, Inject, AfterViewInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { DepartmentService } from '@klient/rest/services/pa/Department.service';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { BaseComponent } from '@common/modules/base-component';
import { FormControl, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { BusinessPartnerService } from '@klient/rest/services/pa/BusinessPartner.service';
import { Util } from '@common/services/util';
import * as _ from 'lodash';

@Component({
  selector: 'app-business-partner-dialog',
  templateUrl: './business-partner-dialog.component.html',
  styleUrls: ['./business-partner-dialog.component.scss']
})
export class BusinessPartnerDialogComponent extends BaseComponent implements OnInit, AfterViewInit {

  partners = [];
  defaulItem = {
    id: null,
    name: ''
  };
  isLoading = false;
  bpChangeDate;
  option = '1';
  showError = false;
  keySearch = '';

  displayFields = ['name', 'companyId', 'address'];

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusinessPartnerDialogComponent>,
    private businessPartnerService: BusinessPartnerService,
    private util: Util
  ) { 
    super();
    this.partners = _.uniqBy([this.defaulItem, ...this.partners], 'id');
    this.controlConfig = {
      businessPartner: new FormControl('', [Validators.required])
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    // this.frm.get('businessPartner').valueChanges
    //   .pipe(debounceTime(500))
    //   .subscribe((value) => {
    //     if (value.length < 3) {
    //       return;
    //     }
    //     this.isLoading = true;
    //     if (typeof value === 'string' || value instanceof String) {
    //       this.businessPartnerService.findBusinessPartnerByName({query: value.trim()}).subscribe(resp => {
    //         this.partners = resp.partners;
    //         this.isLoading = false;
    //       });
    //     }
    //   });
  }

  onSearchPartner(keySearch) {
    if (this.keySearch === keySearch || keySearch === '') {
      return;
    }

    if (keySearch.length < 3) {
      return;
    }

    this.businessPartnerService.findBusinessPartnerByName({query: keySearch.trim()}).subscribe(resp => {
      // this.partners = resp.partners;
      this.partners = _.uniqBy([this.defaulItem, ...resp.partners], 'id');
      this.isLoading = false;
    });
  }

  onDateChange(value) {
    this.bpChangeDate = this.util.formatDate(value, false);
    this.showError = value ? false : true;
  }

  displayFn(item) {
    return item.name;
  }

  close(status) {
    if (status) {
      const {valid, value} = this.frm;
      this.frm.markAsDirty();
      if (this.option === '1' && !this.bpChangeDate) {
        this.showError = true;
        if (!valid || !this.bpChangeDate || !value['businessPartner']) {
          return;
        }
      }

      if (!valid) {
        return;
      }
      this.dialogRef.close({
        businessPartner: value.businessPartner,
        bpChangeDate: this.bpChangeDate
      });
    } else {
      this.dialogRef.close();
    }
  }

  changeOption(event) {
    if (event.value === '2') {
      this.showError = false;
    } else if (!this.bpChangeDate) {
      this.showError = true;
    }
  }

}
