import { Component, OnInit, Input, Optional, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { locale as en} from '../i18n/en';
import { locale as cs} from '../i18n/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

@Component({
  selector: 'business-partner-list',
  templateUrl: './business-partner-list.component.html',
  styleUrls: ['./business-partner-list.component.scss']
})
export class BusinessPartnerListComponent implements OnInit {

  displayedColumns = ['name', 'validFrom', 'validTill'];
  dataSource = [];

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BusinessPartnerListComponent>,
    private translationLoaderService: FuseTranslationLoaderService
  ) { 
    this.translationLoaderService.loadTranslations(en, cs);
  }

  ngOnInit() {
    this.dataSource = this.data.datasources;
  }
}
