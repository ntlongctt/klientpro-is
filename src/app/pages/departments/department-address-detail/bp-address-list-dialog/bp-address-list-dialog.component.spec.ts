import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BpAddressListDialogComponent } from './bp-address-list-dialog.component';

describe('BpAddressListDialogComponent', () => {
  let component: BpAddressListDialogComponent;
  let fixture: ComponentFixture<BpAddressListDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BpAddressListDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BpAddressListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
