import { Component, OnInit, Optional, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { Subject } from 'rxjs/Subject';
import { takeUntil, map } from 'rxjs/operators';

@Component({
  selector: 'app-bp-address-list-dialog',
  templateUrl: './bp-address-list-dialog.component.html',
  styleUrls: ['./bp-address-list-dialog.component.scss']
})
export class BpAddressListDialogComponent implements OnInit, OnDestroy {

  dataSource = [];
  unSubAll = new Subject();

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<BpAddressListDialogComponent>,
    private store: Store<AppState>,
    private actions$: Actions
  ) {
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_ADDRESSES_SUCCEEDED),
      map((action: any) => action.payload.addresses)
    ).subscribe(items => {
      this.dataSource = items;
    });
   }

  ngOnInit() {
    this.store.dispatch(new partnerActions.GetPartnerAddresses(this.data.partnerId));
  }

  onRowClick(row) {
    this.dialogRef.close(row);
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

}
