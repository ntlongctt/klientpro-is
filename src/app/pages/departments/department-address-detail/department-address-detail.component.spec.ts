import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentAddressDetailComponent } from './department-address-detail.component';

describe('DepartmentAddressDetailComponent', () => {
  let component: DepartmentAddressDetailComponent;
  let fixture: ComponentFixture<DepartmentAddressDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentAddressDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentAddressDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
