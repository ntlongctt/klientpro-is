import { Component, OnInit, Optional, Inject, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialogConfig, MatDialog } from '@angular/material';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { Subject } from 'rxjs/Subject';
import * as departmentActions from '@store/department/department.actions';
import { takeUntil, map } from 'rxjs/operators';
import * as businessPartnerActions from '@store/business-patner/business-patner.actions';
import { ListAddressComponent } from '@pages/business-partner/partner-detail/contact-person-detail/list-address/list-address.component';
import { BpAddressListDialogComponent } from './bp-address-list-dialog/bp-address-list-dialog.component';
import { EditBpAddressDialogComponent } from './edit-bp-address-dialog/edit-bp-address-dialog.component';
import { BpAddressDetailFormComponent } from '@common/modules/bp-address-detail-form/bp-address-detail-form.component';
import { DeactiveWarningModalService } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.service';
import * as _ from 'lodash';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { locale as cs } from '../i18n/cs';
import { locale as en } from '../i18n/en';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { ConfirmModalService } from '@common/modules/confirm-modal';

@Component({
  selector: 'app-department-address-detail',
  templateUrl: './department-address-detail.component.html',
  styleUrls: ['./department-address-detail.component.scss'],
})
export class DepartmentAddressDetailComponent implements OnInit, OnDestroy {

  @ViewChild('addressDetail') addressDetailComp: BpAddressDetailFormComponent;

  unSubAll = new Subject();
  bpAddressDetail;
  partnerAddressesDialogRef;
  bpAddressDetailDialogRef;

  valuesInitial = {
    bpAddressId: '',
    id: '',
    isActive: false,
    partnerVisible: false,
    publicVisible: false,
    version: ''
  };

  submitted = false;

  controlConfig = {};
  frm: FormGroup;
  creatingMode = false;
  frmError = false;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DepartmentAddressDetailComponent>,
    private store: Store<AppState>,
    private actions$: Actions,
    private dialog: MatDialog,
    private deactiveWarningModalService: DeactiveWarningModalService,
    private toast: ToastrService,
    private translationLoaderService: FuseTranslationLoaderService,
    private confirmModalService: ConfirmModalService,
  ) {
    this.translationLoaderService.loadTranslations(en, cs);

    this.creatingMode = this.data.departmentAddressId ? false : true;

    this.controlConfig = {
      partnerVisible: new FormControl(false),
      publicVisible: new FormControl(false),
      active: new FormControl(false),
      id: new FormControl(''),
      bpAddressId: new FormControl('', [Validators.required]),
      version: new FormControl(''),
    };

    this.frm = new FormGroup(this.controlConfig);

    // get bp address detail
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENT_ADDRESS_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.bindData(detail);
      this.store.dispatch(new businessPartnerActions.GetPartnerAddressDetail({
        partnerId: this.data.businessPartnerId,
        bpAddressId: detail.bpAddressId
      }));
    });

    // binding address detail
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(businessPartnerActions.GET_PARTNER_ADDRESS_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.bpAddressDetail = detail;
      this.frm.controls['bpAddressId'].patchValue(detail.id);
    });

    // save department address success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.UPDATE_DEPARTMENT_ADDRESS_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.bindData(detail);
      this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.UPDATE_DEPARTMENT_ADDRESS_SUCCESS'));
      this.dialogRef.close();
    });

    // add new address success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.ADD_DEPARTMENT_ADDRESS_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.ADD_DEPARTMENT_ADDRESS_SUCCESS'));
      this.dialogRef.close();
    });

    // delete address success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.DELETE_DEPARTMENT_ADDRESS_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_ADDRESS_SUCCESS'));
      this.dialogRef.close();
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.ADD_DEPARTMENT_ADDRESS_FAILED,
        departmentActions.UPDATE_DEPARTMENT_ADDRESS_FAILED,
        departmentActions.DELETE_DEPARTMENT_ADDRESS_FAILED,
      ),
      map((action: any) => action.payload)
    ).subscribe(() => {
      this.submitted = false;
    });
  }

  ngOnInit() {
    if (this.data.departmentAddressId) {
      this.store.dispatch(new departmentActions.GetDepartmentAddressDetail({
        departmentId: this.data.departmentId,
        departmentAddressId: this.data.departmentAddressId
      }));
    }

    this.dialogRef.backdropClick().subscribe(() => {
      this.cancel();
    });
  }

  bindData(detail) {
    this.frm.patchValue(detail);
    this.valuesInitial = _.cloneDeep(this.frm.value);
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  selectAddress() {
    this.partnerAddressesDialogRef = this.dialog.open(BpAddressListDialogComponent, {
      maxHeight: 600,
      width: '800px',
      autoFocus: false, // disable focus button on modal
      // disableClose: true,
      data: {
        partnerId: this.data.businessPartnerId,
      },
    } as MatDialogConfig);

    this.partnerAddressesDialogRef.afterClosed().subscribe((result) => {
      // load address detail
      if (result && result.id) {
        this.creatingMode = false;
        this.store.dispatch(new businessPartnerActions.GetPartnerAddressDetail({
          partnerId: this.data.businessPartnerId,
          bpAddressId: result.id
        }));
      }
    });
  }

  editAddress() {
    if (!this.bpAddressDetail) {
      return;
    }
    this.bpAddressDetailDialogRef = this.dialog.open(EditBpAddressDialogComponent, {
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        partnerId: this.data.businessPartnerId,
        bpAddressId: this.bpAddressDetail.id,
      },
    } as MatDialogConfig);

    this.bpAddressDetailDialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(new businessPartnerActions.GetPartnerAddressDetail({
          partnerId: this.data.businessPartnerId,
          bpAddressId: result.id
        }));
      }
    });
  }

  createAddress() {
    const dialogRef = this.dialog.open(EditBpAddressDialogComponent, {
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        partnerId: this.data.businessPartnerId,
      },
    } as MatDialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.creatingMode = false;
        this.store.dispatch(new businessPartnerActions.GetPartnerAddressDetail({
          partnerId: this.data.businessPartnerId,
          bpAddressId: result.id
        }));
      }
    });
  }

  onSubmit() {
    const {value, valid} = this.frm;
    this.frmError = value.bpAddressId ? false : true;
    if (!valid) {
      return;
    }

    if (value.id) {
      this.store.dispatch(new departmentActions.UpdateDepartmentAddress({
        departmentId: this.data.departmentId,
        departmentAddressId: value.id,
        address: value,
      }));
    } else {
      delete value['id'];
      delete value['version'];
      this.store.dispatch(new departmentActions.AddDepartmentAddress({
        departmentId: this.data.departmentId,
        address: value
      }));
    }
  }

  delete() {
    this.confirmModalService
      .show(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_ADDRESS_CONFIRM'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new departmentActions.DeleteDepartmentAddress({
            departmentId: this.data.departmentId,
            departmentAddressId: this.data.departmentAddressId,
          }));
        }
      }, 
      this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_ADDRESS_TITLE'));
  }

  cancel() {
    if (!_.isEqual(this.valuesInitial, this.frm.value)) {
      const subject = new Subject<boolean>();
      this.deactiveWarningModalService.subject = subject;

      subject.pipe(takeUntil(this.unSubAll)).subscribe((result) => {
        if (result) {
          this.dialogRef.close();
        }
      });
      this.deactiveWarningModalService.show();
    } else {
      this.dialogRef.close();
    }
  }

}
