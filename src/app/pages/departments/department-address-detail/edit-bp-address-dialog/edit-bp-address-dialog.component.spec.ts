import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBpAddressDialogComponent } from './edit-bp-address-dialog.component';

describe('EditBpAddressDialogComponent', () => {
  let component: EditBpAddressDialogComponent;
  let fixture: ComponentFixture<EditBpAddressDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBpAddressDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBpAddressDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
