import { Component, OnInit, ViewChild, Optional, Inject } from '@angular/core';
import { BpAddressDetailFormComponent } from '@common/modules/bp-address-detail-form/bp-address-detail-form.component';
import { Subject } from 'rxjs/Subject';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AddressDetailComponent } from '@pages/business-partner/partner-detail/address-detail/address-detail.component';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as partnerActions from '@store/business-patner/business-patner.actions';

@Component({
  selector: 'app-edit-bp-address-dialog',
  templateUrl: './edit-bp-address-dialog.component.html',
  styleUrls: ['./edit-bp-address-dialog.component.scss']
})
export class EditBpAddressDialogComponent implements OnInit {

  @ViewChild('addressDetail') addressDetailComp: BpAddressDetailFormComponent;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddressDetailComponent>,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    this.loadData();
    
    this.dialogRef.backdropClick().subscribe(() => {
      this.addressDetailComp.cancel();
    });
  }

  loadData() {
    if (this.data.bpAddressId) {
      this.store.dispatch(new partnerActions.GetPartnerAddressDetail(this.data));
    }
  }

  onCancelChange() {
    this.dialogRef.close(false);
  }

  onActionSuccess(detail) {
    this.dialogRef.close(detail);
  }

}
