import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentAddressesComponent } from './department-addresses.component';

describe('DepartmentAddressesComponent', () => {
  let component: DepartmentAddressesComponent;
  let fixture: ComponentFixture<DepartmentAddressesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentAddressesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentAddressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
