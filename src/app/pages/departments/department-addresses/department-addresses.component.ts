import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { MatDialog, MatDialogConfig } from '@angular/material';
import * as departmentActions from '@store/department/department.actions';
import { Subject } from 'rxjs/Subject';
import { takeUntil, map } from 'rxjs/operators';
import { DepartmentAddressDetailComponent } from '../department-address-detail/department-address-detail.component';

@Component({
  selector: 'department-addresses',
  templateUrl: './department-addresses.component.html',
  styleUrls: ['./department-addresses.component.scss']
})
export class DepartmentAddressesComponent implements OnInit, OnDestroy {

  @Input() departmentId;
  @Input() businessPartnerId;
  unSubAll = new Subject();
  dataSource = [];
  dialogRef;

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private dialog: MatDialog,
  ) { 
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENT_ADDRESSES_SUCCEEDED),
      map((action: any) => action.payload.addresses)
    ).subscribe(addresses => {
      this.dataSource = this.flaternList(addresses);
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.ADD_DEPARTMENT_ADDRESS_SUCCEEDED,
        departmentActions.UPDATE_DEPARTMENT_ADDRESS_SUCCEEDED,
        departmentActions.DELETE_DEPARTMENT_ADDRESS_SUCCEEDED,
      ),
    ).subscribe(() => {
      this.loadData();
    });

  }

  ngOnInit() {
    
    
  }

  flaternList(dataSource: any[]) {
    return dataSource.map(item => {
      return {
        ...item.bpAddress,
        active: item.active,
        departmentAddressId: item.id,
        partnerVisible: item.partnerVisible,
        publicVisible: item.publicVisible,
      };
    });
  }

  loadData() {
    this.store.dispatch(new departmentActions.GetDepartmentAddresses(this.departmentId));
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  onRowClick(row) {
    this.dialogRef = this.dialog.open(DepartmentAddressDetailComponent, {
      minWidth: 700,
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        departmentId: this.departmentId,
        departmentAddressId: row.departmentAddressId,
        businessPartnerId: this.businessPartnerId
      },
    } as MatDialogConfig);
  }

  newAddress() {
    this.dialogRef = this.dialog.open(DepartmentAddressDetailComponent, {
      minWidth: 700,
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        departmentId: this.departmentId,
        businessPartnerId: this.businessPartnerId
      },
    } as MatDialogConfig);
  }

}
