import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentContactPersonDetailComponent } from './department-contact-person-detail.component';

describe('DepartmentContactPersonDetailComponent', () => {
  let component: DepartmentContactPersonDetailComponent;
  let fixture: ComponentFixture<DepartmentContactPersonDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentContactPersonDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentContactPersonDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
