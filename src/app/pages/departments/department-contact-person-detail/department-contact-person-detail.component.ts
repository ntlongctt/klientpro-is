import { Component, OnInit, Optional, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as departmentActions from '@store/department/department.actions';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { Subject } from 'rxjs/Subject';
import { takeUntil, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { SelectContactPersonComponent } from './select-contact-person/select-contact-person.component';
import { EditContactPersonComponent } from './edit-contact-person/edit-contact-person.component';
import { DepartmentContactPersonDTO } from '@klient/rest/models/pa/department/contactperson/DepartmentContactPersonDTO';
import * as _ from 'lodash';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DeactiveWarningModalService } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.service';
import { locale as en} from '../i18n/en';
import { locale as cs} from '../i18n/en';

@Component({
  selector: 'department-contact-person-detail',
  templateUrl: './department-contact-person-detail.component.html',
  styleUrls: ['./department-contact-person-detail.component.scss']
})
export class DepartmentContactPersonDetailComponent implements OnInit, OnDestroy {

  contactDetail;
  submitted = false;

  bpContactDetail;
  bpAddressDetail;

  unSubAll = new Subject();

  controlConfig = {};
  frm: FormGroup;
  frmError = false;
  // valuesInitial;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DepartmentContactPersonDetailComponent>,
    private store: Store<AppState>,
    private actions$: Actions,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private confirmModalService: ConfirmModalService,
    private translationLoader: FuseTranslationLoaderService,
    private toast: ToastrService,
    private deactiveWarningModalService: DeactiveWarningModalService
  ) {

    this.translationLoader.loadTranslations(en, cs);

    this.controlConfig = {
      partnerVisible: new FormControl(false),
      publicVisible: new FormControl(false),
      active: new FormControl(false),
      bpContactPersonId: new FormControl('', [Validators.required]),
      id: new FormControl(''),
      version: new FormControl(''),
    };

    this.frm = new FormGroup(this.controlConfig);

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.frm.patchValue(detail);
      this.contactDetail = this.frm.value;
      this.store.dispatch(new partnerActions.GetPartnerContactPersonDetail({
        partnerId: this.data.partnerId,
        personId: detail.bpContactPersonId
      }));
    });
    

    // bind contact person detail
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_CONTACT_PERSON_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.bpContactDetail = _.cloneDeep(detail);
      this.frm.controls['bpContactPersonId'].patchValue(detail.id);
      // get bpAddress detail
      if (detail.bpaddressId) {
        this.store.dispatch(new partnerActions.GetPartnerAddressDetail({
          partnerId: this.data.partnerId,
          bpAddressId: detail.bpaddressId
        }));
      }
    });

    // bind bpAddress detail
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_ADDRESS_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.bpAddressDetail = detail.address;
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED,
        departmentActions.DELETE_DEPARTMENT_CONTACT_PERSON_SUCCEEDED,
        departmentActions.ADD_DEPARTMENT_CONTACT_PERSON_SUCCEEDED,
      ),
    ).subscribe(detail => {
      switch (detail.type) {
        case departmentActions.UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED:
          this.toast.success(this.translationLoader.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARMENT_CONTACT_SUCCESS'));
          break;
        case departmentActions.DELETE_DEPARTMENT_CONTACT_PERSON_SUCCEEDED:
          this.toast.success(this.translationLoader.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARMENT_CONTACT_SUCCESS'));
          break;
        case departmentActions.ADD_DEPARTMENT_CONTACT_PERSON_SUCCEEDED:
          this.toast.success(this.translationLoader.instant('DEPARTMENTS.MESSAGE.ADD_DEPARMENT_CONTACT_SUCCESS'));
          break;
      }
      this.dialogRef.close();
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_FAILED,
        departmentActions.DELETE_DEPARTMENT_CONTACT_PERSON_FAILED,
        departmentActions.ADD_DEPARTMENT_CONTACT_PERSON_FAILED,
      ),
    ).subscribe(() => {
      this.submitted = false;
    });
  }


  ngOnInit() {
    if (this.data.departmentContactPersonId) {
      this.store.dispatch( new departmentActions.GetDepartmentContactPersonDetail({
        departmentId: this.data.departmentId,
        departmentContactPersonId: this.data.departmentContactPersonId,
      }));
    }

    this.dialogRef.backdropClick().subscribe(() => {
      this.cancel();
    });

    this.contactDetail = _.cloneDeep(this.frm.value);
  }

  onCancelChange() {

  }

  onActionSuccess() {

  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  cancel() {
    if (this.contactDetail && !_.isEqual(this.contactDetail, this.frm.value)) {
      const subject = new Subject<boolean>();
      this.deactiveWarningModalService.subject = subject;

      subject.pipe(takeUntil(this.unSubAll)).subscribe((result) => {
        if (result) {
          this.dialogRef.close();
        }
      });
      this.deactiveWarningModalService.show();
    } else {
      this.dialogRef.close();
    }
  }

  selectContact() {
    const contactListDialogRef = this.dialog.open(SelectContactPersonComponent, {
      // maxHeight: 600,
      maxHeight: 700,
      autoFocus: false, // disable focus button on modal
      data: {
        partnerId: this.data.partnerId
      },
    } as MatDialogConfig);

    contactListDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.store.dispatch(new partnerActions.GetPartnerContactPersonDetail({
          partnerId: this.data.partnerId,
          personId: result.id
        }));
      }
    });
  }

  editContact() {
    if (!this.bpContactDetail) {
      return;
    }
    const dialogRef = this.dialog.open(EditContactPersonComponent, {
      // maxHeight: 600,
      maxHeight: 700,
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        partnerId: this.data.partnerId,
        personId: this.bpContactDetail.id
      },
    } as MatDialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.contactDetail.bpContactPersonId = result;
        this.store.dispatch(new partnerActions.GetPartnerContactPersonDetail({
          partnerId: this.data.partnerId,
          personId: result
        }));
      }
    });
  }

  createContact() {
    const dialogRef = this.dialog.open(EditContactPersonComponent, {
      // maxHeight: 600,
      maxHeight: 700,
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        partnerId: this.data.partnerId,
      },
    } as MatDialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.contactDetail.bpContactPersonId = result;
        this.store.dispatch(new partnerActions.GetPartnerContactPersonDetail({
          partnerId: this.data.partnerId,
          personId: result
        }));
      }
    });
  }

  onSubmit() {
    const {value, valid} = this.frm;
    this.frmError = value.bpContactPersonId ? false : true;
    if (!valid) {
      return;
    } 

    if (value.id) {
      this.store.dispatch(new departmentActions.UpdateDepartmentContactPersonDetail({
        departmentId: this.data.departmentId,
        departmentContactPersonId: this.data.departmentContactPersonId,
        contactPerson: value
      }));
    } else {
      delete value['id'];
      delete value['version'];
      this.store.dispatch(new departmentActions.AddDepartmentContactPerson({
        departmentId:  this.data.departmentId,
        contactPerson: value
      }));
    }
  }

  delete() {
    this.confirmModalService
    .show(this.translationLoader.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARMENT_CONTACT_MESSAGE'), (status) => {
      if (status) {
        this.submitted = true;
        this.store.dispatch(new departmentActions.DeleteDepartmentContactPerson({
          departmentId: this.data.departmentId,
          departmentContactPersonId: this.data.departmentContactPersonId,
        }));
      }
    }, this.translationLoader.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARMENT_CONTACT_TITLE'));
  }

}
