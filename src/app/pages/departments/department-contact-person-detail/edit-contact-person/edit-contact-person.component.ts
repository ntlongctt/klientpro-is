import { Component, OnInit, ViewChild, Optional, Inject } from '@angular/core';
import { BpContactPersonDetailFormComponent } from '@common/modules/bp-contact-person-detail-form/bp-contact-person-detail-form.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AddressDetailComponent } from '@pages/business-partner/partner-detail/address-detail/address-detail.component';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as partnerActions from '@store/business-patner/business-patner.actions';

@Component({
  selector: 'app-edit-contact-person',
  templateUrl: './edit-contact-person.component.html',
  styleUrls: ['./edit-contact-person.component.scss']
})
export class EditContactPersonComponent implements OnInit {

  @ViewChild('contactPerson') contactPerson: BpContactPersonDetailFormComponent;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditContactPersonComponent>,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {

    this.loadData();

    this.dialogRef.backdropClick().subscribe(() => {
      this.contactPerson.cancel();
    });
  }

  loadData() {
    if (this.data.personId) {
      this.store.dispatch(new partnerActions.GetPartnerContactPersonDetail(this.data));
    }
  }

  onCancelChange() {
    this.dialogRef.close(false);
  }

  onActionSuccess(data) {
    this.dialogRef.close(data.id);
  }

}
