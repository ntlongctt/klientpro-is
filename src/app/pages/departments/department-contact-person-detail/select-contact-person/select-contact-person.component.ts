import { Component, OnInit, Optional, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import * as partnerActions from '@store/business-patner/business-patner.actions';
import { takeUntil, map } from 'rxjs/operators';

@Component({
  selector: 'app-select-contact-person',
  templateUrl: './select-contact-person.component.html',
  styleUrls: ['./select-contact-person.component.scss']
})
export class SelectContactPersonComponent implements OnInit, OnDestroy {

  dataSource = [];
  unSubAll = new Subject();

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<SelectContactPersonComponent>,
    private store: Store<AppState>,
    private actions$: Actions
  ) {
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(partnerActions.GET_PARTNER_CONTACT_PERSON_SUCCEEDED),
      map((action: any) => action.payload.contactPersons)
    ).subscribe(items => {
      this.dataSource = items;
    });
   }

  ngOnInit() {
    this.store.dispatch(new partnerActions.GetPartnerContactPerson(this.data));
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  onRowClick(row) {
    this.dialogRef.close(row);
  }

}
