import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentContactPersonComponent } from './department-contact-person.component';

describe('DepartnerContactPersonComponent', () => {
  let component: DepartmentContactPersonComponent;
  let fixture: ComponentFixture<DepartmentContactPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentContactPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentContactPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
