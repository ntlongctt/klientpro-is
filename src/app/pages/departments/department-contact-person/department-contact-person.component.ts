import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import * as departmentActions from '@store/department/department.actions';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { Subject } from 'rxjs/Subject';
import { takeUntil, map } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DepartmentContactPersonDetailComponent } from '../department-contact-person-detail/department-contact-person-detail.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'department-contact-person',
  templateUrl: './department-contact-person.component.html',
  styleUrls: ['./department-contact-person.component.scss']
})
export class DepartmentContactPersonComponent implements OnInit, OnDestroy {

  @Input() departmentId;
  @Input() partnerId;

  unSubAll = new Subject();
  dataSource = [];
  dialogRef;
  keySearch = '';

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private dialog: MatDialog,
  ) { 

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENT_CONTACT_PERSONS_SUCCEEDED),
      map((action: any) => action.payload.contactPersons)
    ).subscribe(items => {
      this.dataSource = this.flaternList(items);
      // console.log(dataSource);
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED,
        departmentActions.DELETE_DEPARTMENT_CONTACT_PERSON_SUCCEEDED,
        departmentActions.ADD_DEPARTMENT_CONTACT_PERSON_SUCCEEDED,
      ),
    ).subscribe(() => {
      this.loadData();
    });
  }

  ngOnInit() {
  }

  flaternList(dataSource: any[]) {
    return dataSource.map(item => {
      return {
        ...item.bpContactPerson,
        id: item.id,
        active: item.active,
        bpContactPerson: item.bpContactPerson.id,
        partnerVisible: item.partnerVisible,
        publicVisible: item.publicVisible,
      };
    });
  }

  loadData() {
    this.store.dispatch(new departmentActions.GetDepartmentContactPersons({
      departmentId: this.departmentId,
      query: this.keySearch
    }));
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  onRowClick(row) {
    this.dialogRef = this.dialog.open(DepartmentContactPersonDetailComponent, {
      // maxHeight: 600,
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        departmentId: this.departmentId,
        departmentContactPersonId: row.id,
        partnerId: this.partnerId
      },
    } as MatDialogConfig);

    this.dialogRef.afterClosed().subscribe((result) => {
    });
  }

  filterChange(keywork) {
    if (this.keySearch === keywork) {
      return;
    }
    this.keySearch = keywork;
    this.loadData();
  }

  newContact() {
    this.dialogRef = this.dialog.open(DepartmentContactPersonDetailComponent, {
      // maxHeight: 600,
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        departmentId: this.departmentId,
        partnerId: this.partnerId
      },
    } as MatDialogConfig);

  }
}
