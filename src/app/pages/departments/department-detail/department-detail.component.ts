import { Component, OnInit, ViewChild, OnDestroy, HostListener, AfterViewInit } from '@angular/core';
import { locale as en} from '../i18n/en';
import { locale as cs} from '../i18n/cs';
import { locale as bpEn} from '../../business-partner/i18n/en';
import { locale as bpCs} from '../../business-partner/i18n/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BasicInfoComponent } from '../basic-info/basic-info.component';
import * as departmentActions from '@store/department/department.actions';
import { AppState } from '@store/store.reducers';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { AppStoreService } from '@store/store.service';
import { DepartmentAddressesComponent } from '../department-addresses/department-addresses.component';
import { DepartmentContactPersonComponent } from '../department-contact-person/department-contact-person.component';
import { DepartmentMcSystemComponent } from '../department-mc-system/department-mc-system.component';
import { BreadscumbComponent } from '@common/modules/breadcrumbs/breadcrumbs.component';
import { MatTabGroup, MatTab, MatTabHeader } from '@angular/material';
import { DeactiveWarningModalService } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.service';
import { ListSubordinateDepartmentComponent } from '../list-subordinate-department/list-subordinate-department.component';

@Component({
  selector: 'department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.scss']
})
export class DepartmentDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('basicInfo') basicInfo: BasicInfoComponent;
  @ViewChild('departmentAddresses') departmentAddresses: DepartmentAddressesComponent;
  @ViewChild('departmentContact') departmentContact: DepartmentContactPersonComponent;
  @ViewChild('breadscrumb') breadscrumb: BreadscumbComponent;
  @ViewChild('departmentMcSystem') departmentMcSystem: DepartmentMcSystemComponent;
  @ViewChild('subordinateDepartment') subordinateDepartment: ListSubordinateDepartmentComponent;
  @ViewChild('tabs') tabs: MatTabGroup;

  departmentId;
  businessPartnerId;
  selectedTab = 0;
  submitted = false;
  unSubAll = new Subject();
  departmentName;
  isFormChange$;

  /**
   * Before tab/browser close or refresh, check if form data has changed,show alter confirm
   */
  @HostListener('window:beforeunload', ['$event'])
  beforeunload(event) {
    const hasChange = this.appStoreService.getState().form.hasChanged;
    if (hasChange) {
      return false;
    }
  }

  constructor(
    private translationLoaderService: FuseTranslationLoaderService,
    private activatedRoute: ActivatedRoute,
    private actions$: Actions,
    private toast: ToastrService,
    private router: Router,
    private confirmModalService: ConfirmModalService,
    private store: Store<AppState>,
    private appStoreService: AppStoreService,
    private deactiveWarningModalService: DeactiveWarningModalService
  ) {

    this.isFormChange$ = this.store.select(s => s.form.hasChanged);

    this.translationLoaderService.loadTranslations(en, cs, bpEn, bpCs);
    this.departmentId = this.activatedRoute.snapshot.params['departmentId'];
    this.businessPartnerId = this.activatedRoute.snapshot.params['partnerId'];
    
   
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.ADD_DEPARTMENT_SUCCEEDED,
        departmentActions.UPDATE_DEPARTMENT_INFO_SUCCEEDED,
        departmentActions.DELETE_DEPARTMENT_SUCCEEDED,
      ),
    ).subscribe((action: any) => {
      switch (action.type) {
        case departmentActions.ADD_DEPARTMENT_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.ADD_DEPARTMENT_SUCCESS'));
          this.submitted = false;
          this.departmentName = action.payload.name;
          this.departmentId = action.payload.id;
          this.basicInfo.departmentId = action.payload.id;
          break;
      
        case departmentActions.UPDATE_DEPARTMENT_INFO_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.UPDATE_DEPARTMENT_SUCCESS'));
          this.submitted = false;
          this.departmentName = action.payload.name;
          break;
      
        case departmentActions.DELETE_DEPARTMENT_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_SUCCESS'));
          this.close();
          break;
      
        default:
          break;
      }
      
    });

    // this.actions$.pipe(
    //   takeUntil(this.unSubAll),
    //   ofType(departmentActions.GET_DEPARTMENT_DETAIL_SUCCEEDED),
    //   map((action: any) => action.payload)
    // ).subscribe(detail => {
    //   console.log(detail);
      
    // });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.ADD_DEPARTMENT_FAILED,
        departmentActions.UPDATE_DEPARTMENT_INFO_FAILED,
        departmentActions.DELETE_DEPARTMENT_FAILED,
      ),
    ).subscribe(() => {
      this.submitted = false;
    });
    
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENT_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((detail) => {
      this.departmentName = detail.name;
    });
  }

  ngOnInit() {
    this.tabs._handleClick = this.interceptTabChange.bind(this);
  }

  ngAfterViewInit() {
    
  }

  interceptTabChange(tab: MatTab, tabHeader: MatTabHeader, idx: number) {
    const agrs = arguments;
    const subject = new Subject<boolean>();
    this.deactiveWarningModalService.subject = subject;

    if (this.selectedTab === 0 && idx !== 0 && this.basicInfo.currentFormStatus) {
      subject.subscribe((result) => {
        if (result) {
          return MatTabGroup.prototype._handleClick.apply(this.tabs, agrs);
        }
      });
      this.deactiveWarningModalService.show();
    } else {
      return MatTabGroup.prototype._handleClick.apply(this.tabs, agrs);
    }
  }

  tabChanged(index) {
    this.selectedTab = index;
    switch (this.selectedTab) {
      case 0: 
        this.basicInfo.loadData();
        break;
      case 1: 
        this.departmentAddresses.loadData();
        break;
      case 2: 
        this.departmentContact.loadData();
        break;
      case 4: 
        this.departmentMcSystem.loadData();
        break;
      case 5: 
        this.subordinateDepartment.loadData();
        break;
    }
  }

  delete() {
    this.confirmModalService
      .show(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_MESSAGE'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new departmentActions.DeleteDepartment(this.departmentId));
        }
      },
      this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_TITLE'));
  }

  onSubmit() {
    this.basicInfo.validateBasicInfo();
    const valid = this.basicInfo.frm.valid;
    if (valid) {
      this.submitted = true;
      this.basicInfo.onSubmit(); 
    }
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  close() {
    if (this.basicInfo.parentId || !this.basicInfo.departmentId) {
      this.breadscrumb.onClick(this.activatedRoute.snapshot.parent.parent);
    } else {
      this.breadscrumb.onClick(this.activatedRoute.snapshot.parent.parent.parent);
    }
  }

  addNew() {
    switch (this.selectedTab) {
      case 1:
        this.departmentAddresses.newAddress();
        break;
      case 2:
        this.departmentContact.newContact();
        break;
      case 4:
        this.departmentMcSystem.newSystem();
        break;
    }
  }

  filterChange(keywork) {
    switch (this.selectedTab) {
      case 2:
        this.departmentContact.filterChange(keywork);
        break;
      case 4:
        this.departmentMcSystem.filterChange(keywork);
        break;
      case 5:
        this.subordinateDepartment.filterChange(keywork);
        break;
      default:
        break;
    }
  }

}
