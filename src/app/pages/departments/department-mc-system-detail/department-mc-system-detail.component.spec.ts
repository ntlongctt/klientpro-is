import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentMcSystemDetailComponent } from './department-mc-system-detail.component';

describe('DepartmentMcSystemDetailComponent', () => {
  let component: DepartmentMcSystemDetailComponent;
  let fixture: ComponentFixture<DepartmentMcSystemDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentMcSystemDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentMcSystemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
