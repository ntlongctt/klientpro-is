import { Component, OnInit, Optional, Inject, OnDestroy, AfterViewInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import * as departmentActions from '@store/department/department.actions';
import { takeUntil, map } from 'rxjs/operators';
import { BaseComponent } from '@common/modules/base-component';
import { FormControl, Validators } from '@angular/forms';
import * as mcSystemActions from '@store/mc-system/mc-system.actions';
import * as commonsActions from '@store/commons/common.actions';
import { CODE_LIST } from '@common/constants/code-list';
import { ToastrService } from 'ngx-toastr';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import * as _ from 'lodash';
import { DeactiveWarningModalService } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.service';


@Component({
  selector: 'app-department-mc-system-detail',
  templateUrl: './department-mc-system-detail.component.html',
  styleUrls: ['./department-mc-system-detail.component.scss']
})
export class DepartmentMcSystemDetailComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

  unSubAll = new Subject();
  mcSystems = [];
  participanttypes = [];

  submitted = false;

  initialFormValue;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DepartmentMcSystemDetailComponent>,
    private store: Store<AppState>,
    private actions$: Actions,
    private toast: ToastrService,
    private translationLoaderService: FuseTranslationLoaderService,
    private confirmModalService: ConfirmModalService,
    private deactiveWarningModalService: DeactiveWarningModalService
  ) {
    
    super();

    this.controlConfig = {
      mcSystemId: new FormControl('', [Validators.required]),
      participationTypeId: new FormControl('', [Validators.required]),
      dateRange: new FormControl({start: null, end: null}),
      version: new FormControl(''),
      id: new FormControl(''),
    };

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENT_MC_SYSTEM_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      // console.log(detail);
      this.frm.patchValue(detail);
      this.frm.get('dateRange').patchValue({start: detail.validFrom, end: detail.validTill});
      this.initialFormValue = _.cloneDeep(this.frm.value);
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(mcSystemActions.GET_MC_SYSTEM_CODE_LIST_SUCCEEDED),
      map((action: any) => action.payload.items)
    ).subscribe(items => {
      this.mcSystems = items;
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(mcSystemActions.GET_MC_SYSTEM_CODE_LIST_SUCCEEDED),
      map((action: any) => action.payload.items)
    ).subscribe(items => {
      this.mcSystems = items;
    });
    
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.UPDATE_DEPARTMENT_MC_SYSTEM_SUCCEEDED,
        departmentActions.ADD_DEPARTMENT_MC_SYSTEM_SUCCEEDED,
        departmentActions.DELETE_DEPARTMENT_MC_SYSTEM_SUCCEEDED,
      ),
    ).subscribe((action: any) => {
      switch (action.type) {
        case departmentActions.UPDATE_DEPARTMENT_MC_SYSTEM_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.UPDATE_DEPARTMENT_MC_SYSTEM_SUCCESS'));
          break;
        case departmentActions.ADD_DEPARTMENT_MC_SYSTEM_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.ADD_DEPARTMENT_MC_SYSTEM_SUCCESS'));
          break;
        case departmentActions.DELETE_DEPARTMENT_MC_SYSTEM_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_MC_SYSTEM_SUCCESS'));
          break;
      }
      this.dialogRef.close();
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.UPDATE_DEPARTMENT_MC_SYSTEM_FAILED,
        departmentActions.ADD_DEPARTMENT_MC_SYSTEM_FAILED,
        departmentActions.DELETE_DEPARTMENT_MC_SYSTEM_FAILED,
      ),
    ).subscribe((action: any) => {
      this.dialogRef.close();
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(commonsActions.GET_CODE_LIST_ITEMS_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe((payload) => {
      switch (payload.codeList) {
        case CODE_LIST.DEPARTMEN_MC_SYSTEM_TYPE:
          this.participanttypes = payload.data.items;
          break;
      }
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.loadData();
    this.initialFormValue = _.cloneDeep(this.frm.value);
  }

  ngAfterViewInit() {
    this.dialogRef.backdropClick().subscribe(() => {
      this.cancel();
    });
  }

  loadData() {
    if (this.data.departmentMcSystemId) {
      this.store.dispatch(new departmentActions.GetDepartmentMcSystemDetail(this.data));
    }
    this.store.dispatch(new mcSystemActions.GetMcSystemCodeList({
      activeOnly: false,
      mcSystemTypeId: null
    }));
    this.store.dispatch(new commonsActions.GetCodeListItems(CODE_LIST.DEPARTMEN_MC_SYSTEM_TYPE));
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  onSubmit() {
    const {value, valid} = this.frm;
    this.frm.markAsDirty();
    this.controlConfig.dateRange.markAsDirty();
    
    if (!valid) {
      return;
    }

    value['validFrom'] = value.dateRange.start;
    value['validTill'] = value.dateRange.end;

    delete value['dateRange'];
    this.submitted = true;
    if (value.id) {
      this.store.dispatch(new departmentActions.UpdateDepartmentMcSystem({
        departmentId: this.data.departmentId,
        departmentMcSystemId: value.id,
        departmentMcSystem: value
      }));
    } else {
      delete value['id'];
      delete value['version'];

      this.store.dispatch(new departmentActions.AddDepartmentMcSystem({
        departmentId: this.data.departmentId,
        departmentMcSystem: value
      }));
    }
  }

  delete() {
    this.confirmModalService
      .show(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_MC_SYSTEM_MSG'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new departmentActions.DeletedDepartmentMcSystem({
            departmentId: this.data.departmentId,
            departmentMcSystemId: this.data.departmentMcSystemId,
          }));
        }
      }, 
      this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.DELETE_DEPARTMENT_MC_SYSTEM_TITLE'));
  }

  cancel() {
    if (!_.isEqual(this.initialFormValue, this.frm.value)) {
      const subject = new Subject<boolean>();
      this.deactiveWarningModalService.subject = subject;

      subject.pipe(takeUntil(this.unSubAll)).subscribe((result) => {
        if (result) {
          this.dialogRef.close();
        }
      });
      this.deactiveWarningModalService.show();
    } else {
      this.dialogRef.close();
    }
  }

}
