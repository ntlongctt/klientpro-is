import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentMcSystemComponent } from './department-mc-system.component';

describe('DepartmentMcSystemComponent', () => {
  let component: DepartmentMcSystemComponent;
  let fixture: ComponentFixture<DepartmentMcSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentMcSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentMcSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
