import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AppState } from '@store/store.reducers';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import * as departmentActions from '@store/department/department.actions';
import { Subject } from 'rxjs/Subject';
import { takeUntil, map } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { DepartmentMcSystemDetailComponent } from '../department-mc-system-detail/department-mc-system-detail.component';

@Component({
  selector: 'department-mc-system',
  templateUrl: './department-mc-system.component.html',
  styleUrls: ['./department-mc-system.component.scss']
})
export class DepartmentMcSystemComponent implements OnInit, OnDestroy {
  @Input() departmentId;

  unSubAll = new Subject();

  displayedColumns = ['mcSystemName', 'participationTypeName', 'validFrom', 'validTill'];
  dataSource = [];
  keySearch = '';

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private dialog: MatDialog
  ) { 
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENT_MC_SYSTEMS_SUCCEEDED),
      map((action: any) => action.payload.participations)
    ).subscribe(items => {
      this.dataSource = items;
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        departmentActions.UPDATE_DEPARTMENT_MC_SYSTEM_SUCCEEDED,
        departmentActions.ADD_DEPARTMENT_MC_SYSTEM_SUCCEEDED,
        departmentActions.DELETE_DEPARTMENT_MC_SYSTEM_SUCCEEDED,
      ),
    ).subscribe(() => {
      this.loadData();
    });

  }

  ngOnInit() {
  }
  
  loadData() {
    this.store.dispatch(new departmentActions.GetDepartmentMcSystems({
      departmentId: this.departmentId,
      query: this.keySearch
    }));
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  filterChange(keywork) {
    if (this.keySearch === keywork) {
      return;
    }
    this.keySearch = keywork;
    this.loadData();
  }

  rowClick(row) {
    this.dialog.open(DepartmentMcSystemDetailComponent, {
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        departmentId: this.departmentId,
        departmentMcSystemId: row.id
      },
    } as MatDialogConfig);
  }

  newSystem() {
    this.dialog.open(DepartmentMcSystemDetailComponent, {
      autoFocus: false, // disable focus button on modal
      disableClose: true,
      data: {
        departmentId: this.departmentId,
      },
    } as MatDialogConfig);
  }

}
