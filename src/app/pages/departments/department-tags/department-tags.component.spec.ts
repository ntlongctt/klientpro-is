import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentTagsComponent } from './department-tags.component';

describe('DepartmentTagsComponent', () => {
  let component: DepartmentTagsComponent;
  let fixture: ComponentFixture<DepartmentTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
