import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'department-tags',
  templateUrl: './department-tags.component.html',
  styleUrls: ['./department-tags.component.scss']
})
export class DepartmentTagsComponent implements OnInit {

  @Input() departmentId;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  onGotoManageTags() {
    this.router.navigate(['business-partners', 100222, 'department', '115', 'edit', 'Department', 'tags']);
  }

  onCancel() {
    // this.cancel();
  }

  onSelectedChange(event) {
    
  }

}
