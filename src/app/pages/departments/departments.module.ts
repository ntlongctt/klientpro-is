import { NgModule } from '@angular/core';
import { DepartmentDetailComponent } from './department-detail/department-detail.component';
import { AppSharedModule } from '@common/shared';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { RichTextEditorModule } from '@common/modules/rich-text-editor/rich-text-editor.module';
import { BusinessPartnerListComponent } from './business-partner-list/business-partner-list.component';
import { MatTableModule, MatDatepickerModule } from '@angular/material';
import { SuperirorDepartmentComponent } from './superiror-department/superiror-department.component';
import { BusinessPartnerDialogComponent } from './business-partner-dialog/business-partner-dialog.component';
import { DepartmentAddressesComponent } from './department-addresses/department-addresses.component';
import { BpAddressesListModule } from '@common/modules/bp-addresses-list/bp-addresses-list.module';
import { DepartmentAddressDetailComponent } from './department-address-detail/department-address-detail.component';
import { BpAddressDetailFormModule } from '@common/modules/bp-address-detail-form/bp-address-detail-form.module';
import { BpAddressListDialogComponent } from './department-address-detail/bp-address-list-dialog/bp-address-list-dialog.component';
import { EditBpAddressDialogComponent } from './department-address-detail/edit-bp-address-dialog/edit-bp-address-dialog.component';
import { BpContactPersonsModule } from '@common/modules/bp-contact-persons/bp-contact-persons.module';
import { DepartmentContactPersonComponent } from './department-contact-person/department-contact-person.component';
import { DepartmentContactPersonDetailComponent } from './department-contact-person-detail/department-contact-person-detail.component';
import { BpContactPersonDetailFormModule } from '@common/modules/bp-contact-person-detail-form/bp-contact-person-detail-form.module';
import { SelectContactPersonComponent } from './department-contact-person-detail/select-contact-person/select-contact-person.component';
import { EditContactPersonComponent } from './department-contact-person-detail/edit-contact-person/edit-contact-person.component';
import { MatSelectSearchModule } from '@common/modules/mat-select-search';
import { InputSearchModule } from '@common/modules/input-search';
import { DepartmentTagsComponent } from './department-tags/department-tags.component';
import { TagsModule } from '@common/modules/tags/tags.module';
import { DepartmentMcSystemComponent } from './department-mc-system/department-mc-system.component';
import { DepartmentMcSystemDetailComponent } from './department-mc-system-detail/department-mc-system-detail.component';
import { BreadscumbModule } from '@common/modules/breadcrumbs/breadcrumbs.module';
import { DateRangePickerModule } from '@common/modules/date-range-picker/date-range-picker.module';
import { ListSubordinateDepartmentComponent } from './list-subordinate-department/list-subordinate-department.component';

@NgModule({
  imports: [
    AppSharedModule,
    BreadscumbModule,
    RichTextEditorModule,
    MatTableModule,
    MatDatepickerModule,
    BpAddressesListModule,
    BpAddressDetailFormModule,
    BpContactPersonsModule,
    BpContactPersonDetailFormModule,
    InputSearchModule,
    TagsModule,
    MatSelectSearchModule,
    InputSearchModule,
    DateRangePickerModule
  ],
  entryComponents: [
    BusinessPartnerListComponent,
    SuperirorDepartmentComponent,
    BusinessPartnerDialogComponent,
    DepartmentAddressDetailComponent,
    BpAddressListDialogComponent,
    EditBpAddressDialogComponent,
    DepartmentContactPersonDetailComponent,
    SelectContactPersonComponent,
    EditContactPersonComponent,
    DepartmentMcSystemDetailComponent
  ],
  declarations: [
    DepartmentDetailComponent,
    BasicInfoComponent,
    BusinessPartnerListComponent,
    SuperirorDepartmentComponent,
    BusinessPartnerDialogComponent,
    DepartmentAddressesComponent,
    DepartmentAddressDetailComponent,
    BpAddressListDialogComponent,
    BusinessPartnerDialogComponent,
    EditBpAddressDialogComponent,
    DepartmentContactPersonComponent,
    DepartmentContactPersonDetailComponent,
    SelectContactPersonComponent,
    EditContactPersonComponent,
    DepartmentTagsComponent,
    DepartmentMcSystemComponent,
    DepartmentMcSystemDetailComponent,
    ListSubordinateDepartmentComponent
  ],
  exports: [
    DepartmentDetailComponent,
    BasicInfoComponent,
    BusinessPartnerListComponent,
    SuperirorDepartmentComponent,
    BusinessPartnerDialogComponent,
    DepartmentAddressesComponent,
    DepartmentAddressDetailComponent,
    BpAddressListDialogComponent,
    BusinessPartnerDialogComponent,
    EditBpAddressDialogComponent
  ]
})
export class DepartmentsModule {}
