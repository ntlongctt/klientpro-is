import { DepartmentDetailComponent } from './department-detail/department-detail.component';
import { CanDeactivateGuard } from '@common/services/deactivate-guard';
import { getTagRouting } from '@common/modules/tags/tag.routing';

export function getDepartmentsRouting() {
  return [
    {
      path: `department`,
      children: [
        {
          path: ':departmentId/edit',
          data: {'title': 'DEPARTMENTS.DEPARTMENT_DETAIL_TITLE'},
          // component: DepartmentDetailComponent,
          // canDeactivate: [CanDeactivateGuard],
          children: [
            {
              path: '',
              component: DepartmentDetailComponent,
              // canDeactivate: [CanDeactivateGuard],
            },
            // Import tag management routing
            ...getTagRouting(':entityName')
          ]
        },
        {
          path: 'create',
          data: {'title': 'DEPARTMENTS.DEPARTMENT_DETAIL_TITLE'},
          component: DepartmentDetailComponent,
          canDeactivate: [CanDeactivateGuard]
        },
        {
          path: 'parent/:parentId/create',
          data: {'title': 'DEPARTMENTS.DEPARTMENT_DETAIL_TITLE'},
          component: DepartmentDetailComponent,
          canDeactivate: [CanDeactivateGuard]
        },
      ]
    },
  ];
}
