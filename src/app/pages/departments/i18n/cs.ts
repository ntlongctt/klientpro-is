export const locale = {
  lang: 'cs',
   data: {
    DEPARTMENTS: {
      TITLE: 'Zařízení',
      DEPARTMENT_DETAIL_TITLE: 'Detail zařízení',
      CHANGE_BUSINESS_PARTNER_TITLE: 'Změnit obchodního partnera',
      CHANGE_BUSINESS_PARTNER_OPTION_1: 'zachovat původní vazbu, u nového partnera od',
      CHANGE_BUSINESS_PARTNER_OPTION_2: 'ihned, nahradit vazbu na stávajícího partnera',
      SEARCH_BUSINESS_PARTNER: 'Najít obchodního partnera',
      SEARCH_DEPARTMENT: 'Najít zařízení',
      CHOOSE_A_DATE: 'Vybrat datum',
      CHANGE_SUPERIOR_DEPARTMENT: 'Vybrat nadřazené zařízení',
      SHOW_DEPARTMENT_CHECK_BOX: 'Zobrazit zařízení jiných partnerů',
      DEFAULT_RESULT_SEARCH: '---Nejvyšší úroveň - bez nadřazeného zařízení---',
	  EDIT_BP_ADDRESS_TITLE: 'Adresa obchodního partnera',
      FILTER_CONTACT_PERSONS: 'Hledat osobu',
	  FILTER_SUB_ORDINATE: 'Hledat zařízení',
      EDIT_BP_CONTACT_TITLE: 'Kontaktní osoba obchodního partnera',
      EDIT_BP_ADDRESS_DETAIL_TITLE: 'Detail adresy obchodního partnera',
	  EDIT_BP_CONTACT_DETAIL_TITLE: 'Detail kontaktní osoby obchodního partnera',
      DETAILS: {
        BASIC_INFO: {
          LABEL: 'Základní informace',
          NAME: 'Název',
          SHORTCUT: 'Zkratka',
          PARENT_DEPARTMENT_NAME: 'Nadřazené zařízení:',
          BUSINESS_PARTNER: 'Obchodní partner:',
          DEPARTMENT_TYPE: 'Typ zařízení',
          SPECIALTY: 'Odbornost',
          AGE_FROM: 'Věk od',
          AGE_TILL: 'do',
          PUBLIC_NOTE: 'Poznámka pro veřejnost',
          PRIVATE_NOTE: 'Vnitřní poznámka',
          PARTNER_NOTE: 'Poznámka pro partnery',
          BUSINESS_PARTNER_NAME: 'Obchodní partner',
          BUSINESS_PARTNER_VALID_FROM: 'Od',
          BUSINESS_PARTNER_VALID_TILL: 'Do',
		  SEE_MORE_PARTNER: 'zobrazit vše'
        },
        ADDRESSES: {
          LABEL: 'Adresy',
		  TITLE: 'Adresa zařízení',
		  ALTERNATIVE_RECIPIENT_NAME: 'Alternativní jméno příjemce',
          STATE: 'Stát',
          CITY: 'Město',
          ZIP_CODE: 'PSČ',
          STREET: 'Ulice',
          STREET_NUMBER: 'Číslo domu',
          STREET_SUPLEMENT: 'Doplněk ulice',
          MAP_LINK: 'Odkaz do mapy',
          NOTE: 'Poznámka',
          TRANSPORT_INFO: 'Dopravní spojení',
		  ADDRESS_TYPE: 'Typ adresy',
          ACTIVE: 'Aktivní',
          INVOIDE_ADDRESS: 'Fakturační',
          POSTAL_ADDRESS: 'Korespondenční',
        },
        CONTACT_PERSONS: {
          LABEL: 'Kontaktní osoby',
          TITLE: 'Kontaktní osoba zařízení',
          SALUTATION: 'Oslovení',
          NAME: 'Jméno',
          POSITION: 'Pozice',
          PHONE: 'Telefon',
          PHONE_2: 'Telefon 2',
          SMS_SUPORT: 'SMS',
          MMS_SUPORT: 'MMS',
          EMAIL: 'E-mail',
          WEBSITE: 'Web',
          OTHER_CONTACT: 'Další kontakty',
          NOTE: 'Poznámka',
          ADDRESS: 'Adresa',
          IS_ACTIVE: 'Aktivní',
        },
        TAGS: {
          LABEL: 'Nálepky'
        },
        PARTICIPATION: {
          LABEL: 'Účast',
		  MANAGED_CARE_SYSTEM: 'Kontrakt / systém',
          PARTICIPATION_TYPE: 'Typ účasti',
          FROM: 'Od',
          TILL: 'Do',
          DETAIL_TITLE: 'Účast v kontraktu / systému řízené péče'
        },
        SUBORDINATE_DEPARTMENTS: {
          LABEL: 'Podřízená zařízení',
          SUBORDINATE_DEPARTMENTS: 'Podřízená zařízení',
          TYPE: 'Typ',
          SPECIALTY: 'Odbornost',
          FROM: 'Od',
          TILL: 'Do',
        },
        BUSINESS_HOURS: {
          LABEL: 'Provozní doba'
        },

      },
      FORM_VALIDATOR: {
        NAME_REQUIRED: 'Název je povinný',
        AGE_RANGE_INVALID: 'Neplatný rozsah pro věk',
        AGE_INVALID: 'Věk musí být mezi 0 a 99',
        DEPARTMENT_REQUIRED: 'Zařízení je poviné',
        DEPARTMENT_TYPE_REQUIRED: 'Typ zařízení je povinný',
		BP_ADDRESS_REQUIRED: 'Vyberte adresu obchodního partnera',
		BP_CONTACT_PERSON_REQUIRED: 'Vyberte kontaktní osobu obchodního partnera',
		VALID_FROM_REQUIRED: 'Počáteční datum je povinné',
		SYSTEM_REQUIRED: 'Zadejte systém / kontrakt',
        PARTICIPATION_TYPE_REQUIRED: 'Typ účasti je povinný',
		DATE_CHANGE_REQUIRED: 'Zadejte datum změny',
        BP_REQUIRED: 'Zvolte obchodního partnera'
      },
      MESSAGE: {
        SHOW_CHANGE_BUSINESS_PARTNER_TITLE: 'Změna obchodního partnera',
        SHOW_CHANGE_BUSINESS_PARTNER_MSG: 'Při přesunu zařízení pod jiného obchodního partnera se ztratí přiřazené adresy a kontaktní osoby! Přesto pokračovat?',
        ADD_DEPARTMENT_SUCCESS: 'Zařízení bylo vytvořeno',
        UPDATE_DEPARTMENT_SUCCESS: 'Zařízení bylo aktualizováno',
        DELETE_DEPARTMENT_SUCCESS: 'Zařízení bylo vymazáno',
        DELETE_DEPARTMENT_MESSAGE: 'Vymazat zařízení?',
        DELETE_DEPARTMENT_TITLE: 'Potvrdit výmaz zařízení',
		DELETE_DEPARMENT_CONTACT_MESSAGE: 'Vymazat tuto kontaktní osobu?',
        DELETE_DEPARMENT_CONTACT_TITLE: 'Potvrzení výmazu kontaktní osoby',
		DELETE_DEPARMENT_CONTACT_SUCCESS: 'Kontaktní osoba zařízení byla odebrána',																			 
		UPDATE_DEPARTMENT_MC_SYSTEM_SUCCESS: 'Účast byla aktualizována',
        ADD_DEPARTMENT_MC_SYSTEM_SUCCESS: 'Účast byla přidána',
        DELETE_DEPARTMENT_MC_SYSTEM_SUCCESS: 'Účast byla vymazána',
        DELETE_DEPARTMENT_MC_SYSTEM_TITLE: 'Potvrzení výmazu účasti',
        DELETE_DEPARTMENT_MC_SYSTEM_MSG: 'Vymazat účast zařízení?',
		ADD_DEPARMENT_CONTACT_SUCCESS: 'Kontaktní osoba byla přidána',
		UPDATE_DEPARTMENT_ADDRESS_SUCCESS: 'Adresa zařízení byla aktualizována',
        ADD_DEPARTMENT_ADDRESS_SUCCESS: 'Adresa zařízení byla přidána',
        DELETE_DEPARTMENT_ADDRESS_TITLE: 'Potvrďte odebrání adresy zařízení',
        DELETE_DEPARTMENT_ADDRESS_CONFIRM: 'Odebrat tuto adresu',
        DELETE_DEPARTMENT_ADDRESS_SUCCESS: 'Adresa zařízení byla odebrána',
      },
	  BUTTON: {
        ADD_NEW_ADDRESS: 'Nová adresa',
        ADD_NEW_CONTACT_PERSON: 'Nový kontakt',
        ADD_NEW_PARTICIPATION: 'Nová účast',
		ADD_NEW_SUB_ORDINATE: 'Nové zařízení',
        ADD_NEW_BP_ADDRESS: 'Nová adresa',
        ADD_NEW_BP_CONTACT: 'Nový kontakt',
		
      },
      TOOLTIP: {
        CHANGE_BP_PARTNER: 'Obchodní partner dle nadřízeného zařízení'
											  
												   
																					 
																				
																				
																		   
																	   
																				 
																				
				   
											  
								  
								  
      },
	  CHECKBOX: {
        ACTIVE: 'Aktivní v zařízení',
        PARTNER: 'Pro partnery',
        PUBLIC: 'Pro veřejnost'
      }
    }
  }
};
