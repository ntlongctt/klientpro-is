import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSubordinateDepartmentComponent } from './list-subordinate-department.component';

describe('ListSubordinateDepartmentComponent', () => {
  let component: ListSubordinateDepartmentComponent;
  let fixture: ComponentFixture<ListSubordinateDepartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSubordinateDepartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSubordinateDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
