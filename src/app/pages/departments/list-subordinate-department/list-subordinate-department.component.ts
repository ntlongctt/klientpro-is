import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { ActivatedRoute, Router } from '@angular/router';
import * as departmentActions from '@store/department/department.actions';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil, map } from 'rxjs/operators';

@Component({
  selector: 'list-subordinate-department',
  templateUrl: './list-subordinate-department.component.html',
  styleUrls: ['./list-subordinate-department.component.scss']
})
export class ListSubordinateDepartmentComponent implements OnInit, OnDestroy {

  @Input() departmentId;
  @Input() partnerId;

  displayedColumns = ['name', 'typeName', 'specialtyName', 'validFrom', 'validTill'];

  dataSource = [];

  unSubAll = new Subject();

  keySearch = '';

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(departmentActions.GET_DEPARTMENTS_SUCCEEDED),
      map((action: any) => action.payload.departments)
    ).subscribe(items => {
      this.dataSource = items;
    });
   }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.partnerId = this.activatedRoute.snapshot.params['partnerId'];
    this.store.dispatch(new departmentActions.GetDepartments({
      query: this.keySearch,
      bpId: this.partnerId,
      departmentId: this.departmentId
    }));
  }

  filterChange(keywork) {
    if (this.keySearch === keywork) {
      return;
    }
    this.keySearch = keywork;
    this.loadData();
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }
  onRowclick(row) {
    this.router.navigate(['business-partners', this.partnerId, 'department', row.id, 'edit']);
  }

}
