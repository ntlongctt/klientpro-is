import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperirorDepartmentComponent } from './superiror-department.component';

describe('SuperirorDepartmentComponent', () => {
  let component: SuperirorDepartmentComponent;
  let fixture: ComponentFixture<SuperirorDepartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperirorDepartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperirorDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
