import { Component, OnInit, Optional, Inject, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { BaseComponent } from '@common/modules/base-component';
import { FormControl, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { DepartmentService } from '@klient/rest/services/pa/Department.service';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { Util } from '@common/services/util';

@Component({
  selector: 'superiror-department',
  templateUrl: './superiror-department.component.html',
  styleUrls: ['./superiror-department.component.scss'],
})
export class SuperirorDepartmentComponent extends BaseComponent implements OnInit, AfterViewInit {

  isShowDepartment = false;
  isLoading = false;
  departments = [];
  defaultItem = {
    id: 'null',
    name: this.translationLoaderService.instant('DEPARTMENTS.DEFAULT_RESULT_SEARCH')
  };
  bpChangeDate;
  isSendDate = false;
  keySearch = '';
  displayFields = ['businessPartnerName', 'name', 'typeName', 'specialtyName'];
  submited = false;
  showError = false;
  option = '1';

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<SuperirorDepartmentComponent>,
    private translationLoaderService: FuseTranslationLoaderService,
    private departmentService: DepartmentService,
    private confirmModalService: ConfirmModalService,
    private trans: TranslateService,
    private util: Util
  ) {
    super();
    this.translationLoaderService.loadTranslations(en, cs);
    this.controlConfig = {
      department: new FormControl('', [Validators.required])
    };

    this.departments = _.uniqBy([this.defaultItem, ...this.departments], 'id');

    this.trans.onLangChange.subscribe(() => {
      this.defaultItem.name = this.translationLoaderService.instant('DEPARTMENTS.DEFAULT_RESULT_SEARCH');
    });
   }

  ngOnInit() {
    super.ngOnInit();
  }

  onClick(event) {
    event.preventDefault();
    this.onCheckBoxChange(!this.isShowDepartment);
  }

  ngAfterViewInit() {
  }

  onSearchState(keySearch) {

    if (this.keySearch === keySearch || keySearch === '') {
      return;
    }

    if (keySearch.length < 3) {
      return;
    }

    const objectSearch = this.isShowDepartment ? {bpId: null, departmentId: null} :  {bpId: this.data.bpId, departmentId: null};

    this.keySearch = keySearch;
    this.departmentService.getDepartments({
      ...objectSearch,
      query: keySearch.trim()
    }).subscribe(resp => {
      this.departments = _.uniqBy([this.defaultItem, ...resp.departments], 'id');
      this.isLoading = false;
    });
  }

  onCheckBoxChange(event) {
    if (event) {
      this.confirmModalService
      .show(this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.SHOW_CHANGE_BUSINESS_PARTNER_MSG'), (status) => {
        if (status) {
          this.isShowDepartment = true;
        } else {
          this.isShowDepartment = false;
        }
      },
      this.translationLoaderService.instant('DEPARTMENTS.MESSAGE.SHOW_CHANGE_BUSINESS_PARTNER_TITLE'));
    } else {
      this.isShowDepartment = false;
    }
    this.frm.reset();
    this.keySearch = '';
    this.departments = _.uniqBy([this.defaultItem, []], 'id');
  }

  close(status) {
    if (status) {
      const {valid, value} = this.frm;
      this.frm.markAsDirty();
      if (this.option === '1' && !this.bpChangeDate && this.isShowDepartment) {
        this.showError = true;
        if (!valid || !this.bpChangeDate) {
          return;
        }
      }
      if (!valid) {
        return;
      }
      this.dialogRef.close({
        parentDepartmentId: this.controlConfig.department.value,
        parentDepartmentName: this.controlConfig.department.value !== 'null' ? this.departments.find(d => d.id === this.controlConfig.department.value).name : null,
        bpChangeDate: this.isSendDate ? this.bpChangeDate : null
      });
    } else {
      this.dialogRef.close();
    }
  }

  onDateChange(value) {
    this.bpChangeDate = this.util.formatDate(value, false);
    this.isSendDate = true;
    this.showError = value ? false : true;
  }

  onChangeOption(option) {
    this.isSendDate = option.value === '1' ? true : false;
    if (option.value === '2') {
      this.showError = false;
    } else if (!this.bpChangeDate) {
      this.showError = true;
    }
  }

}
