export const locale = {
  lang: 'cs',
  data: {
    MC_SYSTEM: {
       TITLE: 'Systémy řízené péče / kontrakty',
      SYSTEM_DETAIL_TITLE: 'Detail systému / kontraktu',
      BUTTON: {
        NEW_SYSTEM: 'Nový systém / kontrakt'
      },
      TABLE: {
        COL_NAME: 'Název',
        COL_TYPE: 'Typ',
        COL_AREA: 'Oblast',
        COL_VALID_FROM: 'Od',
        COL_VALID_TILL: 'Do',
      },
      DETAIL: {
        DEPARTMENT_NAME: 'Název',
        SHORTCUT: 'Zkratka',
        SYSTEM_TYPE: 'Typ',
        AREA: 'Oblast',
        ACCOUNT_CODE: 'Účetní kód',
        PARENT_MC_SYSTEMS: 'Spadá pod',
        VALID_FROM: 'Platný od',
        VALID_TILL: 'Platný do',
        NOTE: 'Poznámka',
		SEARCH_SYSTEM: 'Vyhledat systém / kontrakt'
      },
      FORM_VALIDATOR: {
        SYSTEM_NAME_REQUIRED: 'Název systému je povinný',
        SYSTEM_SHORTCUT_REQUIRED: 'Zkratka je povinná',
        TYPE_REQUIRED: 'Typ systému je povinný',
        PARENT_INVALID: 'Rodičovský systém je neplatný',
		VALID_FROM_REQUIRED: 'Počáteční datum je povinné',
      },
      MESSAGE: {
        CREATE_SYSTEM_SUCCESS: 'Nový systém / kontrakt byl vytvořen',
        UPDATE_SYSTEM_SUCCESS: 'Systém / kontrakt byl aktualizován',
        NO_SYSTEM: 'Neexistují žádné systémy / kontrakty',
        DELETE_SYSTEM_MESSAGE: 'Vymazat systém / kontrakt',
        DELETE_SYSTEM_TITLE: 'Potvrzení výmazu systému řízené péče / kontraktu',
        DELETE_SYSTEM_SUCCESS: 'Systém / kontrakt byl vymazán',
      }
    }
  }
};
