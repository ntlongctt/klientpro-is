export const locale = {
  lang: 'en',
  data: {
    MC_SYSTEM: {
      TITLE: 'Managed care systems',
      SYSTEM_DETAIL_TITLE: 'System detail',
      BUTTON: {
        NEW_SYSTEM: 'New system'
      },
      TABLE: {
        COL_NAME: 'Name',
        COL_TYPE: 'Type',
        COL_AREA: 'Area',
        COL_VALID_FROM: 'From',
        COL_VALID_TILL: 'Till',
      },
      DETAIL: {
        DEPARTMENT_NAME: 'Name',
        SHORTCUT: 'Shortcut',
        SYSTEM_TYPE: 'Type',
        AREA: 'Area',
        ACCOUNT_CODE: 'Account code',
        PARENT_MC_SYSTEMS: 'Is part of',
        VALID_FROM: 'Valid from',
        VALID_TILL: 'Valid till',
        NOTE: 'Note',
        SEARCH_SYSTEM: 'Search for system'
      },
      FORM_VALIDATOR: {
        SYSTEM_NAME_REQUIRED: 'System name is required',
        SYSTEM_SHORTCUT_REQUIRED: 'System shortcut is required',
        TYPE_REQUIRED: 'System type is required',
        PARENT_INVALID: 'Parent System invalid',
        VALID_FROM_REQUIRED: 'From is required',
      },
      MESSAGE: {
        CREATE_SYSTEM_SUCCESS: 'Mc system created success',
        UPDATE_SYSTEM_SUCCESS: 'Mc system updated success',
        NO_SYSTEM: 'There is no systems',
        DELETE_SYSTEM_MESSAGE: 'Are you sure to delete this system',
        DELETE_SYSTEM_TITLE: 'Delete system confirmation',
        DELETE_SYSTEM_SUCCESS: 'Mc system deleled success',
      }
    }
  }
};
