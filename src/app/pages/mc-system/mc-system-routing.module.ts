import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { McSystemComponent } from './mc-system.component';
import { SystemDetailComponent } from './system-detail/system-detail.component';
import { CanDeactivateGuard } from '@common/services/deactivate-guard';

const routes: Routes = [
  {
    path: '',
    component: McSystemComponent
  },
  {
    path: 'create',
    component: SystemDetailComponent,
    canDeactivate: [CanDeactivateGuard],
    data: {'title': 'MC_SYSTEM.SYSTEM_DETAIL_TITLE'},
  },
  {
    path: ':systemId/edit',
    data: {'title': 'MC_SYSTEM.SYSTEM_DETAIL_TITLE'},
    component: SystemDetailComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [CanDeactivateGuard],
  exports: [RouterModule]
})
export class McSystemRoutingModule { }
