import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McSystemComponent } from './mc-system.component';

describe('McSystemComponent', () => {
  let component: McSystemComponent;
  let fixture: ComponentFixture<McSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
