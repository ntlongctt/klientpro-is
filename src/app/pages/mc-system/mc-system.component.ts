import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from './i18n/en';
import { locale as cs} from './i18n/cs';
import { McSystemDataSource } from './mc-system.datasource';
import { McSystemService } from '@klient/rest/services/pa/McSystem.service';
import { PAGING_DEFAULT } from '@common/constants/paging-default';
import { tap, takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { Route } from '@angular/compiler/src/core';
import { RouterService } from '@common/modules/route-caching';
import * as systemActions from '@store/mc-system/mc-system.actions';
import { Actions, ofType } from '@ngrx/effects';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'mc-system',
  templateUrl: './mc-system.component.html',
  styleUrls: ['./mc-system.component.scss']
})
export class McSystemComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('tablePaginator') paginator: MatPaginator;
  @ViewChild(FusePerfectScrollbarDirective) directiveScroll: FusePerfectScrollbarDirective;
  public dataSource: McSystemDataSource;
  public displayedColumns = ['name', 'mcSystemTypeName', 'areaName', 'validFrom', 'validTill'];
  public unSubAll = new Subject();

  constructor(
    private routerService: RouterService,
    private translationLoaderService: FuseTranslationLoaderService,
    private mcSystemService: McSystemService,
    private actions$: Actions
  ) {
    this.translationLoaderService.loadTranslations(en, cs);

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        systemActions.ADD_MC_SYSTEM_SUCCEEDED,
        systemActions.UPDATE_MC_SYSTEM_SUCCEEDED,
        systemActions.DELETE_MC_SYSTEM_SUCCEEDED,
      )
    ).subscribe(() => {
      this.loadSystemPage();
    });
   }

  ngOnInit() {
    this.dataSource = new McSystemDataSource(this.mcSystemService);
    this.dataSource.loadMcsystems(PAGING_DEFAULT.START, PAGING_DEFAULT.LIMIT);
  }

  ngAfterViewInit(): void {
    if (this.paginator) {
      this.paginator.page
        .pipe(
          tap(() => this.loadSystemPage())
        )
        .subscribe();
    }
  }

  loadSystemPage() {
    this.dataSource.loadMcsystems(
      this.paginator.pageIndex * this.paginator.pageSize,
      this.paginator.pageSize
    );
    // Make perfect scrollbar scroll to top list also update content scroll height after change page, page size, ...
    setTimeout(() => {
      this.directiveScroll.scrollToTop();
      this.directiveScroll.update();
    });
  }

  addNew() {
    this.routerService.navigate(['managed-care-systems', 'create'], {}, {
      enableBackView: true,
      scrollElement: this.directiveScroll
    });
  }

  goToDetail(row) {
    this.routerService.navigate(['managed-care-systems', row.id, 'edit'], {}, {
      enableBackView: true,
      scrollElement: this.directiveScroll
    });
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

}
