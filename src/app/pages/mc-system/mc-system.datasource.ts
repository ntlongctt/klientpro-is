import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import { catchError, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/internal/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PAGING_DEFAULT } from '@common/constants/paging-default';
import { McSystemListItemDTO } from '@klient/rest/models/pa/mcsystem/McSystemListItemDTO';
import { McSystemService } from '@klient/rest/services/pa/McSystem.service';

export class McSystemDataSource implements DataSource<McSystemListItemDTO> {

  private dataSubject = new BehaviorSubject<McSystemListItemDTO[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public totalCount = 0;

  constructor(
    private mcSystemService: McSystemService
  ) {

  }

  connect(collectionViewer: CollectionViewer): Observable<McSystemListItemDTO[]> {
      return this.dataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
      this.dataSubject.complete();
      this.loadingSubject.complete();
  }

  loadMcsystems(start = PAGING_DEFAULT.START, limit = PAGING_DEFAULT.LIMIT) {

      this.loadingSubject.next(true);

      this.mcSystemService.getMcSystems({
        limit: limit,
        start: start
      }).pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
      )
      .subscribe((systems: any) => {
        this.totalCount = systems.totalCount;
        this.dataSubject.next(systems.mcSystems);
      });
  }

  clear() {
    this.dataSubject.next([]);
  }
}
