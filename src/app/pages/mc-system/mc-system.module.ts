import { NgModule } from '@angular/core';
import { McSystemRoutingModule } from './mc-system-routing.module';
import { McSystemComponent } from './mc-system.component';
import { AppSharedModule } from '@common/shared';
import { MatTableModule, MatPaginatorIntl } from '@angular/material';
import { CustomMatPaginatorIntl } from '@common/services/custom-paginator/CustomMatPaginatorIntl';
import { SystemDetailComponent } from './system-detail/system-detail.component';
import { MatSelectSearchModule } from '@common/modules/mat-select-search';
import { BreadscumbModule } from '@common/modules/breadcrumbs/breadcrumbs.module';
import { DateRangePickerModule } from '@common/modules/date-range-picker/date-range-picker.module';

@NgModule({
  imports: [
    AppSharedModule,
    McSystemRoutingModule,
    BreadscumbModule,
    MatTableModule,
    MatSelectSearchModule,
    DateRangePickerModule
  ],
  declarations: [McSystemComponent, SystemDetailComponent],
  providers: [
    { provide: MatPaginatorIntl, useClass: CustomMatPaginatorIntl },
  ]
})
export class McSystemModule { }
