import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { BaseComponent } from '@common/modules/base-component';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil, map, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { FormControl, Validators } from '@angular/forms';
import { CODE_LIST } from '@common/constants/code-list';
import * as mcSystemActions from '@store/mc-system/mc-system.actions';
import { McSystemService } from '@klient/rest/services/pa/McSystem.service';
import * as moment from 'moment';
import * as systemActions from '@store/mc-system/mc-system.actions';
import { ToastrService } from 'ngx-toastr';
import { RouterService } from '@common/modules/route-caching';
import * as fromFormActions from '@store/form/form.actions';
import * as _ from 'lodash';
import { AppStoreService } from '@store/store.service';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { INPUT_DEBOUNCE_TIME } from '@common/constants/input-debounce-time';
import { DateAdapter } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Util } from '@common/services/util';

@Component({
  selector: 'system-detail',
  templateUrl: './system-detail.component.html',
  styleUrls: ['./system-detail.component.scss']
})
export class SystemDetailComponent extends BaseComponent implements OnInit, OnDestroy {

  systemId;
  unSubAll = new Subject();
  mcSystemTypes = [];
  areas = [];
  parentMcSystems = [];
  isLoading = false;
  dateError = false;
  initialForm: any;
  currentFormStatus = false;
  systemName;
  submitted = false;

  /**
  * Before tab/browser close or refresh, check if form data has changed,show alter confirm
 */
  @HostListener('window:beforeunload', ['$event'])
  beforeunload(event) {
    const hasChange = this.appStoreService.getState().form.hasChanged;
    if (hasChange) {
      return false;
    }
  }

  constructor(
    private translationLoaderService: FuseTranslationLoaderService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private actions$: Actions,
    private mcSystemServicems: McSystemService,
    private toast: ToastrService,
    private router: RouterService,
    private appStoreService: AppStoreService,
    private confirmModalService: ConfirmModalService,
    private adapter: DateAdapter<any>,
    private translateService: TranslateService,
    private util: Util
  ) { 
    super();

    this.store.dispatch(new fromFormActions.ChangeFormStatus(false));

    this.controlConfig = {
      name: new FormControl('', [Validators.required]),
      shortcut: new FormControl('', [Validators.required]),
      mcSystemTypeId: new FormControl(''),
      areaId: new FormControl(''),
      accountCode: new FormControl(''),
      parentMcSystemId: new FormControl(''),
      note: new FormControl(''),
      id: new FormControl(''),
      version: new FormControl(''),
      dateRange: new FormControl({start: null, end: null}),
    };

    this.systemId = this.activatedRoute.snapshot.params['systemId'];
    if (this.systemId) {
      this.store.dispatch(new mcSystemActions.GetMcSystemDetail(this.systemId));
    }

    this.store.dispatch( new mcSystemActions.GetMcSystemTypes(CODE_LIST.MC_SYSTEM_TYPE));
    this.store.dispatch( new mcSystemActions.GetMcSystemAreas(CODE_LIST.AREA));

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(mcSystemActions.GET_MC_SYSTEM_TYPES_SUCCEEDED),
      map((action: any) => action.payload.items)
    ).subscribe(types => {
      this.mcSystemTypes = types;
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(mcSystemActions.GET_MC_SYSTEM_AREAS_SUCCEEDED),
      map((action: any) => action.payload.items)
    ).subscribe(types => {
      this.areas = types;
    });

    // get detail success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(mcSystemActions.GET_MC_SYSTEM_DETAIL_SUCCEEDED),
      map((action: any) => action.payload)
    ).subscribe(detail => {
      this.frm.patchValue(detail);
      this.frm.get('dateRange').patchValue({start: detail.validFrom, end: detail.validTill});
      this.initialForm = this.frm.value;
      this.systemName = detail.name;
    });
    
    // Add system success
    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        mcSystemActions.UPDATE_MC_SYSTEM_SUCCEEDED,
        mcSystemActions.ADD_MC_SYSTEM_SUCCEEDED,
        mcSystemActions.DELETE_MC_SYSTEM_SUCCEEDED,
      ),
    ).subscribe(action => {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
      switch (action.type) {
        case systemActions.ADD_MC_SYSTEM_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('MC_SYSTEM.MESSAGE.CREATE_SYSTEM_SUCCESS'));
          break;
        case systemActions.UPDATE_MC_SYSTEM_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('MC_SYSTEM.MESSAGE.UPDATE_SYSTEM_SUCCESS'));
          break;
        case systemActions.DELETE_MC_SYSTEM_SUCCEEDED:
          this.toast.success(this.translationLoaderService.instant('MC_SYSTEM.MESSAGE.DELETE_SYSTEM_SUCCESS'));
          break;
      }
      this.router.navigate(['managed-care-systems']);
    });

    this.actions$.pipe(
      takeUntil(this.unSubAll),
      ofType(
        mcSystemActions.UPDATE_MC_SYSTEM_FAILED,
        mcSystemActions.ADD_MC_SYSTEM_FAILED,
        mcSystemActions.DELETE_MC_SYSTEM_FAILED,
      ),
    ).subscribe(() => {
      this.submitted = false;
    });

    // update translate for date picker
    this.translateService.onLangChange.subscribe(e => {
      this.adapter.setLocale(e.lang);
    });

  }

  ngOnInit() {
    super.ngOnInit();

    this.mcSystemServicems.getMcSystemsCodelist({
      activeOnly: false,
      mcSystemTypeId: null
    }).subscribe(resp => {
      this.parentMcSystems = resp.items;
      if (this.systemId && this.initialForm && this.initialForm.parentMcSystemId) {
        this.frm.get('parentMcSystemId').patchValue(this.initialForm.parentMcSystemId);
      }
    });

    this.translationLoaderService.loadTranslations(en, cs);
    this.frm.valueChanges.subscribe(() => {
      this.checkFormStatus();
    });
  }

  ngOnDestroy() {
    this.unSubAll.next();
    this.unSubAll.complete();
  }

  onSubmit() {
    const {valid, value} = this.frm;
    this.controlConfig.dateRange.markAsDirty();

    if (!valid) {
      return;
    }

    if (value.parentMcSystem) {
      value['parentMcSystemId'] = value.parentMcSystem.id;
    }
    delete value.parentMcSystem;

    value['validFrom'] = this.util.formatDate(value.dateRange.start, false);
    value['validTill'] = this.util.formatDate(value.dateRange.end, false);
    delete value.dateRange;

    this.submitted = true;
    if (this.systemId) {
      this.store.dispatch( new systemActions.UpdateMcSystem({
        mcSystemId: this.systemId,
        body: value
      }));
    } else {
      delete value.id;
      delete value.version;
      this.store.dispatch(new systemActions.AddMcSystem(value));
    }
  }

  delete() {
    this.confirmModalService
      .show(this.translationLoaderService.instant('MC_SYSTEM.MESSAGE.DELETE_SYSTEM_MESSAGE'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new systemActions.DeleteMcSystem(this.systemId));
        }
      },
      this.translationLoaderService.instant('MC_SYSTEM.MESSAGE.DELETE_SYSTEM_TITLE'));
  }

  cancel( ) {
    this.router.navigate(['managed-care-systems']);
  }

  /**
   * check if user form or privileges has changes, dispath to store
   */
  checkFormStatus() {
    const isFormChange = !_.isEqual(this.initialForm, this.frm.value) && this.frm.dirty;
    if (isFormChange !== this.currentFormStatus) {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(isFormChange));
      this.currentFormStatus = isFormChange;
    }
  }

  displayFn(item) {
    if (item) {
       return item.name;
    }
  }

}
