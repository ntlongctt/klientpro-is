import {
  NgModule
} from '@angular/core';
import { AppSharedModule } from '@common/shared';

import {
  NoPermissionComponent
} from './no-permission.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material';

@NgModule({
  declarations: [
    NoPermissionComponent
  ],
  imports: [
    AppSharedModule,
    RouterModule,
    MatIconModule
  ]
})
export class NoPermissionModule {
}
