export const locale = {
  lang: 'cs',
  data: {
    NOT_FOUND: {
      MESSAGE: 'Je nám líto, ale stránka, kterou se pokoušíte zobrazit, neexistuje. Pokud myslíte, že se jedná o chybu systému, kontaktujte administrátora.',
      GO_BACK_MESSAGE: 'Přejít na domovskou stránku'
    }
  }
};
