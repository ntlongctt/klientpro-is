export const locale = {
  lang: 'en',
  data: {
    NOT_FOUND: {
      MESSAGE: 'Sorry but we could not find the page you are looking for',
      GO_BACK_MESSAGE: 'Go back to dashboard'
    }
  }
};
