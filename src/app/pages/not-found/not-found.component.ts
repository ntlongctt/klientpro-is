import {
  Component
} from '@angular/core';
import { FuseConfigService } from '@fuse/services/config.service';
import { locale as en } from './i18n/en';
import { locale as cs } from './i18n/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

@Component({
  selector: 'not-found',
  templateUrl: 'not-found.template.html',
  styleUrls: [
    'not-found.style.scss'
  ]
})
export class NotFoundComponent {

  // TypeScript public modifiers
  constructor(
    private _fuseConfigService: FuseConfigService,
    private translationLoaderService: FuseTranslationLoaderService
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: false
        },
        toolbar: {
          hidden: false
        },
        footer: {
          hidden: true
        }
      }
    };

    this.translationLoaderService.loadTranslations(en, cs);
  }
}
