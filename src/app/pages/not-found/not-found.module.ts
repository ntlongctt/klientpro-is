import {
  NgModule
} from '@angular/core';
import { AppSharedModule } from '@common/shared';

import {
  NotFoundComponent
} from './not-found.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material';

@NgModule({
  declarations: [
    NotFoundComponent
  ],
  imports: [
    AppSharedModule,
    RouterModule,
    MatIconModule
  ]
})
export class NotFoundModule {
}
