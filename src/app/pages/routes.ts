import { AuthGuard } from '@services/auth-guard';

export const pageRoutes = [
  {
    canActivate: [AuthGuard],
    path: 'dashboard',
    loadChildren: './+dashboard/dashboard.module#DashboardModule',
  },
  {
    path: 'sign-in',
    loadChildren: './+signin/signin2.module#SigninModule', data: { preload: true }
  },
  {
    path: 'sign-up',
    loadChildren: './+signup/signup2.module#SignupModule', data: { preload: true }
  },
  {
    canActivate: [AuthGuard],
    path: 'change-password',
    loadChildren: './+change-password/change-password.module#ChangePasswordModule', data: { preload: true }
  },
  {
    canActivate: [AuthGuard],
    path: 'pa-user-administrator',
    loadChildren: './users/pa-users/pa-users.module#PAUsersModule',
    data: {'title': 'USERS_MANAGEMENT_HEADER'},
  },
  {
    canActivate: [AuthGuard],
    path: 'business-partners',
    loadChildren: './business-partner/business-partner.module#BusinessPartnerModule',
    data: {'title': 'BUSINESS_PARTNER.TITLE'},
  },
  {
    canActivate: [AuthGuard],
    path: 'test',
    loadChildren: './test/test.module#TestModule'
  },
  {
    canActivate: [AuthGuard],
    path: 'managed-care-systems',
    loadChildren: './mc-system/mc-system.module#McSystemModule',
    data: {'title': 'MC_SYSTEM.TITLE'},
  },
  // {
  //   canActivate: [AuthGuard],
  //   path: 'department',
  //   loadChildren: './departments/departments.module#DepartmentsModule',
  //   data: {'title': 'DEPARTMENTS.TITLE'},
  // },
];

