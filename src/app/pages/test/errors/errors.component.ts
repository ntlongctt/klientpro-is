import { Component, OnInit } from '@angular/core';
import { TestService } from '../../../rest/services/pa/Test.service';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromAuth from '@store/auth/auth.actions';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { HttpClient } from '@angular/common/http';
import * as fromErrorActions from '@store/error/error.actions';
import * as fromLoginActions from '@store/login/login.actions';
import { timeout } from 'rxjs/operators';
import { Actions, ofType } from '@ngrx/effects';
import { SystemService } from './../../../../app/rest/services/pa/System.service';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss']
})
export class ErrorsComponent implements OnInit {

  submitted = false;

  constructor(
    private _testService: TestService,
    private _toastrService: ToastrService,
    private _store: Store<AppState>,
    private _confirmModalService: ConfirmModalService,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private http: HttpClient,
    private actions$: Actions,
    private systemService: SystemService
  ) { 
    this._fuseTranslationLoaderService.loadTranslations(en, cs);
  }

  ngOnInit() {
  }

  testKnownException() {
    this._testService.test({ text: 'exception1' }).subscribe();
  }

  testCommunication() {
    this.submitted = true;
    this.http.get('https://pabe.klientpro.cz:6903')
    .pipe(
      timeout(12000),
    )
    .subscribe(null, (err) => {
      this._store.dispatch(new fromErrorActions.ConnectionTimeoutError());
      this.submitted = false;
    });
  }

  test401() {
    this._store.dispatch(new fromAuth.CleanAuth());
    this.systemService.logout().subscribe(_ => this._testService.test({text: 'test' }).subscribe());
  }

  test403() {
    this._testService.test403().subscribe();
  }

  test500() {
    this._testService.test500().subscribe();
  }

  testInfo() {
    this._toastrService.success(this._fuseTranslationLoaderService.instant('TOAST.INFO'));
  }

  testWarning() {
    this._toastrService.warning(this._fuseTranslationLoaderService.instant('TOAST.WARN'), null, 
    {disableTimeOut: true});
  }

  testError() {
    this._confirmModalService.show(
      this._fuseTranslationLoaderService.instant('MODAL.CONTENT.ERROR_MESSAGE'), 
      null, 
      this._fuseTranslationLoaderService.instant('MODAL.TITLE.ERROR_MESSAGE'), 
      false,
      'error'
    );
  }

  /**
   * Test for multiple error types and multiple errors as same type
   */
  testErrors() {

    // call know exception 3 times
    setTimeout(() => {
      this._testService.test({ text: 'exception1' }).subscribe();
    }, 500);
    setTimeout(() => {
      this._testService.test({ text: 'exception1' }).subscribe();
    }, 500);
    setTimeout(() => {
      this._testService.test({ text: 'exception1' }).subscribe();
    }, 500);

    //
    setTimeout(() => {
      this._testService.test403().subscribe();
    }, 500);
     setTimeout(() => {
      this._testService.test403().subscribe();
    }, 500);

  }

  /**
   * test 401 error 3 tiems
   */
  testMultipleUnauthorizeErrors() {
    this._store.dispatch(new fromAuth.CleanAuth());

    this.systemService.logout().subscribe(_ => {
      setTimeout(() => {
        this._testService.test({text: 'test' }).subscribe();
      }, 500);
      setTimeout(() => {
        this._testService.test({text: 'test' }).subscribe();
      }, 500);
      setTimeout(() => {
        this._testService.test({text: 'test' }).subscribe();
      }, 500);
    });
    
    
  }

}
