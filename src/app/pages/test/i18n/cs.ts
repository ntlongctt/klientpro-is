export const locale = {
  lang: 'cs',
  data: {
    TEST_ERROR_TITLE: 'Test chyb:',
	TEST_TAB: 'Test chyb',
    TOAST: {
      INFO: 'Test info zprávy',
      WARN: 'Test warning zprávy'
    },
    USER_INFO_TAB: {
      TITLE: 'Údaje o uživateli',
      USER_NAME: 'Uživatelské jméno',
      FULL_NAME: 'Celé jméno',
      EMAIL: 'E-mail',
      PERSPECTIVE: 'Perspektiva',
      UPDATED_TIME: 'Poslední aktualizace',
      AUTHORITIES: 'Authorities',
    },
  }
};
