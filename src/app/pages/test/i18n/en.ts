export const locale = {
  lang: 'en',
  data: {
    TEST_ERROR_TITLE: 'Test errors',
    TEST_TAB: 'Test errors',
    TOAST: {
      INFO: 'Test info message',
      WARN: 'Test warning message'
    },
    USER_INFO_TAB: {
      TITLE: 'User infomations',
      USER_NAME: 'User name',
      FULL_NAME: 'Full name',
      EMAIL: 'Email',
      PERSPECTIVE: 'Perspective',
      UPDATED_TIME: 'Updated time',
      AUTHORITIES: 'Authorities',
    },
  }
};
