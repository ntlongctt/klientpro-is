import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from '@pages/test/test.component';
import { ErrorsComponent } from '@pages/test/errors/errors.component';

const routes: Routes = [
  {
    path: '',
    component: TestComponent
  },
  {
    path: 'errors',
    component: ErrorsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule { }
