import { NgModule } from '@angular/core';

import { TestRoutingModule } from './test-routing.module';
import { TestComponent } from './test.component';
import { ErrorsComponent } from './errors/errors.component';
import { AppSharedModule } from '@common/shared';
import { ConfirmModalModule } from '@common/modules/confirm-modal';
import { UserInfoComponent } from './user-info/user-info.component';

@NgModule({
  imports: [
    AppSharedModule,
    TestRoutingModule,
    ConfirmModalModule
  ],
  declarations: [TestComponent, ErrorsComponent, UserInfoComponent],
})
export class TestModule { }
