import { Component, OnInit } from '@angular/core';
import { AppStoreService } from '@store/store.service';

@Component({
  selector: 'user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  userDetail: any;

  constructor(
    private apStoreService: AppStoreService
  ) { }

  ngOnInit() {
    this.userDetail = this.apStoreService.getState().auth.currentUser;
  }

}
