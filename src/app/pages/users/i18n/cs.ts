export const locale = {
  lang: 'cs',
  data: {
    USERS_MANAGEMENT_HEADER: 'Správa uživatelů',
    USER_TAB_ACCOUNT: 'Uživatelský účet',
    USER_TAB_PRIVILEGES: 'Oprávnění',
    USER_TAB_SELECT_TAGS: 'Nálepky',
    USER_TAB_SELECT_TAGS_DISABLED: 'Nejprve uložte uživatele',
    FILTER_USERS: 'Vyhledat uživatele',
    BUTTON_NEW: 'Přidat uživatele',
    USERS_TABLE: {
      COL_NAME: 'Jméno',
      COL_USERNAME: 'Uživatelské jméno',
      COL_STATE: 'Stav'
    },
    MESSAGE: {
      NO_USERS: 'Nenalezeni žádní uživatelé',
      UPDATE_USER_SUCCEEDED: 'Uživatel byl aktualizován',
      DELETE_USER_SUCCEEDED: 'Uživatel byl vymazán',
      CREATE_USER_SUCCEEDED: 'Uživatel byl vytvořen',
      RESET_PASSWORD_USER_SUCCEEDED: 'Uživatelské heslo bylo nastaveno',
      NEW_PASSWORD: 'Nové heslo je: ',
      CANCEL_CHANGE_MESAGE: 'Chcete zahodit Vámi provedené změny?',
      CANCEL_CHANGE_CONFIRM: 'Potvrdit zrušení změn',
      DELETE_USER_CONFIRM_MESSAGE: 'Smazat uživatele?',
      DELETE_USER_CONFIRM_TITLE: 'Potvrdit smazání uživatele',
      RESET_PASSWORD_CONFIRM_MESSAGE: 'Chcete resetovat heslo uživatele?',
      RESET_PASSWORD_CONFIRM_TITLE: 'Potvrdit reset hesla uživatele',
      RESET_PASSWORD_WITH_UNSAVE_DATA:
        'Varování: Máte neuložené změny, pokud budete pokračovat, budou ztraceny'
    },
    USER_DETAIL: {
      NAME: 'Jméno',
      USER_NAME: 'Uživatelské jméno',
      EMAIL: 'E-mail',
      PHONE: 'Telefon',
      NOTE: 'Poznámka'
    },
    PLACEHOLDER: {
      PERSPECTIVE: 'Perspektiva',
      SEARCH_PERSPECTIVE: 'Vyhledat perspektivu',
      SEARCH_STATE: 'Vyhledat stav',
      STATE: 'Stav'
    },
    FORM_VALIDATOR: {
      FULLNAME_REQUIRE: 'Jméno je povinné',
      USERNAME_REQUIRE: 'Uživatelské jméno je povinné',
      PERSPECTIVE_REQUIRE: 'Perspektiva je povinná',
      USER_SATE_REQUIRE: 'Stav uživatele je povinný',
      EMAIL_REQUIRE: 'E-mail je povinný',
      LETTERS_NOT_ALLOWED: 'Písmena nejsou povolena',
      INVALID_REGION_CODE: 'Zahraniční čísla nejsou povolena',
      INVALID_PHONE_NUMBER: 'Chybný formát telefonního čísla',
      INVALID_EMAIL: 'Neplatný e-mail'
    },
    HEADER_TITLE_UPDATE: 'Aktualizovat uživatele: ',
    HEADER_TITLE_CREATE: 'Přidat uživatele',
    NO_NAME: 'Žádné jméno',
    ASSIGNED_ROLES: 'Přiřazené role:',
    AVAILABLE_ROLES: 'Dostupné role:',
    LAST_UPDATE: 'Poslední aktualizace: ',
    PASSWORD_CHANGED: 'Heslo změněno: ',
    HEADER_TITLE_USER_DETAIL: 'Detaily uživatele',
  }
};
