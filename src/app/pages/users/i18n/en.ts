export const locale = {
  lang: 'en',
  data: {
    USERS_MANAGEMENT_HEADER: 'Users management',
    USER_TAB_ACCOUNT: 'Account',
    USER_TAB_PRIVILEGES: 'Privileges',
    USER_TAB_SELECT_TAGS: 'Select tags',
    USER_TAB_SELECT_TAGS_DISABLED: 'Save user first',
    FILTER_USERS: 'Search users',
    BUTTON_NEW: 'Add new user',
    USERS_TABLE: {
      COL_NAME: 'Name',
      COL_USERNAME: 'Username',
      COL_STATE: 'State'
    },
    MESSAGE: {
      NO_USERS: 'There are no users',
      UPDATE_USER_SUCCEEDED: 'User information updated successfully',
      DELETE_USER_SUCCEEDED: 'User deleted successfully',
      CREATE_USER_SUCCEEDED: 'User created successfully',
      RESET_PASSWORD_USER_SUCCEEDED: 'User password reseted succeccfully',
      NEW_PASSWORD: 'New password is: ',
      CANCEL_CHANGE_MESAGE: 'Are you sure to cancel all changes?',
      CANCEL_CHANGE_CONFIRM: 'Cancel edit user confirmation',
      DELETE_USER_CONFIRM_MESSAGE: 'Are you sure to delete this user?',
      DELETE_USER_CONFIRM_TITLE: 'Delete user confirmation',
      RESET_PASSWORD_CONFIRM_MESSAGE: 'Are you sure to reset password of this user?',
      RESET_PASSWORD_CONFIRM_TITLE: 'Reset password confirmation',
      RESET_PASSWORD_WITH_UNSAVE_DATA: 'Warning: There are unsaved changes, if you proceed, they will be lost'
    },
    USER_DETAIL: {
      NAME: 'Name',
      USER_NAME: 'User name',
      EMAIL: 'Email',
      PHONE: 'Phone',
      NOTE: 'Note'
    },
    PLACEHOLDER: {
      PERSPECTIVE: 'Perspective',
      SEARCH_PERSPECTIVE: 'Search Perspective',
      SEARCH_STATE: 'Search State',
      STATE: 'State'
    },
    FORM_VALIDATOR: {
      FULLNAME_REQUIRE: 'Name is required',
      USERNAME_REQUIRE: 'Username is required',
      PERSPECTIVE_REQUIRE: 'Perspective is required',
      USER_SATE_REQUIRE: 'User state is required',
      EMAIL_REQUIRE: 'Email is required',
      LETTERS_NOT_ALLOWED: 'Letters are not allowed',
      INVALID_REGION_CODE: 'Foreign numbers are not allowed',
      INVALID_PHONE_NUMBER: 'Invalid phone number format',
      INVALID_EMAIL: 'Email is invalid'
    },
    HEADER_TITLE_UPDATE: 'Update user: ',
    HEADER_TITLE_CREATE: 'Add new user',
    NO_NAME: 'No name',
    ASSIGNED_ROLES: 'Assigned roles:',
    AVAILABLE_ROLES: 'Available roles:',
    LAST_UPDATE: 'Last update: ',
    PASSWORD_CHANGED: 'Password changed: ',
    HEADER_TITLE_USER_DETAIL: 'User Details',

  }
};
