import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpdateUserComponent } from './create-update.component';
import { AppTestModule } from '../../../../app.module.test';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { RouterService } from '@common/modules/route-caching';
import { of } from 'rxjs/internal/observable/of';
import { AuthService } from '../../../../rest/services/pa/Auth.service';
import { Injectable } from '@angular/core';
import { browser, by, element } from 'protractor';
import { By } from '@angular/platform-browser';
import { CodeListService } from '../../../../rest/services/common/CodeList.service';

@Injectable()
export class AuthServiceTest {
  public getPerspectives() {
    return of(['PERSPECTIVE_BACKOFFICE', 'PERSPECTIVE_CALL', 'PERSPECTIVE_CLIENT']);
  }

  public getRoles() {
    return of([
      {id: 1, name: '-- nová test role --'},
      {id: 10, name: 'pracovník callcentra'},
      {id: 11, name: 'vedoucí callcentra'},
      {id: 12, name: 'pracovník klient. přepážky'},
      {id: 13, name: 'vedoucí klient. přepážky'},
      {id: 14, name: 'pracovník kanceláře'},
      {id: 99, name: 'správce systému'},
      {id: 22, name: 'správce uživatelů portálu pro PP'},
      {id: 21, name: 'správce uživatelů ISMC'},
      {id: 20, name: 'správce schránky portálu pro partnery'}
    ]);
  }
}

@Injectable()
export class CodeListServiceTest {
  public getCodeListItems() {
    return of([
      {id: 1, name: 'Aktívny'},
      {id: 2, name: 'Dočasne blokovaný '},
      {id: 3, name: 'Zrušený'},
      {id: 4, name: 'Blokovaný'}
    ]);
  }
}

describe('CreateUpdateComponent', () => {
  let component: CreateUpdateUserComponent;
  let fixture: ComponentFixture<CreateUpdateUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppTestModule],
      declarations: [ CreateUpdateUserComponent ],
      providers: [
        ConfirmModalService, 
        RouterService,
        { provide: AuthService, useClass: AuthServiceTest },
        { provide: CodeListService, useClass: CodeListServiceTest },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUpdateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render action buttons (Save, Delete, Reset password, Cancel)', () => {
    expect(fixture.debugElement.query(By.css('#btnSave'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('#btnDelete'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('#btnResetPwd'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('#btnCancel'))).toBeTruthy();
  });

  it('should render Account and Privileges tabs',  () => {
    const tabs = fixture.nativeElement.querySelectorAll('.mat-tab-label-content');
    expect(tabs[0].textContent).toContain('USER_TAB_ACCOUNT');
    expect(tabs[1].textContent).toContain('USER_TAB_PRIVILEGES');
  });

  it('form shoudl be invalid', () => {
    component.frm.controls['fullName'].setValue('fullName');
    component.frm.controls['userName'].setValue('userName');
    component.frm.controls['perspective'].setValue('fullName');
    component.frm.controls['userStateId'].setValue(1);
    component.frm.controls['email'].setValue('testmail');
    component.frm.controls['phone'].setValue('0420111222333');
    expect(component.frm.valid).toBeFalsy();
  });

  it('form shoudl be valid', () => {
    component.frm.controls['fullName'].setValue('fullName');
    component.frm.controls['userName'].setValue('userName');
    component.frm.controls['perspective'].setValue('fullName');
    component.frm.controls['userStateId'].setValue(1);
    component.frm.controls['email'].setValue('testmail@gmail.com');
    component.frm.controls['phone'].setValue('+420 111 222 333');
    expect(component.frm.valid).toBeTruthy();
  });
});
