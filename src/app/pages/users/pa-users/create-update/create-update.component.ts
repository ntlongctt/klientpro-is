import { Component, OnInit, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, takeUntil } from 'rxjs/operators';
import { AuthService } from '../../../../rest/services/pa/Auth.service';
import { BaseComponent } from '@common/modules/base-component';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from '../../i18n/en';
import { locale as cs } from '../../i18n/cs';
import { locale as enCommon } from '../../../../i18n/common/en';
import { locale as csCommon } from '../../../../i18n/common/cs';
import { FormControl, Validators } from '@angular/forms';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromUserActions from '@store/user/user.action';
import * as fromFormActions from '@store/form/form.actions';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { Actions, ofType } from '@ngrx/effects';
import { Subject } from 'rxjs/Subject';
import { ToastrService } from 'ngx-toastr';
import { RouterService } from '@common/modules/route-caching';
import * as _ from 'lodash';
import { EmailValidator } from '@common/validators/email.validator';
import { NumberValidator } from '@common/validators/number-only.validator';
import { AppStoreService } from '@store/store.service';
import * as fromMenuActions from '@store/menu/menu.actions';
import * as fromBreadscrumbActions from '@store/breadcrumbs/breadcrumbs.actions';
import { PhoneNumberValidator } from '@common/validators/phone.validator';
import { CodeListService } from '../../../../rest/services/common/CodeList.service';
import * as fromTagActions from '@store/tag/tag.actions';

@Component({
  selector: 'app-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateUserComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  public perspectives = [];
  public userStates = [];
  public userId;
  public user;
  // public headerTitle = '';
  public submitted = false;
  public userFullName = '';
  public _unsubscribe = new Subject();
  public availableRoles = [];
  public currentRoles = [];
  public userRoles = [];
  public phoneNumberOption = {
    initialCountry: 'cz',
    autoPlaceholder: 'off'
  };
  public initialForm;
  public currentFormStatus = false;
  public intlTelInputObj: any;
  public showSelectTag = false;
  public isSelectedTagChange = false;
  public selectedIndex = 0;

  /**
   * Before tab/browser close or refresh, check if form data has changed,show alter confirm
   */
  @HostListener('window:beforeunload', ['$event'])
  beforeunload(event) {
    const hasChange = this.appStoreService.getState().form.hasChanged;
    if (hasChange) {
      return false;
    }
  }
  

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private translationLoader: FuseTranslationLoaderService,
    private codeListService: CodeListService,
    private store: Store<AppState>,
    private _confirmModalService: ConfirmModalService,
    private actions$: Actions,
    private _toast: ToastrService,
    private _router: RouterService,
    private appStoreService: AppStoreService,
  ) {
    super();

    this.translationLoader.loadTranslations(en, cs, enCommon, csCommon);

    this.userFullName = this.translationLoader.instant('NO_NAME');

    this.controlConfig = {
      fullName: new FormControl('', [Validators.required]),
      userName: new FormControl('', Validators.required),
      perspective: new FormControl('', Validators.required),
      userStateId: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, EmailValidator.validEmail()]),
      phone: new FormControl('',  [NumberValidator.numberOnly(), PhoneNumberValidator.validPhoneNumber()]),
      note: new FormControl(''),
      version: new FormControl(''),
    };

    this.userId = this.activatedRoute.snapshot.params['userId'];

    // listen action of userActions, show modal contain new password when reset password success
    this.actions$.pipe(
      ofType(fromUserActions.RESET_PASSWORD_USER_SUCCEEDED),
      takeUntil(this._unsubscribe)
    ).subscribe((action: any) => {
      this.user = action.payload;
      this.frm.patchValue(action.payload);
      const newPassword = action.payload.password ? action.payload.password : '';
      this.submitted = false;
      this._confirmModalService.show(
        `${this.translationLoader.instant('MESSAGE.NEW_PASSWORD')} <strong>${newPassword}</strong?`, 
        null, 
        this.translationLoader.instant('MESSAGE.RESET_PASSWORD_USER_SUCCEEDED'), 
        false);
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
    });

    // list action of userActions, show modal contain new password when create new user success
    this.actions$.pipe(
      ofType(fromUserActions.CREATE_USER_SUCCEEDED),
      takeUntil(this._unsubscribe)
    ).subscribe((action: any) => {
      this.user = action.payload;
      this.userId = this.user.id;

      // Reser form status
      this.currentFormStatus = false;
      this.initialForm = this.frm.value;

      this.userFullName = this.user['fullName'] ? 
              this.user['fullName'] :
              this.translationLoader.instant('NO_NAME');

      // Load assigned tags
      this.store.dispatch(new fromTagActions.GetAssignedTag({entityName: 'PAUser', itemId: this.userId}));

      const newPassword = action.payload.password;
      this.submitted = false;
      this._confirmModalService.show(
        `${this.translationLoader.instant('MESSAGE.NEW_PASSWORD')} <strong>${newPassword}</strong?`, 
        null, 
        this.translationLoader.instant('MESSAGE.CREATE_USER_SUCCEEDED'), 
        false);
      // set formHasChange to fasle by default
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
    });

    // list action of userActions, show toast success and navigate to user list when update or delete user success
    this.actions$.pipe(
      ofType(
        fromUserActions.DELETE_USER_SUCCEEDED, 
        fromUserActions.UPDATE_USER_SUCCEEDED,
        ),
      takeUntil(this._unsubscribe)
    ).subscribe((action) => {
      const message = `MESSAGE.${action.type.split('/')[1]}`;
      this.submitted = false;
      this._toast.success(this.translationLoader.instant(message));
      // set formHasChange to fasle by default
      this.store.dispatch(new fromFormActions.ChangeFormStatus(false));
      this._router.navigate(['pa-user-administrator']);
    });

    // enable button when ation failed
    this.actions$.pipe(
      ofType(
        fromUserActions.RESET_PASSWORD_USER_FAILED,
        fromUserActions.UPDATE_USER_FAILED,
        fromUserActions.DELETE_USER_FAILED,
        fromUserActions.CREATE_USER_FAILED
      )
    ).subscribe(() => {
      this.submitted = false;
    });

    // set formHasChange to fasle by default
    this.store.dispatch(new fromFormActions.ChangeFormStatus(false));

   
   }

  // init user form, get list perspectives, list code and role then bind into select list
  // get user detail and bind user info into form
  ngOnInit() {
    super.ngOnInit();
    this.initialForm = this.frm.value;
    combineLatest(
      this.authService.getPerspectives(), 
      this.codeListService.getCodeListItems({codelistName: 'UserState'}),
      this.authService.getRoles().pipe(map(data => data.roles))
    ).subscribe(([perspectives, codes, roles]) => {
      this.perspectives = perspectives.map(p => {
        return {
          id: p,
          name: this.translationLoader.instant(p)
        };
      });

      this.userStates = codes.items;
      this.availableRoles = roles;
      
      if (this.userId) {
        this.authService.getUser({userId: this.userId})
          .subscribe((userDetail => {
            this.user = userDetail;
            this.currentRoles = [...this.user.roles];
            this.userRoles = [...this.user.roles];
            this.currentRoles.forEach(i => {
              const idx = this.availableRoles.findIndex(r => r.id === i.id);
              this.availableRoles.splice(idx, 1);
            });

            this.userFullName = this.user['fullName'] ? 
              this.user['fullName'] :
              this.translationLoader.instant('NO_NAME');
            this.frm.patchValue(userDetail);
            this.initialForm = this.frm.value;
          }));
          
      }
    });
    
    this.frm.valueChanges.subscribe(() => {
      this.checkFormStatus();
    });

    // Set active tab is 'Select tags' when back from tags module
    const preURL = this.appStoreService.getState().router.state.previousUrl;
    console.log('%c PRE-ROUTE', 'color: orange', preURL);
    if (preURL && preURL.includes('/tag')) {
      this.selectedIndex = 2;
      this.tabChanged(2);
    }

  }

  ngAfterViewInit() {
    this.frm.valueChanges.subscribe(() => {
      this.checkFormStatus();
    });
  }

  ngOnDestroy() {
    this._unsubscribe.next();
    this._unsubscribe.complete();
  }
  
  /**
   * When user click on 'Save' button, check if form valid then repare data.
   * If userId not null dispatch action to upate user, ortherwise dispatch action create user
   */
  onSubmit() {
    const {valid, value} = this.frm;
    
    if (valid) {
      const dataPost = {
        ...value,
        id: this.userId ? this.userId : '',
        roles: this.userRoles,
        phone: value.phone ? value.phone : null,
        version: this.user ? this.user['version'] : 0
      };
      
      if (this.userId) {
        this.store.dispatch(new fromUserActions.UpdateUser(dataPost));
      } else {
        this.store.dispatch(new fromUserActions.CreateUser(dataPost));
      }
    } else {
      this.buildFormErrorMessage();
    }

  }

  /**
   * Show a reset password confirm modal to user, if user accept, dispatch action 'reset password'
   */
  resetPassword() {
    const warningMsg = this.appStoreService.getState().form.hasChanged ? 
      this.translationLoader.instant('MESSAGE.RESET_PASSWORD_WITH_UNSAVE_DATA') : '';
    this._confirmModalService
    .show(this.translationLoader.instant('MESSAGE.RESET_PASSWORD_CONFIRM_MESSAGE'), (status) => {
      if (status) {
        this.submitted = true;
        this.store.dispatch(new fromUserActions.ResetPassword(this.userId));
      }
    }, this.translationLoader.instant('MESSAGE.RESET_PASSWORD_CONFIRM_TITLE'), true, null, null, warningMsg);
  }

  /**
   * Show a delete confirm modal to user, if user accept, dispatch action 'delete user'
   */
  delete() {
    this._confirmModalService
      .show(this.translationLoader.instant('MESSAGE.DELETE_USER_CONFIRM_MESSAGE'), (status) => {
        if (status) {
          this.submitted = true;
          this.store.dispatch(new fromUserActions.DeleteUser(this.userId));
        }
      }, 
      this.translationLoader.instant('MESSAGE.DELETE_USER_CONFIRM_TITLE'));
  }

  /**
   * back to user list
   */
  cancel() {
    this._router.navigate(['pa-user-administrator/']);
  }

  /**
   * Update user privileges
   * @param event list selected privileges of user
   */
  onRolesChange(event) {
    this.userRoles = event;
    this.checkFormStatus();

  }

  /**
   * return true if user form or privileges has changes
   */
  isUserChange() {
    const isFormChange = !_.isEqual(this.initialForm, this.frm.value) && this.frm.dirty;

    if (this.userId === undefined) {
      return this.userRoles.length !== 0 || isFormChange;
    }
    const temp = this.user ? this.user.roles : [];
    const lst1 = _.orderBy([...temp], ['id']);
    const lst2 = _.orderBy([...this.userRoles], ['id']);
    const isRoleChange = !_.isEqual(lst1, lst2);

    return isFormChange || isRoleChange || this.isSelectedTagChange;
  }

  /**
   * check if user form or privileges has changes, dispath to store
   */
  checkFormStatus() {
    const isFormChange = this.isUserChange();
    if (isFormChange !== this.currentFormStatus) {
      this.store.dispatch(new fromFormActions.ChangeFormStatus(isFormChange));
      this.currentFormStatus = isFormChange;
    }
  }

  tabChanged(index) {
    this.showSelectTag = (index === 2 && this.userId) ? true : false;
  }

  /**
   * Cancel all change of assigned tag and back to user list
   */
  onCancel() {
    this.cancel();
  }

  /**
   * When assigned tag has change value, check form status
   */
  onSelectedChange(hasChange) {
    this.isSelectedTagChange = hasChange;
    this.checkFormStatus();
  }

  /**
   * Navigate to tag managenent
   */
  onGotoManageTags() {
    this._router.navigate(['pa-user-administrator', this.userId, 'PAUser', 'tags'], {queryParams: {'userId': this.userFullName}});
  }
}
