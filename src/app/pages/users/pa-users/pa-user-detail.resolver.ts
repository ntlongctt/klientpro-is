import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../../../rest/services/pa/Auth.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';
import { throwError } from 'rxjs/internal/observable/throwError';

@Injectable()
export class PAUserDetailResolver implements Resolve<any> {
    constructor(
      private authService: AuthService,
    ) {}

    resolve(route: ActivatedRouteSnapshot) {
      const id: number = parseInt(route.params['userId'], 0);
      return this.authService.getUser({userId: id}).pipe(
        catchError(() => {
          return throwError('Connection time out');
        })
      );
    }
}
