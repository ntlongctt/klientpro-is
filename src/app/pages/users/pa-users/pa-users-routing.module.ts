import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PAUsersComponent } from '@pages/users/pa-users/pa-users.component';
import { PAUserDetailResolver } from '@pages/users/pa-users/pa-user-detail.resolver';
import { CreateUpdateUserComponent } from './create-update/create-update.component';
import { getTagRouting } from '../../../common/modules/tags/tag.routing';
import { AuthGuard } from '@common/services/auth-guard';
import { CanDeactivateGuard } from '@common/services/deactivate-guard';

const routes: Routes = [
  {
    path: '',
    component: PAUsersComponent,
  },
  {
    path: 'create',
    component: CreateUpdateUserComponent,
    canDeactivate: [CanDeactivateGuard],
    data: {'title': 'HEADER_TITLE_USER_DETAIL'},
  },
  {
    path: ':userId',
    data: {'title': 'HEADER_TITLE_USER_DETAIL', 'key': 'userId'},
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: CreateUpdateUserComponent,
        // resolve: {userDetail: PAUserDetailResolver},
        canDeactivate: [CanDeactivateGuard],
      },
      // Import tag management routing
      ...getTagRouting(':entityName')
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [CanDeactivateGuard],
  exports: [RouterModule]
})
export class PAUsersRoutingModule { }
