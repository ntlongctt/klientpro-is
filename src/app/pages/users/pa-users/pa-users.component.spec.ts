import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PAUsersComponent } from './pa-users.component';
import { AppTestModule } from '../../../app.module.test';
import { MatTableModule } from '@angular/material';
import { PAUsersRoutingModule } from '@pages/users/pa-users/pa-users-routing.module';
import { InputSearchModule } from '@common/modules/input-search';
import { MatSelectSearchModule } from '@common/modules/mat-select-search';
import { ConfirmModalModule } from '@common/modules/confirm-modal';
import { FieldChooserModule } from '@common/modules/field-chooser/field-chooser.module';
import { CreateUpdateUserComponent } from './create-update/create-update.component';

describe('UsersComponent', () => {
  let component: PAUsersComponent;
  let fixture: ComponentFixture<PAUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestModule,
        PAUsersRoutingModule,
        InputSearchModule,
        MatTableModule,
        MatSelectSearchModule,
        ConfirmModalModule,
        FieldChooserModule,
      ],
      declarations: [ PAUsersComponent, CreateUpdateUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PAUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
