import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as en } from '../i18n/en';
import { locale as cs } from '../i18n/cs';
import { AuthService } from '../../../rest/services/pa/Auth.service';
import { tap, takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material';
import { PAGING_DEFAULT } from '@common/constants/paging-default';
import { Subject } from 'rxjs/Subject';
import { RouterService } from '@common/modules/route-caching';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { ofType, Actions } from '@ngrx/effects';
import * as fromUserActions from '@store/user/user.action';
import { PAUsersDataSource } from './pa-users.datasource';
import { ActivatedRoute } from '@angular/router';
import { Util } from '@services/util';

@Component({
  selector: 'app-users',
  templateUrl: './pa-users.component.html',
  styleUrls: ['./pa-users.component.scss']
})
export class PAUsersComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('tablePaginator') paginator: MatPaginator;
  @ViewChild(FusePerfectScrollbarDirective) directiveScroll: FusePerfectScrollbarDirective;
  public dataSource: PAUsersDataSource;
  public searchText = '';
  public displayedColumns = ['fullName', 'userName', 'userState'];
  private _unsubscribeAll = new Subject();

  constructor(
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _authService: AuthService,
    private _routerService: RouterService,
    private actions$: Actions,
    private activatedRoute: ActivatedRoute,
    private _util: Util
  ) {
    this._fuseTranslationLoaderService.loadTranslations(en, cs);

    // listen action of userActions and reload data when new user created, updated or deleted
    this.actions$.pipe(
      ofType(
        fromUserActions.CREATE_USER_SUCCEEDED,
        fromUserActions.DELETE_USER_SUCCEEDED,
        fromUserActions.UPDATE_USER_SUCCEEDED),
      takeUntil(this._unsubscribeAll)
    ).subscribe(() => {
      this.reloadData();
    });
  }

  ngOnInit() {
    this.dataSource = new PAUsersDataSource(this._authService);
    this.dataSource.loadUsers('', PAGING_DEFAULT.START, PAGING_DEFAULT.LIMIT);
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  ngAfterViewInit(): void {
    if (this.paginator) {
      this.paginator.page
        .pipe(
          tap(() => this.loadUsersPage())
        )
        .subscribe();
    }
  }

  loadUsersPage() {
    this.dataSource.loadUsers(
      this.searchText,
      this.paginator.pageIndex * this.paginator.pageSize,
      this.paginator.pageSize
    );
    // Make perfect scrollbar scroll to top list also update content scroll height after change page, page size, ...
    setTimeout(() => {
      this.directiveScroll.scrollToTop();
      this.directiveScroll.update();
    });
  }

  goToDetail(row) {
    // this._router.navigate(['users', row.id]);
    const queryParams = {...this.activatedRoute.snapshot.queryParams, userId: row.fullName};
    // this._routerService.navigate([this.platformLocation.pathname, 'edit', tag.id], {queryParams: queryParams});
    this._routerService.navigate([this._util.getPathName(), row.id], {queryParams: queryParams}, {
      enableBackView: true,
      scrollElement: this.directiveScroll
    });
  }

  reloadData() {
    this.dataSource.clear(); // clear current data
    this.paginator.firstPage(); // reset paginator
    this.loadUsersPage(); // load data
  }

  filterChange(keyword) {
    if (this.searchText === keyword) {
      return;
    }
    this.searchText = keyword;
    this.reloadData();
  }

  addNew() {
    this._routerService.navigate(['pa-user-administrator', 'create'], {}, {
      enableBackView: true,
      scrollElement: this.directiveScroll
    });
  }

}
