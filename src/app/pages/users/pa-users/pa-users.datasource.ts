import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import { catchError, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/internal/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuthService } from '../../../rest/services/pa/Auth.service';
import { PaUserDTO } from '../../../rest/models/pa/system/login/PaUserDTO';
import { PAGING_DEFAULT } from '@common/constants/paging-default';


export class PAUsersDataSource implements DataSource<PaUserDTO> {

  private usersSubject = new BehaviorSubject<PaUserDTO[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public totalCount = 0;

  constructor(private authService: AuthService) {}

  connect(collectionViewer: CollectionViewer): Observable<PaUserDTO[]> {
      return this.usersSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
      this.usersSubject.complete();
      this.loadingSubject.complete();
  }

  loadUsers(searchQuery = '',  start = PAGING_DEFAULT.START, limit = PAGING_DEFAULT.LIMIT) {

      this.loadingSubject.next(true);

      this.authService.getUsers({
        searchQuery: searchQuery,
        start: start,
        limit: limit
      }).pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
      )
      .subscribe((users: any) => {
        this.totalCount = users.totalCount;
        this.usersSubject.next(users.users);
      });
  }

  clear() {
    this.usersSubject.next([]);
  }
}
