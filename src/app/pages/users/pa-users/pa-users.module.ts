import { NgModule } from '@angular/core';

import { PAUsersComponent } from './pa-users.component';
import { AppSharedModule } from '@common/shared';
import { InputSearchModule } from '@common/modules/input-search';
import { MatTableModule, MatPaginatorIntl } from '@angular/material';
import { PAUserDetailResolver } from '@pages/users/pa-users/pa-user-detail.resolver';
import { MatSelectSearchModule } from '@common/modules/mat-select-search';
import { ConfirmModalModule } from '@common/modules/confirm-modal';
import { FieldChooserModule } from '@common/modules/field-chooser/field-chooser.module';
import {Ng2TelInputModule} from 'ng2-tel-input';
import { PAUsersRoutingModule } from './pa-users-routing.module';
import { CreateUpdateUserComponent } from './create-update/create-update.component';
import { TreeviewChooserModule } from '@common/modules/treeview-chooser/treeview-chooser.module';
import { CustomMatPaginatorIntl } from '@common/services/custom-paginator/CustomMatPaginatorIntl';
import { TagsModule } from '@common/modules/tags/tags.module';
import { DatePipe } from '@angular/common';
import { PhoneNumberInputModule } from '@common/modules/phone-number-input/phone-number-input.module';
import { BreadscumbModule } from '@common/modules/breadcrumbs/breadcrumbs.module';
import { DeactiveWarningModalModule } from '@common/modules/deactivate-warning-modal/deactivate-warning-modal.module';

@NgModule({
  imports: [
    AppSharedModule,
    PAUsersRoutingModule,
    InputSearchModule,
    MatTableModule,
    MatSelectSearchModule,
    ConfirmModalModule,
    FieldChooserModule,
    Ng2TelInputModule,
    DeactiveWarningModalModule,
    TreeviewChooserModule, 
    TagsModule,
    PhoneNumberInputModule,
    BreadscumbModule
  ],
  declarations: [PAUsersComponent, CreateUpdateUserComponent ],
  providers: [
    PAUserDetailResolver,
    { provide: MatPaginatorIntl, useClass: CustomMatPaginatorIntl },
    DatePipe
  ],
})
export class PAUsersModule { }
