import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PcUsersRoutingModule } from './pc-users-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PcUsersRoutingModule
  ],
  declarations: []
})
export class PcUsersModule { }
