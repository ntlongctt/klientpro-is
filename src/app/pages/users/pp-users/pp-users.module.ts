import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PpUsersRoutingModule } from './pp-users-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PpUsersRoutingModule
  ],
  declarations: []
})
export class PpUsersModule { }
