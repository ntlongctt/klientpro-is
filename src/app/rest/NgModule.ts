import { NgModule } from '@angular/core';
import { SystemService } from './services/pa/System.service';
import { TestService } from './services/pa/Test.service';
import { CmsService } from './services/pa/Cms.service';
import { CodeListService } from './services/common/CodeList.service';
import { DepartmentService } from './services/pa/Department.service';
import { TagService } from './services/pa/Tag.service';
import { MessageboxService } from './services/pa/Messagebox.service';
import { MenuService } from './services/pa/Menu.service';
import { AuthService } from './services/pa/Auth.service';
import { BusinessPartnerService } from './services/pa/BusinessPartner.service';
import { McSystemService } from './services/pa/McSystem.service';
import { ContactService } from './services/common/Contact.service';
 
@NgModule({
    imports: [],
    exports: [],
    providers: [
        SystemService,
        TestService,
        CmsService,
        CodeListService,
        DepartmentService,
        TagService,
        MessageboxService,
        MenuService,
        AuthService,
        BusinessPartnerService,
        McSystemService,
        ContactService
    ]
})
 
export class RestModule {
}
