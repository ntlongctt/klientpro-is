export interface BankAccountDTO {
    bankCode: string;
    note: string;
    bankStateCode: string;
    iban: string;
    bankName: string;
    bankStateName: string;
    id: number;
    accountNumber: string;
    version: number;
    bic: string;
    bankCity: string;
}
