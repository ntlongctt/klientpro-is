export interface PaymentMethodDTO {
    updateDisabled: boolean;
    name: string;
    id: number;
    version: number;
}
