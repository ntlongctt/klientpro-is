import { CodeListItemDTO } from './CodeListItemDTO';
 
export interface CodeListDTO {
    items: CodeListItemDTO[];
}
