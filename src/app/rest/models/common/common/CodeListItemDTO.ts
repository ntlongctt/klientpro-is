export interface CodeListItemDTO {
    name: string;
    id: number;
}
