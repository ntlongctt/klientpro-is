export interface ExceptionDTO {
    note: string;
    business: boolean;
    id: string;
    message: string;
    type: string;
}
