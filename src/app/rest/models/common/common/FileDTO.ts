export interface FileDTO {
    fileName: string;
    contentType: string;
    content: string;
}
