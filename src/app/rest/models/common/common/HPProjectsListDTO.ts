import { CodeListItemDTO } from './CodeListItemDTO';
 
export interface HPProjectsListDTO {
    projects: CodeListItemDTO[];
    clientProjectId: number;
}
