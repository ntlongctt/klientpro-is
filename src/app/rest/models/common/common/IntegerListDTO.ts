import { IntegerValueDTO } from './IntegerValueDTO';
 
export interface IntegerListDTO {
    items: IntegerValueDTO[];
}
