import { LongValueDTO } from './LongValueDTO';
 
export interface LongListDTO {
    items: LongValueDTO[];
}
