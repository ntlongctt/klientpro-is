import { StringCodeListItemDTO } from './StringCodeListItemDTO';
 
export interface StringCodeListDTO {
    items: StringCodeListItemDTO[];
}
