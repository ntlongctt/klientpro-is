export interface StringCodeListItemDTO {
    code: string;
    name: string;
}
