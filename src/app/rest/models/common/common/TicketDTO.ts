export interface TicketDTO {
    phone: string;
    ticketTypeId: number;
    content: string;
    email: string;
}
