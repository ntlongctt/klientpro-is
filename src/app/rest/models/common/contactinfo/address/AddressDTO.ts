export interface AddressDTO {
    zipCode: string;
    note: string;
    streetNumber: string;
    cityCode: string;
    streetNote: string;
    version: number;
    geoLongitude: number;
    geoLatitude: number;
    cityName: string;
    stateName: string;
    street: string;
    stateCode: string;
    id: number;
    geoCoordsStateId: number;
}
