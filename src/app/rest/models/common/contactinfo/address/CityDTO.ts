export interface CityDTO {
    zipCode: string;
    cityName: string;
    cityCode: string;
    id: number;
}
