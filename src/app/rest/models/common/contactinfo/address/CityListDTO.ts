import { CityDTO } from './CityDTO';
 
export interface CityListDTO {
    cities: CityDTO[];
}
