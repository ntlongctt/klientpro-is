import { CityDTO } from './CityDTO';
import { RegionDTO } from './RegionDTO';
 
export interface DistrictDTO {
    code: string;
    oldCode: string;
    cities: CityDTO[];
    name: string;
    region: RegionDTO;
    shortName: string;
}
