import { DistrictDTO } from './DistrictDTO';
 
export interface RegionDTO {
    code: string;
    oldCode: number;
    name: string;
    districts: DistrictDTO[];
    shortName: string;
}
