export interface StateDTO {
    code: string;
    name: string;
    internationalName: string;
}
