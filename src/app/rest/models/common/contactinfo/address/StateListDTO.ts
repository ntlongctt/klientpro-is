import { StateDTO } from './StateDTO';
 
export interface StateListDTO {
    states: StateDTO[];
}
