export interface StreetDTO {
    streetName: string;
    streetCode: string;
    id: number;
}
