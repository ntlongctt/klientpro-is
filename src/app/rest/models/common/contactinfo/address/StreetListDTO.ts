import { StreetDTO } from './StreetDTO';
 
export interface StreetListDTO {
    streets: StreetDTO[];
}
