export interface ContactDTO {
    note: string;
    documentPassword: string;
    website: string;
    preferredLanguage: string;
    otherContact: string;
    smsSupport: boolean;
    mmsSupport: boolean;
    phone2: string;
    version: number;
    phone: string;
    noSms: boolean;
    id: number;
    fax: string;
    email: string;
}
