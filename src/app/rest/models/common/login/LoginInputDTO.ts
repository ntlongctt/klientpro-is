export interface LoginInputDTO {
    password: string;
    username: string;
}
