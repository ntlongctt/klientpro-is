export interface SpecialtyDTO {
    code: string;
    shortcut: string;
    keywords: string;
    updateDisabled: boolean;
    name: string;
    laicName: string;
    id: number;
    version: number;
}
