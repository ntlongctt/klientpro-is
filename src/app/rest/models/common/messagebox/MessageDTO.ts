export interface MessageDTO {
    dateCreated: string;
    subject: string;
    author: string;
    messageTypeId: number;
    dateUpdateNotification: string;
    id: number;
    contentHtml: string;
    dateSent: string;
    version: number;
    messageTypeName: string;
    dateUpdated: string;
}
