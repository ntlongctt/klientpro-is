import { MenuItemDTO } from './MenuItemDTO';
 
export interface MenuItemDTO {
    locale: string;
    childs: MenuItemDTO[];
    url: string;
}
