import { MenuItemDTO } from './MenuItemDTO';
 
export interface PerspectiveDTO {
    locale: string;
    menu: MenuItemDTO[];
}
