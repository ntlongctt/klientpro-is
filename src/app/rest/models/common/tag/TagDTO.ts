export interface TagDTO {
    publicVisible: boolean;
    partnerVisible: boolean;
    tagTypeId: number;
    name: string;
    id: number;
    version: number;
}
