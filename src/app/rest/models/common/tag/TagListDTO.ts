import { TagTypeListItemDTO } from './TagTypeListItemDTO';
 
export interface TagListDTO {
    tagTypes: TagTypeListItemDTO[];
}
