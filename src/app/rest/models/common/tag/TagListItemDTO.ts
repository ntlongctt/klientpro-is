export interface TagListItemDTO {
    name: string;
    assigned: boolean;
    id: number;
}
