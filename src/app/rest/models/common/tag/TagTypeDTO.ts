export interface TagTypeDTO {
    name: string;
    id: number;
    version: number;
}
