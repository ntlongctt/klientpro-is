import { TagTypeDTO } from './TagTypeDTO';
 
export interface TagTypeListDTO {
    tagTypes: TagTypeDTO[];
}
