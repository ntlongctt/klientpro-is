import { TagListItemDTO } from './TagListItemDTO';
 
export interface TagTypeListItemDTO {
    name: string;
    id: number;
    tags: TagListItemDTO[];
}
