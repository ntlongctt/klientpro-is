import { BusinessPartnerCodeListItemDTO } from './BusinessPartnerCodeListItemDTO';
 
export interface BusinessPartnerCodeListDTO {
    partners: BusinessPartnerCodeListItemDTO[];
}
