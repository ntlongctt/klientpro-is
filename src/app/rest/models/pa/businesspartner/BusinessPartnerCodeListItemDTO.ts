export interface BusinessPartnerCodeListItemDTO {
    companyId: string;
    address: string;
    name: string;
    id: number;
}
