import { BpAddressDTO } from './detail/address/BpAddressDTO';
 
export interface BusinessPartnerDTO {
    nameShortcut: string;
    partnerFrom: string;
    note: string;
    vatin: string;
    vatFrequency: number;
    partnerTill: string;
    registrationRecord: string;
    headquartersAddress: BpAddressDTO;
    version: number;
    vatin2: string;
    nameNote: string;
    executive: string;
    companyId: string;
    ean: string;
    paymentMethodId: number;
    name: string;
    id: number;
    salutation: string;
}
