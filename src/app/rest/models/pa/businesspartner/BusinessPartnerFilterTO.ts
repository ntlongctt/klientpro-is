export interface BusinessPartnerFilterTO {
    activeOnly: boolean;
    departmentTypeId: number;
    departmentTags: number[];
    searchQuery: string;
    mcSystemId: number;
}
