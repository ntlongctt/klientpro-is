import { BusinessPartnerListItemDTO } from './BusinessPartnerListItemDTO';
 
export interface BusinessPartnerListDTO {
    partners: BusinessPartnerListItemDTO[];
    pageNo: number;
    totalCount: number;
}
