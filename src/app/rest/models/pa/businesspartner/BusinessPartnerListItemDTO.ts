export interface BusinessPartnerListItemDTO {
    nameShortcut: string;
    departmentName: string;
    validTill: string;
    departmentId: number;
    name: string;
    id: number;
    validFrom: string;
    subDepartmentsCount: number;
}
