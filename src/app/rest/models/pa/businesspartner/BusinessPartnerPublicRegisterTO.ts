import { BusinessPartnerDTO } from './BusinessPartnerDTO';
 
export interface BusinessPartnerPublicRegisterTO {
    data: BusinessPartnerDTO;
    searchCode: string;
}
