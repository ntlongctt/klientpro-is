import { BusinessPartnerDTO } from '../BusinessPartnerDTO';
 
export interface BpSuccessorDTO {
    businessPartnerByNewBpid: BusinessPartnerDTO;
    note: string;
    businessPartnerByOldBpid: BusinessPartnerDTO;
    changeDate: string;
    id: number;
    version: number;
}
