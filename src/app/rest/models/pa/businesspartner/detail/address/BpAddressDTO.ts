import { AddressDTO } from '../../../../common/contactinfo/address/AddressDTO';
import { BpAddressTypeDTO } from './BpAddressTypeDTO';
 
export interface BpAddressDTO {
    address: AddressDTO;
    postalAddress: boolean;
    transportInfo: string;
    recipient: string;
    bpaddressType: BpAddressTypeDTO;
    invoiceAddress: boolean;
    id: number;
    version: number;
    mapLink: string;
}
