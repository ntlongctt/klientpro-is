import { BpAddressListItemDTO } from './BpAddressListItemDTO';
 
export interface BpAddressListDTO {
    addresses: BpAddressListItemDTO[];
}
