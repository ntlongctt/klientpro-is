export interface BpAddressListItemDTO {
    zip: string;
    postalAddress: boolean;
    bpAddressTypeName: string;
    city: string;
    stateName: string;
    street: string;
    recipient: string;
    bpAddressTypeId: number;
    stateCode: string;
    invoiceAddress: boolean;
    id: number;
}
