export interface BpAddressTypeDTO {
    name: string;
    id: number;
    version: number;
}
