import { BankAccountDTO } from '../../../../common/bankaccount/BankAccountDTO';
 
export interface BpBankAccountDTO {
    bankAccount: BankAccountDTO;
    validTill: string;
    id: number;
    validFrom: string;
    version: number;
}
