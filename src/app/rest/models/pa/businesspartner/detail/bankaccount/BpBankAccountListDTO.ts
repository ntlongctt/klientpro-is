import { BpBankAccountListItemDTO } from './BpBankAccountListItemDTO';
 
export interface BpBankAccountListDTO {
    bankAccounts: BpBankAccountListItemDTO[];
}
