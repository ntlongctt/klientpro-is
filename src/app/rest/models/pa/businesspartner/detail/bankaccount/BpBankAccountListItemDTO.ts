export interface BpBankAccountListItemDTO {
    bankCode: string;
    validTill: string;
    id: number;
    validFrom: string;
    accountNumber: string;
}
