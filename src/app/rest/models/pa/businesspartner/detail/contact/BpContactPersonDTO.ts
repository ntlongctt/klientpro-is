import { ContactDTO } from '../../../../common/contactinfo/contact/ContactDTO';
 
export interface BpContactPersonDTO {
    note: string;
    contact: ContactDTO;
    name: string;
    id: number;
    salutation: string;
    position: string;
    version: number;
    bpaddressId: number;
}
