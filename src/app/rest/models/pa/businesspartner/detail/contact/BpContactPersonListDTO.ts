import { BpContactPersonListItemDTO } from './BpContactPersonListItemDTO';
 
export interface BpContactPersonListDTO {
    contactPersons: BpContactPersonListItemDTO[];
}
