export interface BpContactPersonListItemDTO {
    address: string;
    phone: string;
    name: string;
    id: number;
    position: string;
    email: string;
}
