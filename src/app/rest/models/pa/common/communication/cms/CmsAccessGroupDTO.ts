import { CmsAccessRuleDTO } from './CmsAccessRuleDTO';
 
export interface CmsAccessGroupDTO {
    rules: CmsAccessRuleDTO[];
    id: number;
}
