import { CmsAccessGroupDTO } from './CmsAccessGroupDTO';
 
export interface CmsAccessGroupListDTO {
    accessGroups: CmsAccessGroupDTO[];
}
