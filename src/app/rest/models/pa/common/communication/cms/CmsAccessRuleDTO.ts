export interface CmsAccessRuleDTO {
    valueName: string;
    name: string;
    id: number;
    value: number;
    version: number;
}
