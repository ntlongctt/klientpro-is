import { CmsAccessRuleListItemDTO } from './CmsAccessRuleListItemDTO';
 
export interface CmsAccessRuleListDTO {
    rules: CmsAccessRuleListItemDTO[];
}
