export interface CmsAccessRuleListItemDTO {
    name: string;
    id: number;
    tableName: string;
}
