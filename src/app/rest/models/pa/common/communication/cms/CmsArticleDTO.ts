export interface CmsArticleDTO {
    name: string;
    articleTypeId: number;
    id: number;
    version: number;
    content: string;
    order: number;
}
