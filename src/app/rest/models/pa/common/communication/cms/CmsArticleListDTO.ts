import { CmsArticleListItemDTO } from './CmsArticleListItemDTO';
 
export interface CmsArticleListDTO {
    articles: CmsArticleListItemDTO[];
}
