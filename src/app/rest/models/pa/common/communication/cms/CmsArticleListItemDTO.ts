import { CmsAccessGroupDTO } from './CmsAccessGroupDTO';
 
export interface CmsArticleListItemDTO {
    accessGroups: CmsAccessGroupDTO[];
    name: string;
    id: number;
    order: number;
}
