import { BusinessPartnerListItemDTO } from '../businesspartner/BusinessPartnerListItemDTO';
 
export interface DepartmentDTO {
    parentDepartmentId: number;
    departmentTypeId: number;
    businessPartners: BusinessPartnerListItemDTO[];
    partnerNote: string;
    version: number;
    parentDepartmentName: string;
    shortcut: string;
    privateNote: string;
    maxAge: number;
    specialtyId: number;
    minAge: number;
    name: string;
    publicNote: string;
    id: number;
}
