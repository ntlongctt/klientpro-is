import { DepartmentListItemDTO } from './DepartmentListItemDTO';
 
export interface DepartmentListDTO {
    departments: DepartmentListItemDTO[];
}
