export interface DepartmentListItemDTO {
    validTill: string;
    specialtyName: string;
    name: string;
    typeName: string;
    businessPartnerName: string;
    id: number;
    validFrom: string;
    subDepartmentsCount: number;
}
