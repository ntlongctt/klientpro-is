export interface DepartmentMCSystemDTO {
    validTill: string;
    id: number;
    participationTypeId: number;
    validFrom: string;
    version: number;
    mcSystemId: number;
}
