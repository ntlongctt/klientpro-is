import { DepartmentMCSystemListItemDTO } from './DepartmentMCSystemListItemDTO';
 
export interface DepartmentMCSystemListDTO {
    participations: DepartmentMCSystemListItemDTO[];
}
