export interface DepartmentMCSystemListItemDTO {
    validTill: string;
    mcSystemName: string;
    participationTypeName: string;
    id: number;
    validFrom: string;
}
