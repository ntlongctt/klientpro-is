export interface DepartmentAddressDTO {
    bpAddressId: number;
    publicVisible: boolean;
    partnerVisible: boolean;
    id: number;
    version: number;
}
