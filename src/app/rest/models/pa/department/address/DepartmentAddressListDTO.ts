import { DepartmentAddressListItemDTO } from './DepartmentAddressListItemDTO';
 
export interface DepartmentAddressListDTO {
    addresses: DepartmentAddressListItemDTO[];
}
