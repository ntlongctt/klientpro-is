import { BpAddressListItemDTO } from '../../businesspartner/detail/address/BpAddressListItemDTO';
 
export interface DepartmentAddressListItemDTO {
    bpAddress: BpAddressListItemDTO;
    publicVisible: boolean;
    partnerVisible: boolean;
    id: number;
}
