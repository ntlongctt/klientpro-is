export interface DepartmentContactPersonDTO {
    bpContactPersonId: number;
    publicVisible: boolean;
    partnerVisible: boolean;
    id: number;
    version: number;
}
