import { DepartmentContactPersonListItemDTO } from './DepartmentContactPersonListItemDTO';
 
export interface DepartmentContactPersonListDTO {
    contactPersons: DepartmentContactPersonListItemDTO[];
}
