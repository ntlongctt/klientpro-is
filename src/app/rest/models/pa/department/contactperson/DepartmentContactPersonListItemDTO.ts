import { BpContactPersonListItemDTO } from '../../businesspartner/detail/contact/BpContactPersonListItemDTO';
 
export interface DepartmentContactPersonListItemDTO {
    publicVisible: boolean;
    partnerVisible: boolean;
    bpContactPerson: BpContactPersonListItemDTO;
    id: number;
}
