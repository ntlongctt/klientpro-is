export interface McSystemDTO {
    note: string;
    accountCode: string;
    validTill: string;
    shortcut: string;
    areaId: number;
    mcSystemTypeId: number;
    name: string;
    parentMcSystemId: number;
    id: number;
    validFrom: string;
    version: number;
}
