import { McSystemListItemDTO } from './McSystemListItemDTO';
 
export interface McSystemListDTO {
    pageNo: number;
    mcSystems: McSystemListItemDTO[];
    totalCount: number;
}
