export interface McSystemListItemDTO {
    validTill: string;
    mcSystemTypeName: string;
    areaName: string;
    name: string;
    id: number;
    validFrom: string;
}
