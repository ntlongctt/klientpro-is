export interface MessageAttachmentDTO {
    fileName: string;
    fileSize: number;
    id: number;
    version: number;
    contentType: string;
}
