export interface MessageCreateDTO {
    subject: string;
    author: string;
    messageTypeId: number;
    contentHtml: string;
    messageTypeName: string;
}
