import { MessageListItemDTO } from './MessageListItemDTO';
 
export interface MessageListDTO {
    pageNo: number;
    messages: MessageListItemDTO[];
    totalCount: number;
}
