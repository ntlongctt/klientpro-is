export interface MessageListItemDTO {
    dateCreated: string;
    subject: string;
    author: string;
    messageTypeId: number;
    dateUpdateNotification: string;
    id: number;
    dateSent: string;
    messageTypeName: string;
    dateUpdated: string;
}
