import { MessageAttachmentDTO } from './MessageAttachmentDTO';
 
export interface MessageRecipientDTO {
    recipientId: number;
    messageAttachements: MessageAttachmentDTO[];
    id: number;
    messageRecipientTypeName: string;
    version: number;
    messageRecipientTypeId: number;
}
