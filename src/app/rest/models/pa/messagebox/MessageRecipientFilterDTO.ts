export interface MessageRecipientFilterDTO {
    mcSystemType: number;
    mcSystemBefore: string;
    mcSystemAfter: string;
    mcSystem: number;
    tags: number[];
}
