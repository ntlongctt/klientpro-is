import { PaRoleDTO } from './PaRoleDTO';
 
export interface PaRolesDTO {
    roles: PaRoleDTO[];
}
