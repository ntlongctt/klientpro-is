import { PaRoleDTO } from './PaRoleDTO';
 
export interface PaUserDTO {
    note: string;
    updatedTime: string;
    roles: PaRoleDTO[];
    userStateName: string;
    fullName: string;
    userName: string;
    userStateId: number;
    version: number;
    password: string;
    phone: string;
    passwordChangedTime: string;
    perspective: string;
    id: number;
    email: string;
}
