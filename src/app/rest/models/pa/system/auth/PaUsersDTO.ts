import { PaUserDTO } from './PaUserDTO';
 
export interface PaUsersDTO {
    pageNo: number;
    totalCount: number;
    users: PaUserDTO[];
}
