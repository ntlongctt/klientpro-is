export interface UserStateDTO {
    valid: boolean;
    updateDisabled: boolean;
    name: string;
    id: number;
    version: number;
}
