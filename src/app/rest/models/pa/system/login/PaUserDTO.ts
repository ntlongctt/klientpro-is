export interface PaUserDTO {
    note: string;
    updatedTime: string;
    phone: string;
    passwordChangedTime: string;
    perspective: string;
    fullName: string;
    id: number;
    userName: string;
    userStateId: number;
    authorities: string[];
    email: string;
}
