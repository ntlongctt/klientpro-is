export interface TestTimeDTO {
    dtoDate: string;
    dtoLocalTime: string;
    dtoLocalDateTime: string;
    urlLocalDateTime: string;
    urlLocalDate: string;
    dtoZonedDateTime: string;
    urlZonedDateTime: string;
    dtoLocalDate: string;
    urlLocalTime: string;
}
