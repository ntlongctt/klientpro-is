import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { CodeListDTO } from '../../models/common/common/CodeListDTO';

/**
 *  Rest {RestApiServiceType} controller class for CodeList module
 *  <p>
 *  The service class instances needed by this controller are injected by Spring (see the @Autowired annotations)
 *  <p>
 *  
 *  @author Jan Tesar over {@link ModuleGenerator}
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class CodeListService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Returns items of specified codelist, using structure: id, name 
     *  
     *  @param codelistName - requested codelist name
     *  @return list of codelist items, structure: id, name 
     *  @throws CodeListException, InvalidParameterException
     */
    getCodeListItems({codelistName}: {codelistName: string}): Observable<CodeListDTO> {
 
        let params = new HttpParams();
        params = params.append('codelistName', codelistName);
 
        return this._http.Get<CodeListDTO>(`/rest/common/codeListService/codeListItems`, {params});
    }

}
