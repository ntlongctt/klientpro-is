import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { StateListDTO } from '../../models/common/contactinfo/address/StateListDTO';
import { StreetListDTO } from '../../models/common/contactinfo/address/StreetListDTO';
import { CityListDTO } from '../../models/common/contactinfo/address/CityListDTO';

@Injectable()
export class ContactService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Find state by query or return all states if query is null
     *  @param query - part of state name
     *  @return list of states
     */
    findState({query}: {query: string}): Observable<StateListDTO> {
 
        let params = new HttpParams();
        if(query){
            params = params.append('query', query);
        }
 
        return this._http.Get<StateListDTO>(`/rest/common/contactService/states/`, {params});
    }

    /**
     *  Find city by query or return all cities if query is null
     *  @param query - part of city name
     *  @return list of cities
     */
    findCity({query}: {query: string}): Observable<CityListDTO> {
 
        let params = new HttpParams();
        if(query){
            params = params.append('query', query);
        }
 
        return this._http.Get<CityListDTO>(`/rest/common/contactService/cities/`, {params});
    }

    /**
     *  Find city's street by query or return all streets of city if query is null
     *  @param query - part of street name
     *  @return list of streets
     */
    findCityStreet({cityCode, query}: {cityCode: string, query: string}): Observable<StreetListDTO> {
 
        let params = new HttpParams();
        if(query){
            params = params.append('query', query);
        }
 
        return this._http.Get<StreetListDTO>(`/rest/common/contactService/cities/${cityCode}/streets/`, {params});
    }

}
