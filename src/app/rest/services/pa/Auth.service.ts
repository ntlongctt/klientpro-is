import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { PaUserDTO } from '../../models/pa/system/auth/PaUserDTO';
import { PaRolesDTO } from '../../models/pa/system/auth/PaRolesDTO';
import { PaUsersDTO } from '../../models/pa/system/auth/PaUsersDTO';

/**
 *  Rest PA controller class for PaAuth module
 *  - manage users of the administration portal
 *  <p>
 *  The service class instances needed by this controller are injected by Spring (see the @Autowired annotations)
 *  <p>
 *
 *  @author Vaclav Hromadko over {@link ModuleCreator}
 *
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class AuthService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Get a page from the list of users
     *  
     *  @param searchQuery (optional)
     *  @param start - number of the first record
     *  @param limit - page size
     *  @return specified portion of the user list; total number of users
     *  @throws PagingException
     *  @throws InvalidParameterException
     */
    getUsers({searchQuery, start, limit}: {searchQuery: string, start: number, limit: number}): Observable<PaUsersDTO> {
 
        let params = new HttpParams();
        if(searchQuery){
            params = params.append('searchQuery', searchQuery);
        }
        params = params.append('start', start.toString());
        params = params.append('limit', limit.toString());
 
        return this._http.Get<PaUsersDTO>(`/rest/pa/authService/users`, {params});
    }

    /**
     *  Create new user
     *  @return new user object from database
     *  @throws InvalidParameterException
     *  @throws AuthException
     *  @throws SpecialUserException
     */
    addUser({userDTO}: {userDTO: PaUserDTO}): Observable<PaUserDTO> {
 
        return this._http.Post<PaUserDTO>(`/rest/pa/authService/users`, userDTO ? userDTO : {});
    }

    /**
     *  Load user data
     *  @return loaded user object from database
     */
    getUser({userId}: {userId: number}): Observable<PaUserDTO> {
 
        return this._http.Get<PaUserDTO>(`/rest/pa/authService/users/${userId}`);
    }

    /**
     *  Update user data
     *  @return update state
     *  @throws AuthException
     *  @throws InvalidParameterException
     *  @throws SpecialUserException
     */
    updateUser({userId, userDTO}: {userId: number, userDTO: PaUserDTO}): Observable<PaUserDTO> {
 
        return this._http.Put<PaUserDTO>(`/rest/pa/authService/users/${userId}`, userDTO ? userDTO : {});
    }

    /**
     *  Delete user
     */
    deleteUser({userId}: {userId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/authService/users/${userId}`);
    }

    /**
     *  Load all PA roles
     *  @return all PA roles
     */
    getRoles(): Observable<PaRolesDTO> {
        return this._http.Get<PaRolesDTO>(`/rest/pa/authService/roles`);
    }
    /**
     *  Reset user password
     *  @return changed user object WITH READABLE NEW PASSWORD
     *  @throws AuthException
     *  @throws InvalidParameterException
     *  @throws SystemException
     *  @throws SpecialUserException
     */
    resetUserPassword({userId}: {userId: number}): Observable<PaUserDTO> {
 
        return this._http.Put<PaUserDTO>(`/rest/pa/authService/users/${userId}/resetPassword`, {});
    }

    /**
     * Return list of perspectives
     */
    getPerspectives(): Observable<string[]> {
        return this._http.Get<string[]>(`/rest/pa/authService/perspectives`);
    }
}
