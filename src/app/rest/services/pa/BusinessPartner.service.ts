import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { BusinessPartnerListDTO } from '../../models/pa/businesspartner/BusinessPartnerListDTO';
import { BpAddressDTO } from '../../models/pa/businesspartner/detail/address/BpAddressDTO';
import { BpContactPersonDTO } from '../../models/pa/businesspartner/detail/contact/BpContactPersonDTO';
import { BusinessPartnerDTO } from '../../models/pa/businesspartner/BusinessPartnerDTO';
import { BusinessPartnerPublicRegisterTO } from '../../models/pa/businesspartner/BusinessPartnerPublicRegisterTO';
import { BpBankAccountDTO } from '../../models/pa/businesspartner/detail/bankaccount/BpBankAccountDTO';
import { BpBankAccountListDTO } from '../../models/pa/businesspartner/detail/bankaccount/BpBankAccountListDTO';
import { BpContactPersonListDTO } from '../../models/pa/businesspartner/detail/contact/BpContactPersonListDTO';
import { BusinessPartnerFilterTO } from '../../models/pa/businesspartner/BusinessPartnerFilterTO';
import { BusinessPartnerCodeListDTO } from '../../models/pa/businesspartner/BusinessPartnerCodeListDTO';
import { BpAddressListDTO } from '../../models/pa/businesspartner/detail/address/BpAddressListDTO';

/**
 *  Rest {RestApiServiceType} controller class for BusinessPartner module
 *  <p>
 *  The service class instances needed by this controller are injected by Spring (see the @Autowired annotations)
 *  <p>
 *  
 *  @author Vaclav Hromadko over {@link ModuleCreator}
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class BusinessPartnerService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Search for business partners
     *  <li>with pagination support</li>
     *  <li>with filtering options</li>
     *  Filtering options:
     *  <li>searchQuery: search string in companyId, business partner name, department name, address </li>
     *  <li>departmentTypeId: selected department type only</li>
     *  <li>mcSystemId: department is part of selected MCSystem</li>
     *  <li>activeOnly: business partner is valid</li>
     *  <li>departmentTags: department has selected Tags</li>
     *  
     */
    findBusinessPartners({start, limit, filter}: {start: number, limit: number, filter: BusinessPartnerFilterTO}): Observable<BusinessPartnerListDTO> {
 
        let params = new HttpParams();
        params = params.append('start', start.toString());
        params = params.append('limit', limit.toString());
 
        return this._http.Post<BusinessPartnerListDTO>(`/rest/pa/businessPartnerService/findBusinessPartners`, filter ? filter : {}, {params});
    }

    /**
     *  Search business partners by name
     *  - search query must be at least two characters long
     *  @param query - business partner name
     *  @return
     */
    findBusinessPartnerByName({query}: {query: string}): Observable<BusinessPartnerCodeListDTO> {
 
        let params = new HttpParams();
        params = params.append('query', query);
 
        return this._http.Get<BusinessPartnerCodeListDTO>(`/rest/pa/businessPartnerService/findBusinessPartnersByName`, {params});
    }

    /**
     *  Search business partner data in public register.
     *  
     *  @param companyId - in Czech republic is ICO (identificate number of organization)
     *  @return business partner data and searchCode (string: EXIST,FOUND or NOT-FOUND)
     *  @author Vaclav Hromadko
     *  @throws SystemException 
     */
    findBusinessPartnerInPublicRegister({companyId}: {companyId: number}): Observable<BusinessPartnerPublicRegisterTO> {
 
        return this._http.Get<BusinessPartnerPublicRegisterTO>(`/rest/pa/businessPartnerService/publicRegister/${companyId}`);
    }

    /**
     *  Create business partner (without adresses, contacts,...).
     *  
     *  @param BODY - business partner data
     *  @author Vaclav Hromadko
     *  @throws KnownException 
     */
    addBusinessPartner({body}: {body: BusinessPartnerDTO}): Observable<BusinessPartnerDTO> {
 
        return this._http.Post<BusinessPartnerDTO>(`/rest/pa/businessPartnerService/businessPartners`, body ? body : {});
    }

    /**
     *  Find business partner by ID.
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @return bussiness partner by id 
     *  @author Vaclav Hromadko
     *  @throws KnownException 
     */
    getBusinessPartner({partnerId}: {partnerId: number}): Observable<BusinessPartnerDTO> {
 
        return this._http.Get<BusinessPartnerDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}`);
    }

    /**
     *  Update business partner data (without adresses, contacts,...).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param BODY - business partner data 
     *  @author Vaclav Hromadko
     *  @throws KnownException 
     */
    updateBusinessPartner({partnerId, body}: {partnerId: number, body: BusinessPartnerDTO}): Observable<BusinessPartnerDTO> {
 
        return this._http.Put<BusinessPartnerDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}`, body ? body : {});
    }

    /**
     *  Delete business partner by ID.
     *  
     *  @param partnerId - id of business partner (not company id) 
     *  @author Vaclav Hromadko
     *  @throws KnownException 
     */
    deleteBusinessPartner({partnerId}: {partnerId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}`);
    }

    /**
     *  Find all addresses of business partner (only basic data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @return all addresses of bussines partner
     */
    getBusinessPartnerAddresses({partnerId}: {partnerId: number}): Observable<BpAddressListDTO> {
 
        return this._http.Get<BpAddressListDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/addresses`);
    }

    /**
     *  Add new address of business partner.
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param BODY - business partner's address data
     *  @return new address data (with generated id)
     *  @throws KnownException 
     */
    addBusinessPartnerAddress({partnerId, address}: {partnerId: number, address: BpAddressDTO}): Observable<BpAddressDTO> {
 
        return this._http.Post<BpAddressDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/addresses`, address ? address : {});
    }

    /**
     *  Find specified address of business partner (full data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param bpAddressId - id of business partner's address
     *  @return address by id
     *  @throws KnownException 
     */
    getBusinessPartnerAddress({partnerId, bpAddressId}: {partnerId: number, bpAddressId: number}): Observable<BpAddressDTO> {
 
        return this._http.Get<BpAddressDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/addresses/${bpAddressId}`);
    }

    /**
     *  Update specified address of business partner (full data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param partnerId - id of business partner's updated address
     *  @param BODY - business partner's address data
     *  @return updated address data
     *  @throws KnownException 
     */
    updateBusinessPartnerAddress({partnerId, bpAddressId, address}: {partnerId: number, bpAddressId: number, address: BpAddressDTO}): Observable<BpAddressDTO> {
 
        return this._http.Put<BpAddressDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/addresses/${bpAddressId}`, address ? address : {});
    }

    /**
     *  Delete specified address of business partner.
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param partnerId - id of business partner's updated address
     *  @throws KnownException 
     */
    deleteBusinessPartnerAddress({partnerId, bpAddressId}: {partnerId: number, bpAddressId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/addresses/${bpAddressId}`);
    }

    /**
     *  Find all or search by query contact persons of business partner (only basic data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param query (optional) - name or surname of contact person
     *  @return all contact persons or searched by query (if filled)
     */
    getBusinessPartnerContactPersons({partnerId, query}: {partnerId: number, query: string}): Observable<BpContactPersonListDTO> {
 
        let params = new HttpParams();
        if(query){
            params = params.append('query', query);
        }
 
        return this._http.Get<BpContactPersonListDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/contactPersons`, {params});
    }

    /**
     *  Add new contact person of business partner.
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param BODY - business partner's contact person data
     *  @return new contact person data (with generated id)
     *  @throws KnownException 
     */
    addBusinessPartnerContactPerson({partnerId, person}: {partnerId: number, person: BpContactPersonDTO}): Observable<BpContactPersonDTO> {
 
        return this._http.Post<BpContactPersonDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/contactPersons`, person ? person : {});
    }

    /**
     *  Find specified contact person of business partner by ID (full data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param personId - id of business partner's contact person
     *  @return contact person by id
     *  @throws KnownException 
     */
    getBusinessPartnerContactPerson({partnerId, personId}: {partnerId: number, personId: number}): Observable<BpContactPersonDTO> {
 
        return this._http.Get<BpContactPersonDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/contactPersons/${personId}`);
    }

    /**
     *  Update specified contact person of business partner (full data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param partnerId - id of business partner's updated contact person
     *  @param BODY - business partner's contact person data
     *  @return updated contact person data
     *  @throws KnownException 
     */
    updateBusinessPartnerContactPerson({partnerId, personId, person}: {partnerId: number, personId: number, person: BpContactPersonDTO}): Observable<BpContactPersonDTO> {
 
        return this._http.Put<BpContactPersonDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/contactPersons/${personId}`, person ? person : {});
    }

    /**
     *  Delete specified contact person of business partner.
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param partnerId - id of business partner's contact person
     *  @throws KnownException 
     */
    deleteBusinessPartnerContactPerson({partnerId, personId}: {partnerId: number, personId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/contactPersons/${personId}`);
    }

    /**
     *  Find all bank accounts of business partner (only basic data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @return all bank accounts
     */
    getBusinessPartnerBankAccounts({partnerId}: {partnerId: number}): Observable<BpBankAccountListDTO> {
 
        return this._http.Get<BpBankAccountListDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/bankAccounts`);
    }

    /**
     *  Add new bank account of business partner.
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param BODY - business partner's bank account data
     *  @return new bank account data (with generated id)
     *  @throws KnownException 
     */
    addBusinessPartnerBankAccount({partnerId, account}: {partnerId: number, account: BpBankAccountDTO}): Observable<BpBankAccountDTO> {
 
        return this._http.Post<BpBankAccountDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/bankAccounts`, account ? account : {});
    }

    /**
     *  Find specified bank account of business partner by ID (full data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param accountId - id of business partner's bank account
     *  @return bank account by id
     *  @throws KnownException 
     */
    getBusinessPartnerBankAccount({partnerId, accountId}: {partnerId: number, accountId: number}): Observable<BpBankAccountDTO> {
 
        return this._http.Get<BpBankAccountDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/bankAccounts/${accountId}`);
    }

    /**
     *  Update specified bank account of business partner (full data).
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param partnerId - id of business partner's updated bank account
     *  @param BODY - business partner's bank account data
     *  @return updated bank account data
     *  @throws KnownException 
     */
    updateBusinessPartnerBankAccount({partnerId, accountId, account}: {partnerId: number, accountId: number, account: BpBankAccountDTO}): Observable<BpBankAccountDTO> {
 
        return this._http.Put<BpBankAccountDTO>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/bankAccounts/${accountId}`, account ? account : {});
    }

    /**
     *  Delete specified bank account of business partner.
     *  
     *  @param partnerId - id of business partner (not company id)
     *  @param partnerId - id of business partner's bank account
     *  @throws KnownException 
     */
    deleteBusinessPartnerBankAccount({partnerId, accountId}: {partnerId: number, accountId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/businessPartnerService/businessPartners/${partnerId}/bankAccounts/${accountId}`);
    }

}
