import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { CmsArticleDTO } from '../../models/pa/common/communication/cms/CmsArticleDTO';
import { CmsAccessGroupListDTO } from '../../models/pa/common/communication/cms/CmsAccessGroupListDTO';
import { CmsAccessGroupDTO } from '../../models/pa/common/communication/cms/CmsAccessGroupDTO';
import { CmsArticleListDTO } from '../../models/pa/common/communication/cms/CmsArticleListDTO';
import { CmsAccessRuleListDTO } from '../../models/pa/common/communication/cms/CmsAccessRuleListDTO';

/**
 *  Rest {RestApiServiceType} controller class for Cms module
 *  <p>
 *  The service class instances needed by this controller are injected by Spring (see the @Autowired annotations)
 *  <p>
 *  
 *  @author tesar over {@link ModuleCreator}
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class CmsService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Vraci seznam podporovanych pravidel pro urceny typ clanku
     *  
     *  @param articleTypeId (null) - typ clanku
     *  @return
     *  @throws KnownException
     */
    getSupportedCmsAccessRules({articleTypeId}: {articleTypeId: number}): Observable<CmsAccessRuleListDTO> {
 
        return this._http.Get<CmsAccessRuleListDTO>(`/rest/pa/cmsService/cmsAccessRules/${articleTypeId}`);
    }

    /**
     *  Vraci seznam clanku spolu s pristupovymi pravidly
     *  @param cmsArticleId
     *  @return
     *  @throws KnownException
     */
    getCmsArticles({articleTypeId}: {articleTypeId: number}): Observable<CmsArticleListDTO> {
 
        let params = new HttpParams();
        params = params.append('articleTypeId', articleTypeId.toString());
 
        return this._http.Get<CmsArticleListDTO>(`/rest/pa/cmsService/cmsArticles`, {params});
    }

    /**
     *  Detail clanku
     */
    getCmsArticle({articleId}: {articleId: number}): Observable<CmsArticleDTO> {
 
        return this._http.Get<CmsArticleDTO>(`/rest/pa/cmsService/cmsArticles/${articleId}`);
    }

    /**
     *  Vytvoreni noveho clanku
     */
    createCmsArticle({body}: {body: CmsArticleDTO}): Observable<CmsArticleDTO> {
 
        return this._http.Post<CmsArticleDTO>(`/rest/pa/cmsService/cmsArticles`, body ? body : {});
    }

    /**
     *  Aktualizace clanku
     */
    updateCmsArticle({articleId, body}: {articleId: number, body: CmsArticleDTO}): Observable<CmsArticleDTO> {
 
        return this._http.Put<CmsArticleDTO>(`/rest/pa/cmsService/cmsArticles/${articleId}`, body ? body : {});
    }

    /**
     *  Smazani clanku
     */
    deleteCmsArticle({articleId}: {articleId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/cmsService/cmsArticles/${articleId}`);
    }

    /**
     *  Vraci seznam skupin, ktere urcuji pristupova prava k clanku
     *  @param cmsArticleId
     *  @return
     *  @throws KnownException
     */
    getCmsArticleAccessGroups({articleId}: {articleId: number}): Observable<CmsAccessGroupListDTO> {
 
        return this._http.Get<CmsAccessGroupListDTO>(`/rest/pa/cmsService/cmsArticles/${articleId}/cmsAccessGroups`);
    }

    /**
     *  Vytvoreni nove skupiny pravidel
     */
    createCmsAccessGroup({articleId, body}: {articleId: number, body: CmsAccessGroupDTO}): Observable<CmsAccessGroupDTO> {
 
        return this._http.Post<CmsAccessGroupDTO>(`/rest/pa/cmsService/cmsArticles/${articleId}/cmsAccessGroups`, body ? body : {});
    }

    /**
     *  Aktualizace pravidel dane skupiny
     */
    updateCmsAccessGroup({articleId, accessGroupId, body}: {articleId: number, accessGroupId: number, body: CmsAccessGroupDTO}): Observable<CmsAccessGroupDTO> {
 
        return this._http.Put<CmsAccessGroupDTO>(`/rest/pa/cmsService/cmsArticles/${articleId}/cmsAccessGroups${accessGroupId}`, body ? body : {});
    }

    /**
     *  Smazani skupiny pravidel pro pristup ke clanku
     */
    deleteCmsAccessGroup({articleId, accessGroupId}: {articleId: number, accessGroupId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/cmsService/cmsArticles/${articleId}/cmsAccessGroups${accessGroupId}`);
    }

}
