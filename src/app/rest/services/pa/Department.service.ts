import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { DepartmentAddressDTO } from '../../models/pa/department/address/DepartmentAddressDTO';
import { DepartmentDTO } from '../../models/pa/department/DepartmentDTO';
import { DepartmentAddressListDTO } from '../../models/pa/department/address/DepartmentAddressListDTO';
import { DepartmentListDTO } from '../../models/pa/department/DepartmentListDTO';
import { DepartmentContactPersonDTO } from '../../models/pa/department/contactperson/DepartmentContactPersonDTO';
import { DepartmentContactPersonListDTO } from '../../models/pa/department/contactperson/DepartmentContactPersonListDTO';
import { DepartmentMCSystemDTO } from '../../models/pa/department/DepartmentMCSystemDTO';
import { DepartmentMCSystemListDTO } from '../../models/pa/department/DepartmentMCSystemListDTO';

/**
 *  Rest {RestApiServiceType} controller class for Department module
 *  <p>
 *  The service class instances needed by this controller are injected by Spring (see the @Autowired annotations)
 *  <p>
 *  
 *  @author Jan Tesar over {@link ModuleCreator}
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class DepartmentService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Add new department.
     *  
     *  @param BODY - department data
     *  @return new department data (with generated id)
     *  @throws KnownException 
     */
    addDepartment({bpId, department}: {bpId: number, department: DepartmentDTO}): Observable<DepartmentDTO> {
 
        let params = new HttpParams();
        if(bpId){
            params = params.append('bpId', bpId.toString());
        }
 
        return this._http.Post<DepartmentDTO>(`/rest/pa/departmentService/department`, department ? department : {}, {params});
    }

    /**
     *  Update department.
     *  
     *  @param BODY - department data
     *  @param bpChangeDate - business partner change date
     *  @return updated department data
     *  @throws KnownException 
     */
    updateDepartment({departmentId, bpChangeId, bpChangeDate, department}: {departmentId: number, bpChangeId: number, bpChangeDate: string, department: DepartmentDTO}): Observable<DepartmentDTO> {
 
        let params = new HttpParams();
        if(bpChangeId){
            params = params.append('bpChangeId', bpChangeId.toString());
        }
        if(bpChangeDate){
            params = params.append('bpChangeDate', bpChangeDate);
        }
 
        return this._http.Put<DepartmentDTO>(`/rest/pa/departmentService/department/${departmentId}`, department ? department : {}, {params});
    }

    /**
     *  delete department.
     *  
     *  @throws KnownException 
     */
    deleteDepartment({departmentId}: {departmentId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/departmentService/department/${departmentId}`);
    }

    /**
     *  get department by id
     *  
     *  @return department data
     *  @throws KnownException 
     */
    getDepartment({departmentId}: {departmentId: number}): Observable<DepartmentDTO> {
 
        return this._http.Get<DepartmentDTO>(`/rest/pa/departmentService/department/${departmentId}`);
    }

    /**
     *  Add new address of department.
     *  
     *  @param departmentId - id of department
     *  @param BODY - business partner's address data
     *  @return new address data (with generated id)
     *  @throws KnownException 
     */
    addDepartmentAddress({departmentId, address}: {departmentId: number, address: DepartmentAddressDTO}): Observable<DepartmentAddressDTO> {
 
        return this._http.Post<DepartmentAddressDTO>(`/rest/pa/departmentService/department/${departmentId}/addresses`, address ? address : {});
    }

    /**
     *  Update specified address of department (full data).
     *  
     *  @param departmentId - id of department
     *  @param departmentAddressId - id of business partner's updated address
     *  @param BODY - business partner's address data
     *  @return updated address data
     *  @throws KnownException 
     */
    updateDepartmentAddress({departmentId, departmentAddressId, address}: {departmentId: number, departmentAddressId: number, address: DepartmentAddressDTO}): Observable<DepartmentAddressDTO> {
 
        return this._http.Put<DepartmentAddressDTO>(`/rest/pa/departmentService/department/${departmentId}/addresses/${departmentAddressId}`, address ? address : {});
    }

    /**
     *  Delete specified address of department.
     *  
     *  @param departmentId - id of department
     *  @param departmentAddressId - id of business partner's updated address
     *  @throws KnownException 
     */
    deleteBusinessPartnerAddress({departmentId, departmentAddressId}: {departmentId: number, departmentAddressId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/departmentService/department/${departmentId}/addresses/${departmentAddressId}`);
    }

    /**
     *  Find specified address of business partner (full data).
     *  
     *  @param departmentId - id of department
     *  @param departmentAddressId - id of department's address
     *  @return address by id
     *  @throws KnownException 
     */
    getDepartmentAddress({departmentId, departmentAddressId}: {departmentId: number, departmentAddressId: number}): Observable<DepartmentAddressDTO> {
 
        return this._http.Get<DepartmentAddressDTO>(`/rest/pa/departmentService/departments/${departmentId}/addresses/${departmentAddressId}`);
    }

    /**
     *  Find all addresses of department (only basic data).
     *  
     *  @param departmentId - id of department
     *  @return all addresses of department
     */
    getDepartmentAddresses({departmentId}: {departmentId: number}): Observable<DepartmentAddressListDTO> {
 
        return this._http.Get<DepartmentAddressListDTO>(`/rest/pa/departmentService/departments/${departmentId}/addresses`);
    }

    /**
     *  Add new contact person of department.
     *  
     *  @param departmentId - id of department
     *  @param BODY - business partner's contact person data
     *  @return new contact person data (with generated id)
     *  @throws KnownException 
     */
    addDepartmentContactPerson({departmentId, contactPerson}: {departmentId: number, contactPerson: DepartmentContactPersonDTO}): Observable<DepartmentContactPersonDTO> {
 
        return this._http.Post<DepartmentContactPersonDTO>(`/rest/pa/departmentService/department/${departmentId}/contactpersons`, contactPerson ? contactPerson : {});
    }

    /**
     *  Update specified address of department (full data).
     *  
     *  @param departmentId - id of department
     *  @param departmentContactPersonId - id of business partner's updated address
     *  @param BODY - business partner's contact person data
     *  @return updated contact person data
     *  @throws KnownException 
     */
    updateDepartmentContactPerson({departmentId, departmentContactPersonId, contactPerson}: {departmentId: number, departmentContactPersonId: number, contactPerson: DepartmentContactPersonDTO}): Observable<DepartmentContactPersonDTO> {
 
        return this._http.Put<DepartmentContactPersonDTO>(`/rest/pa/departmentService/department/${departmentId}/contactpersons/${departmentContactPersonId}`, contactPerson ? contactPerson : {});
    }

    /**
     *  Delete specified address of department.
     *  
     *  @param departmentId - id of department
     *  @param departmentContactPersonId - id of business partner's updated contact person
     *  @throws KnownException 
     */
    deleteBusinessPartnerContactPerson({departmentId, departmentContactPersonId}: {departmentId: number, departmentContactPersonId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/departmentService/department/${departmentId}/contactpersons/${departmentContactPersonId}`);
    }

    /**
     *  Find specified contact person of department (full data).
     *  
     *  @param departmentId - id of department
     *  @param departmentContactPersonId - id of department's contact person
     *  @return department contact person by id
     *  @throws KnownException 
     */
    getDepartmentContactPerson({departmentId, departmentContactPersonId}: {departmentId: number, departmentContactPersonId: number}): Observable<DepartmentContactPersonDTO> {
 
        return this._http.Get<DepartmentContactPersonDTO>(`/rest/pa/departmentService/departments/${departmentId}/contactpersons/${departmentContactPersonId}`);
    }

    /**
     *  Find all contact persons of department (only basic data).
     *  
     *  @param departmentId - id of department
     *  @return all contact persons of department
     */
    getDepartmentContactPersons({departmentId, query}: {departmentId: number, query: string}): Observable<DepartmentContactPersonListDTO> {
 
        let params = new HttpParams();
        if(query){
            params = params.append('query', query);
        }
 
        return this._http.Get<DepartmentContactPersonListDTO>(`/rest/pa/departmentService/departments/${departmentId}/contactpersons`, {params});
    }

    /**
     *  Add new mcsystem participation of department.
     *  
     *  @param departmentId - id of department
     *  @param BODY - mcsystem participation data
     *  @return new mcsystem participation data (with generated id)
     *  @throws KnownException 
     */
    addDepartmentMcSystem({departmentId, departmentMcSystem}: {departmentId: number, departmentMcSystem: DepartmentMCSystemDTO}): Observable<DepartmentMCSystemDTO> {
 
        return this._http.Post<DepartmentMCSystemDTO>(`/rest/pa/departmentService/department/${departmentId}/mcsystems`, departmentMcSystem ? departmentMcSystem : {});
    }

    /**
     *  Update mcsystem participation of department (full data).
     *  
     *  @param departmentId - id of department
     *  @param departmentMcSystemId - id of mc system participation
     *  @param BODY - mc system participation data
     *  @return updated contact person data
     *  @throws KnownException 
     */
    updateDepartmentMcSystem({departmentId, departmentMcSystemId, departmentMcSystem}: {departmentId: number, departmentMcSystemId: number, departmentMcSystem: DepartmentMCSystemDTO}): Observable<DepartmentMCSystemDTO> {
 
        return this._http.Put<DepartmentMCSystemDTO>(`/rest/pa/departmentService/department/${departmentId}/mcsystems/${departmentMcSystemId}`, departmentMcSystem ? departmentMcSystem : {});
    }

    /**
     *  Delete specified address of department.
     *  
     *  @param departmentId - id of department
     *  @param departmentContactPersonId - id of business partner's updated contact person
     *  @throws KnownException 
     */
    deleteDepartmentMcSystem({departmentId, departmentMcSystemId}: {departmentId: number, departmentMcSystemId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/departmentService/department/${departmentId}/mcsystems/${departmentMcSystemId}`);
    }

    /**
     *  Find specified contact person of department (full data).
     *  
     *  @param departmentId - id of department
     *  @param departmentContactPersonId - id of department's contact person
     *  @return department contact person by id
     *  @throws KnownException 
     */
    getDepartmentMcSystem({departmentId, departmentMcSystemId}: {departmentId: number, departmentMcSystemId: number}): Observable<DepartmentMCSystemDTO> {
 
        return this._http.Get<DepartmentMCSystemDTO>(`/rest/pa/departmentService/departments/${departmentId}/mcsystems/${departmentMcSystemId}`);
    }

    /**
     *  Find all contact persons of department (only basic data).
     *  
     *  @param departmentId - id of department
     *  @return all contact persons of department
     */
    getDepartmentMcSystems({departmentId, query}: {departmentId: number, query: string}): Observable<DepartmentMCSystemListDTO> {
 
        let params = new HttpParams();
        if(query){
            params = params.append('query', query);
        }
 
        return this._http.Get<DepartmentMCSystemListDTO>(`/rest/pa/departmentService/departments/${departmentId}/mcsystems`, {params});
    }

    /**
     *  Find all departments:
     *  <li>subordinate to given business partner</li>
     *  <li>subordinate to given department</li>
     *  Business partner id is ignored if both business partner and department are specified 
     *  In addition departments can be filtered by query string (searching in department name)
     *  
     *  @param bpId - business partner id
     *  @param departmentId - id of department
     *  @param query - search string in department name
     *  @return all addresses of department
     *  @throws InvalidParameterException 
     */
    getDepartments({bpId, departmentId, query}: {bpId: number, departmentId: number, query: string}): Observable<DepartmentListDTO> {
 
        let params = new HttpParams();
        if(bpId){
            params = params.append('bpId', bpId.toString());
        }
        if(departmentId){
            params = params.append('departmentId', departmentId.toString());
        }
        if(query){
            params = params.append('query', query);
        }
 
        return this._http.Get<DepartmentListDTO>(`/rest/pa/departmentService/departments`, {params});
    }

}
