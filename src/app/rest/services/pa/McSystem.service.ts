import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { CodeListDTO } from '../../models/common/common/CodeListDTO';
import { McSystemDTO } from '../../models/pa/mcsystem/McSystemDTO';
import { McSystemListDTO } from '../../models/pa/mcsystem/McSystemListDTO';

/**
 *  Rest {RestApiServiceType} controller class for McSystem module
 *  <p>
 *  The service class instances needed by this controller are injected by Spring (see the @Autowired annotations)
 *  <p>
 *  
 *  @author tesar over {@link ModuleCreator}
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class McSystemService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Get list of all MC systems
     *  <li> pageable </li>
     *  <li> ordered by validFrom, name</li>
     *  @param mcSystemId
     *  @return
     *  @throws KnownException
     */
    getMcSystems({start, limit}: {start: number, limit: number}): Observable<McSystemListDTO> {
 
        let params = new HttpParams();
        params = params.append('start', start.toString());
        params = params.append('limit', limit.toString());
 
        return this._http.Get<McSystemListDTO>(`/rest/pa/mcSystemService/mcSystems`, {params});
    }

    /**
     *  Get list of MC systems - for use as source for selectbox
     *  <li> ordered by name</li>
     *  @param mcSystemId
     *  @return
     *  @throws KnownException
     */
    getMcSystemsCodelist({mcSystemTypeId, activeOnly}: {mcSystemTypeId: number, activeOnly: boolean}): Observable<CodeListDTO> {
 
        let params = new HttpParams();
        if(mcSystemTypeId){
            params = params.append('mcSystemTypeId', mcSystemTypeId.toString());
        }
        params = params.append('activeOnly', activeOnly.toString());
 
        return this._http.Get<CodeListDTO>(`/rest/pa/mcSystemService/mcSystemsCodelist`, {params});
    }

    /**
     *  Read MC System from DB
     *  @param mcSystemId
     *  @return
     *  @throws KnownException
     */
    getMcSystem({mcSystemId}: {mcSystemId: number}): Observable<McSystemDTO> {
 
        return this._http.Get<McSystemDTO>(`/rest/pa/mcSystemService/mcSystems/${mcSystemId}`);
    }

    /**
     *  Create new MC system
     *  @param body
     *  @return
     *  @throws KnownException
     */
    createMcSystem({body}: {body: McSystemDTO}): Observable<McSystemDTO> {
 
        return this._http.Post<McSystemDTO>(`/rest/pa/mcSystemService/mcSystems`, body ? body : {});
    }

    /**
     *  Update MC System
     *  @param mcSystemId
     *  @param body
     *  @return
     *  @throws KnownException
     */
    updateMcSystem({mcSystemId, body}: {mcSystemId: number, body: McSystemDTO}): Observable<McSystemDTO> {
 
        return this._http.Put<McSystemDTO>(`/rest/pa/mcSystemService/mcSystems/${mcSystemId}`, body ? body : {});
    }

    /**
     *  Delete MC system 
     *  rare operation
     *  @param mcSystemId
     *  @throws KnownException
     */
    deleteMcSystem({mcSystemId}: {mcSystemId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/mcSystemService/mcSystems/${mcSystemId}`);
    }

}
