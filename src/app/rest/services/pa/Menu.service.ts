import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { MenuItemDTO } from '../../models/common/system/MenuItemDTO';

/**
 *  MENU SERVICES
 *  -------------
 *  
 *  Services for menu contains method getMenu.
 *  
 *  @author Vaclav Hromadko
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class MenuService {
    constructor(private _http: CustomHttpClient) {}

    /**
     * Returns main menu
     *  @return structured menu items 
     */
    getMainMenu(): Observable<MenuItemDTO[]> {
        return this._http.Get<MenuItemDTO[]>(`/rest/pa/menuService/getMainMenu`);
    }
    /**
     * Returns test menu (only for JUnit tests!)
     *  @return structured menu items 
     */
    getTestMenu(): Observable<MenuItemDTO[]> {
        return this._http.Get<MenuItemDTO[]>(`/rest/pa/menuService/getTestMenu`);
    }
    /**
     * Change selected perspective and return main menu
     *  @param perspective - string identificator of perspective
     *  @return structured menu items 
     */
    selectPerspective({perspective}: {perspective: string}): Observable<MenuItemDTO[]> {
 
        return this._http.Post<MenuItemDTO[]>(`/rest/pa/menuService/selectPerspective`, `"${perspective}"`);
    }

}
