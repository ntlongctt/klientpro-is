import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { MessageRecipientDTO } from '../../models/pa/messagebox/MessageRecipientDTO';
import { MessageDTO } from '../../models/common/messagebox/MessageDTO';
import { MessageRecipientFilterDTO } from '../../models/pa/messagebox/MessageRecipientFilterDTO';
import { MessageCreateDTO } from '../../models/pa/messagebox/MessageCreateDTO';
import { MessageAttachmentDTO } from '../../models/pa/messagebox/MessageAttachmentDTO';
import { MessageListDTO } from '../../models/pa/messagebox/MessageListDTO';

/**
 *  Rest {RestApiServiceType} controller class for Messagebox module
 *  <p>
 *  The service class instances needed by this controller are injected by Spring (see the @Autowired annotations)
 *  <p>
 *  
 *  @author Václav Hromádko over {@link ModuleCreator}
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class MessageboxService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Returns one page of messages (sorted by date descending)
     *  
     *  @param start - number of the first record
     *  @param limit - page size 
     *  @param filter (not required) filtering by *name* or *content*
     *  @return list of messages, total page count, number of returned page
     *  @throws KnownException
     */
    getMessages({start, limit, filter}: {start: number, limit: number, filter: string}): Observable<MessageListDTO> {
 
        let params = new HttpParams();
        params = params.append('start', start.toString());
        params = params.append('limit', limit.toString());
        if(filter){
            params = params.append('filter', filter);
        }
 
        return this._http.Get<MessageListDTO>(`/rest/pa/messageboxService/messages`, {params});
    }

    /**
     *  Returns data for display message by ID
     *  
     *  @param messageId
     *  @return data for selected message
     *  @throws KnownException
     */
    getMessage({messageId}: {messageId: number}): Observable<MessageDTO> {
 
        return this._http.Get<MessageDTO>(`/rest/pa/messageboxService/messages/${messageId}`);
    }

    /**
     *  Send selected message
     *  
     *  @param messageId
     *  @throws KnownException
     */
    sendMessage({messageId}: {messageId: number}): Observable<any> {
 
        return this._http.Put<any>(`/rest/pa/messageboxService/messages/${messageId}/send`, {});
    }

    /**
     *  Notify recipients about update of selected message
     *  
     *  @param messageId
     *  @throws KnownException
     */
    updateNotifyMessage({messageId}: {messageId: number}): Observable<any> {
 
        return this._http.Put<any>(`/rest/pa/messageboxService/messages/${messageId}/update-notify`, {});
    }

    /**
     *  Create new message and return it (with id and version filled)
     *  
     *  @param body
     *  @return new message data object
     *  @throws KnownException
     */
    createMessage({body}: {body: MessageCreateDTO}): Observable<MessageDTO> {
 
        return this._http.Post<MessageDTO>(`/rest/pa/messageboxService/messages/`, body ? body : {});
    }

    /**
     *  Copy message as new (with recipients and attachments)
     *  @param messageId
     *  @return id of new message (UI may be thus redirected to the new message)
     *  @throws KnownException
     */
    copyMessageAsNew({messageId, withAttachments, withRecipients}: {messageId: number, withAttachments: boolean, withRecipients: boolean}): Observable<number> {
 
        let params = new HttpParams();
        if(withAttachments){
            params = params.append('withAttachments', withAttachments.toString());
        }
        if(withRecipients){
            params = params.append('withRecipients', withRecipients.toString());
        }
 
        return this._http.Post<number>(`/rest/pa/messageboxService/messages/${messageId}`, {}, {params});
    }

    /**
     *  Updates message
     *  
     *  @param messageId
     *  @param body
     *  @return message data
     *  @throws KnownException
     */
    updateMessage({messageId, body}: {messageId: number, body: MessageDTO}): Observable<MessageDTO> {
 
        return this._http.Put<MessageDTO>(`/rest/pa/messageboxService/messages/${messageId}`, body ? body : {});
    }

    /**
     *  Delete message including all it's recipients, attachments and visits
     *  @param messageId
     *  @throws KnownException
     */
    deleteMessage({messageId}: {messageId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/messageboxService/messages/${messageId}`);
    }

    /**
     *  Returns attachment(s) for message (no pagination)
     *  @param messageId
     *  @return List of attachments
     *  @throws KnownException
     */
    getCommonAttachments({messageId}: {messageId: number}): Observable<any> {
 
        return this._http.Get<any>(`/rest/pa/messageboxService/messages/${messageId}/attachments`);
    }

    /**
     *  Download attachment file
     *  @param messageId
     *  @param attachmentId
     *  @return file
     *  @throws KnownException
     */
    getAttachment({messageId, attachmentId}: {messageId: number, attachmentId: number}): Observable<any> {
 
        return this._http.Get<any>(`/rest/pa/messageboxService/messages/${messageId}/attachments/${attachmentId}`);
    }

    /**
     *  Create new message attachment
     *  
     *  @param messageId
     *  @param recipientId (not required) common attachment if not filled
     *  @param file
     *  @return attachment metadata (to be displayed without full page redraw)
     *  @throws KnownException
     */
    addAttachment({messageId, recipientId, file}: {messageId: number, recipientId: number, file: File}): Observable<MessageAttachmentDTO> {
 
        const formdata: FormData = new FormData();
        formdata.append('file', file);
 
        let params = new HttpParams();
        if(recipientId){
            params = params.append('recipientId', recipientId.toString());
        }
 
        return this._http.Post<MessageAttachmentDTO>(`/rest/pa/messageboxService/messages/${messageId}/attachments`, formdata, {params});
    }

    /**
     *  Delete selected attachment from message
     *  
     *  @param messageId
     *  @param attachmentId
     *  @throws KnownException
     */
    deleteAttachment({messageId, attachmentId}: {messageId: number, attachmentId: number}): Observable<any> {
 
        return this._http.Delete<any>(`/rest/pa/messageboxService/messages/${messageId}/attachments/${attachmentId}`);
    }

    /**
     *  Returns recipient(s) and their attachments for message (no pagination)
     *  @param messageId
     *  @return List of recipients and theirs attachments
     *  @throws KnownException
     */
    getRecipients({filter, messageId}: {filter: string, messageId: number}): Observable<any> {
 
        let params = new HttpParams();
        if(filter){
            params = params.append('filter', filter);
        }
 
        return this._http.Get<any>(`/rest/pa/messageboxService/messages/${messageId}/recipients`, {params});
    }

    /**
     *  Add all departments as recipients
     *  @return added recipient's data
     *  @throws KnownException
     */
    addRecipientsAllDepartments({messageId}: {messageId: number}): Observable<MessageRecipientDTO> {
 
        return this._http.Post<MessageRecipientDTO>(`/rest/pa/messageboxService/messages/${messageId}/recipients/add-all-departments`, {});
    }

    /**
     *  Add one department as a recipient
     *  @return updated list of recipients and theirs attachments
     *  @throws KnownException
     */
    addRecipientDepartment({recipientId, messageId}: {recipientId: number, messageId: number}): Observable<MessageRecipientDTO[]> {
 
        let params = new HttpParams();
        params = params.append('recipientId', recipientId.toString());
 
        return this._http.Post<MessageRecipientDTO[]>(`/rest/pa/messageboxService/messages/${messageId}/recipients/add-department`, {}, {params});
    }

    /**
     *  Add departments matching filter as recipients
     *  @return updated list of recipients and theirs attachments
     *  @throws KnownException
     */
    addRecipientsFilteredDepartments({messageId, filter}: {messageId: number, filter: MessageRecipientFilterDTO}): Observable<any> {
 
        return this._http.Post<any>(`/rest/pa/messageboxService/messages/${messageId}/recipients/add-filtered-departments`, filter ? filter : {});
    }

    /**
     *  Remove departments matching filter from recipient list
     *  @return updated list of recipients and theirs attachments
     *  @throws KnownException
     */
    removeRecipientsFilteredDepartments({messageId, filter}: {messageId: number, filter: MessageRecipientFilterDTO}): Observable<any> {
 
        return this._http.Post<any>(`/rest/pa/messageboxService/messages/${messageId}/recipients/remove-filtered-departments`, filter ? filter : {});
    }

}
