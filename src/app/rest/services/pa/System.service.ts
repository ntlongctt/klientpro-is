import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { PaUserDTO } from '../../models/pa/system/login/PaUserDTO';

/**
 *  SYSTEM CONTROLLER <br>
 *  
 *  Firstly use {@link #getToken getToken} for generate token.  
 *  After authentizate token by {@link #login login}. Now you can call other services with authentizated token.
 *  
 *  @author Vaclav Hromadko
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class SystemService {
    constructor(private _http: CustomHttpClient) {}

    /**
     * Generate, register and returns new TokenID 
     */
    getToken(): Observable<string> {
        return this._http.Get<string>(`/rest/pa/getToken`);
    }
    /**
     *  Change password of PA user
     *  @return new user object from database
     *  @throws AuthException 
     *  @throws InvalidParameterException 
     *  @throws SpecialUserException 
     */
    changeUserPassword({oldPassword, newPassword}: {oldPassword: string, newPassword: string}): Observable<PaUserDTO> {
 
        let params = new HttpParams();
        params = params.append('oldPassword', oldPassword);
        params = params.append('newPassword', newPassword);
 
        return this._http.Post<PaUserDTO>(`/rest/pa/changePassword`, {}, {params});
    }

    /**
     *  Authentication of token
     *  @param username
     *  @param password (in body)
     *  @param version
     *  @return logged in user data object
     */
    login({username, version, password}: {username: string, version: string, password: string}): Observable<PaUserDTO> {
 
        let params = new HttpParams();
        params = params.append('username', username);
        params = params.append('version', version);
 
        return this._http.Post<PaUserDTO>(`/rest/pa/login`, `"${password}"`, {params});
    }

    getUserData(): Observable<PaUserDTO> {
        return this._http.Get<PaUserDTO>(`/rest/pa/userdata`);
    }
    logout(): Observable<any> {
        return this._http.Get<any>(`/rest/pa/logout`);
    }
}
