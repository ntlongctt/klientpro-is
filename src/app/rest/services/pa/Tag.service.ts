import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { TagTypeListDTO } from '../../models/common/tag/TagTypeListDTO';
import { TagTypeDTO } from '../../models/common/tag/TagTypeDTO';
import { IntegerListDTO } from '../../models/common/common/IntegerListDTO';
import { TagListDTO } from '../../models/common/tag/TagListDTO';
import { TagDTO } from '../../models/common/tag/TagDTO';

/**
 *  Rest {RestApiServiceType} controller class for Tag module
 *  - manage tag types
 *  - manage tag assignment for entity items (e.g. for Departments)
 *  <p>
 *  The service class instances needed by this controller are injected by Spring (see the @Autowired annotations)
 *  <p>
 *  
 *  @author Jan Tesar over {@link ModuleCreator}
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class TagService {
    constructor(private _http: CustomHttpClient) {}

    /**
     *  Returns tag types for specified entity
     *  
     *  @param entityName - requested entity name
     *  @return list of type items, structure: id, name 
     *  @throws TagException, InvalidParameterException
     */
    getTagTypes({entityName}: {entityName: string}): Observable<TagTypeListDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Get<TagTypeListDTO>(`/rest/pa/tagService/tagTypes`, {params});
    }

    /**
     *  Returns tag types for specified entity
     *  
     *  @param entityName - requested entity name
     *  @param tagTypeId - tag type id
     *  @return tag type detail
     *  @throws TagException, InvalidParameterException
     */
    getTagType({tagTypeId, entityName}: {tagTypeId: number, entityName: string}): Observable<TagTypeDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Get<TagTypeDTO>(`/rest/pa/tagService/tagTypes/${tagTypeId}`, {params});
    }

    /**
     *  Create new tag type
     *  Leave id and version empty (null)
     *   
     *  @param entityName - requested entity name
     *  @return created tag type
     *  @throws TagException, InvalidParameterException
     */
    createTagType({entityName, tagType}: {entityName: string, tagType: TagTypeDTO}): Observable<TagTypeDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Post<TagTypeDTO>(`/rest/pa/tagService/tagTypes`, tagType ? tagType : {}, {params});
    }

    /**
     *  Update tag type
     *  Version must be the same as previously by getTagType
     *   
     *  @param entityName - requested entity name
     *  @return updated tag type
     *  @throws TagException, InvalidParameterException
     */
    updateTagType({tagTypeId, entityName, tagType}: {tagTypeId: number, entityName: string, tagType: TagTypeDTO}): Observable<TagTypeDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Put<TagTypeDTO>(`/rest/pa/tagService/tagTypes/${tagTypeId}`, tagType ? tagType : {}, {params});
    }

    /**
     *  Delete tag type
     *   
     *  @param entityName - requested entity name
     *  @throws TagException, InvalidParameterException
     */
    deleteTagType({tagTypeId, entityName}: {tagTypeId: number, entityName: string}): Observable<any> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Delete<any>(`/rest/pa/tagService/tagTypes/${tagTypeId}`, {params});
    }

    /**
     *  Returns tags for specified entity
     *  - two level list - tag types / tags
     *  - 
     *  @param entityName - requested entity name
     *  @return list of tag types, each type contains list of tags  
     *  @throws TagException, InvalidParameterException
     */
    getTags({entityName}: {entityName: string}): Observable<TagListDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Get<TagListDTO>(`/rest/pa/tagService/tags`, {params});
    }

    /**
     *  Returns tag for specified entity
     *  
     *  @param entityName - requested entity name
     *  @param tagId - tag id
     *  @return tag detail
     *  @throws TagException, InvalidParameterException
     */
    getTag({tagId, entityName}: {tagId: number, entityName: string}): Observable<TagDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Get<TagDTO>(`/rest/pa/tagService/tags/${tagId}`, {params});
    }

    /**
     *  Create new tag
     *  Leave id and version empty (null)
     *  
     *  @param entityName - requested entity name
     *  @return created or updated tag
     *  @throws TagException, InvalidParameterException
     */
    createTag({entityName, tag}: {entityName: string, tag: TagDTO}): Observable<TagDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Post<TagDTO>(`/rest/pa/tagService/tags`, tag ? tag : {}, {params});
    }

    /**
     *  Update tag
     *  Version must be the same as previously by getTagType
     *  
     *  @param entityName - requested entity name
     *  @return created or updated tag
     *  @throws TagException, InvalidParameterException
     */
    updateTag({tagId, entityName, tag}: {tagId: number, entityName: string, tag: TagDTO}): Observable<TagDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Put<TagDTO>(`/rest/pa/tagService/tags/${tagId}`, tag ? tag : {}, {params});
    }

    /**
     *  Delete tag 
     *   
     *  @param entityName - requested entity name
     *  @throws TagException, InvalidParameterException
     */
    deleteTag({tagId, entityName}: {tagId: number, entityName: string}): Observable<any> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
 
        return this._http.Delete<any>(`/rest/pa/tagService/tags/${tagId}`, {params});
    }

    /**
     *  Returns all tags for specified entity. Tags currently assigned to given item 
     *  have the <i>assigned</i> attribute set to TRUE 
     *  - two level list - tag types / tags
     *  @param entityName - requested entity name
     *  @return list of tag types, each type contains list of tags  
     *  @throws TagException, InvalidParameterException
     */
    getAssignedTags({entityName, itemId}: {entityName: string, itemId: number}): Observable<TagListDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
        params = params.append('itemId', itemId.toString());
 
        return this._http.Get<TagListDTO>(`/rest/pa/tagService/assignedTags`, {params});
    }

    /**
     *  Update tags assigned to specified item
     *  Currently assigned tags are replaced by provided tags 
     *  (tag are identified by id)
     *  
     *  @param entityName - requested entity name
     *  @param itemId - id of item to reassign tags
     *  @return list of tags assigned to item 
     *  @throws TagException, InvalidParameterException
     */
    updateAssignedTags({entityName, itemId, intList}: {entityName: string, itemId: number, intList: IntegerListDTO}): Observable<TagListDTO> {
 
        let params = new HttpParams();
        params = params.append('entityName', entityName);
        params = params.append('itemId', itemId.toString());
 
        return this._http.Post<TagListDTO>(`/rest/pa/tagService/assignedTags`, intList ? intList : {}, {params});
    }

}
