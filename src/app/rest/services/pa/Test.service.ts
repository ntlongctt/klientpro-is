import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { CustomHttpClient } from '@services/http';
import { Observable } from 'rxjs/Observable';
import { HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { TestTimeDTO } from '../../models/pa/test/TestTimeDTO';
import { StringValueDTO } from '../../models/common/common/StringValueDTO';

/**
 *  Rest methods for testing backend
 *  
 *  @author Vaclav Hromadko
 * 
 *  ---
 *  ANGULAR API service automaticaly generated from REST controllers by RestServiceProcessor!
 */
@Injectable()
export class TestService {
    constructor(private _http: CustomHttpClient) {}

    /**
     * 
     *  Slouzi pro otestovani REST (napr JUnit zkousi zda lze tuto metodu zavolat po prihlaseni)
     *  
     *  @return String ktery byl odeslan v parametru 'text'
     */
    test({text}: {text: string}): Observable<StringValueDTO> {
 
        let params = new HttpParams();
        params = params.append('text', text);
 
        return this._http.Get<StringValueDTO>(`/rest/pa/testService/test`, {params});
    }

    /**
     *  jen test zamitnuti pozadavku (403) kvuli pravum 
     *  (vyzaduje neexistujici roli 'TATO_ROLE_NEEXISTUJE')
     */
    test403(): Observable<StringValueDTO> {
        return this._http.Get<StringValueDTO>(`/rest/pa/testService/test403`);
    }
    /**
     *  jen test sluzby, ktera vyhazuje exception (mela by vratit 500 bez stacktrace)
     */
    test500(): Observable<any> {
        return this._http.Get<any>(`/rest/pa/testService/test500`);
    }
    testTime({localTime, localDate, localDateTime, zonedDateTime, dto}: {localTime: string, localDate: string, localDateTime: string, zonedDateTime: string, dto: TestTimeDTO}): Observable<TestTimeDTO> {
 
        let params = new HttpParams();
        if(localTime){
            params = params.append('localTime', localTime);
        }
        if(localDate){
            params = params.append('localDate', localDate);
        }
        if(localDateTime){
            params = params.append('localDateTime', localDateTime);
        }
        if(zonedDateTime){
            params = params.append('zonedDateTime', zonedDateTime);
        }
 
        return this._http.Post<TestTimeDTO>(`/rest/pa/testService/testTime`, dto ? dto : {}, {params});
    }

}
