import { NoPermissionComponent } from '@pages/no-permission';
import { NotFoundComponent } from '@pages/not-found';

export const appRoutes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  // { path: '+dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: '401', component: NoPermissionComponent },
  { path: '**', component: NotFoundComponent }
];

