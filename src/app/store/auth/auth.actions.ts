import { Action } from '@ngrx/store';
import { AppStoreService } from '@store/store.service';

export const LOG_OUT = AppStoreService.createActionType('@AUTH/LOG_OUT');
export const LOG_OUT_SUCCEEDED = AppStoreService.createActionType('@AUTH/LOG_OUT_SUCCEEDED');
export const SET_CURRENT_USER = AppStoreService.createActionType('@AUTH/SET_CURRENT_USER');
export const SET_IS_LOGGED_IN = AppStoreService.createActionType('@AUTH/SET_IS_LOGGED_IN');
export const SET_TOKEN = AppStoreService.createActionType('@AUTH/SET_TOKEN');
export const SET_AUTH_TOKEN = AppStoreService.createActionType('@AUTH/SET_AUTH_TOKEN');
export const GET_PROFILE_SUCCESS = AppStoreService.createActionType('@AUTH/GET_PROFILE_SUCCESS');
export const CLEAN_TOKEN = AppStoreService.createActionType('@AUTH/CLEAN_TOKEN');
export const SET_RETURN_URL = AppStoreService.createActionType('@AUTH/SET_RETURN_URL');
export const CLEAN_RETURN_URL = AppStoreService.createActionType('@AUTH/CLEAN_RETURN_URL');
export const SET_PREVIOUS_USER = AppStoreService.createActionType('@AUTH/SET_PREVIOUS_USER');
export const CLEAN_PREVIOUS_USER = AppStoreService.createActionType('@AUTH/CLEAN_PREVIOUS_USER');
export const CLEAN_AUTH = AppStoreService.createActionType('@AUTH/CLEAN_AUTH');


export class LogOut implements Action {
  public readonly type = LOG_OUT;
}

export class LogOutSucceeded implements Action {
  public readonly type = LOG_OUT_SUCCEEDED;
}

export class SetCurrentUser implements Action {
  public readonly type = SET_CURRENT_USER;

  constructor(public payload: any) {
  }
}

export class SetIsLoggedIn implements Action {
  public readonly type = SET_IS_LOGGED_IN;

  constructor(public payload: boolean) {
  }
}

export class SetToken implements Action {
  public readonly type = SET_TOKEN;

  constructor(public payload: any) {
  }
}

export class SetAuthToken implements Action {
  public readonly type = SET_AUTH_TOKEN;

  constructor(public payload: {authToken: string}) {
  }
}

export class GetProfileSuccess implements Action {
  public readonly type = GET_PROFILE_SUCCESS;

  constructor(public payload: any) {
  }
}

export class SetReturnUrl implements Action {
  public readonly type = SET_RETURN_URL;

  constructor(public payload: any) {
  }
}

export class SetPreviousUser implements Action {
  public readonly type = SET_PREVIOUS_USER;

  constructor(public payload: any) {
  }
}

export class CleanToken implements Action {
  public readonly type = CLEAN_TOKEN;
}

export class CleanAuth implements Action {
  public readonly type = CLEAN_AUTH;
}

export class CleanPreviousUser implements Action {
  public readonly type = CLEAN_PREVIOUS_USER;
}

export class CleanReturnURL implements Action {
  public readonly type = CLEAN_RETURN_URL;
}

export type AuthActions =
  SetCurrentUser
  | SetIsLoggedIn
  | LogOut
  | SetToken
  | SetAuthToken
  | GetProfileSuccess
  | LogOutSucceeded
  | CleanToken
  | SetReturnUrl
  | SetPreviousUser
  | CleanAuth
  | CleanPreviousUser
  | CleanReturnURL;
