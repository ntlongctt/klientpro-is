import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import * as fromAuthActions from '@store/auth/auth.actions';
import { mergeMap, catchError, map, tap } from 'rxjs/operators';
import { SystemService } from '../../rest/services/pa/System.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as Raven from 'raven-js';
import { defer } from 'rxjs/observable/defer';
import { AppStoreService } from '@store/store.service';
import { UserInfo } from '@common/models/UserInfo';
import { AppState } from '@store/store.reducers';
import * as fromMenuActions from '@store/menu/menu.actions';

@Injectable()
export class AuthEffects {
  @Effect()
  logout$: Observable<Action> = this.actions$.pipe(
    ofType(fromAuthActions.LOG_OUT),
    mergeMap(() => this.systemService.logout()),
    map(() => {
      this.router.navigate(['sign-in']);
      return new fromAuthActions.LogOutSucceeded();
    }),
    catchError((e, caught) => {
      this.router.navigate(['sign-in']);
      return caught;
    })
  );

  @Effect({ dispatch: false })
  getProfileSuccess: Observable<Action> = this.actions$.pipe(
    ofType(fromAuthActions.GET_PROFILE_SUCCESS),
    tap((action: { payload: UserInfo }) => {
      // this.store.dispatch(new fromMenuActions.GetMenu());
      Raven.setUserContext({
        id: action.payload.id,
        email: action.payload.email,
      });
    })
  );

  @Effect({ dispatch: false })
  storeInit$: Observable<any> = defer(() => {
    const appState = this.storeService.getState();
    if (appState.auth.currentUser) {
      Raven.setUserContext({
        id: appState.auth.currentUser.id,
        email: appState.auth.currentUser.email,
      });
    }
  });

  constructor(
    private actions$: Actions,
    private router: Router,
    private systemService: SystemService,
    private storeService: AppStoreService,
    private store: Store<AppState>
  ) {
  }
}
