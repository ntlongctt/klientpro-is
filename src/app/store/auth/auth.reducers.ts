import {
  AuthActions,
  SET_CURRENT_USER,
  SET_IS_LOGGED_IN,
  SET_TOKEN,
  LOG_OUT_SUCCEEDED,
  GET_PROFILE_SUCCESS, SET_AUTH_TOKEN, CLEAN_TOKEN, SET_RETURN_URL, SET_PREVIOUS_USER, CLEAN_AUTH, CLEAN_PREVIOUS_USER, CLEAN_RETURN_URL
} from './auth.actions';
import { UserInfo } from '@common/models/UserInfo';

export interface AuthState {
  currentUser: UserInfo;
  isLoggedIn: boolean;
  accessToken: string;
  refreshToken: string;
  authToken: string;
  returnUrl: string;
  previousUser: string;
}

const initialState: AuthState = {
  currentUser: null,
  isLoggedIn: false,
  accessToken: '',
  refreshToken: '',
  authToken: '',
  returnUrl: '',
  previousUser: ''
};

export function AuthReducer(state: AuthState = initialState, action: AuthActions) {
  switch (action.type) {
    // case LOG_OUT_SUCCEEDED:
    //   return initialState;
    case SET_CURRENT_USER:
      return { ...state, currentUser: new UserInfo(action.payload.currentUser), isLoggedIn: true };
    case SET_IS_LOGGED_IN:
      return {
        ...state,
        isLoggedIn: action.payload
      };
    case SET_AUTH_TOKEN:
      return {
        ...state,
        authToken: action.payload.authToken,
        // isLoggedIn: true
      };
    case GET_PROFILE_SUCCESS:
      return { ...state, currentUser: action.payload };
    case SET_TOKEN:
      return {
        ...state,
        refreshToken: action.payload.refresh_token,
        accessToken: action.payload.access_token,
        authToken: action.payload.auth_token,
        // isLoggedIn: true
      };
    case CLEAN_TOKEN: 
      return initialState;
    case SET_RETURN_URL:
      return {
        ...state,
        returnUrl: action.payload
      };
    case SET_PREVIOUS_USER:
      return {
        ...state,
        previousUser: action.payload
      };
    case CLEAN_AUTH:
      return {
        ...state,
        authToken: ''
      };
    case CLEAN_PREVIOUS_USER:
      return {
        ...state,
        previousUser: ''
      };
    case CLEAN_RETURN_URL:
      return {
        ...state,
        returnUrl: ''
      };
    default:
      return state;
  }
}
