import { AppStoreService } from '@store/store.service';
import { Action } from '@ngrx/store';

export const SET_BREADCRUMBS = AppStoreService.createActionType('@BREADCRUMBS/SET_BREADCRUMBS');
export const SET_USER_NAME = AppStoreService.createActionType('@BREADCRUMBS/SET_USER_NAME');
export const SET_TAG_NAME = AppStoreService.createActionType('@BREADCRUMBS/SET_TAG_NAME');
export const SET_TAG_TYPE_NAME = AppStoreService.createActionType('@BREADCRUMBS/SET_TAG_TYPE_NAME');
export const SET_PATH = AppStoreService.createActionType('@BREADCRUMBS/SET_PATH');

export class SetBreadCrumbs implements Action {
  public readonly type = SET_BREADCRUMBS;
  constructor(public payload: any) {}
}
export class SetUserName implements Action {
  public readonly type = SET_USER_NAME;
  constructor(public payload: any) {}
}

export class SetTagName implements Action {
  public readonly type = SET_TAG_NAME;
  constructor(public payload: any) {}
}

export class SetTagTypeName implements Action {
  public readonly type = SET_TAG_TYPE_NAME;
  constructor(public payload: any) {}
}

export class SetPath implements Action {
  public readonly type = SET_PATH;
  constructor(public payload: any) {}
}

export type BreadCrumbsActions = 
   SetBreadCrumbs
  |SetUserName
  |SetTagName
  |SetTagTypeName
  |SetPath;
