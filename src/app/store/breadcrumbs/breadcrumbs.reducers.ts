import { ErrorActions, SERVER_INTERNAL_ERROR, ADD_ERROR_SHOWING, REMOVE_ERROR_SHOWING } from '@store/error/error.actions';
import { BreadCrumbsActions, SET_BREADCRUMBS, SET_USER_NAME, SET_TAG_NAME, SET_TAG_TYPE_NAME, SET_PATH } from './breadcrumbs.actions';

export interface BreadscrumbState {
  path: string;
  userName: string;
  tagName: string;
  tagTypeName: string;
  paths: string[];
}

const initialState: BreadscrumbState = {
  path: '',
  tagName: '',
  tagTypeName: '',
  userName: '',
  paths: []
};

export function BreadscrumReducer(state: BreadscrumbState = initialState, action: BreadCrumbsActions) {
  switch (action.type) {
    case SET_BREADCRUMBS:
      return {
        ...state,
        path: action.payload
      };
    case SET_USER_NAME:
      return {
        ...state,
        userName: action.payload
      };
    case SET_TAG_NAME:
      return {
        ...state,
        tagName: action.payload
      };
    case SET_TAG_TYPE_NAME:
      return {
        ...state,
        tagTypeName: action.payload
      };
    case SET_PATH:
      return {
        ...state,
        paths: action.payload
      };
    default:
      return state;
  }
}
