import { AppStoreService } from '@store/store.service';
import { Action } from '@ngrx/store';

export const SEARCH_BUSSINESS_PATNER = AppStoreService.createActionType('@BUSSINESS_PATNER/UNAUTHORIZED_ERROR');

export const SET_FILTER = AppStoreService.createActionType('@BUSSINESS_PATNER/SET_FILTER');
export const UPDATE_FILTER = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_FILTER');
export const GET_FILTER_DATA = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_FILTER_DATA');
export const GET_FILTER_DATA_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_FILTER_DATA_SUCCEEDED');
export const GET_FILTER_DATA_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_FILTER_DATA_FAILED');
export const SET_FILTER_DATA = AppStoreService.createActionType('@BUSSINESS_PATNER/SET_FILTER_DATA');

export const FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER = AppStoreService.createActionType('@BUSSINESS_PATNER/FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER');
export const FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_SUCCEEDED');
export const FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_FAILED');

export const ADD_PARTNER = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_PARTNER');
export const ADD_PARTNER_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_PARTNER_SUCCEEDED');
export const ADD_PARTNER_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_PARTNER_FAILED');

export const DELETE_PARTNER = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_PARTNER');
export const DELETE_PARTNER_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_PARTNER_SUCCEEDED');
export const DELETE_PARTNER_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_PARTNER_FAILED');

export const GET_PARTNER_ADDRESSES = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_ADDRESSES');
export const GET_PARTNER_ADDRESSES_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_ADDRESSES_SUCCEEDED');
export const GET_PARTNER_ADDRESSES_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_ADDRESSES_FAILED');

export const GET_PARTNER_ADDRESS_DETAIL = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_ADDRESS_DETAIL');
export const GET_PARTNER_ADDRESS_DETAIL_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_ADDRESS_DETAIL_SUCCEEDED');
export const GET_PARTNER_ADDRESS_DETAIL_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_ADDRESS_DETAIL_FAILED');

export const GET_ADDRESS_TYPES = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_ADDRESS_TYPES');
export const GET_ADDRESS_TYPES_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_ADDRESS_TYPES_SUCCEEDED');
export const GET_ADDRESS_TYPES_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_ADDRESS_TYPES_FAILED');

export const ADD_PARTNER_ADDRESS = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_PARTNER_ADDRESS');
export const ADD_PARTNER_ADDRESS_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_PARTNER_ADDRESS_SUCCEEDED');
export const ADD_PARTNER_ADDRESS_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_PARTNER_ADDRESS_FAILED');

export const UPDATE_PARTNER_ADDRESS = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_PARTNER_ADDRESS');
export const UPDATE_PARTNER_ADDRESS_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_PARTNER_ADDRESS_SUCCEEDED');
export const UPDATE_PARTNER_ADDRESS_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_PARTNER_ADDRESS_FAILED');

export const DELETE_PARTNER_ADDRESS = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_PARTNER_ADDRESS');
export const DELETE_PARTNER_ADDRESS_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_PARTNER_ADDRESS_SUCCEEDED');
export const DELETE_PARTNER_ADDRESS_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_PARTNER_ADDRESS_FAILED');

export const GET_PARTER_DETAIL = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTER_DETAIL');
export const GET_PARTER_DETAIL_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTER_DETAIL_SUCCEEDED');
export const GET_PARTER_DETAIL_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTER_DETAIL_FAILED');

export const UPDATE_PARTNER = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_PARTNER');
export const UPDATE_PARTNER_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_PARTNER_SUCCEEDED');
export const UPDATE_PARTNER_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_PARTNER_FAILED');

export const GET_PARTNER_CONTACT_PERSON = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_CONTACT_PERSON');
export const GET_PARTNER_CONTACT_PERSON_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_CONTACT_PERSON_SUCCEEDED');
export const GET_PARTNER_CONTACT_PERSON_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_CONTACT_PERSON_FAILED');

export const GET_PARTNER_CONTACT_PERSON_DETAIL = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_CONTACT_PERSON_DETAIL');
export const GET_PARTNER_CONTACT_PERSON_DETAIL_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_CONTACT_PERSON_DETAIL_SUCCEEDED');
export const GET_PARTNER_CONTACT_PERSON_DETAIL_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_PARTNER_CONTACT_PERSON_DETAIL_FAILED');

export const CREATE_CONTACT_PERSON = AppStoreService.createActionType('@BUSSINESS_PATNER/CREATE_CONTACT_PERSON');
export const CREATE_CONTACT_PERSON_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/CREATE_CONTACT_PERSON_SUCCEEDED');
export const CREATE_CONTACT_PERSON_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/CREATE_CONTACT_PERSON_FAILED');

export const UPDATE_CONTACT_PERSON = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_CONTACT_PERSON');
export const UPDATE_CONTACT_PERSON_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_CONTACT_PERSON_SUCCEEDED');
export const UPDATE_CONTACT_PERSON_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_CONTACT_PERSON_FAILED');

export const DELETE_CONTACT_PERSON = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_CONTACT_PERSON');
export const DELETE_CONTACT_PERSON_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_CONTACT_PERSON_SUCCEEDED');
export const DELETE_CONTACT_PERSON_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_CONTACT_PERSON_FAILED');

export const GET_BANK_ACCOUNTS = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_BANK_ACCOUNTS');
export const GET_BANK_ACCOUNTS_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_BANK_ACCOUNTS_SUCCEEDED');
export const GET_BANK_ACCOUNTS_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_BANK_ACCOUNTS_FAILED');

export const GET_BANK_ACCOUNT_DETAIL = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_BANK_ACCOUNT_DETAIL');
export const GET_BANK_ACCOUNT_DETAIL_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_BANK_ACCOUNT_DETAIL_SUCCEEDED');
export const GET_BANK_ACCOUNT_DETAIL_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/GET_BANK_ACCOUNT_DETAIL_FAILED');

export const ADD_BANK_ACCOUNT = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_BANK_ACCOUNT');
export const ADD_BANK_ACCOUNT_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_BANK_ACCOUNT_SUCCEEDED');
export const ADD_BANK_ACCOUNT_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/ADD_BANK_ACCOUNT_FAILED');

export const UPDATE_BANK_ACCOUNT = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_BANK_ACCOUNT');
export const UPDATE_BANK_ACCOUNT_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_BANK_ACCOUNT_SUCCEEDED');
export const UPDATE_BANK_ACCOUNT_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/UPDATE_BANK_ACCOUNT_FAILED');

export const DELETE_BANK_ACCOUNT = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_BANK_ACCOUNT');
export const DELETE_BANK_ACCOUNT_SUCCEEDED = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_BANK_ACCOUNT_SUCCEEDED');
export const DELETE_BANK_ACCOUNT_FAILED = AppStoreService.createActionType('@BUSSINESS_PATNER/DELETE_BANK_ACCOUNT_FAILED');


export class SearchBussinessPatner implements Action {
  public type = SEARCH_BUSSINESS_PATNER;
  constructor(public payload: any) {
  }
}

export class SetFilter implements Action {
  public type = SET_FILTER;
  constructor(public payload: any) {
  }
}
export class UpdateFilter implements Action {
  public type = UPDATE_FILTER;
  constructor(public payload: any) {
  }
}

export class GetFilterData implements Action {
  public type = GET_FILTER_DATA;
  constructor(public payload: any) {
  }
}

export class GetFilterDataSucceeded implements Action {
  public type = GET_FILTER_DATA_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetFilterDataFailed implements Action {
  public type = GET_FILTER_DATA_FAILED;
  constructor(public payload: any) {
  }
}

export class SetFilterData implements Action {
  public type = SET_FILTER_DATA;
  constructor(public payload: any) {
  }
}
export class FindBusinessPartnerInPublicRegister implements Action {
  public type = FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER;
  constructor(public payload: any) {
  }
}
export class FindBusinessPartnerInPublicRegisterSuccess implements Action {
  public type = FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class FindBusinessPartnerInPublicRegisterFailed implements Action {
  public type = FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_FAILED;
  constructor(public payload: any) {
  }
}

export class AddPartner implements Action {
  public type = ADD_PARTNER;
  constructor(public payload: any) {
  }
}

export class AddPartnerSucceeded implements Action {
  public type = ADD_PARTNER_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class AddPartnerFailed implements Action {
  public type = ADD_PARTNER_FAILED;
  constructor(public payload: any) {
  }
}


export class UpdatePartner implements Action {
  public type = UPDATE_PARTNER;
  constructor(public payload: any) {
  }
}

export class UpdatePartnerSucceeded implements Action {
  public type = UPDATE_PARTNER_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class UpdatePartnerFailed implements Action {
  public type = UPDATE_PARTNER_FAILED;
  constructor(public payload: any) {
  }
}

export class DeletePartner implements Action {
  public type = DELETE_PARTNER;
  constructor(public payload: any) {
  }
}

export class DeletePartnerSucceeded implements Action {
  public type = DELETE_PARTNER_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class DeletePartnerFailed implements Action {
  public type = DELETE_PARTNER_FAILED;
  constructor(public payload: any) {
  }
}

export class GetPartnerAddresses implements Action {
  public type = GET_PARTNER_ADDRESSES;
  constructor(public payload: any) {
  }
}

export class GetPartnerAddressesSucceeded implements Action {
  public type = GET_PARTNER_ADDRESSES_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetPartnerAddressesFailed implements Action {
  public type = GET_PARTNER_ADDRESSES_FAILED;
  constructor(public payload: any) {
  }
}

export class GetPartnerAddressDetail implements Action {
  public type = GET_PARTNER_ADDRESS_DETAIL;
  constructor(public payload: any) {
  }
}

export class GetPartnerAddressDetailSucceeded implements Action {
  public type = GET_PARTNER_ADDRESS_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetPartnerAddressDetailFailed implements Action {
  public type = GET_PARTNER_ADDRESS_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}


export class GetAddressTypes implements Action {
  public type = GET_ADDRESS_TYPES;
  constructor(public payload: any) {
  }
}

export class GetAddressTypesSuccceeded implements Action {
  public type = GET_ADDRESS_TYPES_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetAddressTypesFailed implements Action {
  public type = GET_ADDRESS_TYPES_FAILED;
  constructor(public payload: any) {
  }
}

export class AddPartnerAddress implements Action {
  public type = ADD_PARTNER_ADDRESS;
  constructor(public payload: any) {
  }
}

export class AddPartnerAddressSucceeded implements Action {
  public type = ADD_PARTNER_ADDRESS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class AddPartnerAddressFailed implements Action {
  public type = ADD_PARTNER_ADDRESS_FAILED;
  constructor(public payload: any) {
  }
}

export class UpdatePartnerAddress implements Action {
  public type = UPDATE_PARTNER_ADDRESS;
  constructor(public payload: any) {
  }
}


export class UpdatePartnerAddressSucceeded implements Action {
  public type = UPDATE_PARTNER_ADDRESS_SUCCEEDED;
  constructor(public payload: any) {
  }
}


export class UpdatePartnerAddressFailed implements Action {
  public type = UPDATE_PARTNER_ADDRESS_FAILED;
  constructor(public payload: any) {
  }
}

export class DeletetPartnerAddress implements Action {
  public type = DELETE_PARTNER_ADDRESS;
  constructor(public payload: any) {
  }
}

export class DeletetPartnerAddressSucceeded implements Action {
  public type = DELETE_PARTNER_ADDRESS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class DeletetPartnerAddressFailed implements Action {
  public type = DELETE_PARTNER_ADDRESS_FAILED;
  constructor(public payload: any) {
  }
}

export class GetPartnerDetail implements Action {
  public type = GET_PARTER_DETAIL;
  constructor(public payload: any) {
  }
}

export class GetPartnerDetailSucceeded implements Action {
  public type = GET_PARTER_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetPartnerDetailFailed implements Action {
  public type = GET_PARTER_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}

export class GetPartnerContactPerson implements Action {
  public type = GET_PARTNER_CONTACT_PERSON;
  constructor(public payload: any) {
  }
}

export class GetPartnerContactPersonSucceeded implements Action {
  public type = GET_PARTNER_CONTACT_PERSON_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetPartnerContactPersonFailed implements Action {
  public type = GET_PARTNER_CONTACT_PERSON_FAILED;
  constructor(public payload: any) {
  }
}

export class GetPartnerContactPersonDetail implements Action {
  public type = GET_PARTNER_CONTACT_PERSON_DETAIL;
  constructor(public payload: any) {
  }
}

export class GetPartnerContactPersonDetailSucceeded implements Action {
  public type = GET_PARTNER_CONTACT_PERSON_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetPartnerContactPersonDetailFailed implements Action {
  public type = GET_PARTNER_CONTACT_PERSON_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}

export class CreateContactPerson implements Action {
  public type = CREATE_CONTACT_PERSON;
  constructor(public payload: any) {
  }
}

export class CreateContactPersonSucceeded implements Action {
  public type = CREATE_CONTACT_PERSON_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class CreateContactPersonFailed implements Action {
  public type = CREATE_CONTACT_PERSON_FAILED;
  constructor(public payload: any) {
  }
}

export class UpdateContactPerson implements Action {
  public type = UPDATE_CONTACT_PERSON;
  constructor(public payload: any) {
  }
}

export class UpdateContactPersonSucceeded implements Action {
  public type = UPDATE_CONTACT_PERSON_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class UpdateContactPersonFailed implements Action {
  public type = UPDATE_CONTACT_PERSON_FAILED;
  constructor(public payload: any) {
  }
}

export class DeleteContactPerson implements Action {
  public type = DELETE_CONTACT_PERSON;
  constructor(public payload: any) {
  }
}

export class DeleteContactPersonSucceeded implements Action {
  public type = DELETE_CONTACT_PERSON_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class DeleteContactPersonFailed implements Action {
  public type = DELETE_CONTACT_PERSON_FAILED;
  constructor(public payload: any) {
  }
}

export class GetBankAccounts implements Action {
  public type = GET_BANK_ACCOUNTS;
  constructor(public payload: any) {
  }
}

export class GetBankAccountsSucceeded implements Action {
  public type = GET_BANK_ACCOUNTS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetBankAccountsFailed implements Action {
  public type = GET_BANK_ACCOUNTS_FAILED;
  constructor(public payload: any) {
  }
}

export class AddBankAccount implements Action {
  public type = ADD_BANK_ACCOUNT;
  constructor(public payload: any) {
  }
}

export class AddBankAccountSucceeded implements Action {
  public type = ADD_BANK_ACCOUNT_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class AddBankAccountFailed implements Action {
  public type = ADD_BANK_ACCOUNT_FAILED;
  constructor(public payload: any) {
  }
}

export class GetBankAccountDetail implements Action {
  public type = GET_BANK_ACCOUNT_DETAIL;
  constructor(public payload: any) {
  }
}
export class GetBankAccountDetailSucceeded implements Action {
  public type = GET_BANK_ACCOUNT_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}
export class GetBankAccountDetailFailed implements Action {
  public type = GET_BANK_ACCOUNT_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}

export class UpdateBankAccount implements Action {
  public type = UPDATE_BANK_ACCOUNT;
  constructor(public payload: any) {
  }
}

export class UpdateBankAccountSucceeded implements Action {
  public type = UPDATE_BANK_ACCOUNT_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class UpdateBankAccountFailed implements Action {
  public type = UPDATE_BANK_ACCOUNT_FAILED;
  constructor(public payload: any) {
  }
}

export class DeleteBankAccount implements Action {
  public type = DELETE_BANK_ACCOUNT;
  constructor(public payload: any) {
  }
}

export class DeleteBankAccountSucceeded implements Action {
  public type = DELETE_BANK_ACCOUNT_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class DeleteBankAccountFailed implements Action {
  public type = DELETE_BANK_ACCOUNT_FAILED;
  constructor(public payload: any) {
  }
}

export type BusinessPartnerActions = 
  SearchBussinessPatner 
| SetFilter
| GetFilterData
| GetFilterDataSucceeded
| GetFilterDataFailed
| UpdateFilter
| SetFilterData
| AddPartner
| AddPartnerSucceeded
| AddPartnerFailed
| UpdatePartner
| DeletePartner
| DeletePartnerSucceeded
| DeletePartnerFailed
| GetPartnerAddresses
| GetPartnerAddressDetail
| GetAddressTypes
| AddPartnerAddress
| UpdatePartnerAddress
| GetBankAccounts
| AddBankAccount
| GetBankAccountDetail
| UpdateBankAccount
| DeleteBankAccount
| GetPartnerContactPerson
| GetPartnerContactPerson
| AddPartnerAddress
| UpdatePartnerAddress
| GetPartnerDetail;
