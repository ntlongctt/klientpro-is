import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as fromBusinessPartnerActions from '@store/business-patner/business-patner.actions';
import { Observable } from 'rxjs/Observable';
import { Action, Store } from '@ngrx/store';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { AppState } from '@store/store.reducers';
import { TagService } from '@klient/rest/services/pa/Tag.service';
import { McSystemService } from '@klient/rest/services/pa/McSystem.service';
import { CodeListService } from '@klient/rest/services/common/CodeList.service';
import * as _ from 'lodash';
import { BusinessPartnerService } from '@klient/rest/services/pa/BusinessPartner.service';

@Injectable()
export class BusinessPartnerEffects {
  constructor(
    private actions$: Actions,
    private codeListService: CodeListService,
    private mcSystemService: McSystemService,
    private tagService: TagService,
    private store: Store<AppState>,
    private businessPartnerService: BusinessPartnerService
  ) {

  }

  @Effect()
  updateAssignedTags$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_FILTER_DATA),
    mergeMap((action: any) => {
      return combineLatest(
        this.codeListService.getCodeListItems({codelistName: action.payload.codelistName}),
        this.mcSystemService.getMcSystemsCodelist({ activeOnly: action.payload.activeOnly, mcSystemTypeId: action.payload.mcSystemTypeId }),
        this.tagService.getTags({ entityName: action.payload.entityName })
      );
    }),
    map(([departmentList, mcSystemList, tags]) => {
      const tagsClone = _.cloneDeep(tags);
      const departments = departmentList.items;
      const mcSystems = mcSystemList.items;
      return new fromBusinessPartnerActions.GetFilterDataSucceeded({departments, mcSystems, tagsClone});
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetFilterDataFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getFilterDataSuccess$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_FILTER_DATA_SUCCEEDED),
    map((action: any) => {
      return new fromBusinessPartnerActions.SetFilterData(action.payload);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetFilterDataFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  findBusinessPartnerInPublicRegister$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER),
    mergeMap((data: any) => {
      return this.businessPartnerService.findBusinessPartnerInPublicRegister({
        companyId: data.payload
      });
    }),
    map((data) => (new fromBusinessPartnerActions.FindBusinessPartnerInPublicRegisterSuccess(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.FindBusinessPartnerInPublicRegisterFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  addPartner$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.ADD_PARTNER),
    mergeMap((data: any) => {
      return this.businessPartnerService.addBusinessPartner({
        body: data.payload
      });
    }),
    map((data) => (new fromBusinessPartnerActions.AddPartnerSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.AddPartnerFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  updatePartner$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.UPDATE_PARTNER),
    mergeMap((action: any) => {
      return this.businessPartnerService.updateBusinessPartner({
        partnerId: action.payload.partnerId,
        body: action.payload.body
      });
    }),
    map((data) => (new fromBusinessPartnerActions.UpdatePartnerSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.UpdatePartnerFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  deletePartner$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.DELETE_PARTNER),
    mergeMap((data: any) => {
      return this.businessPartnerService.deleteBusinessPartner({
        partnerId: data.payload,
      });
    }),
    map((data) => (new fromBusinessPartnerActions.DeletePartnerSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.DeletePartnerFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getPartnerAddresses$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_PARTNER_ADDRESSES),
    mergeMap((action: any) => {
      return this.businessPartnerService.getBusinessPartnerAddresses({
        partnerId: action.payload,
      });
    }),
    map((data) => (new fromBusinessPartnerActions.GetPartnerAddressesSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetPartnerAddressesFailed({err: e.error, ...caught}));
      return caught;
    }),
  );

  @Effect()
  getPartnerAddressDetail$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_PARTNER_ADDRESS_DETAIL),
    mergeMap((action: any) => {
      return this.businessPartnerService.getBusinessPartnerAddress({
        partnerId: action.payload.partnerId,
        bpAddressId: action.payload.bpAddressId
      });
    }),
    map((data) => (new fromBusinessPartnerActions.GetPartnerAddressDetailSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetPartnerAddressDetailFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getAddressTypes$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_ADDRESS_TYPES),
    mergeMap((action: any) => {
      return this.codeListService.getCodeListItems({
        codelistName: action.payload
      });
    }),
    map((data) => (new fromBusinessPartnerActions.GetAddressTypesSuccceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetAddressTypesFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  addPartnerAddress$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.ADD_PARTNER_ADDRESS),
    mergeMap((action: any) => {
      return this.businessPartnerService.addBusinessPartnerAddress({
        partnerId: action.payload.partnerId,
        address: action.payload.address
      });
    }),
    map((data) => (new fromBusinessPartnerActions.AddPartnerAddressSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.AddPartnerAddressFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  updatePartnerAddress$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.UPDATE_PARTNER_ADDRESS),
    mergeMap((action: any) => {
      return this.businessPartnerService.updateBusinessPartnerAddress({
        bpAddressId: action.payload.bpAddressId,
        partnerId: action.payload.partnerId,
        address: action.payload.address
      });
    }),
    map((data) => (new fromBusinessPartnerActions.UpdatePartnerAddressSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.UpdatePartnerAddressFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  udeletePartnerAddress$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.DELETE_PARTNER_ADDRESS),
    mergeMap((action: any) => {
      return this.businessPartnerService.deleteBusinessPartnerAddress({
        bpAddressId: action.payload.bpAddressId,
        partnerId: action.payload.partnerId,
      });
    }),
    map((data) => (new fromBusinessPartnerActions.DeletetPartnerAddressSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.DeletetPartnerAddressFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getPartnerDetail$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_PARTER_DETAIL),
    mergeMap((action: any) => {
      return this.businessPartnerService.getBusinessPartner({
        partnerId: action.payload,
      });
    }),
    map((data) => (new fromBusinessPartnerActions.GetPartnerDetailSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetPartnerDetailFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getPartnerContactPerson$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_PARTNER_CONTACT_PERSON),
    mergeMap((action: any) => {
      return this.businessPartnerService.getBusinessPartnerContactPersons({
        partnerId: action.payload.partnerId,
        query: action.payload.query
      });
    }),
    map((data) => (new fromBusinessPartnerActions.GetPartnerContactPersonSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetPartnerContactPersonFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getPartnerContactPersonDEtail$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_PARTNER_CONTACT_PERSON_DETAIL),
    mergeMap((action: any) => {
      return this.businessPartnerService.getBusinessPartnerContactPerson({
        partnerId: action.payload.partnerId,
        personId: action.payload.personId
      });
    }),
    map((data) => (new fromBusinessPartnerActions.GetPartnerContactPersonDetailSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetPartnerContactPersonDetailFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  createContactPerson$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.CREATE_CONTACT_PERSON),
    mergeMap((action: any) => {
      return this.businessPartnerService.addBusinessPartnerContactPerson({
        partnerId: action.payload.partnerId,
        person: action.payload.person
      });
    }),
    map((data) => (new fromBusinessPartnerActions.CreateContactPersonSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.CreateContactPersonFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  updateContactPerson$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.UPDATE_CONTACT_PERSON),
    mergeMap((action: any) => {
      return this.businessPartnerService.updateBusinessPartnerContactPerson({
        partnerId: action.payload.partnerId,
        personId: action.payload.personId,
        person: action.payload.person,
      });
    }),
    map((data) => (new fromBusinessPartnerActions.UpdateContactPersonSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.UpdateContactPersonFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  deleteContactPerson$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.DELETE_CONTACT_PERSON),
    mergeMap((action: any) => {
      return this.businessPartnerService.deleteBusinessPartnerContactPerson({
        partnerId: action.payload.partnerId,
        personId: action.payload.personId,
      });
    }),
    map((data) => (new fromBusinessPartnerActions.DeleteContactPersonSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.DeleteContactPersonFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getBankAccounts$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_BANK_ACCOUNTS),
    mergeMap((action: any) => {
      return this.businessPartnerService.getBusinessPartnerBankAccounts({
        partnerId: action.payload,
      });
    }),
    map((data) => (new fromBusinessPartnerActions.GetBankAccountsSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetBankAccountsFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  addBankAccount$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.ADD_BANK_ACCOUNT),
    mergeMap((action: any) => {
      return this.businessPartnerService.addBusinessPartnerBankAccount({
        partnerId: action.payload.partnerId,
        account: action.payload.account
      });
    }),
    map((data) => (new fromBusinessPartnerActions.AddBankAccountSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.AddBankAccountFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getBankAccountDetail$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.GET_BANK_ACCOUNT_DETAIL),
    mergeMap((action: any) => {
      return this.businessPartnerService.getBusinessPartnerBankAccount({
        partnerId: action.payload.partnerId,
        accountId: action.payload.accountId
      });
    }),
    map((data) => (new fromBusinessPartnerActions.GetBankAccountDetailSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.GetBankAccountDetailFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  updateBankAccount$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.UPDATE_BANK_ACCOUNT),
    mergeMap((action: any) => {
      return this.businessPartnerService.updateBusinessPartnerBankAccount({
        partnerId: action.payload.partnerId,
        accountId: action.payload.accountId,
        account: action.payload.account
      });
    }),
    map((data) => (new fromBusinessPartnerActions.UpdateBankAccountSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.UpdateBankAccountFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  deleteBankAccount$: Observable<Action> = this.actions$.pipe(
    ofType(fromBusinessPartnerActions.DELETE_BANK_ACCOUNT),
    mergeMap((action: any) => {
      return this.businessPartnerService.deleteBusinessPartnerBankAccount({
        partnerId: action.payload.partnerId,
        accountId: action.payload.accountId,
      });
    }),
    map((data) => (new fromBusinessPartnerActions.DeleteBankAccountSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromBusinessPartnerActions.DeleteBankAccountFailed(e.error));
      return caught;
    }),
  );

}
