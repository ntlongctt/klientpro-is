import { 
  SET_FILTER, 
  BusinessPartnerActions, 
  SET_FILTER_DATA, UPDATE_FILTER, 
  FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_SUCCEEDED, 
  GET_PARTER_DETAIL_SUCCEEDED,
  GET_PARTNER_CONTACT_PERSON,
  GET_PARTNER_CONTACT_PERSON_SUCCEEDED,
  GET_PARTNER_CONTACT_PERSON_FAILED
} from './business-patner.actions';

export interface BusinessPartnerState {
  filter: any;
  filterData: any;
  findRegisterPartnerResult: any;
  isLoading: boolean;
}

const initialState: BusinessPartnerState = {
  filter: null,
  filterData: null,
  findRegisterPartnerResult: null,
  isLoading: false
};

export function BusinessPartnerReducer(state: BusinessPartnerState = initialState, action: BusinessPartnerActions) {
  switch (action.type) {
    case SET_FILTER:
      return {
        ...state,
        filter: action.payload
      };
    case SET_FILTER_DATA:
      return {
        ...state,
        filterData: action.payload
      };
    case UPDATE_FILTER:
      return {
        ...state,
        filter: action.payload
      };
    case FIND_BUSINESS_PARTNER_IN_PUBLIC_REGISTER_SUCCEEDED:
      return {
        ...state,
        findRegisterPartnerResult: action.payload
      };
    case GET_PARTER_DETAIL_SUCCEEDED:
      return {
        ...state,
        findRegisterPartnerResult: action.payload
      };
    case GET_PARTNER_CONTACT_PERSON:
      return {
        ...state,
        isLoading: true
      };
    case GET_PARTNER_CONTACT_PERSON_SUCCEEDED:
    case GET_PARTNER_CONTACT_PERSON_FAILED:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
}
