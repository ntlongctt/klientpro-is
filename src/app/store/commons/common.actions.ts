import { AppStoreService } from '@store/store.service';
import { Action } from '@ngrx/store';

export const GET_CODE_LIST_ITEMS = AppStoreService.createActionType('@COMMON/GET_CODE_LIST_ITEMS');
export const GET_CODE_LIST_ITEMS_SUCCEEDED = AppStoreService.createActionType('@COMMON/GET_CODE_LIST_ITEMS_SUCCEEDED');
export const GET_CODE_LIST_ITEMS_FAILED = AppStoreService.createActionType('@COMMON/GET_CODE_LIST_ITEMS_FAILED');

export class GetCodeListItems implements Action {
  public type = GET_CODE_LIST_ITEMS;
  constructor(public payload: any) {
  }
}
export class GetCodeListItemsSucceeded implements Action {
  public type = GET_CODE_LIST_ITEMS_SUCCEEDED;
  constructor(public payload: any) {
  }
}
export class GetCodeListItemsFailed implements Action {
  public type = GET_CODE_LIST_ITEMS_FAILED;
  constructor(public payload: any) {
  }
}


export type CommonActions = GetCodeListItems;
