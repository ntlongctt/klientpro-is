import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AppState } from '@store/store.reducers';
import { Store, Action, ActionsSubject } from '@ngrx/store';
import { CodeListService } from '@klient/rest/services/common/CodeList.service';
import { Observable } from 'rxjs/Observable';
import { mergeMap, map, catchError } from 'rxjs/operators';
import * as commonActions from '@store/commons/common.actions';

@Injectable()
export class CommonEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private codeListService: CodeListService
  ) {

  }

  @Effect()
  getCodeListItems$: Observable<Action> = this.actions$.pipe(
    ofType(commonActions.GET_CODE_LIST_ITEMS),
    mergeMap((action: any) => {
      return this.codeListService.getCodeListItems({
        codelistName: action.payload
      }).pipe(map(result => ({codeList: action.payload, data: result})));
    }),
    map(({codeList, data}) => (new commonActions.GetCodeListItemsSucceeded({codeList, data}))),
    catchError((err, caught) => {
      this.store.dispatch(new commonActions.GetCodeListItemsFailed(err.error));
      return caught;
    }),
  );
}
