import { CommonActions } from './common.actions';

export interface CommonState {
  isLoading: boolean;
}

const initialState: CommonState = {
  isLoading: false
};

export function CommonReducer(state: CommonState = initialState, action: CommonActions) {
  switch (action.type) {
    default:
      return state;
  }
}
