import { AppStoreService } from '@store/store.service';
import { Action } from '@ngrx/store';

export const GET_DEPARTMENTS = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENTS');
export const GET_DEPARTMENTS_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENTS_SUCCEEDED');
export const GET_DEPARTMENTS_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENTS_FAILED');

export const GET_DEPARTMENT_DETAIL = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_DETAIL');
export const GET_DEPARTMENT_DETAIL_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_DETAIL_SUCCEEDED');
export const GET_DEPARTMENT_DETAIL_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_DETAIL_FAILED');

export const UPDATE_DEPARTMENT_INFO = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_INFO');
export const UPDATE_DEPARTMENT_INFO_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_INFO_SUCCEEDED');
export const UPDATE_DEPARTMENT_INFO_FAILED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_INFO_FAILED');

export const ADD_DEPARTMENT = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT');
export const ADD_DEPARTMENT_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_SUCCEEDED');
export const ADD_DEPARTMENT_FAILED = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_FAILED');

export const DELETE_DEPARTMENT = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT');
export const DELETE_DEPARTMENT_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_SUCCEEDED');
export const DELETE_DEPARTMENT_FAILED = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_FAILED');

// ----------------------
export const GET_DEPARTMENT_ADDRESSES = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_ADDRESSES');
export const GET_DEPARTMENT_ADDRESSES_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_ADDRESSES_SUCCEEDED');
export const GET_DEPARTMENT_ADDRESSES_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_ADDRESSES_FAILED');

export const GET_DEPARTMENT_ADDRESS_DETAIL = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_ADDRESS_DETAIL');
export const GET_DEPARTMENT_ADDRESS_DETAIL_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_ADDRESS_DETAIL_SUCCEEDED');
export const GET_DEPARTMENT_ADDRESS_DETAIL_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_ADDRESS_DETAIL_FAILED');

export const UPDATE_DEPARTMENT_ADDRESS = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_ADDRESS');
export const UPDATE_DEPARTMENT_ADDRESS_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_ADDRESS_SUCCEEDED');
export const UPDATE_DEPARTMENT_ADDRESS_FAILED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_ADDRESS_FAILED');

export const ADD_DEPARTMENT_ADDRESS = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_ADDRESS');
export const ADD_DEPARTMENT_ADDRESS_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_ADDRESS_SUCCEEDED');
export const ADD_DEPARTMENT_ADDRESS_FAILED = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_ADDRESS_FAILED');

export const DELETE_DEPARTMENT_ADDRESS = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_ADDRESS');
export const DELETE_DEPARTMENT_ADDRESS_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_ADDRESS_SUCCEEDED');
export const DELETE_DEPARTMENT_ADDRESS_FAILED = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_ADDRESS_FAILED');

// ----------------------
export const GET_DEPARTMENT_CONTACT_PERSONS = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_CONTACT_PERSONS');
export const GET_DEPARTMENT_CONTACT_PERSONS_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_CONTACT_PERSONS_SUCCEEDED');
export const GET_DEPARTMENT_CONTACT_PERSONS_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_CONTACT_PERSONS_FAILED');

export const GET_DEPARTMENT_CONTACT_PERSON_DETAIL = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_CONTACT_PERSON_DETAIL');
export const GET_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED');
export const GET_DEPARTMENT_CONTACT_PERSON_DETAIL_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_CONTACT_PERSON_DETAIL_FAILED');

export const UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL');
export const UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED');
export const UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_FAILED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_FAILED');

export const ADD_DEPARTMENT_CONTACT_PERSON = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_CONTACT_PERSON');
export const ADD_DEPARTMENT_CONTACT_PERSON_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_CONTACT_PERSON_SUCCEEDED');
export const ADD_DEPARTMENT_CONTACT_PERSON_FAILED = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_CONTACT_PERSON_FAILED');

export const DELETE_DEPARTMENT_CONTACT_PERSON = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_CONTACT_PERSON');
export const DELETE_DEPARTMENT_CONTACT_PERSON_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_CONTACT_PERSON_SUCCEEDED');
export const DELETE_DEPARTMENT_CONTACT_PERSON_FAILED = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_CONTACT_PERSON_FAILED');

// -------------------------------------
export const GET_DEPARTMENT_MC_SYSTEMS = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_MC_SYSTEMS');
export const GET_DEPARTMENT_MC_SYSTEMS_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_MC_SYSTEMS_SUCCEEDED');
export const GET_DEPARTMENT_MC_SYSTEMS_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_MC_SYSTEMS_FAILED');

export const GET_DEPARTMENT_MC_SYSTEM_DETAIL = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_MC_SYSTEM_DETAIL');
export const GET_DEPARTMENT_MC_SYSTEM_DETAIL_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_MC_SYSTEM_DETAIL_SUCCEEDED');
export const GET_DEPARTMENT_MC_SYSTEM_DETAIL_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_DEPARTMENT_MC_SYSTEM_DETAIL_FAILED');

export const UPDATE_DEPARTMENT_MC_SYSTEM = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_MC_SYSTEM');
export const UPDATE_DEPARTMENT_MC_SYSTEM_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_MC_SYSTEM_SUCCEEDED');
export const UPDATE_DEPARTMENT_MC_SYSTEM_FAILED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_DEPARTMENT_MC_SYSTEM_FAILED');

export const ADD_DEPARTMENT_MC_SYSTEM = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_MC_SYSTEM');
export const ADD_DEPARTMENT_MC_SYSTEM_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_MC_SYSTEM_SUCCEEDED');
export const ADD_DEPARTMENT_MC_SYSTEM_FAILED = AppStoreService.createActionType('@DEPARTMENT/ADD_DEPARTMENT_MC_SYSTEM_FAILED');

export const DELETE_DEPARTMENT_MC_SYSTEM = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_MC_SYSTEM');
export const DELETE_DEPARTMENT_MC_SYSTEM_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_MC_SYSTEM_SUCCEEDED');
export const DELETE_DEPARTMENT_MC_SYSTEM_FAILED = AppStoreService.createActionType('@DEPARTMENT/DELETE_DEPARTMENT_MC_SYSTEM_FAILED');


export class GetDepartments implements Action {
  public type = GET_DEPARTMENTS;
  constructor(public payload: any) {
  }
}
export class GetDepartmentsSucceeded implements Action {
  public type = GET_DEPARTMENTS_SUCCEEDED;
  constructor(public payload: any) {
  }
}
export class GetDepartmentsFailed implements Action {
  public type = GET_DEPARTMENTS_FAILED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentDetail implements Action {
  public type = GET_DEPARTMENT_DETAIL;
  constructor(public payload: any) {
  }
}
export class GetDepartmentDetailSucceeded implements Action {
  public type = GET_DEPARTMENT_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}
export class GetDepartmentDetailFailed implements Action {
  public type = GET_DEPARTMENT_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentInfo implements Action {
  public type = UPDATE_DEPARTMENT_INFO;
  constructor(public payload: any) {
  }
}
export class UpdateDepartmentInfoSucceeded implements Action {
  public type = UPDATE_DEPARTMENT_INFO_SUCCEEDED;
  constructor(public payload: any) {
  }
}
export class UpdateDepartmentInfoFailed implements Action {
  public type = UPDATE_DEPARTMENT_INFO_FAILED;
  constructor(public payload: any) {
  }
}

export class AddDepartment implements Action {
  public type = ADD_DEPARTMENT;
  constructor(public payload: any) {
  }
}export class AddDepartmentSucceeded implements Action {
  public type = ADD_DEPARTMENT_SUCCEEDED;
  constructor(public payload: any) {
  }
}
export class AddDepartmentFailed implements Action {
  public type = ADD_DEPARTMENT_FAILED;
  constructor(public payload: any) {
  }
}

export class DeleteDepartment implements Action {
  public type = DELETE_DEPARTMENT;
  constructor(public payload: any) {
  }
}
export class DeleteDepartmentSucceeded implements Action {
  public type = DELETE_DEPARTMENT_SUCCEEDED;
  constructor(public payload: any) {
  }
}
export class DeleteDepartmentFailed implements Action {
  public type = DELETE_DEPARTMENT_FAILED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentAddresses implements Action {
  public type = GET_DEPARTMENT_ADDRESSES;
  constructor(public payload: any) {
  }
}

export class GetDepartmentAddressesSucceeded implements Action {
  public type = GET_DEPARTMENT_ADDRESSES_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentAddressesFailed implements Action {
  public type = GET_DEPARTMENT_ADDRESSES_FAILED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentAddressDetail implements Action {
  public type = GET_DEPARTMENT_ADDRESS_DETAIL;
  constructor(public payload: any) {
  }
}
export class GetDepartmentAddressDetailSucceeded implements Action {
  public type = GET_DEPARTMENT_ADDRESS_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}
export class GetDepartmentAddressDetailFailed implements Action {
  public type = GET_DEPARTMENT_ADDRESS_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentAddress implements Action {
  public type = UPDATE_DEPARTMENT_ADDRESS;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentAddressSucceeded implements Action {
  public type = UPDATE_DEPARTMENT_ADDRESS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentAddressFailed implements Action {
  public type = UPDATE_DEPARTMENT_ADDRESS_FAILED;
  constructor(public payload: any) {
  }
}


export class DeleteDepartmentAddress implements Action {
  public type = DELETE_DEPARTMENT_ADDRESS;
  constructor(public payload: any) {
  }
}

export class DeleteDepartmentAddressSucceeded implements Action {
  public type = DELETE_DEPARTMENT_ADDRESS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class DeleteDepartmentAddressFailed implements Action {
  public type = DELETE_DEPARTMENT_ADDRESS_FAILED;
  constructor(public payload: any) {
  }
}

export class AddDepartmentAddress implements Action {
  public type = ADD_DEPARTMENT_ADDRESS;
  constructor(public payload: any) {
  }
}

export class AddDepartmentAddressSucceeded implements Action {
  public type = ADD_DEPARTMENT_ADDRESS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class AddDepartmentAddressFailed implements Action {
  public type = ADD_DEPARTMENT_ADDRESS_FAILED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentContactPersons implements Action {
  public type = GET_DEPARTMENT_CONTACT_PERSONS;
  constructor(public payload: any) {
  }
}

export class GetDepartmentContactPersonsSucceeded implements Action {
  public type = GET_DEPARTMENT_CONTACT_PERSONS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentContactPersonsFailed implements Action {
  public type = GET_DEPARTMENT_CONTACT_PERSONS_FAILED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentContactPersonDetail implements Action {
  public type = GET_DEPARTMENT_CONTACT_PERSON_DETAIL;
  constructor(public payload: any) {
  }
}

export class GetDepartmentContactPersonDetailSucceeded implements Action {
  public type = GET_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentContactPersonDetailFailed implements Action {
  public type = GET_DEPARTMENT_CONTACT_PERSON_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}


export class UpdateDepartmentContactPersonDetail implements Action {
  public type = UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentContactPersonDetailSucceeded implements Action {
  public type = UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentContactPersonDetailFailed implements Action {
  public type = UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}

export class AddDepartmentContactPerson implements Action {
  public type = ADD_DEPARTMENT_CONTACT_PERSON;
  constructor(public payload: any) {
  }
}

export class AddDepartmentContactPersonSucceeded implements Action {
  public type = ADD_DEPARTMENT_CONTACT_PERSON_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class AddDepartmentContactPersonFailed implements Action {
  public type = ADD_DEPARTMENT_CONTACT_PERSON_FAILED;
  constructor(public payload: any) {
  }
}

export class DeleteDepartmentContactPerson implements Action {
  public type = DELETE_DEPARTMENT_CONTACT_PERSON;
  constructor(public payload: any) {
  }
}

export class DeleteDepartmentContactPersonSucceeded implements Action {
  public type = DELETE_DEPARTMENT_CONTACT_PERSON_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class DeleteDepartmentContactPersonFailed implements Action {
  public type = DELETE_DEPARTMENT_CONTACT_PERSON_FAILED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentMcSystems implements Action {
  public type = GET_DEPARTMENT_MC_SYSTEMS;
  constructor(public payload: any) {
  }
}

export class GetDepartmentMcSystemsSucceeded implements Action {
  public type = GET_DEPARTMENT_MC_SYSTEMS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentMcSystemsFailed implements Action {
  public type = GET_DEPARTMENT_MC_SYSTEMS_FAILED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentMcSystemDetail implements Action {
  public type = GET_DEPARTMENT_MC_SYSTEM_DETAIL;
  constructor(public payload: any) {
  }
}

export class GetDepartmentMcSystemDetailSucceeded implements Action {
  public type = GET_DEPARTMENT_MC_SYSTEM_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetDepartmentMcSystemDetailFailed implements Action {
  public type = GET_DEPARTMENT_MC_SYSTEM_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentMcSystem implements Action {
  public type = UPDATE_DEPARTMENT_MC_SYSTEM;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentMcSystemSucceeded implements Action {
  public type = UPDATE_DEPARTMENT_MC_SYSTEM_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class UpdateDepartmentMcSystemFailed implements Action {
  public type = UPDATE_DEPARTMENT_MC_SYSTEM_FAILED;
  constructor(public payload: any) {
  }
}

export class AddDepartmentMcSystem implements Action {
  public type = ADD_DEPARTMENT_MC_SYSTEM;
  constructor(public payload: any) {
  }
}

export class AddDepartmentMcSystemSucceeded implements Action {
  public type = ADD_DEPARTMENT_MC_SYSTEM_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class AddDepartmentMcSystemFailed implements Action {
  public type = ADD_DEPARTMENT_MC_SYSTEM_FAILED;
  constructor(public payload: any) {
  }
}

export class DeletedDepartmentMcSystem implements Action {
  public type = DELETE_DEPARTMENT_MC_SYSTEM;
  constructor(public payload: any) {
  }
}

export class DeletedDepartmentMcSystemSucceeded implements Action {
  public type = DELETE_DEPARTMENT_MC_SYSTEM_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class DeletedDepartmentMcSystemFailed implements Action {
  public type = DELETE_DEPARTMENT_MC_SYSTEM_FAILED;
  constructor(public payload: any) {
  }
}


export type DepartmentActions = 
   GetDepartments
  |GetDepartmentsSucceeded
  |GetDepartmentsFailed;
