import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { Observable } from 'rxjs/Observable';
import * as departmentActions from '@store/department/department.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { DepartmentService } from '@klient/rest/services/pa/Department.service';
import { CodeListService } from '@klient/rest/services/common/CodeList.service';

@Injectable()
export class DepartmentEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private departmentService: DepartmentService,
    private codeListService: CodeListService
  ) {
   
  }

  @Effect()
  getDepartments$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.GET_DEPARTMENTS),
    mergeMap((action: any) => {
      return this.departmentService.getDepartments({
        bpId: action.payload.bpId,
        departmentId: action.payload.departmentId,
        query: action.payload.query
      });
    }),
    map((data) => (new departmentActions.GetDepartmentsSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentsFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getDepartmentDEtail$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.GET_DEPARTMENT_DETAIL),
    mergeMap((action: any) => {
      return this.departmentService.getDepartment({
        departmentId: action.payload
      });
    }),
    map((data) => (new departmentActions.GetDepartmentDetailSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentDetailFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  updateDepartmentInfo$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.UPDATE_DEPARTMENT_INFO),
    mergeMap((action: any) => {
      return this.departmentService.updateDepartment({
        bpChangeDate: action.payload.bpChangeDate,
        departmentId: action.payload.departmentId,
        department: action.payload.department,
        bpChangeId: action.payload.bpChangeId
      });
    }),
    map((data) => (new departmentActions.UpdateDepartmentInfoSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.UpdateDepartmentInfoFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  addDepartment$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.ADD_DEPARTMENT),
    mergeMap((action: any) => {
      return this.departmentService.addDepartment({
        department: action.payload.department,
        bpId: action.payload.bpId
      });
    }),
    map((data) => (new departmentActions.AddDepartmentSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.AddDepartmentFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  deleteDepartment$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.DELETE_DEPARTMENT),
    mergeMap((action: any) => {
      return this.departmentService.deleteDepartment({
        departmentId: action.payload
      });
    }),
    map((data) => (new departmentActions.DeleteDepartmentSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.DeleteDepartmentFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getDepartmentAddresses$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.GET_DEPARTMENT_ADDRESSES),
    mergeMap((action: any) => {
      return this.departmentService.getDepartmentAddresses({
        departmentId: action.payload
      });
    }),
    map((data) => (new departmentActions.GetDepartmentAddressesSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentAddressesFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getDepartmentAddressDetail$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.GET_DEPARTMENT_ADDRESS_DETAIL),
    mergeMap((action: any) => {
      return this.departmentService.getDepartmentAddress({
        departmentId: action.payload.departmentId,
        departmentAddressId: action.payload.departmentAddressId
      });
    }),
    map((data) => (new departmentActions.GetDepartmentAddressDetailSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentAddressDetailFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  updateDepartmentAddressDetail$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.UPDATE_DEPARTMENT_ADDRESS),
    mergeMap((action: any) => {
      return this.departmentService.updateDepartmentAddress({
        departmentId: action.payload.departmentId,
        departmentAddressId: action.payload.departmentAddressId,
        address: action.payload.address
      });
    }),
    map((data) => (new departmentActions.UpdateDepartmentAddressSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentAddressDetailFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  deleteDepartmentAddress$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.DELETE_DEPARTMENT_ADDRESS),
    mergeMap((action: any) => {
      return this.departmentService.deleteBusinessPartnerAddress({
        departmentId: action.payload.departmentId,
        departmentAddressId: action.payload.departmentAddressId,
      });
    }),
    map((data) => (new departmentActions.DeleteDepartmentAddressSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.DeleteDepartmentAddressFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  addDepartmentAddress$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.ADD_DEPARTMENT_ADDRESS),
    mergeMap((action: any) => {
      return this.departmentService.addDepartmentAddress({
        departmentId: action.payload.departmentId,
        address: action.payload.address
      });
    }),
    map((data) => (new departmentActions.AddDepartmentAddressSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.AddDepartmentAddressFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getDepartmentContactPersons$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.GET_DEPARTMENT_CONTACT_PERSONS),
    mergeMap((action: any) => {
      return this.departmentService.getDepartmentContactPersons({
        departmentId: action.payload.departmentId,
        query: action.payload.query
      });
    }),
    map((data) => (new departmentActions.GetDepartmentContactPersonsSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentContactPersonsFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getDepartmentContactPersonDetail$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.GET_DEPARTMENT_CONTACT_PERSON_DETAIL),
    mergeMap((action: any) => {
      return this.departmentService.getDepartmentContactPerson({
        departmentId: action.payload.departmentId,
        departmentContactPersonId: action.payload.departmentContactPersonId
      });
    }),
    map((data) => (new departmentActions.GetDepartmentContactPersonDetailSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentContactPersonDetailFailed(err.error));
      return caught;
    }),
  );
  
  @Effect()
  updateDepartmentContactPersonDetail$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.UPDATE_DEPARTMENT_CONTACT_PERSON_DETAIL),
    mergeMap((action: any) => {
      return this.departmentService.updateDepartmentContactPerson({
        departmentId: action.payload.departmentId,
        departmentContactPersonId: action.payload.departmentContactPersonId,
        contactPerson: action.payload.contactPerson
      });
    }),
    map((data) => (new departmentActions.UpdateDepartmentContactPersonDetailSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.UpdateDepartmentContactPersonDetailFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  addDepartmentContactPerson$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.ADD_DEPARTMENT_CONTACT_PERSON),
    mergeMap((action: any) => {
      return this.departmentService.addDepartmentContactPerson({
        departmentId: action.payload.departmentId,
        contactPerson: action.payload.contactPerson
      });
    }),
    map((data) => (new departmentActions.AddDepartmentContactPersonSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.AddDepartmentContactPersonFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  deleteDepartmentContactPerson$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.DELETE_DEPARTMENT_CONTACT_PERSON),
    mergeMap((action: any) => {
      return this.departmentService.deleteBusinessPartnerContactPerson({
        departmentId: action.payload.departmentId,
        departmentContactPersonId: action.payload.departmentContactPersonId
      });
    }),
    map((data) => (new departmentActions.DeleteDepartmentContactPersonSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.DeleteDepartmentContactPersonFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getDepartmentMcSystem$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.GET_DEPARTMENT_MC_SYSTEMS),
    mergeMap((action: any) => {
      return this.departmentService.getDepartmentMcSystems({
        departmentId: action.payload.departmentId,
        query: action.payload.query
      });
    }),
    map((data) => (new departmentActions.GetDepartmentMcSystemsSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentMcSystemsFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getDepartmentMcSystemDetail$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.GET_DEPARTMENT_MC_SYSTEM_DETAIL),
    mergeMap((action: any) => {
      return this.departmentService.getDepartmentMcSystem({
        departmentId: action.payload.departmentId,
        departmentMcSystemId: action.payload.departmentMcSystemId
      });
    }),
    map((data) => (new departmentActions.GetDepartmentMcSystemDetailSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.GetDepartmentMcSystemDetailFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  updateDepartmentMcSystem$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.UPDATE_DEPARTMENT_MC_SYSTEM),
    mergeMap((action: any) => {
      return this.departmentService.updateDepartmentMcSystem({
        departmentId: action.payload.departmentId,
        departmentMcSystemId: action.payload.departmentMcSystemId,
        departmentMcSystem: action.payload.departmentMcSystem
      });
    }),
    map((data) => (new departmentActions.UpdateDepartmentMcSystemSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.UpdateDepartmentMcSystemFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  addDepartmentMcSystem$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.ADD_DEPARTMENT_MC_SYSTEM),
    mergeMap((action: any) => {
      return this.departmentService.addDepartmentMcSystem({
        departmentId: action.payload.departmentId,
        departmentMcSystem: action.payload.departmentMcSystem
      });
    }),
    map((data) => (new departmentActions.AddDepartmentMcSystemSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.AddDepartmentMcSystemFailed(err.error));
      return caught;
    }),
  );
  @Effect()
  deleteDepartmentMcSystem$: Observable<Action> = this.actions$.pipe(
    ofType(departmentActions.DELETE_DEPARTMENT_MC_SYSTEM),
    mergeMap((action: any) => {
      return this.departmentService.deleteDepartmentMcSystem({
        departmentId: action.payload.departmentId,
        departmentMcSystemId: action.payload.departmentMcSystemId,
      });
    }),
    map((data) => (new departmentActions.DeletedDepartmentMcSystemSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new departmentActions.DeletedDepartmentMcSystemFailed(err.error));
      return caught;
    }),
  );
}
