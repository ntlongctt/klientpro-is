import { DepartmentActions } from './department.actions';

export interface DepartmentState {
  isLoading: boolean;
}

const initialState: DepartmentState = {
  isLoading: false
};

export function DepartmentReducer(state: DepartmentState = initialState, action: DepartmentActions) {
  switch (action.type) {
    default:
      return state;
  }
}
