import { AppStoreService } from '@store/store.service';
import { Action } from '@ngrx/store';

export const UNAUTHORIZED_ERROR = AppStoreService.createActionType('@ERROR/UNAUTHORIZED_ERROR');
export const FORBIDDEN_ERROR = AppStoreService.createActionType('@ERROR/FORBIDDEN_ERROR');
export const SERVER_INTERNAL_ERROR = AppStoreService.createActionType('@ERROR/SERVER_INTERNAL_ERROR');
export const KNOWN_ERROR = AppStoreService.createActionType('@ERROR/KNOWN_ERROR');
export const CONNECTION_TIMEOUT_ERROR = AppStoreService.createActionType('@ERROR/CONNECTION_TIMEOUT_ERROR');
export const UNKOWN_ERROR = AppStoreService.createActionType('@ERROR/UNKOWN_ERROR');
export const ADD_ERROR_SHOWING = AppStoreService.createActionType('@ERROR/ADD_ERROR_SHOWING');
export const REMOVE_ERROR_SHOWING = AppStoreService.createActionType('@ERROR/REMOVE_ERROR_SHOWING');
export const CLOSE_LOGOUT_MODAL = AppStoreService.createActionType('@ERROR/CLOSE_LOGOUT_MODAL');


export class UnauthorizedError implements Action {
  public readonly type = UNAUTHORIZED_ERROR;
}

export class ForbiddenError implements Action {
  public readonly type = FORBIDDEN_ERROR;
}

export class ServerInternalError implements Action {
  public readonly type = SERVER_INTERNAL_ERROR;
}

export class ConnectionTimeoutError implements Action {
  public readonly type = CONNECTION_TIMEOUT_ERROR;
}

export class KnownError implements Action {
  public readonly type = KNOWN_ERROR;
  constructor(public payload: any) {}
}

export class UnKnownError implements Action {
  public readonly type = UNKOWN_ERROR;
  constructor(public payload: any) {}
}

export class AddErrorShowing implements Action {
  public readonly type = ADD_ERROR_SHOWING;
  constructor(public payload: any) {}
}

export class RemoveErrorShowing implements Action {
  public readonly type = REMOVE_ERROR_SHOWING;
  constructor(public payload: any) {}
}

export class CloseLogoutModal implements Action {
  public readonly type = CLOSE_LOGOUT_MODAL;
}

export type ErrorActions = 
  UnauthorizedError|
  ForbiddenError|
  ServerInternalError|
  KnownError|
  AddErrorShowing|
  RemoveErrorShowing|
  CloseLogoutModal|
  ConnectionTimeoutError|
  UnKnownError;
