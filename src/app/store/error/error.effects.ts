import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmModalService } from '@common/modules/confirm-modal';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Util } from '@common/services/util';
import { ofType, Actions } from '@ngrx/effects';
import * as fromErrorActions from '@store/error/error.actions';
import { locale as enCommon } from '../../i18n/common/en';
import { AppStoreService } from '@store/store.service';
import { UnauthorizedModalService } from '@common/modules/unauthorized-modal';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import * as fromAuthActions from '@store/auth/auth.actions';
import { MatDialog } from '@angular/material';


@Injectable()
export class ErrorEffects {

  constructor(
    private actions$: Actions,
    private router: Router,
    private _confirmModalService: ConfirmModalService,
    private _unauthorizedModalService: UnauthorizedModalService,
    private _translationLoaderService: FuseTranslationLoaderService,
    private _util: Util,
    private _appStoreService: AppStoreService,
    private _store: Store<AppState>,
    private dialog: MatDialog
  ) {


    // handel 403 error
    this.actions$.pipe(
      ofType(fromErrorActions.FORBIDDEN_ERROR),
    ).subscribe(() => {

      // check if this error type is showing, dont show new modal
      const isShowing = this._appStoreService.getState().error.showing.includes('FORBIDDEN_ERROR');
      this._store.dispatch(new fromErrorActions.AddErrorShowing('FORBIDDEN_ERROR'));
      if (isShowing) {
        return;
      }

      this._confirmModalService.show(
        this._translationLoaderService.instant('MODAL.CONTENT.FORBIDDEN'), 
        () => {
          // remove error show from store when modal closed
          this._store.dispatch(new fromErrorActions.RemoveErrorShowing('FORBIDDEN_ERROR'));
        }, 
        this._translationLoaderService.instant('MODAL.TITLE.FORBIDDEN'), 
        false, 'error', 'FORBIDDEN_ERROR'
      );
    });
    
    // handel 401 error
    this.actions$.pipe(
      ofType(fromErrorActions.UNAUTHORIZED_ERROR),
    ).subscribe(() => {

      const userName = this._appStoreService.getState().auth.currentUser ?
         this._appStoreService.getState().auth.currentUser.userName : '';

      const currentPage = this._appStoreService.getState().router.state.url;

      this._store.dispatch( new fromAuthActions.CleanToken());

      // check if this error type is showing, dont show new modal
      const isShowing = this._appStoreService.getState().error.showing.includes('UNAUTHORIZED_ERROR');
      this._store.dispatch(new fromErrorActions.AddErrorShowing('UNAUTHORIZED_ERROR'));
      if (isShowing) {
        return;
      }

      this._unauthorizedModalService.show((status) => {
        if (status) {
          this.dialog.closeAll();
          this._store.dispatch( new fromAuthActions.SetReturnUrl(currentPage));
          this._store.dispatch( new fromAuthActions.SetPreviousUser(userName));
          
          this.router.navigate(['sign-in']);
        }
        // remove error show from store when modal closed
        this._store.dispatch(new fromErrorActions.RemoveErrorShowing('UNAUTHORIZED_ERROR'));
      });
    });

    // handel 500 error
    this.actions$.pipe(
      ofType(fromErrorActions.SERVER_INTERNAL_ERROR),
    ).subscribe(() => {
      const currentUrl = this._appStoreService.getState().router.state.url;
      // dont show modal in sign-in page
      if (currentUrl === '/sign-in') {
        return;
      }

      // check if this error type is showing, dont show new modal
      const isShowing = this._appStoreService.getState().error.showing.includes('SERVER_INTERNAL_ERROR');
      this._store.dispatch(new fromErrorActions.AddErrorShowing('SERVER_INTERNAL_ERROR'));
      if (isShowing) {
        return;
      }
      
      this._confirmModalService.show(
        this._translationLoaderService.instant('MODAL.CONTENT.SERVER_INTERNAL_ERROR'), () => {
          // remove error show from store when modal closed
          this._store.dispatch(new fromErrorActions.RemoveErrorShowing('SERVER_INTERNAL_ERROR'));
        }, 
        this._translationLoaderService.instant('MODAL.TITLE.SERVER_INTERNAL_ERROR'), false, 
        'error', 
        'SERVER_INTERNAL_ERROR'
      );
    });

    // handel knowned error
    this.actions$.pipe(
      ofType(fromErrorActions.KNOWN_ERROR),
    ).subscribe((action: any) => {

      // check if this error type is showing, dont show new modal
      const isShowing = this._appStoreService.getState().error.showing.includes(action.payload);
      this._store.dispatch(new fromErrorActions.AddErrorShowing(action.payload));
      if (isShowing) {
        return;
      }

      this._confirmModalService.show(
        this._util.getErrorMessge(action.payload), 
        () => {
          // remove error show from store when modal closed
          this._store.dispatch(new fromErrorActions.RemoveErrorShowing(action.payload));
        }, 
        this._translationLoaderService.instant('MODAL.TITLE.ERROR_MESSAGE'), false, 'error', action.payload
      );
    });

    // handel unknow error
    this.actions$.pipe(
      ofType(fromErrorActions.UNKOWN_ERROR),
    ).subscribe((action: any) => {

      // check if this error type is showing, dont show new modal
      const isShowing = this._appStoreService.getState().error.showing.includes('UNKOWN_ERROR');
      this._store.dispatch(new fromErrorActions.AddErrorShowing('UNKOWN_ERROR'));
      if (isShowing) {
        return;
      }
      
      this._confirmModalService.show(
        this._translationLoaderService.instant(action.payload), () => {
          // remove error show from store when modal closed
          this._store.dispatch(new fromErrorActions.RemoveErrorShowing('UNKOWN_ERROR'));
        }, 
        this._translationLoaderService.instant('MODAL.TITLE.UNKNOWN_ERROR'), false, 
        'error', 
        'UNKOWN_ERROR'
      );
    });

    // handel connection timeout error
    this.actions$.pipe(
      ofType(fromErrorActions.CONNECTION_TIMEOUT_ERROR),
    ).subscribe((action: any) => {

      // check if this error type is showing, dont show new modal
      const isShowing = this._appStoreService.getState().error.showing.includes('CONNECTION_TIMEOUT_ERROR');
      this._store.dispatch(new fromErrorActions.AddErrorShowing('CONNECTION_TIMEOUT_ERROR'));
      if (isShowing) {
        return;
      }
      
      this._confirmModalService.show(
        this._translationLoaderService.instant('MODAL.CONTENT.CONNECTION_TIMEOUT_ERROR'), () => {
          // remove error show from store when modal closed
          this._store.dispatch(new fromErrorActions.RemoveErrorShowing('CONNECTION_TIMEOUT_ERROR'));
        }, 
        this._translationLoaderService.instant('MODAL.TITLE.CONNECTION_TIMEOUT_ERROR'), false, 
        'error', 
        'CONNECTION_TIMEOUT_ERROR'
      );
    });
  }

}
