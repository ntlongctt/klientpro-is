import { ErrorActions, SERVER_INTERNAL_ERROR, ADD_ERROR_SHOWING, REMOVE_ERROR_SHOWING } from '@store/error/error.actions';

export interface ErrorState {
  errorMsg: string;
  showing: string[];
  showLogoutConfirm: boolean;
}

const initialState: ErrorState = {
  errorMsg: null,
  showing: [],
  showLogoutConfirm: false
};

export function ErrorReducer(state: ErrorState = initialState, action: ErrorActions) {
  switch (action.type) {
    case ADD_ERROR_SHOWING:
      return {
        ...state,
        showing: [...state.showing, action.payload]
      };
    case REMOVE_ERROR_SHOWING:
      return {
        ...state,
        showing: state.showing.filter(e => e !== action.payload)
      };
    default:
      return state;
  }
}
