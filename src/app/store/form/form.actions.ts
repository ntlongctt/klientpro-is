import { AppStoreService } from '@store/store.service';
import { Action } from '@ngrx/store';

export const CHANGE_FORM_STATUS = AppStoreService.createActionType('@FORM/CHANGE_FORM_STATUS');

export class ChangeFormStatus implements Action {
  public type = CHANGE_FORM_STATUS;
  constructor(public payload: any){}
}

export type FormActions = ChangeFormStatus;
