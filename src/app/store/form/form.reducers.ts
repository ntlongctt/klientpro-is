import { FormActions, CHANGE_FORM_STATUS } from '@store/form/form.actions';

export interface FormState {
  valid: boolean;
  hasChanged: boolean;
}

const initialState: FormState = {
  valid: null,
  hasChanged: null
};

export function FormReducer(state: FormState = initialState, action: FormActions) {
  switch (action.type) {
    case CHANGE_FORM_STATUS: 
      return {
        ...state,
        hasChanged: action.payload
      };

    default:
      return state;
  }
}
