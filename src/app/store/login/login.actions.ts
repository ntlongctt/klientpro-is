import { Action } from '@ngrx/store';
import { AppStoreService } from '@store/store.service';

export const LOGIN = AppStoreService.createActionType('@LOGIN/LOGIN');
export const LOGIN_REQUEST = AppStoreService.createActionType('@LOGIN/LOGIN_REQUEST');
export const LOGIN_FAILED = AppStoreService.createActionType('@LOGIN/LOGIN_FAILED');
export const LOGIN_SUCCESS = AppStoreService.createActionType('@LOGIN/LOGIN_SUCCESS');
export const START_LOGIN = AppStoreService.createActionType('@LOGIN/START_LOGIN');
export const END_LOGIN = AppStoreService.createActionType('@LOGIN/END_LOGIN');
export const GET_TOKEN = AppStoreService.createActionType('@LOGIN/GET_TOKEN');
export const GET_TOKEN_SUCCESS = AppStoreService.createActionType('@LOGIN/GET_TOKEN_SUCCESS');


export class Login implements Action {
  public readonly type = LOGIN;

  constructor(public payload: { username: string, password: string }) {
  }
}

export class LoginRequest implements Action {
  public readonly type = LOGIN_REQUEST;

  constructor(public payload: { username: string, password: string }) {
  }
}

export class StartLogin implements Action {
  public readonly type = START_LOGIN;
}

export class EndLogin implements Action {
  public readonly type = END_LOGIN;
}

export class LoginFailed implements Action {
  public readonly type = LOGIN_FAILED;

  constructor(public payload: any) {
    this.payload = this.payload;
  }
}

export class LoginSuccess implements Action {
  public readonly type = LOGIN_SUCCESS;

  constructor(public payload: {value: any}) {
  }
}

export class GetToken implements Action {
  public readonly type = GET_TOKEN;

  constructor(public payload: { login: any }) {
  }
}

export class GetTokenSuccess implements Action {
  public readonly type = GET_TOKEN_SUCCESS;

  constructor(public payload: { token: string, login: any }) {
  }
}


export type LoginActions =
  StartLogin
  | LoginRequest
  | Login
  | LoginFailed
  | LoginSuccess
  | EndLogin
  | GetToken
  | GetTokenSuccess;
