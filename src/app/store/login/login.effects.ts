import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, tap, debounceTime } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as fromLoginActions from '@store/login/login.actions';
import * as fromAuthActions from '@store/auth/auth.actions';
import * as fromMenuActions from '@store/menu/menu.actions';
import * as fromUserActions from '@store/user/user.action';
import { AppState } from '@store/store.reducers';
import { SystemService } from '../../rest/services/pa/System.service';
import { LoginRequest } from '@store/login/login.actions';
import { RestPaConstants } from '../../rest/RestPaConstants';
import { Observable } from 'rxjs/Observable';
import { AppStoreService } from '@store/store.service';
import * as Raven from 'raven-js';

@Injectable()
export class LoginEffects {
  @Effect()
  login$: Observable<Action> = this.actions$.pipe(
    ofType(fromLoginActions.LOGIN),
    tap(() => this.store.dispatch(new fromLoginActions.StartLogin())),
    map((action: fromLoginActions.Login) => (new fromLoginActions.GetToken({ login: action.payload }))),
    // If request fails, dispatch failed action
    catchError((e, caught) => {
      this.store.dispatch(new fromLoginActions.LoginFailed(e.error));
      return caught;
    })
  );

  @Effect()
  getToken$: Observable<Action> = this.actions$.pipe(
    ofType(fromLoginActions.GET_TOKEN),
    mergeMap((action: fromLoginActions.GetToken) => this.systemService.getToken().pipe(map((token) => ({ token, login: action.payload.login })))),
    map(({ token, login }) => (new fromLoginActions.GetTokenSuccess({ token, login }))),
    // If request fails, dispatch failed action
    catchError((e, caught) => {
      this.store.dispatch(new fromLoginActions.LoginFailed(e.error));
      return caught;
    })
  );

  @Effect()
  getTokenSuccess$: Observable<Action> = this.actions$.pipe(
    ofType(fromLoginActions.GET_TOKEN_SUCCESS),
    tap((action: any) => this.store.dispatch(new fromAuthActions.SetAuthToken({ authToken: action.payload.token }))),
    debounceTime(500), // wait until the token is saved
    map((action: any) => (new LoginRequest(action.payload.login))),
    // If request fails, dispatch failed action
    catchError((e, caught) => {
      this.store.dispatch(new fromLoginActions.LoginFailed(e.error));
      return caught;
    })
  );

  @Effect()
  loginRequest$: Observable<Action> = this.actions$.pipe(
    ofType(fromLoginActions.LOGIN_REQUEST),
    mergeMap((action: fromLoginActions.LoginRequest) => this.systemService.login({
      username: action.payload.username,
      password: action.payload.password,
      version: RestPaConstants.VERSION
    })),
    map(data => ({ type: fromLoginActions.LOGIN_SUCCESS, payload: data })),
    tap(() => this.store.dispatch(new fromLoginActions.EndLogin())),
    // If request fails, dispatch failed action
    catchError((e, caught) => {
      this.store.dispatch(new fromLoginActions.LoginFailed(e.error));
      return caught;
    })

  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(fromLoginActions.LOGIN_SUCCESS),
    tap((data: any) => {
      const returnUrl = this.appStoreService.getState().auth.returnUrl;
      const previousUser = this.appStoreService.getState().auth.previousUser;

      // this.store.dispatch(new fromUserAction.GetUserInfoSucceeded(data));
      this.store.dispatch(new fromAuthActions.SetCurrentUser({ currentUser: data.payload }));
      this.store.dispatch(new fromMenuActions.GetMenu());
      this.store.dispatch(new fromUserActions.GetUserInfo());
      // Clean previous user and return url after login success
      this.store.dispatch(new fromAuthActions.CleanPreviousUser());
      this.store.dispatch(new fromAuthActions.CleanReturnURL());


      // set user info to Sentry scope
      Raven.setUserContext({
        id: data.payload.id,
        email: data.payload.email,
      });

      // back to previous page
      if (returnUrl && (previousUser === data.payload.userName || !previousUser)) {
        this.router.navigateByUrl(returnUrl);
      } else {
        this.router.navigate(['dashboard']);
      }

    }),
  );

  @Effect({ dispatch: false })
  loginFailed$ = this.actions$.pipe(
    ofType(fromLoginActions.LOGIN_FAILED),
    // tap(() => this.router.navigate(['/+dashboard']))
  );

  /**
   * When user logout succeeded, fire action to another tabs, tell them show logout modal
   */
  @Effect({ dispatch: false })
  logOutSucees$ = this.actions$.pipe(
    ofType(fromAuthActions.LOG_OUT_SUCCEEDED),
    tap(() => {
      this.store.dispatch( new fromAuthActions.CleanToken());
      this.store.dispatch( new fromMenuActions.CleanMenu());
      // clear sentry scope
      Raven.setUserContext({});
    })
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<AppState>,
    private systemService: SystemService,
    private appStoreService: AppStoreService
  ) {
  }
}
