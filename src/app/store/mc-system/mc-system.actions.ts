import { AppStoreService } from '@store/store.service';
import { Action } from '@ngrx/store';

export const GET_MC_SYSTEM_DETAIL = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_DETAIL');
export const GET_MC_SYSTEM_DETAIL_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_DETAIL_SUCCEEDED');
export const GET_MC_SYSTEM_DETAIL_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_DETAIL_FAILED');

export const GET_MC_SYSTEM_TYPES = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_TYPES');
export const GET_MC_SYSTEM_TYPES_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_TYPES_SUCCEEDED');
export const GET_MC_SYSTEM_TYPES_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_TYPES_FAILED');

export const GET_MC_SYSTEM_AREAS = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_AREAS');
export const GET_MC_SYSTEM_AREAS_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_AREAS_SUCCEEDED');
export const GET_MC_SYSTEM_AREAS_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_AREAS_FAILED');

export const GET_MC_SYSTEM_CODE_LIST = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_CODE_LIST');
export const GET_MC_SYSTEM_CODE_LIST_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_CODE_LIST_SUCCEEDED');
export const GET_MC_SYSTEM_CODE_LIST_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEM_CODE_LIST_FAILED');

export const ADD_MC_SYSTEM = AppStoreService.createActionType('@DEPARTMENT/ADD_MC_SYSTEM');
export const ADD_MC_SYSTEM_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/ADD_MC_SYSTEM_SUCCEEDED');
export const ADD_MC_SYSTEM_FAILED = AppStoreService.createActionType('@DEPARTMENT/ADD_MC_SYSTEM_FAILED');

export const UPDATE_MC_SYSTEM = AppStoreService.createActionType('@DEPARTMENT/UPDATE_MC_SYSTEM');
export const UPDATE_MC_SYSTEM_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_MC_SYSTEM_SUCCEEDED');
export const UPDATE_MC_SYSTEM_FAILED = AppStoreService.createActionType('@DEPARTMENT/UPDATE_MC_SYSTEM_FAILED');

export const DELETE_MC_SYSTEM = AppStoreService.createActionType('@DEPARTMENT/DELETE_MC_SYSTEM');
export const DELETE_MC_SYSTEM_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/DELETE_MC_SYSTEM_SUCCEEDED');
export const DELETE_MC_SYSTEM_FAILED = AppStoreService.createActionType('@DEPARTMENT/DELETE_MC_SYSTEM_FAILED');

export const GET_MC_SYSTEMS = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEMS');
export const GET_MC_SYSTEMS_SUCCEEDED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEMS_SUCCEEDED');
export const GET_MC_SYSTEMS_FAILED = AppStoreService.createActionType('@DEPARTMENT/GET_MC_SYSTEMS_FAILED');


export class GetMcSystemDetail implements Action {
  public type = GET_MC_SYSTEM_DETAIL;
  constructor(public payload: any) {
  }
}

export class GetMcSystemDetailSucceeded implements Action {
  public type = GET_MC_SYSTEM_DETAIL_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetMcSystemDetailFailed implements Action {
  public type = GET_MC_SYSTEM_DETAIL_FAILED;
  constructor(public payload: any) {
  }
}

export class GetMcSystemTypes implements Action {
  public type = GET_MC_SYSTEM_TYPES;
  constructor(public payload: any) {
  }
}

export class GetMcSystemTypesSucceeded implements Action {
  public type = GET_MC_SYSTEM_TYPES_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetMcSystemTypesFailed implements Action {
  public type = GET_MC_SYSTEM_TYPES_FAILED;
  constructor(public payload: any) {
  }
}

export class GetMcSystemAreas implements Action {
  public type = GET_MC_SYSTEM_AREAS;
  constructor(public payload: any) {
  }
}

export class GetMcSystemAreasSucceeded implements Action {
  public type = GET_MC_SYSTEM_AREAS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetMcSystemAreasFailed implements Action {
  public type = GET_MC_SYSTEM_AREAS_FAILED;
  constructor(public payload: any) {
  }
}

export class GetMcSystemCodeList implements Action {
  public type = GET_MC_SYSTEM_CODE_LIST;
  constructor(public payload: any) {
  }
}

export class GetMcSystemCodeListSucceeded implements Action {
  public type = GET_MC_SYSTEM_CODE_LIST_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetMcSystemCodeListFailed implements Action {
  public type = GET_MC_SYSTEM_CODE_LIST_FAILED;
  constructor(public payload: any) {
  }
}

export class AddMcSystem implements Action {
  public type = ADD_MC_SYSTEM;
  constructor(public payload: any) {
  }
}

export class AddMcSystemSucceeded implements Action {
  public type = ADD_MC_SYSTEM_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class AddMcSystemFailed implements Action {
  public type = ADD_MC_SYSTEM_FAILED;
  constructor(public payload: any) {
  }
}

export class UpdateMcSystem implements Action {
  public type = UPDATE_MC_SYSTEM;
  constructor(public payload: any) {
  }
}

export class UpdateMcSystemSucceeded implements Action {
  public type = UPDATE_MC_SYSTEM_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class UpdateMcSystemFailed implements Action {
  public type = UPDATE_MC_SYSTEM_FAILED;
  constructor(public payload: any) {
  }
}

export class DeleteMcSystem implements Action {
  public type = DELETE_MC_SYSTEM;
  constructor(public payload: any) {
  }
}

export class DeleteMcSystemSucceeded implements Action {
  public type = DELETE_MC_SYSTEM_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class DeleteMcSystemFailed implements Action {
  public type = DELETE_MC_SYSTEM_FAILED;
  constructor(public payload: any) {
  }
}

export class GetMcSystems implements Action {
  public type = GET_MC_SYSTEMS;
  constructor(public payload: any) {
  }
}

export class GetMcSystemsSucceeded implements Action {
  public type = GET_MC_SYSTEMS_SUCCEEDED;
  constructor(public payload: any) {
  }
}

export class GetMcSystemsFailed implements Action {
  public type = GET_MC_SYSTEMS_FAILED;
  constructor(public payload: any) {
  }
}

export type McSystemActions = GetMcSystemDetail;
