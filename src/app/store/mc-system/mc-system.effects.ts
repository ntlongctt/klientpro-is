import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { CodeListService } from '@klient/rest/services/common/CodeList.service';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import * as mcSystemActions from '@store/mc-system/mc-system.actions';
import { McSystemService } from '@klient/rest/services/pa/McSystem.service';

@Injectable()
export class McSystemEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private codeListService: CodeListService,
    private mcSystemService: McSystemService
  ) {

  }

  @Effect()
  getMcSystemDetail$: Observable<Action> = this.actions$.pipe(
    ofType(mcSystemActions.GET_MC_SYSTEM_DETAIL),
    mergeMap((action: any) => {
      return this.mcSystemService.getMcSystem({
        mcSystemId: action.payload
      });
    }),
    map((data) => (new mcSystemActions.GetMcSystemDetailSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new mcSystemActions.GetMcSystemDetailFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getMcSystemTypes$: Observable<Action> = this.actions$.pipe(
    ofType(mcSystemActions.GET_MC_SYSTEM_TYPES),
    mergeMap((action: any) => {
      return this.codeListService.getCodeListItems({
        codelistName: action.payload
      });
    }),
    map((data) => (new mcSystemActions.GetMcSystemTypesSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new mcSystemActions.GetMcSystemTypesFailed(err.error));
      return caught;
    }),
  );
  
  @Effect()
  getMcSystemAreas$: Observable<Action> = this.actions$.pipe(
    ofType(mcSystemActions.GET_MC_SYSTEM_AREAS),
    mergeMap((action: any) => {
      return this.codeListService.getCodeListItems({
        codelistName: action.payload
      });
    }),
    map((data) => (new mcSystemActions.GetMcSystemAreasSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new mcSystemActions.GetMcSystemAreasFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  addMcSystem$: Observable<Action> = this.actions$.pipe(
    ofType(mcSystemActions.ADD_MC_SYSTEM),
    mergeMap((action: any) => {
      return this.mcSystemService.createMcSystem({
        body: action.payload
      });
    }),
    map((data) => (new mcSystemActions.AddMcSystemSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new mcSystemActions.AddMcSystemFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  updateMcSystem$: Observable<Action> = this.actions$.pipe(
    ofType(mcSystemActions.UPDATE_MC_SYSTEM),
    mergeMap((action: any) => {
      return this.mcSystemService.updateMcSystem({
        mcSystemId: action.payload.mcSystemId,
        body: action.payload.body
      });
    }),
    map((data) => (new mcSystemActions.UpdateMcSystemSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new mcSystemActions.UpdateMcSystemFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  deleteMcSystem$: Observable<Action> = this.actions$.pipe(
    ofType(mcSystemActions.DELETE_MC_SYSTEM),
    mergeMap((action: any) => {
      return this.mcSystemService.deleteMcSystem({
        mcSystemId: action.payload,
      });
    }),
    map((data) => (new mcSystemActions.DeleteMcSystemSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new mcSystemActions.DeleteMcSystemFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getMcSystems$: Observable<Action> = this.actions$.pipe(
    ofType(mcSystemActions.GET_MC_SYSTEMS),
    mergeMap((action: any) => {
      return this.mcSystemService.getMcSystems({
        start: action.payload.start,
        limit: action.payload.limit
      });
    }),
    map((data) => (new mcSystemActions.GetMcSystemsSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new mcSystemActions.GetMcSystemsFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getMcSystemCodeList$: Observable<Action> = this.actions$.pipe(
    ofType(mcSystemActions.GET_MC_SYSTEM_CODE_LIST),
    mergeMap((action: any) => {
      return this.mcSystemService.getMcSystemsCodelist({
        activeOnly: action.payload.activeOnly,
        mcSystemTypeId: action.payload.mcSystemTypeId
      });
    }),
    map((data) => (new mcSystemActions.GetMcSystemCodeListSucceeded(data))),
    catchError((err, caught) => {
      this.store.dispatch(new mcSystemActions.GetMcSystemCodeListFailed(err.error));
      return caught;
    }),
  );
}
