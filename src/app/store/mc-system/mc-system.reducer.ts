import { McSystemActions } from './mc-system.actions';

export interface McSystemState {
  isLoading: boolean;
}

const initialState: McSystemState = {
  isLoading: false
};

export function McSystemReducer(state: McSystemState = initialState, action: McSystemActions) {
  switch (action.type) {
    default:
      return state;
  }
}
