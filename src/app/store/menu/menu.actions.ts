import { Action } from '@ngrx/store';
import { AppStoreService } from '@store/store.service';

export const GET_MENU = AppStoreService.createActionType('@MENU/GET_MENU');
export const GET_MENU_SUCCEEDED = AppStoreService.createActionType('@MENU/GET_MENU_SUCCEEDED');
export const GET_MENU_FAILED = AppStoreService.createActionType('@MENU/GET_MENU_FAILED');
export const CHANGE_LANGUAGE = AppStoreService.createActionType('@MENU/CHANGE_LANGUAGE');
export const CLEAN_MENU = AppStoreService.createActionType('@MENU/CLEAN_MENU');

export class GetMenu implements Action{
  public readonly type = GET_MENU;
}

export class GetMenuSucceeded implements Action {
  public readonly type = GET_MENU_SUCCEEDED;
  constructor (public payload: any) {}
}

export class GetMenuFailed implements Action {
  public readonly type = GET_MENU_FAILED;
  constructor ( public payload: any) {}
}

export class ChangeLanguage implements Action {
  public readonly type = CHANGE_LANGUAGE;
  constructor ( public payload: any) {}
}

export class CleanMenu implements Action {
  public readonly type = CLEAN_MENU;
}

export type MenuActions = 
    GetMenu
  | GetMenuSucceeded
  | GetMenuFailed
  | ChangeLanguage
  | CleanMenu;
