import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as fromMenuAction from '@store/menu/menu.actions';
import { mergeMap, tap, catchError, map } from 'rxjs/operators';
import { MenuService } from '../../rest/services/pa/Menu.service';

@Injectable()
export class MenuEffects {

  @Effect()
  getMenu$ = this.actions$.pipe(
    ofType(fromMenuAction.GET_MENU),
    mergeMap(() => this.menuService.getMainMenu()),
    map((menu) => (new fromMenuAction.GetMenuSucceeded(menu))),
    catchError((e, caught) => {
      return caught;
    }),
  );

  constructor(
    private actions$: Actions,
    private menuService: MenuService
  ) {
  }
}


