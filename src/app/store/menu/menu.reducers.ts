import { MenuActions, GET_MENU, GET_MENU_SUCCEEDED, GET_MENU_FAILED, CLEAN_MENU } from '@store/menu/menu.actions';

export interface MenuState {
  errorMsg: string;
  menu: any;
}

const initialState: MenuState = {
  errorMsg: null,
  menu: null
};

export function MenuReducer(state: MenuState = initialState, action: MenuActions) {
  switch (action.type) {
    case GET_MENU_SUCCEEDED: 
      return {
        ...state,
        menu: action.payload
      };
    case GET_MENU_FAILED:
      return {
        ...state,
        errorMsg: action.payload
      };
    case CLEAN_MENU:
      return {
        ...state,
        menu: null
      };
    default:
      return state;
  }
}
