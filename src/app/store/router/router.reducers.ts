import * as fromRouter from '@ngrx/router-store';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Params } from '@angular/router';
import { createFeatureSelector } from '@ngrx/store';

export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  params: Params;
  previousUrl: string;
}

export const getRouterState = createFeatureSelector<fromRouter.RouterReducerState<RouterStateUrl>>('router');

/**
 * Method to ensure the router in ngrx only contains neccessary information: url, queryParams, params, previousUrl
 */
export class CustomSerializer implements fromRouter.RouterStateSerializer<RouterStateUrl> {

  public previousUrl;

  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const { url } = routerState;
    const { queryParams } = routerState.root;

    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      state = state.firstChild;
    }
    const { params } = state;

    const previousUrl = this.previousUrl;
    this.previousUrl = url;

    return {
      url,
      queryParams,
      params,
      previousUrl
    };
  }
}
