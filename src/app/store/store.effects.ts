import { RouterEffects } from '@store/router/router.effect';
import { LoginEffects } from '@store/login/login.effects';
import { AuthEffects } from '@store/auth/auth.effects';
import { UserEffects } from '@store/user/user.effects';
import { MenuEffects } from '@store/menu/menu.effects';
import { ErrorEffects } from '@store/error/error.effects';
import { TagEffects } from './tag/tag.effects';
import { BusinessPartnerEffects } from './business-patner/business-patner.effects';
import { DepartmentEffects } from './department/department.effects';
import { McSystemEffects } from './mc-system/mc-system.effects';
import { CommonEffects } from './commons/common.effects';

export const effects: any[] = [
    RouterEffects,
    LoginEffects,
    AuthEffects,
    UserEffects,
    MenuEffects,
    ErrorEffects,
    TagEffects,
    BusinessPartnerEffects,
    DepartmentEffects,
    McSystemEffects,
    CommonEffects
];
