import { ActionReducerMap, ActionReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import * as fromRouter from '@ngrx/router-store';
import { RouterStateUrl } from '@store/router/router.reducers';
import { storeFreeze } from 'ngrx-store-freeze'; // Prevent mutate the State directly in Development mode
import { MetaReducer } from '@ngrx/store';
import { environment } from 'environments/environment';
import { LoginState, LoginReducer } from './login/login.reducers';
import { AuthState, AuthReducer } from './auth/auth.reducers';
import { MenuState, MenuReducer } from '@store/menu/menu.reducers';
import { UsersState, UsersReducer } from '@store/user/user.reducers';
import { FormState, FormReducer } from '@store/form/form.reducers';
import { ErrorState, ErrorReducer } from './error/error.reducers';
import { TagsState, TagsReducer } from './tag/tag.reducers';
import { BreadscrumbState, BreadscrumReducer } from './breadcrumbs/breadcrumbs.reducers';
import { BusinessPartnerState, BusinessPartnerReducer } from './business-patner/business-patner.reducers';
import { DepartmentState, DepartmentReducer } from './department/department.reducers';
import { McSystemState, McSystemReducer } from './mc-system/mc-system.reducer';
import { CommonState, CommonReducer } from './commons/common.reducers';

export interface AppState {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
  auth: AuthState;
  login: LoginState;
  menu: MenuState;
  users: UsersState;
  form: FormState;
  error: ErrorState;
  tags: TagsState;
  breadscrumb: BreadscrumbState;
  businessPartner: BusinessPartnerState;
  department: DepartmentState;
  mcSystems: McSystemState;
  commons: CommonState;
}

export const AppReducers: ActionReducerMap<AppState> = {
  router: fromRouter.routerReducer,
  auth: AuthReducer,
  login: LoginReducer,
  menu: MenuReducer,
  users: UsersReducer,
  form: FormReducer,
  error: ErrorReducer,
  tags: TagsReducer,
  breadscrumb: BreadscrumReducer,
  businessPartner: BusinessPartnerReducer,
  department: DepartmentReducer,
  mcSystems: McSystemReducer,
  commons: CommonReducer
};

export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return (state: AppState, action: any): AppState => {
    console.log('%c [Ngrx-DEBUG] STATE', 'color: #007fff', state);
    console.log('%c [Ngrx-DEBUG] ACTION', 'color: #007fff', action);
    return reducer(state, action);
  };
}

export function localStorageSyncReducer(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return localStorageSync({
    keys: [''], // allowed keys to be saved to local storage
    rehydrate: true,   // allowed init state from local storage
    storage: sessionStorage
  })(reducer);
}

export const metaReducers: MetaReducer<any>[] = !environment.production
  ? [storeFreeze, logger, localStorageSyncReducer]
  : [localStorageSyncReducer];
