import { Action } from '@ngrx/store';
import { AppStoreService } from '@store/store.service';

export const GET_ASSIGNED_TAG = AppStoreService.createActionType('@TAG/GET_ASSIGNED_TAG');
export const GET_ASSIGNED_TAG_SUCCEEDED = AppStoreService.createActionType('@TAG/GET_ASSIGNED_TAG_SUCCEEDED');
export const GET_ASSIGNED_TAG_FAILED = AppStoreService.createActionType('@TAG/GET_ASSIGNED_TAG_FAILED');

export const GET_TAGS = AppStoreService.createActionType('@TAG/GET_TAGS');
export const GET_TAGS_SUCCEEDED = AppStoreService.createActionType('@TAG/GET_TAGS_SUCCEEDED');
export const GET_TAGS_FAILED = AppStoreService.createActionType('@TAG/GET_TAGS_FAILED');

export const UPDATE_ASSIGNED_TAG = AppStoreService.createActionType('@TAG/UPDATE_ASSIGNED_TAG');
export const UPDATE_ASSIGNED_TAG_SUCCEEDED = AppStoreService.createActionType('@TAG/UPDATE_ASSIGNED_TAG_SUCCEEDED');
export const UPDATE_ASSIGNED_TAG_FAILED = AppStoreService.createActionType('@TAG/UPDATE_ASSIGNED_TAG_FAILED');

export const GET_TAG_TYPES = AppStoreService.createActionType('@TAG/GET_TAG_TYPES');
export const GET_TAG_TYPES_SUCCEEDED = AppStoreService.createActionType('@TAG/GET_TAG_TYPES_SUCCEEDED');
export const GET_TAG_TYPES_FAILED = AppStoreService.createActionType('@TAG/GET_TAG_TYPES_FAILED');

export const GET_TAG_TYPES_DETAIL = AppStoreService.createActionType('@TAG/GET_TAG_TYPES_DETAIL');
export const GET_TAG_TYPES_DETAIL_SUCCEEDED = AppStoreService.createActionType('@TAG/GET_TAG_TYPES_DETAIL_SUCCEEDED');
export const GET_TAG_TYPES_DETAIL_FAILED = AppStoreService.createActionType('@TAG/GET_TAG_TYPES_DETAIL_FAILED');

export const GET_TAG_DETAIL = AppStoreService.createActionType('@TAG/GET_TAG_DETAIL');
export const GET_TAG_DETAIL_SUCCEEDED = AppStoreService.createActionType('@TAG/GET_TAG_DETAIL_SUCCEEDED');
export const GET_TAG_DETAIL_FAILED = AppStoreService.createActionType('@TAG/GET_TAG_DETAIL_FAILED');

export const UPDATE_TAG = AppStoreService.createActionType('@TAG/UPDATE_TAG');
export const UPDATE_TAG_SUCCEEDED = AppStoreService.createActionType('@TAG/UPDATE_TAG_SUCCEEDED');
export const UPDATE_TAG_FAILED = AppStoreService.createActionType('@TAG/UPDATE_TAG_FAILED');

export const UPDATE_TAG_TYPE = AppStoreService.createActionType('@TAG/UPDATE_TAG_TYPE');
export const UPDATE_TAG_TYPE_SUCCEEDED = AppStoreService.createActionType('@TAG/UPDATE_TAG_TYPE_SUCCEEDED');
export const UPDATE_TAG_TYPE_FAILED = AppStoreService.createActionType('@TAG/UPDATE_TAG_TYPE_FAILED');

export const CREATE_TAG = AppStoreService.createActionType('@TAG/CREATE_TAG');
export const CREATE_TAG_SUCCEEDED = AppStoreService.createActionType('@TAG/CREATE_TAG_SUCCEEDED');
export const CREATE_TAG_FAILED = AppStoreService.createActionType('@TAG/CREATE_TAG_FAILED');

export const CREATE_TAG_TYPE = AppStoreService.createActionType('@TAG/CREATE_TAG_TYPE');
export const CREATE_TAG_TYPE_SUCCEEDED = AppStoreService.createActionType('@TAG/CREATE_TAG_TYPE_SUCCEEDED');
export const CREATE_TAG_TYPE_FAILED = AppStoreService.createActionType('@TAG/CREATE_TAG_TYPE_FAILED');

export const DELETE_TAG = AppStoreService.createActionType('@TAG/DELETE_TAG');
export const DELETE_TAG_SUCCEEDED = AppStoreService.createActionType('@TAG/DELETE_TAG_SUCCEEDED');
export const DELETE_TAG_FAILED = AppStoreService.createActionType('@TAG/DELETE_TAG_FAILED');

export const DELETE_TAG_TYPE = AppStoreService.createActionType('@TAG/DELETE_TAG_TYPE');
export const DELETE_TAG_TYPE_SUCCEEDED = AppStoreService.createActionType('@TAG/DELETE_TAG_TYPE_SUCCEEDED');
export const DELETE_TAG_TYPE_FAILED = AppStoreService.createActionType('@TAG/DELETE_TAG_TYPE_FAILED');

export class GetAssignedTag implements Action {
  public type = GET_ASSIGNED_TAG;
  constructor(public payload: any) {}
}

export class GetAssignedTagSucceeded implements Action {
  public type = GET_ASSIGNED_TAG_SUCCEEDED;
  constructor(public payload: any) {}
}

export class GetAssignedTagFailed implements Action {
  public type = GET_ASSIGNED_TAG_FAILED;
  constructor(public payload: any) {}
}

export class UpdateAssignedTag implements Action {
  public type = UPDATE_ASSIGNED_TAG;
  constructor(public payload: any) {}
}

export class UpdateAssignedTagSucceeded implements Action {
  public type = UPDATE_ASSIGNED_TAG_SUCCEEDED;
  constructor(public payload: any) {}
}

export class UpdateAssignedTagFailed implements Action {
  public type = UPDATE_ASSIGNED_TAG_FAILED;
  constructor(public payload: any) {}
}

export class GetTags implements Action {
  public type = GET_TAGS;
  constructor(public payload: any) {}
}

export class GetTagsSucceeded implements Action {
  public type = GET_TAGS_SUCCEEDED;
  constructor(public payload: any) {}
}

export class GetTagsFailed implements Action {
  public type = GET_TAGS_FAILED;
  constructor(public payload: any) {}
}

export class GetTagTypes implements Action {
  public type = GET_TAG_TYPES;
  constructor(public payload: any) {}
}

export class GetTagTypesSucceeded implements Action {
  public type = GET_TAG_TYPES_SUCCEEDED;
  constructor(public payload: any) {}
}

export class GetTagTypesFailed implements Action {
  public type = GET_TAG_TYPES_FAILED;
  constructor(public payload: any) {}
}
export class GetTagDetail implements Action {
  public type = GET_TAG_DETAIL;
  constructor(public payload: any) {}
}

export class GetTagDetailSucceeded implements Action {
  public type = GET_TAG_DETAIL_SUCCEEDED;
  constructor(public payload: any) {}
}

export class GetTagDetailFailed implements Action {
  public type = GET_TAG_DETAIL_FAILED;
  constructor(public payload: any) {}
}

export class UpdateTag implements Action {
  public type = UPDATE_TAG;
  constructor(public payload: any) {}
}

export class UpdateTagSucceeded implements Action {
  public type = UPDATE_TAG_SUCCEEDED;
  constructor(public payload: any) {}
}

export class UpdateTagFailed implements Action {
  public type = UPDATE_TAG_FAILED;
  constructor(public payload: any) {}
}

export class CreateTag implements Action {
  public type = CREATE_TAG;
  constructor(public payload: any) {}
}

export class CreateTagSucceeded implements Action {
  public type = CREATE_TAG_SUCCEEDED;
  constructor(public payload: any) {}
}

export class CreateTagFailed implements Action {
  public type = CREATE_TAG_FAILED;
  constructor(public payload: any) {}
}

export class DeleteTag implements Action {
  public type = DELETE_TAG;
  constructor(public payload: any) {}
}

export class DeleteTagSucceeded implements Action {
  public type = DELETE_TAG_SUCCEEDED;
  constructor(public payload: any) {}
}

export class DeleteTagFailed implements Action {
  public type = DELETE_TAG_FAILED;
  constructor(public payload: any) {}
}

export class GetTagTypeDetail implements Action {
  public type = GET_TAG_TYPES_DETAIL;
  constructor(public payload: any) {}
}

export class GetTagTypeDetailSucceeded implements Action {
  public type = GET_TAG_TYPES_DETAIL_SUCCEEDED;
  constructor(public payload: any) {}
}

export class GetTagTypeDetailFailed implements Action {
  public type = GET_TAG_TYPES_DETAIL_FAILED;
  constructor(public payload: any) {}
}

export class UpdateTagType implements Action {
  public type = UPDATE_TAG_TYPE;
  constructor(public payload: any) {}
}

export class UpdateTagTypeSucceeded implements Action {
  public type = UPDATE_TAG_TYPE_SUCCEEDED;
  constructor(public payload: any) {}
}

export class UpdateTagTypeFailed implements Action {
  public type = UPDATE_TAG_TYPE_FAILED;
  constructor(public payload: any) {}
}

export class DeleteTagType implements Action {
  public type = DELETE_TAG_TYPE;
  constructor(public payload: any) {}
}

export class DeleteTagTypeSucceeded implements Action {
  public type = DELETE_TAG_TYPE_SUCCEEDED;
  constructor(public payload: any) {}
}

export class DeleteTagTypeFailed implements Action {
  public type = DELETE_TAG_TYPE_FAILED;
  constructor(public payload: any) {}
}

export class CreateTagType implements Action {
  public type = CREATE_TAG_TYPE;
  constructor(public payload: any) {}
}

export class CreateTagTypeSucceeded implements Action {
  public type = CREATE_TAG_TYPE_SUCCEEDED;
  constructor(public payload: any) {}
}

export class CreateTagTypeFailed implements Action {
  public type = CREATE_TAG_TYPE_FAILED;
  constructor(public payload: any) {}
}

export type TagActions = 
  GetAssignedTag 
| GetAssignedTagSucceeded
| GetAssignedTagFailed
| GetTags
| GetTagsSucceeded
| GetTagsFailed
| UpdateAssignedTag
| UpdateAssignedTagSucceeded
| UpdateAssignedTagFailed
| GetTagTypes
| GetTagTypesSucceeded
| GetTagTypesFailed
| GetTagDetail
| GetTagDetailSucceeded
| GetTagDetailFailed
| CreateTag
| CreateTagSucceeded
| CreateTagFailed
| DeleteTag
| DeleteTagSucceeded
| DeleteTagFailed
| GetTagTypeDetail
| GetTagTypeDetailSucceeded
| GetTagTypeDetailFailed
| UpdateTag
| UpdateTagSucceeded
| UpdateTagFailed;
