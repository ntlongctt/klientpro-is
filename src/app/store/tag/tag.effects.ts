import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action, Store } from '@ngrx/store';
import * as fromTagActions from '@store/tag/tag.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { AppState } from '@store/store.reducers';
import { TagService } from '../../rest/services/pa/Tag.service';

@Injectable()
export class TagEffects {
  @Effect()
  getAssignedTags$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.GET_ASSIGNED_TAG),
    mergeMap((action: any) => this.tagService.getAssignedTags({
      entityName: action.payload.entityName,
      itemId: action.payload.itemId
    }).pipe(
      map(data => ({ payload: action.payload, data }))
    ) 
    ),
    map(({payload, data }) => {
      return new fromTagActions.GetAssignedTagSucceeded({ entityName: payload.entityName, tags: data.tagTypes });
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.GetAssignedTagFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getTags$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.GET_TAGS),
    mergeMap((action: any) => this.tagService.getTags({
      entityName: action.payload.entityName
    }).pipe(
      map(data => ({ payload: action.payload, data }))
    )),
    map(({payload, data }) => {
      return new fromTagActions.GetTagsSucceeded({ entityName: payload.entityName, tags: data.tagTypes });
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.GetTagsFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  updateAssignedTags$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.UPDATE_ASSIGNED_TAG),
    mergeMap((action: any) => this.tagService.updateAssignedTags({
      entityName: action.payload.entityName,
      itemId: action.payload.itemId,
      intList: action.payload.intList
    }) 
    ),
    map((data) => {
      return new fromTagActions.UpdateAssignedTagSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.UpdateAssignedTagFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getTagTypes$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.GET_TAG_TYPES),
    mergeMap((action: any) => this.tagService.getTagTypes({
      entityName: action.payload.entityName,
    }) 
    ),
    map((data) => {
      return new fromTagActions.GetTagTypesSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.GetTagTypesFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getTagDetail$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.GET_TAG_DETAIL),
    mergeMap((action: any) => this.tagService.getTag({
      entityName: action.payload.entityName,
      tagId: action.payload.tagId
    }) 
    ),
    map((data) => {
      return new fromTagActions.GetTagDetailSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.GetTagDetailFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  updateTag$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.UPDATE_TAG),
    mergeMap((action: any) => this.tagService.updateTag({
      entityName: action.payload.entityName,
      tagId: action.payload.tagId,
      tag: action.payload.tag
    })
    ),
    map((data) => {
      return new fromTagActions.UpdateTagSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.UpdateTagFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  createTag$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.CREATE_TAG),
    mergeMap((action: any) => this.tagService.createTag({
      entityName: action.payload.entityName,
      tag: action.payload.tag
    })
    ),
    map((data) => {
      return new fromTagActions.CreateTagSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.CreateTagFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  deleteTag$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.DELETE_TAG),
    mergeMap((action: any) => this.tagService.deleteTag({
      entityName: action.payload.entityName,
      tagId: action.payload.tagId
    })
    ),
    map((data) => {
      return new fromTagActions.DeleteTagSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.DeleteTagFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  getTagtype$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.GET_TAG_TYPES_DETAIL),
    mergeMap((action: any) => this.tagService.getTagType({
      tagTypeId: action.payload.tagTypeId,
      entityName: action.payload.entityName
    })
    ),
    map((data) => {
      return new fromTagActions.GetTagTypeDetailSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.GetTagTypeDetailFailed(err.error));
      return caught;
    }),
  );
  
  @Effect()
  updateTagType$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.UPDATE_TAG_TYPE),
    mergeMap((action: any) => this.tagService.updateTagType({
      entityName: action.payload.entityName,
      tagType: action.payload.tagType,
      tagTypeId: action.payload.tagTypeId,
    })
    ),
    map((data) => {
      return new fromTagActions.UpdateTagTypeSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.UpdateTagTypeFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  createTagType$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.CREATE_TAG_TYPE),
    mergeMap((action: any) => this.tagService.createTagType({
      entityName: action.payload.entityName,
      tagType: action.payload.tagType,
    })
    ),
    map((data) => {
      return new fromTagActions.CreateTagTypeSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.CreateTagTypeFailed(err.error));
      return caught;
    }),
  );

  @Effect()
  deleteTagType$: Observable<Action> = this.actions$.pipe(
    ofType(fromTagActions.DELETE_TAG_TYPE),
    mergeMap((action: any) => this.tagService.deleteTagType({
      entityName: action.payload.entityName,
      tagTypeId: action.payload.tagTypeId
    })
    ),
    map((data) => {
      return new fromTagActions.DeleteTagTypeSucceeded(data);
    }),
    catchError((err, caught) => {
      this.store.dispatch(new fromTagActions.DeleteTagTypeFailed(err.error));
      return caught;
    }),
  );
  

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private tagService: TagService
  ) {
  }
}
