import { TagActions, GET_ASSIGNED_TAG_SUCCEEDED, GET_TAGS_SUCCEEDED } from './tag.actions';

export interface Tags {
  entityName: string;
  tags: any[];
}

export interface TagsState {
  errorMsg: string;
  tags: Tags;
  assignedTags: Tags;
}

const initialState: TagsState = {
  errorMsg: null,
  tags: { entityName: null, tags: [] },
  assignedTags: { entityName: null, tags: [] }
};

export function TagsReducer(state: TagsState = initialState, action: TagActions) {
  switch (action.type) {
    case GET_ASSIGNED_TAG_SUCCEEDED: 
      return {
        ...state,
        assignedTags: { entityName: action.payload.entityName, tags: [...action.payload.tags] }
      };
    case GET_TAGS_SUCCEEDED: 
      return {
        ...state,
        tags: { entityName: action.payload.entityName, tags: [...action.payload.tags] }
      };
    default:
      return state;
  }
}
