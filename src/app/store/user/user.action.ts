import { Action } from '@ngrx/store';
import { AppStoreService } from '@store/store.service';

export const CHANGE_PASSWORD = AppStoreService.createActionType('@USERS/CHANGE_PASSWORD');
export const CHANGE_PASSWORD_SUCCEEDED = AppStoreService.createActionType('@USERS/CHANGE_PASSWORD_SUCCESS');
export const CHANGE_PASSWORD_FAILED = AppStoreService.createActionType('@USERS/CHANGE_PASSWORD_FAILED');

export const GET_LIST_USER = AppStoreService.createActionType('@USERS/GET_LIST_USER');
export const GET_LIST_USER_SUCCEEDED = AppStoreService.createActionType('@USERS/GET_LIST_USER_SUCCEEDED');
export const GET_LIST_USER_FAILED = AppStoreService.createActionType('@USERS/GET_LIST_USER_FAILED');

export const UPDATE_USER = AppStoreService.createActionType('@USERS/UPDATE_USER');
export const UPDATE_USER_SUCCEEDED = AppStoreService.createActionType('@USERS/UPDATE_USER_SUCCEEDED');
export const UPDATE_USER_FAILED = AppStoreService.createActionType('@USERS/UPDATE_USER_FAILED');

export const CREATE_USER = AppStoreService.createActionType('@USERS/CREATE_USER');
export const CREATE_USER_SUCCEEDED = AppStoreService.createActionType('@USERS/CREATE_USER_SUCCEEDED');
export const CREATE_USER_FAILED = AppStoreService.createActionType('@USERS/CREATE_USER_FAILED');

export const RESET_PASSWORD_USER = AppStoreService.createActionType('@USERS/RESET_PASSWORD_USER');
export const RESET_PASSWORD_USER_SUCCEEDED = AppStoreService.createActionType('@USERS/RESET_PASSWORD_USER_SUCCEEDED');
export const RESET_PASSWORD_USER_FAILED = AppStoreService.createActionType('@USERS/RESET_PASSWORD_USER_FAILED');

export const DELETE_USER = AppStoreService.createActionType('@USERS/DELETE_USER');
export const DELETE_USER_SUCCEEDED = AppStoreService.createActionType('@USERS/DELETE_USER_SUCCEEDED');
export const DELETE_USER_FAILED = AppStoreService.createActionType('@USERS/DELETE_USER_FAILED');

export const GET_USER_DETAIL = AppStoreService.createActionType('@USERS/GET_USER_DETAIL');
export const GET_USER_DETAIL_SUCCEEDED = AppStoreService.createActionType('@USERS/GET_USER_DETAIL_SUCCEEDED');
export const GET_USER_DETAIL_FAILED = AppStoreService.createActionType('@USERS/GET_USER_DETAIL_FAILED');

export const GET_USER_INFO = AppStoreService.createActionType('@USERS/GET_USER_INFO');
export const GET_USER_INFO_SUCCEEDED = AppStoreService.createActionType('@USERS/GET_USER_INFO_SUCCEEDED');
export const GET_USER_INFO_FAILED = AppStoreService.createActionType('@USERS/GET_USER_INFO_FAILED');


export class ChangePassword implements Action {
  public readonly type = CHANGE_PASSWORD;
  constructor(public payload: any) {}
}

export class ChangePasswordASucceeded implements Action {
  public readonly type = CHANGE_PASSWORD_SUCCEEDED;
}

export class ChangePasswordFailed implements Action {
  public readonly type = CHANGE_PASSWORD_FAILED;
  constructor(public payload: any) {}
}

export class GetListUser implements Action {
  public readonly type = GET_LIST_USER;
  constructor(public keywork: string, public start: number, public limit: number) {}
}

export class GetListUserSucceeded implements Action {
  public readonly type = GET_LIST_USER_SUCCEEDED;
  constructor (public payload: any) {}
}
export class GetListUserFailed implements Action {
  public readonly type = GET_LIST_USER_FAILED;
  constructor (public payload: any) {}
}

export class UpdateUser implements Action {
  public readonly type = UPDATE_USER;
  constructor(public payload: any) {}
}

export class UpdateUserSucceeded implements Action {
  public readonly type = UPDATE_USER_SUCCEEDED;
  constructor(public payload: any) {}
}

export class UpdateUserFailed implements Action {
  public readonly type = UPDATE_USER_FAILED;
  constructor(public payload: any) {}
}
export class CreateUser implements Action {
  public readonly type = CREATE_USER;
  constructor(public payload: any) {}
}

export class CreateUserSucceeded implements Action {
  public readonly type = CREATE_USER_SUCCEEDED;
  constructor(public payload: any) {}
}

export class CreateUserFailed implements Action {
  public readonly type = CREATE_USER_FAILED;
  constructor(public payload: any) {}
}

export class ResetPassword implements Action {
  public readonly type = RESET_PASSWORD_USER;
  constructor(public payload: any) {}
}

export class ResetPasswordSucceeded implements Action {
  public readonly type = RESET_PASSWORD_USER_SUCCEEDED;
  constructor(public payload: any) {}
}

export class ResetPasswordFailed implements Action {
  public readonly type = RESET_PASSWORD_USER_FAILED;
  constructor(public payload: any) {}
}

export class DeleteUser implements Action {
  public readonly type = DELETE_USER;
  constructor(public payload: any) {}
}

export class DeleteUserSucceeded implements Action {
  public readonly type = DELETE_USER_SUCCEEDED;
  constructor(public payload: any) {}
}

export class DeleteUserFailed implements Action {
  public readonly type = DELETE_USER_FAILED;
  constructor(public payload: any) {}
}

export class GetUserDetail implements Action {
  public readonly type = GET_USER_DETAIL;
  constructor(public payload: any) {}
}

export class GetUserDetailSucceeded implements Action {
  public readonly type = GET_USER_DETAIL_SUCCEEDED;
  constructor(public payload: any) {}
}

export class GetUserDetailFailed implements Action {
  public readonly type = GET_USER_DETAIL_FAILED;
  constructor(public payload: any) {}
}


export class GetUserInfo implements Action {
  public readonly type = GET_USER_INFO;
}

export class GetUserInfoSucceeded implements Action {
  public readonly type = GET_USER_INFO_SUCCEEDED;
  constructor(public payload: any) {}
}

export class GetUserInfoFailed implements Action {
  public readonly type = GET_USER_INFO_FAILED;
  constructor(public payload: any) {}
}


export type UserActions = 
    ChangePassword 
  | ChangePasswordASucceeded
  | ChangePasswordFailed
  | GetListUser
  | GetListUserSucceeded
  | GetListUserFailed
  | UpdateUser
  | UpdateUserSucceeded
  | UpdateUserFailed
  | CreateUser
  | CreateUserSucceeded
  | CreateUserFailed
  | ResetPassword
  | ResetPasswordSucceeded
  | ResetPasswordFailed
  | DeleteUser
  | DeleteUserSucceeded
  | DeleteUserFailed
  | GetUserDetail
  | GetUserDetailSucceeded
  | GetUserDetailFailed
  | GetUserInfo
  | GetUserInfoSucceeded
  | GetUserInfoFailed;
