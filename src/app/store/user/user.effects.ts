import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import * as fromUserActions from '@store/user/user.action';
import { mergeMap, catchError, map } from 'rxjs/operators';
import { SystemService } from '../../rest/services/pa/System.service';
import { AppState } from '@store/store.reducers';
import { AuthService } from '../../rest/services/pa/Auth.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class UserEffects {
  @Effect()
  changePassword$: Observable<Action> = this.actions$.pipe(
    ofType(fromUserActions.CHANGE_PASSWORD),
    mergeMap((data: any) => this.systemService.changeUserPassword(
      {
        oldPassword: data.payload.oldPassword, 
        newPassword: data.payload.newPassword
      })
    ),
    map(() => ({ type: fromUserActions.CHANGE_PASSWORD_SUCCEEDED })),
    catchError((err, caught) => {
      this.store.dispatch(new fromUserActions.ChangePasswordFailed(err.error));
      return caught;
    }),
  );
  
  @Effect()
  getListUsers$: Observable<Action> = this.actions$.pipe(
    ofType(fromUserActions.GET_LIST_USER),
    mergeMap((data: any) => {
      return this.authService.getUsers({
        searchQuery: data.keywork,
        start: data.start,
        limit: data.limit
      });
    }),
    map((data) => (new fromUserActions.GetListUserSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromUserActions.GetListUserFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  updateUser$: Observable<Action> = this.actions$.pipe(
    ofType(fromUserActions.UPDATE_USER),
    mergeMap((data: any) => {
      return this.authService.updateUser({
        userId: data.payload.id,
        userDTO: data.payload
      });
    }),
    map((data) => (new fromUserActions.UpdateUserSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromUserActions.UpdateUserFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  createUser$: Observable<Action> = this.actions$.pipe(
    ofType(fromUserActions.CREATE_USER),
    mergeMap((data: any) => {
      return this.authService.addUser({
        userDTO: data.payload
      });
    }),
    map((data) => (new fromUserActions.CreateUserSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromUserActions.CreateUserFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  resetPassword$: Observable<Action> = this.actions$.pipe(
    ofType(fromUserActions.RESET_PASSWORD_USER),
    mergeMap((data: any) => {
      return this.authService.resetUserPassword({
        userId: data.payload
      });
    }),
    map((data) => (new fromUserActions.ResetPasswordSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromUserActions.ResetPasswordFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  deleteUser$: Observable<Action> = this.actions$.pipe(
    ofType(fromUserActions.DELETE_USER),
    mergeMap((data: any) => {
      return this.authService.deleteUser({
        userId: data.payload
      });
    }),
    map((data) => (new fromUserActions.DeleteUserSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromUserActions.DeleteUserFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getUserDetail$: Observable<Action> = this.actions$.pipe(
    ofType(fromUserActions.GET_USER_DETAIL),
    mergeMap((data: any) => {
      return this.authService.getUser({
        userId: data.payload
      });
    }),
    map((data) => (new fromUserActions.GetUserDetailSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromUserActions.DeleteUserFailed(e.error));
      return caught;
    }),
  );

  @Effect()
  getInfo$: Observable<Action> = this.actions$.pipe(
    ofType(fromUserActions.GET_USER_INFO),
    mergeMap(() => {
      return this.systemService.getUserData();
    }),
    map((data) => (new fromUserActions.GetUserInfoSucceeded(data))),
    catchError((e, caught) => {
      this.store.dispatch(new fromUserActions.GetUserInfoFailed(e.error));
      // this.router.navigate(['sign-in']);
      return caught;
    }),
  );

  constructor(
    private actions$: Actions,
    private systemService: SystemService,
    private store: Store<AppState>,
    private authService: AuthService,
    private router: Router
  ) {
  }
}
