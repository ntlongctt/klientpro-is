import { UserActions, GET_LIST_USER_SUCCEEDED, GET_LIST_USER_FAILED } from '@store/user/user.action';


export interface UsersState {
  errorMsg: string;
  users: any;
}

const initialState: UsersState = {
  errorMsg: null,
  users: null
};

export function UsersReducer(state: UsersState = initialState, action: UserActions) {
  switch (action.type) {
    case GET_LIST_USER_SUCCEEDED: 
      return {
        ...state,
        users: action.payload.users
      };
    case GET_LIST_USER_FAILED:
      return {
        ...state,
        errorMsg: action.payload
      };
    default:
      return state;
  }
}
