import { Component, Input, OnInit, ViewEncapsulation, OnDestroy, AfterViewInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { locale as en } from '../../../../i18n/common/en';
import { locale as cs } from '../../../../i18n/common/cs';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Store } from '@ngrx/store';
import { AppState } from '@store/store.reducers';
import { GetMenu } from '@store/menu/menu.actions';
import { ofType, Actions } from '@ngrx/effects';
import * as fromMenuActions from '@store/menu/menu.actions';
import { AppStoreService } from '@store/store.service';
import { Subject } from 'rxjs/Subject';
import * as fromLoginActions from '@store/login/login.actions';
import * as fromUserActions from '@store/user/user.action';

@Component({
    selector     : 'fuse-navigation',
    templateUrl  : './navigation.component.html',
    styleUrls    : ['./navigation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseNavigationComponent implements OnInit, OnDestroy, AfterViewInit
{
    @Input()
    layout = 'vertical';

    @Input()
    navigation = [];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _store: Store<AppState>,
        private actions$: Actions,
        private appStoreService: AppStoreService,
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this._fuseTranslationLoaderService.loadTranslations(en, cs);

        this.actions$
        .pipe(
            ofType(fromMenuActions.GET_MENU_SUCCEEDED),
            takeUntil(this._unsubscribeAll))
        .subscribe((menu: any) => {
            this.navigation = [];
            this.getMenuItem(menu.payload, this.navigation);
        });

        // list change language event to re-render menu
        this.actions$
        .pipe(
            ofType(fromMenuActions.CHANGE_LANGUAGE),
            takeUntil(this._unsubscribeAll))
        .subscribe(() => {
            const menu = this.appStoreService.getState().menu.menu;
            this.navigation = [];
            this.getMenuItem(menu, this.navigation);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        
    }

    ngAfterViewInit() {
        setTimeout(() => {
            const menus = this.appStoreService.getState().menu.menu;
            if (!this.navigation.length && menus) {
                this.navigation = [];
                this.getMenuItem(menus, this.navigation);
            }
        });
    }

    /**
     * On Destroy
     */
    ngOnDestroy() {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

     /**
     * Build Menu base on return of server
     * @param menu tree menu from return by server
     * @param list list childrends
     */
    private getMenuItem(menu, list) {
        menu.forEach(m => {
            // exclude logout menu
            if (m.locale !== 'MENU_LOGOUT') {
                const item = {
                    id       : m.locale,
                    title    : m.locale,
                    translate: this._fuseTranslationLoaderService.instant(m.locale),
                    type     : m.childs ? 'collapse' : 'item',
                    // icon     : 'domain',
                    url      : m['url'] === '/' ? '/dashboard' : m['url'],
                    children : []
                };
                if (m.childs) {
                    this.getMenuItem(m.childs, item.children);
                }
                list.push(item);
            }
        });
    }

}
