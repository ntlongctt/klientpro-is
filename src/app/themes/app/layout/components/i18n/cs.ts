export const locale = {
  lang: 'cs',
  data: {
    TOOL_BAR: {
      USER_MENU: {
        MY_PROFILE: 'My Profile',
        CHANGE_PASSWORD: 'Change password',
        LOG_OUT: 'Log out'
      }
    }
  }
};
