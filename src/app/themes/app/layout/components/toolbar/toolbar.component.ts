import { Component, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router, NavigationCancel, NavigationError } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { navigation } from 'app/navigation/navigation';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../../../../../i18n/common/en';
import { locale as czech } from '../../../../../i18n/common/cs';
import { Store } from '@ngrx/store';
import * as AuthActions from '@store/auth/auth.actions';
import { AppStoreService } from '@store/store.service';
import { Subject } from 'rxjs/Subject';
import { AppConstant } from '../../../../../app.constant';
import { AppState } from '@store/store.reducers';
import * as fromMenuActions from '@store/menu/menu.actions';
import * as fromUserActions from '@store/user/user.action';
import * as fromAuthActions from '@store/auth/auth.actions';
import * as fromErrorActions from '@store/error/error.actions';
import { Actions, ofType } from '@ngrx/effects';
import { map } from 'rxjs-compat/operator/map';

@Component({
    selector   : 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls  : ['./toolbar.component.scss']
})

export class ToolbarComponent implements OnInit, OnDestroy, AfterViewInit
{
    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    languages: any;
    navigation: any;
    selectedLanguage: any;
    showLoadingBar: boolean;
    userStatusOptions: any[];
    fullName: string;
    enableChangeLanguage = AppConstant.enableChangeLanguage;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {Router} _router
     * @param {TranslateService} _translateService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _router: Router,
        private _translateService: TranslateService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _store: Store<AppState>,
        private _appStoreService: AppStoreService,
        private action$: Actions
    )
    {
        // Set the defaults
        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon' : 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon' : 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon' : 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                id   : 'en',
                title: 'English',
                flag : 'us'
            },
            {
                id   : 'cs',
                title: 'Czech',
                flag : 'cz'
            }
        ];

        this.navigation = navigation;
        
        this._fuseTranslationLoaderService.loadTranslations(english, czech);
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Subscribe to the router events to show/hide the loading bar
        this._router.events
            .pipe(
                filter((event) => event instanceof NavigationStart),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe((event) => {
                this.showLoadingBar = true;
            });

        this._router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError)
            )
            .subscribe((event) => {
                this.showLoadingBar = false;
            });

        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((settings) => {
                this.horizontalNavbar = settings.layout.navbar.position === 'top';
                this.rightNavbar = settings.layout.navbar.position === 'right';
                this.hiddenNavbar = settings.layout.navbar.hidden === true;
            });

        this.action$.pipe(
            takeUntil(this._unsubscribeAll),
            ofType(fromErrorActions.CONNECTION_TIMEOUT_ERROR)
        ).subscribe(() => this.showLoadingBar = false);

        // Get user infomation
        this.action$.pipe(
            takeUntil(this._unsubscribeAll),
            ofType(fromUserActions.GET_USER_INFO_SUCCEEDED),
        )
        .subscribe(((action: any) => {
            if (action.payload.fullName) {
                this.fullName = action.payload.fullName;
            }
            this._store.dispatch(new fromAuthActions.GetProfileSuccess(action.payload));
        }));

        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, {'id': this._translateService.currentLang});

        const currentUser = this._appStoreService.getState().auth.currentUser;
        if (currentUser) {
            this.fullName = currentUser.fullName;
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    ngAfterViewInit() {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

     /**
     * Log out
     */
    logout() {
        this._store.dispatch(new AuthActions.LogOut()); 
    }
    
    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void
    {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    /**
     * Search
     *
     * @param value
     */
    search(value): void
    {
        // Do your search here...
        console.log(value);
    }

    /**
     * Set the language
     *
     * @param langId
     */
    setLanguage(langId): void
    {
        // Set the selected language for toolbar
        this.selectedLanguage = _.find(this.languages, {'id': langId});

        // Use the selected language for translations
        this._translateService.use(langId);

        // dispatch action change language
        this._store.dispatch(new fromMenuActions.ChangeLanguage(this.selectedLanguage));
    }
}
