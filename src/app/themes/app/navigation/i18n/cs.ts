export const locale = {
  lang: 'cs',
  data: {
    'NAV': {
      'APPLICATIONS': 'Applications',
      'SAMPLE': {
        'TITLE': 'Sample',
        'BADGE': '25'
      }
    }
  }
};
