export const environment = {
  production: true,
  hmr: false,
  env: 'Demo',
  domain: '/IS-BE',
  baseHref: '/IS-FE/',
  defaultLanguage: 'cs', // en, cs
  enableChangeLanguage: false,
  defaultStateCode: 'CZ',
  defaultStateName: 'Česko',
  defaultTimeZoneDisplay: 'Europe/Prague',
  SENTRY_DSN: 'https://b619ad5ea5724fb4a0e2906c7806fe8f@sentry.io/1306637',
  SENTRY_ENABLED: false,
  REQUIRE_TOKEN: false,
};
