export const environment = {
  production: false,
  hmr: true,
  env: 'Development',
  domain: 'https://pabe.klientpro.cz:8843/IS-BE',
  baseHref: '/',
  defaultLanguage: 'en', // en, cs
  enableChangeLanguage: true,
  defaultStateCode: 'CZ',
  defaultStateName: 'Česko',
  defaultTimeZoneDisplay: 'Europe/Prague',
  SENTRY_DSN: 'https://b619ad5ea5724fb4a0e2906c7806fe8f@sentry.io/1306637',
  SENTRY_ENABLED: false,
  REQUIRE_TOKEN: false,
};
