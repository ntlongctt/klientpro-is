// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,                                // A variable define whenever the build is for production. Production code will have a better performance

  hmr: false,                                       // HMR (Hot Module Replacement) enable. This is should enable in development mode only

  env: 'Development',                               // A variable to define what is the environment in runtime.

  domain: 'http://pabe.klientpro.cz:9012/IS-BE',    // Base domain for API request. Remove domain name (leave empty) to use the current page domain as api domain
                                                    // http://localhost:333 is a reverse proxy server by NodeJS, we created this for bypass the ssl self-signed issue in development

  baseHref: '',                                     // Base Href configuration. If the application is placed in a sub directory then we put the subdirectory name in here.

  defaultLanguage: 'en',                            // Default language, only accepts: 'en' or 'cs'
  enableChangeLanguage: true,                       // On/Off button change language on toolbar
  defaultStateCode: 'CZ',                           // Default state code
  defaultStateName: 'Česko',                        // Default state name
  defaultTimeZoneDisplay: 'Europe/Prague',           // Default time zone display in app (using moment-timezone)

  SENTRY_DSN: 'https://b619ad5ea5724fb4a0e2906c7806fe8f@sentry.io/1306637',  // Sentry DNS Variable. Go the https://sentry.io, create acount and create new project to get DSN key
  SENTRY_ENABLED: false,                             // Turn Sentry On/Off
  REQUIRE_TOKEN: false,                              // Include token in request 
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
